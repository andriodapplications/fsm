package com.app.fsm;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.app.fsm.utils.CommonUtils;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

public class CustomInfoWindowGoogleMap implements GoogleMap.InfoWindowAdapter {

    private Context context;

    public CustomInfoWindowGoogleMap(Context ctx){
        context = ctx;
    }

    @Override
    public View getInfoWindow(Marker marker) {
        return null;
    }

    @Override
    public View getInfoContents(Marker marker) {
        View view = ((Activity)context).getLayoutInflater()
                .inflate(R.layout.popup_info, null);

        TextView tvticketname = view.findViewById(R.id.tv_name);
        TextView tvcustomer = view.findViewById(R.id.txt_customer);
        TextView tvcategory = view.findViewById(R.id.txt_category);
        TextView tvsubcategory = view.findViewById(R.id.txt_subcategory);
        TextView tvstartdate = view.findViewById(R.id.edt_customer_number);
        TextView tvenddate = view.findViewById(R.id.edt_customer_name);
        TextView tvstatus = view.findViewById(R.id.tv_status);
        TextView tvaddress = view.findViewById(R.id.tv_address);
        TextView tvcontact = view.findViewById(R.id.tv_contact);
        TextView tveta = view.findViewById(R.id.tv_eta);

        MarkerInfo infoWindowData = (MarkerInfo) marker.getTag();

        String startdate_time;
        String enddate_time;
        String eta_time;
        if(infoWindowData.getStartdate()!=null){
            String starttime=infoWindowData.getStartdate();
            String st_time=starttime.substring(starttime.length()-5);
            int firsttwo= Integer.parseInt(st_time.substring(0,2));
            if(firsttwo>= 13 && firsttwo < 24){
                st_time=st_time+" PM";
            }else{
                st_time=st_time+" AM";
            }

            startdate_time= CommonUtils.getDateEta(infoWindowData.getStartdate())+" , "+st_time;
        }else {
            startdate_time="";
        }

        if(infoWindowData.getEnddate()!=null){
            String endtime=infoWindowData.getEnddate();
            String ed_time=endtime.substring(endtime.length()-5);
            int firsttwo= Integer.parseInt(ed_time.substring(0,2));
            if(firsttwo>= 13 && firsttwo < 24){
                ed_time=ed_time+" PM";
            }else{
                ed_time=ed_time+" AM";
            }

            enddate_time=CommonUtils.getDateEta(infoWindowData.getEnddate())+" , "+ed_time;
        }else {
            enddate_time="";
        }

        if(infoWindowData.getEtatime()!=null){
            String eta=infoWindowData.getEtatime();
            String time=eta.substring(eta.length()-5);
            int firsttwo= Integer.parseInt(time.substring(0,2));
            if(firsttwo>= 13 && firsttwo < 24){
                time=time+" PM";
            }else{
                time=time+" AM";
            }
            eta_time=CommonUtils.getDateEta(infoWindowData.getEtatime())+" , "+time;
        }else {
            eta_time="";
        }

        tvticketname.setText("Ticket : "+infoWindowData.getTitle());
        tvcustomer.setText("Customer : "+infoWindowData.getCustomer());
        tvcategory.setText("Category : "+infoWindowData.getCategory());
        tvsubcategory.setText("SubCategory : "+infoWindowData.getSubcategory());
        tvstartdate.setText("StartDate : "+startdate_time);
        tvenddate.setText("EndDate : "+enddate_time);
        tvstatus.setText("Status : "+infoWindowData.getStatus());
        tvaddress.setText("Address : "+infoWindowData.getAddress());
        tvcontact.setText("Mobile No : "+infoWindowData.getPhoneno());
        tveta.setText("ETA :"+eta_time);
        return view;
    }
}

