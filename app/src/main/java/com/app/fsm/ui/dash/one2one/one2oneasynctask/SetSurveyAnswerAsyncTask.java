package com.app.fsm.ui.dash.one2one.one2oneasynctask;

import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.app.fsm.local_network.network.model.survey.SurveyAnswerModel;
import com.app.fsm.local_network.repository.OaasDatabase;
import com.app.fsm.local_network.repository.dao.SurveyAnswerDao;
import com.app.fsm.ui.dash.one2one.SurveyAnswerCompleted;

public final class SetSurveyAnswerAsyncTask extends AsyncTask<Void, Void, Long> {

    private OaasDatabase oaasDatabase;
    private SurveyAnswerModel surveyAnswerModel;
    private SurveyAnswerCompleted onCompleteListener;
    private Integer backendid=0;

    public SetSurveyAnswerAsyncTask(@NonNull SurveyAnswerCompleted onCompleteListener, @NonNull OaasDatabase oaasDatabase
            , SurveyAnswerModel surveyAnswerModel) {
        this.oaasDatabase = oaasDatabase;
        this.surveyAnswerModel = surveyAnswerModel;
        this.onCompleteListener=onCompleteListener;
    }

    @Override
    protected Long doInBackground(Void... params) {
        SurveyAnswerDao surveyAnswerDao = oaasDatabase.provideSurveyAnswerDao();

        if (surveyAnswerDao.getCount() == 0) {
            surveyAnswerModel.setBackendid(1);
        } else {
            SurveyAnswerModel AnswerModel = surveyAnswerDao.getsurvey();
            backendid=AnswerModel.getBackendid()+1;
            surveyAnswerModel.setBackendid(backendid);
        }

        return surveyAnswerDao.insertSurveyModel(surveyAnswerModel);
    }

    @Override
    protected void onPostExecute(Long id) {
        super.onPostExecute(id);
        if(onCompleteListener!=null){
            onCompleteListener.OnSurveyPosted(id,backendid);
        }
    }
}

