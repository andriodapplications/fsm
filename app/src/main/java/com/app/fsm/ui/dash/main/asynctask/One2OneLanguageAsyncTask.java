package com.app.fsm.ui.dash.main.asynctask;

import android.content.Context;
import android.os.AsyncTask;

import com.app.fsm.local_network.network.model.survey.One2OneLanguage;
import com.app.fsm.local_network.repository.OaasDatabase;
import com.app.fsm.local_network.repository.OassDatabaseBuilder;
import com.app.fsm.local_network.repository.dao.SurveyQuestionDao;

import java.lang.ref.WeakReference;
import java.util.List;

public final class One2OneLanguageAsyncTask extends AsyncTask<Void, Void, Boolean> {

    //Prevent leak
    private WeakReference<Context> weakActivity;
    private List<One2OneLanguage> one2OneLanguageList;

    public One2OneLanguageAsyncTask(Context context, List<One2OneLanguage> one2OneLanguageList) {
        weakActivity = new WeakReference<>(context);
        this.one2OneLanguageList =one2OneLanguageList;
    }

    @Override
    protected Boolean doInBackground(Void... params) {

        OaasDatabase oaasDatabase = OassDatabaseBuilder.provideOassDatabase(weakActivity.get());
        SurveyQuestionDao surveyQuestionDao= oaasDatabase.provideSurveyQQuestiondao();
        surveyQuestionDao.insertOne2OneQuestionLanguage(one2OneLanguageList);
        return true;
    }

    @Override
    protected void onPostExecute(Boolean isInserted) {

    }
}

