package com.app.fsm.ui.document;


import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.fsm.BaseFragment;
import com.app.fsm.R;
import com.app.fsm.local_network.network.model.appconfig.AppConfigModel;
import com.app.fsm.local_network.network.model.member.MemberModel;
import com.app.fsm.local_network.network.model.serviceevents.ServiceEventModel;
import com.app.fsm.local_network.network.model.survey.SurveyAnswerModel;
import com.app.fsm.local_network.network.model.survey.SurveyQuestionModel;
import com.app.fsm.local_network.network.model.todo.TodoModel;
import com.app.fsm.local_network.network.model.updatetypes.UpdateTypeModel;
import com.app.fsm.ui.OnCompleteListener;
import com.app.fsm.ui.dash.main.DashboardActivity;
import com.app.fsm.ui.dash.main.OnAppConfigListner;
import com.app.fsm.ui.dash.main.ServiceEventListner;
import com.app.fsm.ui.dash.main.UpdateTypeListner;
import com.app.fsm.ui.dash.main.asynctask.AppConfigAsyncTask;
import com.app.fsm.ui.dash.main.asynctask.ServiceEventAsyncTask;
import com.app.fsm.ui.dash.main.asynctask.UpdateTypeAsyncTask;
import com.app.fsm.ui.dash.memberregistration.memberprofile.memberasynctask.UpdateMemberAsyncTask;
import com.app.fsm.ui.dash.one2one.OnFetchAnswersComplete;
import com.app.fsm.ui.dash.one2one.OnFetchComplete;
import com.app.fsm.ui.dash.one2one.One2OneViewPagerAdapter;
import com.app.fsm.ui.dash.one2one.SurveyAnswerCompleted;
import com.app.fsm.ui.dash.one2one.one2oneasynctask.GetAllSurveyAnswersAsynTask;
import com.app.fsm.ui.dash.one2one.one2oneasynctask.GetAllSurveyQuestionAsynTask;
import com.app.fsm.ui.dash.one2one.one2oneasynctask.SetSurveyAnswerAsyncTask;
import com.app.fsm.ui.dash.one2one.one2oneasynctask.SetTodoAsyncTask;
import com.app.fsm.ui.dash.one2one.one2oneasynctask.UpdateSurveyAnswersAsyncTask;
import com.app.fsm.ui.dash.one2one.one2onequestion.One2OneQuestionFragment;
import com.app.fsm.ui.dash.one2one.one2onequestion.QuestionSliderCallbackFragment;
import com.app.fsm.utils.AppConstant;
import com.app.fsm.utils.CommonUtils;
import com.app.fsm.utils.UserSessionManager;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class One2OneFragment extends BaseFragment implements
        ViewPager.OnPageChangeListener, OnCompleteListener
        , OnFetchComplete, OnFetchAnswersComplete,
        OnAppConfigListner, ServiceEventListner, UpdateTypeListner, SurveyAnswerCompleted {

    @BindView(R.id.layout_user_details)
    ViewGroup llUserDetails;
    @BindView(R.id.search_member)
    ViewGroup viewGroupSearchMember;/*
    @BindView(R.id.iv_arrow_down)
    ImageView ivArrowDown;*/
    @BindView(R.id.ll_expand_collapse)
    LinearLayout llExpandCollapse;
    @BindView(R.id.view_pager)
    ViewPager vpQuestion;
//    @BindView(R.id.question_Layout)
//    LinearLayout llQuestion;
    @BindView(R.id.txt_title_child_fragment)
    TextView txtTitleChildFragment;
    @BindView(R.id.img_to_previous_fragment)
    ImageView imgPreviousFragment;
    @BindView(R.id.ll_indicator_container)
    LinearLayout llIndicatorContainer;
    @BindView(R.id.img_to_next_fragment)
    ImageView imgNextFragment;
    @BindView(R.id.txt_fragment_count)
    TextView txtFragmentCount;
    @BindView(R.id.et_search_task)
    TextView etSearchTask;
    @BindView(R.id.txt_bandwidth)
    TextView etBandwidth;
    @BindView(R.id.txt_radius_username)
    TextView etRadiusUsername;
    @BindView(R.id.txt_radius_password)
    TextView etRadiusPassword;
//    @BindView(R.id.iv_search_task)
//    ImageView ivSearchTask;
    @BindView(R.id.txt_session)
    AppCompatTextView txtSession;
    @BindView(R.id.ll_search_user)
    LinearLayout llSearchUser;
    @BindView(R.id.cv_member_search)
    CardView cvMemberSearch;
    @BindView(R.id.ll_arrow)
    LinearLayout llArrow;
    @BindView(R.id.bt_rollback)
    Button btrollback;
    @BindView(R.id.im_openpdf)
    ImageView imopenpdf;
    @BindView(R.id.im_downloadpdf)
    ImageView imdownloadpdf;

    @BindView(R.id.txt_header_full_name)
    TextView txtHeaderFullName;
    @BindView(R.id.txt_full_name)
    TextView txtFullName;
    @BindView(R.id.txt_header_typology)
    TextView txtHeaderTypology;
    @BindView(R.id.txt_typology)
    TextView txtTypology;
    @BindView(R.id.txt_header_dob)
    TextView txtHeaderDob;
    @BindView(R.id.txt_dob)
    TextView txtDob;
    @BindView(R.id.txt_header_nick)
    TextView txtHeaderNick;
    @BindView(R.id.txt_nick)
    TextView txtNick;
    @BindView(R.id.txt_header_tl_id)
    TextView txtHeaderTlId;
    @BindView(R.id.txt_tl_id)
    TextView txtTlId;
    @BindView(R.id.txt_header_age)
    TextView txtHeaderAge;
    @BindView(R.id.txt_age)
    TextView txtAge;
    @BindView(R.id.rl_session_date)
    RelativeLayout rlSessionDate;

    private Unbinder mUnbinder;
    private One2OneViewPagerAdapter one2OneViewPagerAdapter;
    private MemberModel memberModel;
    private SurveyAnswerModel surveyAnswerModel;
    private TodoModel todoModel;
    private int childFragmentPos;
    public List<SurveyQuestionModel> AllsurveyQuestionModel;
    public List<SurveyQuestionModel> surveyQuestionModelList;
    public List<SurveyQuestionModel> dummysurveyQuestionModelList;
    public List<SurveyAnswerModel> surveyAnswerModelList;
    public List<SurveyAnswerModel> dummysurveyAnswerModelList;
    private List<ServiceEventModel> serviceEventModelList;
    private List<UpdateTypeModel> updateTypeModelList;
    private boolean Inprogress = false;
    private int totalquestionsSize,totalsurveyanswered;
    private boolean rollback =false,buttondisable=false,allQuestionsAnswered=true;
    private int questioncount=0,nextquestionid=0,previousquestionid=0,
            rccode=0,curr_position=0,rollback_count=0,lastid,serviceeventid=0,updatetypeid=0;
    private String lastanswer,lastremark,lastupload,serviceidentifiername=null,aspname=null;

    public SurveyQuestionModel surveyQuestionModelobj;
    private SurveyAnswerModel pdfanswermodel;

    private int surveylocalid=0;
    private int surveyrollbackId=0;
    private int groupserialno=1;
    private SurveyAnswerModel answerModel;

    public static One2OneFragment newInstance(MemberModel memberModel) {
        One2OneFragment one2OneFragment = new One2OneFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(AppConstant.MEMBER_MODEL, memberModel);
        one2OneFragment.setArguments(bundle);
        return one2OneFragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_one2_one, container, false);
        mUnbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (getArguments() != null)
            memberModel = (MemberModel) getArguments().getSerializable(AppConstant.MEMBER_MODEL);
        groupserialno = memberModel.getNextquestionId()==0?1:memberModel.getNextquestionId();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //ivArrowDown.setTag(false);
        setTitle("Ticket");
        init();
    }

    public void init() {

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        AllsurveyQuestionModel = new ArrayList<>();
        surveyQuestionModelList = new ArrayList<>();
        surveyAnswerModelList = new ArrayList<>();
        dummysurveyQuestionModelList = new ArrayList<>();
        dummysurveyAnswerModelList = new ArrayList<>();
        updateTypeModelList=new ArrayList<>();
        serviceEventModelList=new ArrayList<>();
        UserSessionManager.getUserSessionManager().setQRCode(null);

        new AppConfigAsyncTask(this,provideOaasDatabase()).execute();

        new ServiceEventAsyncTask(this,provideOaasDatabase()).execute();

        new UpdateTypeAsyncTask(this,provideOaasDatabase()).execute();

        new GetAllSurveyQuestionAsynTask(this, provideOaasDatabase(),
                memberModel.getCustomerId(),memberModel.getCategoryId()).execute();

        setSliderView(memberModel);

        new GetAllSurveyAnswersAsynTask(this, provideOaasDatabase(),
                memberModel.getServerId()).execute();

        btrollback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(buttondisable && !memberModel.getRollbackStatus().equalsIgnoreCase("R")){
                    updateMemberrollback(memberModel);
                }
                //updateMemberrollback(memberModel);
            }
        });

        imopenpdf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(pdfanswermodel!=null){
                    startActivity(new Intent(getActivity(), PDFViewer.class)
                            .putExtra("surveyanswers",pdfanswermodel));
                }
            }
        });
        imdownloadpdf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(pdfanswermodel!=null){
                    String url="https://idcamp-api.herokuapp.com/api/v1/containers/converbiz/download/"+pdfanswermodel.getUploadedfiles();
                    new DownloadTask(getActivity(), url);
                }
            }
        });

        if(memberModel.getRollbackStatus().equalsIgnoreCase("A")){
            rollback=true;
            memberModel.setRollbackupdate("A");
            imgNextFragment.setEnabled(true);
            imgPreviousFragment.setEnabled(true);
        } else if(memberModel.getRollbackStatus().equalsIgnoreCase("R")){
            memberModel.setRollbackupdate("N");
            imgNextFragment.setEnabled(true);
            imgPreviousFragment.setEnabled(false);
            btrollback.setBackground(getResources().getDrawable(R.drawable.background_rollback_red));
        } else if(memberModel.getRollbackStatus().equalsIgnoreCase("N")){
            memberModel.setRollbackupdate("N");
        }
//        if(memberModel.getRcCode()==93){
            imgNextFragment.setEnabled(true);
            imgPreviousFragment.setEnabled(true);
//        }
    }

    private void setSliderView(MemberModel memberModel) {
        txtSession.setText(CommonUtils.updateDate(calendar.getTime()));
        etSearchTask.setText(memberModel.getDispatchno());
        etBandwidth.setText(memberModel.getBandwidth()==null?"":memberModel.getBandwidth());
        etRadiusUsername.setText(memberModel.getRadiususername()==null?"":memberModel.getRadiususername());
        etRadiusPassword.setText(memberModel.getRadiuspassword()==null?"":memberModel.getRadiuspassword());
        /*txtFullName.setText(memberModel.getFullname());
        txtTypology.setText("--");
        txtDob.setText(memberModel.getDob()==null?"--":CommonUtils.getDateFromISO(memberModel.getDob()));
        txtNick.setText(memberModel.getAddress());
        txtTlId.setText("--");
        txtAge.setText(memberModel.getAge());*/
    }



    /*@OnClick(R.id.iv_arrow_down)
    void onExpandCollapseClicked() {
        Boolean isVisible = (Boolean) ivArrowDown.getTag();
        *//*if(isVisible){
            llUserDetails.setVisibility(View.GONE);
            ivArrowDown.setRotation(0f);
            //overlayView.setVisibility(View.GONE);
            handleExpandCollapseView(false);
        } else {
            llUserDetails.setVisibility(View.VISIBLE);
            ivArrowDown.setRotation(180f);
            //overlayView.setVisibility(View.VISIBLE);
            handleExpandCollapseView(true);
        }*//*
        //animateLayout(!isVisible);
        ivArrowDown.setTag(!isVisible);
    }*/

    private void handleExpandCollapseView(boolean expand) {
        if (expand) {
            llExpandCollapse.setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.white));
        } else {
            llExpandCollapse.setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.background_light_grey));
        }
    }

    /*public void animateLayout(Boolean toAnimate) {
        if (toAnimate) {
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            llArrow.setLayoutParams(layoutParams);
        } else {
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            llArrow.setLayoutParams(layoutParams);
        }

    }*/

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mUnbinder.unbind();
    }

    @Override
    public void onPageScrolled(int i, float v, int i1) {

    }

    @Override
    public void onPageSelected(int i) {
//        Log.e("onPageSelected",String.valueOf(i));
//        Log.e("count",String.valueOf(one2OneViewPagerAdapter.getCount()));
        UserSessionManager.getUserSessionManager().setQRCode(null);
        int pos = 0;
        if(i>0 && ((one2OneViewPagerAdapter.getCount()-1)>i)){
            pos = i-1;
        }else{
            pos=i;
        }
        QuestionSliderCallbackFragment questionSliderCallbackFragment = (QuestionSliderCallbackFragment) one2OneViewPagerAdapter.getFragment(pos);
        SurveyQuestionModel surveyQuestionModel = questionSliderCallbackFragment.getSurveyQuestionModel();
        questionSliderCallbackFragment.isScrollSurveyQuestionAnswered(surveyQuestionModel);
//        JsonObject benjson = new JsonObject();
//        JsonObject rcjson = new JsonObject();
//        try {
//            JsonParser parser = new JsonParser();
//            benjson = parser.parse(new Gson().toJson(
//                    surveyQuestionModel, SurveyQuestionModel.class)).getAsJsonObject();
//
//            JsonParser rcparser = new JsonParser();
//            rcjson = rcparser.parse(new Gson().toJson(
//                    memberModel, MemberModel.class)).getAsJsonObject();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        Log.e("surveyQuestionModel", String.valueOf(benjson));
//        Log.e("membergetDispatchno()", String.valueOf(rcjson));
        this.childFragmentPos = i;
        if (i == 0) {
            imgPreviousFragment.setVisibility(View.INVISIBLE);
        } else {
            imgPreviousFragment.setVisibility(View.VISIBLE);
        }

        if(getUserSessionManager().getOnetoOneFlag().equalsIgnoreCase("t")){
            imgPreviousFragment.setVisibility(View.INVISIBLE);
        }

        if (i == vpQuestion.getAdapter().getCount() - 1) {
            imgNextFragment.setImageResource(R.drawable.ic_done);
            imgNextFragment.setTag(AppConstant.DONE);
            imgNextFragment.setVisibility(View.VISIBLE);
        } else {
            imgNextFragment.setImageResource(R.drawable.ic_next);
            imgNextFragment.setTag(AppConstant.NEXT);
            imgNextFragment.setVisibility(View.GONE);
        }
    }

    @Override
    public void onPageScrollStateChanged(int i) {

    }

    private Boolean canGoBack = false;

    @OnClick(R.id.img_to_previous_fragment)
    public void imgLeftClicked() {
        if(groupserialno>1){
        groupserialno = groupserialno - 1;


            for(int i = one2OneViewPagerAdapter.getCount() - 1; i >= 0; i--){
                if (imgPreviousFragment.getVisibility() == View.VISIBLE) {
                    QuestionSliderCallbackFragment questionSliderCallbackFragment = (QuestionSliderCallbackFragment) one2OneViewPagerAdapter.getFragment(i);
                    questionSliderCallbackFragment.getPreviousQuestionId();
                    if (questionSliderCallbackFragment.getPreviousQuestionId() != -1) {
                        int previousFragmentPos = skipQuestion(questionSliderCallbackFragment.getPreviousQuestionId(), surveyQuestionModelList);
                        if (previousFragmentPos != -1 && canGoBack) {
                            vpQuestion.setCurrentItem(previousFragmentPos);
                            canGoBack = false;
                            return;
                        }
                    }
                    //for go back to the previous screens
                    if (canGoBack) {
                        vpQuestion.setCurrentItem(--childFragmentPos);
                        canGoBack = true;
                        return;
                    } else {
                        Toast.makeText(requireActivity(), "You can just edit one question.", Toast.LENGTH_SHORT).show();
                    }
                }
            }

        if (!surveyQuestionModelList.isEmpty()) {
            List<Fragment> fragmentList = new ArrayList<>();
            for (SurveyQuestionModel surveyQuestionModel : surveyQuestionModelList) {
                if (surveyQuestionModel.getQuestiontype() != null && surveyQuestionModel.getGroupserialno() == groupserialno) {
                    fragmentList.add(One2OneQuestionFragment.newInstance(surveyQuestionModel,
                            surveyQuestionModelList, AllsurveyQuestionModel, serviceEventModelList, updateTypeModelList));
                }
                surveyQuestionModelobj = surveyQuestionModel;
            }
            if (fragmentList.size() > 0) {
                if(fragmentList.size() > 1){
                    imgNextFragment.setVisibility(View.GONE);
                }
                one2OneViewPagerAdapter = new One2OneViewPagerAdapter(requireActivity().getSupportFragmentManager(), fragmentList);
                vpQuestion.setAdapter(one2OneViewPagerAdapter);
                vpQuestion.addOnPageChangeListener(this);
                imgNextFragment.setTag(AppConstant.NEXT);
            } else {
                openTaskListFragment(memberModel);
            }


        }
    }

    }

    @OnClick(R.id.img_to_next_fragment)
    public void imgRightClicked() {
        allQuestionsAnswered = true;

        for (int i = 0; i < one2OneViewPagerAdapter.getCount(); i++) {
            QuestionSliderCallbackFragment questionSliderCallbackFragment = (QuestionSliderCallbackFragment) one2OneViewPagerAdapter.getFragment(i);
            SurveyQuestionModel surveyQuestionModel = questionSliderCallbackFragment.getSurveyQuestionModel();

            try {
//                if (((String) imgNextFragment.getTag()).equalsIgnoreCase(AppConstant.DONE)) {
                if (questionSliderCallbackFragment.isSurveyQuestionAnswered()||!surveyQuestionModel.getMandatory()) {

                } else {
                    allQuestionsAnswered = false;
                    //Toast.makeText(requireActivity(), "Please Answer.", Toast.LENGTH_SHORT).show();
                }

//                } else {
//                    completeSurveyQuestion(questionSliderCallbackFragment,surveyQuestionModel);
//                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (allQuestionsAnswered) {
            UserSessionManager.getUserSessionManager().setQRCode(null);
            for (int i = 0; i < one2OneViewPagerAdapter.getCount(); i++) {
                QuestionSliderCallbackFragment questionSliderCallbackFragment = (QuestionSliderCallbackFragment) one2OneViewPagerAdapter.getFragment(i);
                SurveyQuestionModel surveyQuestionModel = questionSliderCallbackFragment.getSurveyQuestionModel();


                totalsurveyanswered++;
                buttondisable = true;
                try {
//                if (((String) imgNextFragment.getTag()).equalsIgnoreCase(AppConstant.DONE)) {
                    if (questionSliderCallbackFragment.isSurveyQuestionAnswered()) {//Is survey question answered

//                        rccode=surveyQuestionModel.getNextrccode();
//                        nextquestionid=surveyQuestionModel.getNextquestionId();
//                        previousquestionid=surveyQuestionModel.getPreviousquestionId();
//                        serviceeventid=surveyQuestionModel.getServiceeventid();
//                        updatetypeid=surveyQuestionModel.getUpdatetypeid();
//                        serviceidentifiername=surveyQuestionModel.getServiceidentifiername();
//                        aspname=surveyQuestionModel.getAspname();
                        surveyAnswerModel = new SurveyAnswerModel();

                        surveyAnswerModel.setAnswer(surveyQuestionModel.getSurveyanswer());
                        surveyAnswerModel.setQuestionid(surveyQuestionModel.getId());
                        surveyAnswerModel.setMemberid(memberModel.getServerId());
                        surveyAnswerModel.setDeleteflag(false);
                        surveyAnswerModel.setUploadedfiles(surveyQuestionModel.getUploadfile());
//                        surveyAnswerModel.setRemark(surveyQuestionModel.getRemarkvalue());
//                        surveyAnswerModel.setUpdatetypeid(updatetypeid);
                        surveyAnswerModel.setLoginUserId(Integer.valueOf(getUserSessionManager().getUserId()));
//                        surveyAnswerModel.setServiceeventid(serviceeventid);
                        surveyAnswerModel.setStatusid(4);
//                        surveyAnswerModel.setServiceidentifier(serviceidentifiername);
                        surveyAnswerModel.setPushed(false);
                        if (!surveyQuestionModel.getSkipquestion()) {
//                            if(surveyQuestionModel.getSurveyanswer()==null){
                            surveyAnswerModel.setUpdated(false);
                            surveyAnswerModel.setInserted(true);
                            new SetSurveyAnswerAsyncTask(this, provideOaasDatabase(), surveyAnswerModel).execute();//Setting survey answer
                            if (totalquestionsSize == (totalsurveyanswered + dummysurveyAnswerModelList.size())) {
                                //updateMemberComplete(memberModel);
                            }
//                            }else{
//                                surveyAnswerModel.setUpdated(true);
//                                surveyAnswerModel.setInserted(false);
//                                surveyAnswerModel.setLocalid(surveyQuestionModel.getSurveylocalid());
//                                surveyAnswerModel.setBackendid(surveyQuestionModel.getSurveyserverid());
//                                new UpdateSurveyAnswersAsyncTask(this,provideOaasDatabase(), surveyAnswerModel).execute();//Setting survey answer
//                                if(totalquestionsSize==(totalsurveyanswered+dummysurveyAnswerModelList.size())){
//                                    updateMemberComplete(memberModel);
//                                }
//                            }
//                            openTaskListFragment(memberModel);
                        } else if (surveyQuestionModel.getSkipquestion()) {

                            if (surveyQuestionModel.getSurveyanswer().equalsIgnoreCase(surveyQuestionModel.getSkipquestionvalue())||"skip".equalsIgnoreCase(surveyQuestionModel.getSkipquestionvalue())) {
                                groupserialno = Integer.valueOf(surveyQuestionModel.getSkipquestionid());
                            }
                            surveyAnswerModel.setUpdated(false);
                            surveyAnswerModel.setInserted(true);
                            new SetSurveyAnswerAsyncTask(this, provideOaasDatabase(), surveyAnswerModel).execute();//Setting survey answer
                            if (totalquestionsSize == (totalsurveyanswered + dummysurveyAnswerModelList.size())) {
                                //updateMemberComplete(memberModel);
                            }
//                            if (questionSliderCallbackFragment.isTodoDone()) {//this condition do need stodo to be done
//                                this.todoModel = questionSliderCallbackFragment.getTodoModel();
//                                this.todoModel.setMemberid(memberModel.getServerId());
//                                new SetSurveyAnswerAsyncTask(this,provideOaasDatabase(), surveyAnswerModel).execute();//Setting survey answer
//                                new SetTodoAsyncTask(provideOaasDatabase(), todoModel).execute();
//                                if(totalquestionsSize==surveyAnswerModelList.size()+dummysurveyAnswerModelList.size()){
//                                    updateMemberComplete(memberModel);
//                                }
////                                openTaskListFragment(memberModel);
//                            } else {
//                                Toast.makeText(requireActivity(), "ToDo not done", Toast.LENGTH_SHORT).show();
//                            }

                        }
                    } else {
                        allQuestionsAnswered = false;
                        //Toast.makeText(requireActivity(), "Please Answer.", Toast.LENGTH_SHORT).show();
                    }

//                } else {
//                    completeSurveyQuestion(questionSliderCallbackFragment,surveyQuestionModel);
//                }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }



            groupserialno = groupserialno + 1;
        if (!surveyQuestionModelList.isEmpty()) {
            List<Fragment> fragmentList = new ArrayList<>();
            for (SurveyQuestionModel surveyQuestionModel : surveyQuestionModelList) {
                if (surveyQuestionModel.getQuestiontype() != null && surveyQuestionModel.getGroupserialno() == groupserialno) {
                    fragmentList.add(One2OneQuestionFragment.newInstance(surveyQuestionModel,
                            surveyQuestionModelList, AllsurveyQuestionModel, serviceEventModelList, updateTypeModelList));
                }
                surveyQuestionModelobj = surveyQuestionModel;
            }
            if (fragmentList.size() > 0) {
                if(fragmentList.size() > 1){
                    imgNextFragment.setVisibility(View.GONE);
                }
                memberModel.setStatusId(4);
                memberModel.setCompletedFlag(false);
                if(memberModel.getStarttime()==null){
                    memberModel.setStarttime(getLocalDateTime());
                }
                updateMemberComplete(memberModel);
                one2OneViewPagerAdapter = new One2OneViewPagerAdapter(requireActivity().getSupportFragmentManager(), fragmentList);
                vpQuestion.setAdapter(one2OneViewPagerAdapter);
                vpQuestion.addOnPageChangeListener(this);
                imgNextFragment.setTag(AppConstant.NEXT);
            } else {
                memberModel.setStatusId(5);
                memberModel.setCompletedFlag(true);
                memberModel.setEndtime(getLocalDateTime());
                updateMemberComplete(memberModel);
                openTaskListFragment(memberModel);
            }


        }
    }else{
            Toast.makeText(requireActivity(), "Please Answer all the questions.", Toast.LENGTH_SHORT).show();
        }



    }

    public void openTaskListFragment(MemberModel memberModel) {
        if (requireActivity() instanceof DashboardActivity) {
            DashboardActivity dashboardActivity = (DashboardActivity) requireActivity();
            dashboardActivity.openTaskList(memberModel);
        }
    }

    public void completeSurveyQuestion(QuestionSliderCallbackFragment questionSliderCallbackFragment,SurveyQuestionModel surveyQuestionModel){
        Log.d("check","todo");
        if (questionSliderCallbackFragment.isSurveyQuestionAnswered()) {
            checkTodoDone(questionSliderCallbackFragment, surveyQuestionModel);
        } else {
            Toast.makeText(requireActivity(), "Please Answer.", Toast.LENGTH_SHORT).show();
        }
    }

    public void checkTodoDone(QuestionSliderCallbackFragment questionSliderCallbackFragment, SurveyQuestionModel surveyQuestionModel) {
        surveyAnswerModel=new SurveyAnswerModel();
        surveyAnswerModel = questionSliderCallbackFragment.getSurveyAnswerModel();
        surveyAnswerModel.setMemberid(memberModel.getServerId());
//        surveyAnswerModel.setRemark(surveyQuestionModel.getRemarkvalue());
        surveyAnswerModel.setDeleteflag(false);

//        serviceeventid=surveyQuestionModel.getServiceeventid();
//        updatetypeid=surveyQuestionModel.getUpdatetypeid();
//        serviceidentifiername=surveyQuestionModel.getServiceidentifiername();
//        aspname=surveyQuestionModel.getAspname();
//        rccode=surveyQuestionModel.getNextrccode();
//        nextquestionid=surveyQuestionModel.getNextquestionId();
//        previousquestionid=surveyQuestionModel.getPreviousquestionId();
//        curr_position=surveyQuestionModel.getDdQuestioncount();


        surveyAnswerModel.setPushed(false);
        surveyAnswerModel.setUploadedfiles(surveyQuestionModel.getUploadfile());
        surveyAnswerModel.setUpdatetypeid(updatetypeid);
        surveyAnswerModel.setServiceeventid(serviceeventid);
        surveyAnswerModel.setServiceidentifier(serviceidentifiername);
        surveyAnswerModel.setStatusid(4);
        Log.d("check","todo2");
        //For testing purpose I have written this
        if (!surveyQuestionModel.getSkipquestion()&& surveyQuestionModel.getApplytomessage()!=null
                && surveyQuestionModel.getApplytomessage().equalsIgnoreCase(AppConstant.NO)) {
            Log.d("check","todo3");
            if(surveyQuestionModel.getSurveyanswer()==null
                    || memberModel.getRollbackupdate().equalsIgnoreCase("A")){
                Log.d("check","todo post");
                //surveyAnswerModel.setUpdated(false);
                surveyAnswerModel.setInserted(true);
                new SetSurveyAnswerAsyncTask(this,provideOaasDatabase(), surveyAnswerModel).execute();//Setting survey answer
                if(!Inprogress){
                    updateMemberProgress(memberModel);
                }
            }else{
                Log.d("check","todo put");
                //surveyAnswerModel.setUpdated(true);
                //surveyAnswerModel.setInserted(false);
                surveyAnswerModel.setLocalid(surveyQuestionModel.getSurveylocalid());
                surveyAnswerModel.setBackendid(surveyQuestionModel.getSurveyserverid());
                new UpdateSurveyAnswersAsyncTask(this,provideOaasDatabase(),surveyAnswerModel).execute();
                if(!Inprogress){
                    updateMemberProgress(memberModel);
                }
            }

            if(surveyQuestionModel.getQuestiontype().equalsIgnoreCase("11")
                    || surveyQuestionModel.getQuestiontype().equalsIgnoreCase("12")){
                //imgPreviousFragment.setVisibility(View.INVISIBLE);
                childFragmentPos=curr_position-1;
            }
            //new SetSurveyAnswerAsyncTask(provideOaasDatabase(), surveyAnswerModel).execute();//Setting survey answer
            if(rccode!=93){
                if (childFragmentPos != vpQuestion.getAdapter().getCount() - 1) {
                    vpQuestion.setCurrentItem(++childFragmentPos);
                    canGoBack = true;
                }
            }else{
                imgNextFragment.setEnabled(true);
            }
            // questionSliderCallbackFragment.isTodoDone(false, false);//this condition do not need stodo to be done
        } else if (!surveyQuestionModel.getSkipquestion()&& surveyQuestionModel.getApplytomessage()!=null
                && surveyQuestionModel.getApplytomessage().equalsIgnoreCase(AppConstant.YES)) {
            Log.d("check","todo4");
            if (questionSliderCallbackFragment.isTodoDone()) {//this condition do need stodo to be done
                this.todoModel = questionSliderCallbackFragment.getTodoModel();
                this.todoModel.setMemberid(memberModel.getServerId());
                if(surveyQuestionModel.getSurveyanswer()==null){
                    new SetSurveyAnswerAsyncTask(this,provideOaasDatabase(), surveyAnswerModel).execute();//Setting survey answer
                }
                //new SetSurveyAnswerAsyncTask(provideOaasDatabase(), surveyAnswerModel).execute();//Setting survey answer
                new SetTodoAsyncTask(provideOaasDatabase(), todoModel).execute();
                if (childFragmentPos != vpQuestion.getAdapter().getCount() - 1) {
                    vpQuestion.setCurrentItem(++childFragmentPos);
                    canGoBack = true;
                }
            } else {
                Toast.makeText(requireActivity(), "ToDo not done", Toast.LENGTH_SHORT).show();
            }

        } else if (surveyQuestionModel.getSkipquestion()&& surveyQuestionModel.getApplytomessage()!=null
                && surveyQuestionModel.getApplytomessage().equalsIgnoreCase(AppConstant.YES)) {
            Log.d("check","todo5");
            if (questionSliderCallbackFragment.isTodoDone()) {//this condition do need stodo to be done
                this.todoModel = questionSliderCallbackFragment.getTodoModel();
                this.todoModel.setMemberid(memberModel.getServerId());
                if(surveyQuestionModel.getSurveyanswer()==null){
                    new SetSurveyAnswerAsyncTask(this,provideOaasDatabase(), surveyAnswerModel).execute();//Setting survey answer
                }
                //new SetSurveyAnswerAsyncTask(provideOaasDatabase(), surveyAnswerModel).execute();//Setting survey answer
                new SetTodoAsyncTask(provideOaasDatabase(), todoModel).execute();
                int skipIndex=performQuestionSkip(surveyQuestionModel,surveyAnswerModel);
                if(skipIndex!=-1){
                    if (childFragmentPos != vpQuestion.getAdapter().getCount() - 1) {
                        vpQuestion.setCurrentItem(skipIndex);
                        canGoBack = true;
                    }
                }else{
                    if (childFragmentPos != vpQuestion.getAdapter().getCount() - 1) {
                        vpQuestion.setCurrentItem(++childFragmentPos);
                        canGoBack = true;
                    }
                }
            } else {
                Toast.makeText(requireActivity(), "ToDo not done", Toast.LENGTH_SHORT).show();
            }

        } else if (!surveyQuestionModel.getSkipquestion()&& surveyQuestionModel.getApplytomessage()==null) {//Remove this condition after testing
            Log.d("check","todo6");
            if(surveyQuestionModel.getSurveyanswer()==null){
                new SetSurveyAnswerAsyncTask(this,provideOaasDatabase(), surveyAnswerModel).execute();//Setting survey answer
            }
            //new SetSurveyAnswerAsyncTask(provideOaasDatabase(), surveyAnswerModel).execute();
            if (childFragmentPos != vpQuestion.getAdapter().getCount() - 1) {
                vpQuestion.setCurrentItem(++childFragmentPos);
                canGoBack = true;
            }
            // questionSliderCallbackFragment.isTodoDone(false, false);//this condition do not need stodo to be done
        } else if (surveyQuestionModel.getSkipquestion()) {//Here there is no need to check sTodo
            Log.d("check","todo7");
            if(surveyQuestionModel.getSurveyanswer()==null){
                new SetSurveyAnswerAsyncTask(this,provideOaasDatabase(), surveyAnswerModel).execute();//Setting survey answer
            }
            //new SetSurveyAnswerAsyncTask(provideOaasDatabase(), surveyAnswerModel).execute();//Setting survey answer
            int skipIndex=performQuestionSkip(surveyQuestionModel,surveyAnswerModel);
            if(skipIndex!=-1){
                if (childFragmentPos != vpQuestion.getAdapter().getCount() - 1) {
                    vpQuestion.setCurrentItem(skipIndex);
                    canGoBack = true;
                }
            }else{
                if (childFragmentPos != vpQuestion.getAdapter().getCount() - 1) {
                    vpQuestion.setCurrentItem(++childFragmentPos);
                    canGoBack = true;
                }
            }
        }
    }

    public int performQuestionSkip(SurveyQuestionModel surveyQuestionModel, SurveyAnswerModel surveyAnswerModel) {

        if (surveyQuestionModel.getSkipquestionvalue().equalsIgnoreCase(surveyAnswerModel.getAnswer())) {
            int index=-1;
            for (SurveyQuestionModel surveyQuestionModel1 : surveyQuestionModelList) {
                if (surveyQuestionModel.getSkipquestionid().equalsIgnoreCase(String.valueOf(surveyQuestionModel1.getId()))) {
                    index= surveyQuestionModelList.indexOf(surveyQuestionModel1);
                    break;
                }
            }
            return index;
        }else{
            return -1;
        }
    }

    public int skipQuestion(int previousQuestionId, List<SurveyQuestionModel> surveyQuestionModelList) {
        for (SurveyQuestionModel surveyQuestionModel1 : surveyQuestionModelList) {
            if (previousQuestionId == surveyQuestionModel1.getId()) {
                return surveyQuestionModelList.indexOf(surveyQuestionModel1);
            }
        }
        return -1;
    }

    @Override
    public void onComplete(List<SurveyQuestionModel> surveyQuestionModel) {
        totalquestionsSize = surveyQuestionModel.size();
        AllsurveyQuestionModel = surveyQuestionModel;
        Log.d("surveyquestionstotal",""+totalquestionsSize);
        for(SurveyQuestionModel model : surveyQuestionModel){
            if(getUserSessionManager().getOnetoOneFlag().equalsIgnoreCase("t")){
                model.setOnetooneFlag(true);
            } else {
                model.setOnetooneFlag(false);
            }

            if(model.getPlatform().equalsIgnoreCase("mobile") ||
                    model.getPlatform().equalsIgnoreCase("web and mobile")){
                model.setQuestioncount(questioncount);
                model.setMemberid(memberModel.getServerId());
                ++questioncount;
                surveyQuestionModelList.add(model);
            } else {
                dummysurveyQuestionModelList.add(model);
            }
            //for rollbackprevious question
            /*if(memberModel.getPrevquestionId().equals(model.getSerialno())){
                Log.d("model",""+model.getQuestioncount());
                rollback=true;
            }*/

        }
        Log.d("rollbackflag",""+rollback);
        Log.d("surveyquestions",""+surveyQuestionModelList.size());
    }

    Calendar calendar = Calendar.getInstance();

    DatePickerDialog.OnDateSetListener onDateSetListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            calendar.set(Calendar.YEAR, year);//Here calender object is updated
            calendar.set(Calendar.MONTH, month);
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            txtSession.setText(CommonUtils.updateDate(calendar.getTime()));
        }
    };

    @OnClick(R.id.rl_session_date)
    public void onSessionDateClicked() {
        DatePickerDialog datePickerDialog = new DatePickerDialog(requireContext(), onDateSetListener
                , calendar.get(Calendar.YEAR)
                , calendar.get(Calendar.MONTH)
                , calendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        datePickerDialog.show();
    }

    public void updateMemberrollback(MemberModel memberModel) {
        Log.d("updaterollback","done");
        memberModel.setPushed(false);
        memberModel.setPrevquestionId(previousquestionid);
        memberModel.setRollbackFlag(true);
        memberModel.setRollbackStatus("R");
        memberModel.setUpdateflag(true);
        memberModel.setLastloginUserId(0);
        memberModel.setLastloginUserId(Integer.valueOf(getUserSessionManager().getUserId()));
        memberModel.setLastmodifiedtime(getDateTime());
        new UpdateMemberAsyncTask(this,provideOaasDatabase(),memberModel).execute();
        if(surveylocalid!=0 &&surveyrollbackId!=0){
            updatesurveyrollback(surveyrollbackId,surveylocalid);
        }
    }

    public void updatesurveyrollback(int rollbackId, int localid){
        //surveyAnswerModel=new SurveyAnswerModel();
        surveyAnswerModel.setRollbackstatusId(1);
        surveyAnswerModel.setRollbackRequestTime(getDateTime());
        surveyAnswerModel.setRollbackRoleId(getUserSessionManager().getUser().getRoleId());
        //surveyAnswerModel.setUpdated(true);
        surveyAnswerModel.setLoginUserId(Integer.valueOf(getUserSessionManager().getUserId()));
        surveyAnswerModel.setLastmodifiedtime(getDateTime());
        surveyAnswerModel.setPushed(false);
        surveyAnswerModel.setLocalid(localid);
        surveyAnswerModel.setBackendid(rollbackId);
        new UpdateSurveyAnswersAsyncTask(this,provideOaasDatabase(),surveyAnswerModel).execute();
        Toast.makeText(getActivity(), "RollBack Updated", Toast.LENGTH_SHORT).show();
        imgNextFragment.setEnabled(false);
        imgPreviousFragment.setEnabled(false);
        btrollback.setBackground(getResources().getDrawable(R.drawable.background_rollback_red));
    }

    public void updateMemberComplete(MemberModel memberModel) {
        Log.d("updatecomplete","done");
        memberModel.setPushed(false);
        memberModel.setUpdateflag(true);
        memberModel.setServiceeventid(serviceeventid);
        memberModel.setProducttypeid(updatetypeid);
        memberModel.setServiceidentifier(serviceidentifiername);
        memberModel.setAspname(aspname);
        //memberModel.setLastloginUserId(Integer.valueOf(getUserSessionManager().getUserId()));
        memberModel.setProcessflag(true);
        memberModel.setRcCode(rccode);
        memberModel.setNextquestionId(groupserialno);
//        memberModel.setStatusId(5);
        memberModel.setLastmodifiedtime(getDateTime());
//        memberModel.setCompletedFlag(true);
        new UpdateMemberAsyncTask(this,provideOaasDatabase(),memberModel).execute();
    }

    public void updateMemberProgress(MemberModel memberModel) {
        Log.d("updateprogress","done"+memberModel);
        //to update once unmask Inprogress
        //Inprogress=true;
        if(memberModel.getRollbackStatus().equalsIgnoreCase("A")){
            memberModel.setRollbackupdate("N");
        }
        //memberModel.setLastloginUserId(Integer.valueOf(getUserSessionManager().getUserId()));
        memberModel.setServiceeventid(serviceeventid);
        memberModel.setProducttypeid(updatetypeid);
        memberModel.setServiceidentifier(serviceidentifiername);
        memberModel.setAspname(aspname);
        memberModel.setProcessflag(true);
        memberModel.setPushed(false);
        memberModel.setRcCode(rccode);
        memberModel.setLastmodifiedtime(getDateTime());
        memberModel.setNextquestionId(nextquestionid);
        memberModel.setUpdateflag(true);
        memberModel.setStatusId(4);
        new UpdateMemberAsyncTask(this,provideOaasDatabase(),memberModel).execute();
    }


    @Override
    public void onComplete() {

    }

    @Override
    public void onUpdate() {
        //Toast.makeText(getActivity(), "data inserted", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onCompleteSurvey(List<SurveyAnswerModel> surveyAnswerModels) {
        Log.d("surveyanswers",""+surveyAnswerModels.size());

        for(SurveyQuestionModel questionModel : surveyQuestionModelList){
            for(SurveyAnswerModel answerModel :surveyAnswerModels){
                if(questionModel.getId().equals(answerModel.getQuestionid()) && memberModel.getServerId().equals(answerModel.getMemberid())){
                    questionModel.setSurveylocalid(answerModel.getLocalid());
                    questionModel.setSurveyserverid(answerModel.getBackendid());
                    questionModel.setSurveyanswer(answerModel.getAnswer());
                    questionModel.setRemarkvalue(answerModel.getRemark());
                    questionModel.setUploadfile(answerModel.getUploadedfiles());
                    questionModel.setServiceidentifiername(answerModel.getServiceidentifier());
                    questionModel.setUpdatetypeid(answerModel.getUpdatetypeid());
                    questionModel.setServiceeventid(answerModel.getServiceeventid());
                    surveyAnswerModelList.add(answerModel);
                }else{
                    questionModel.setSurveylocalid(0);
                    questionModel.setSurveyserverid(0);
                    questionModel.setSurveyanswer(null);
                    questionModel.setRemarkvalue(null);
                    questionModel.setUploadfile(null);
                    questionModel.setServiceidentifiername(null);
                    questionModel.setUpdatetypeid(null);
                    questionModel.setServiceeventid(null);
                }
            }
        }

        totalsurveyanswered=surveyAnswerModelList.size();

        for(SurveyQuestionModel questionModel : dummysurveyQuestionModelList){
            for(SurveyAnswerModel answerModel :surveyAnswerModels){
                if(questionModel.getId().equals(answerModel.getQuestionid())){
                    dummysurveyAnswerModelList.add(answerModel);
                }
            }
        }
        Log.d("surveyanswersmobile",""+surveyAnswerModelList.size()+","+memberModel);
        Log.d("surveyanswersweb",""+dummysurveyAnswerModelList.size());

        if(surveyAnswerModelList.size()>0){
            try {
                surveyAnswerModel=surveyAnswerModelList.get(surveyAnswerModelList.size()-1);
                surveylocalid=surveyAnswerModelList.get(surveyAnswerModelList.size()-1).getLocalid()==null?0:surveyAnswerModelList.get(surveyAnswerModelList.size()-1).getLocalid();
                surveyrollbackId=surveyAnswerModelList.get(surveyAnswerModelList.size()-1).getBackendid()==null?0:surveyAnswerModelList.get(surveyAnswerModelList.size()-1).getBackendid();;
                buttondisable=true;
            }catch (Exception e){
                e.printStackTrace();
            }
        }

        for(SurveyAnswerModel model:dummysurveyAnswerModelList){
            Integer substring = Integer.valueOf(model.getAnswer().substring(Math.max(model.getAnswer().length() - 2, 0)));
            if(model.getUploadedfiles()!=null && substring==10){
                pdfanswermodel=model;
            }
        }

        if (!surveyQuestionModelList.isEmpty()) {
            List<Fragment> fragmentList = new ArrayList<>();
            for (SurveyQuestionModel surveyQuestionModel : surveyQuestionModelList) {
                if(surveyQuestionModel.getQuestiontype() != null && surveyQuestionModel.getGroupserialno()==groupserialno) {
                    fragmentList.add(One2OneQuestionFragment.newInstance(surveyQuestionModel,
                            surveyQuestionModelList,AllsurveyQuestionModel,serviceEventModelList,updateTypeModelList));
                }
                surveyQuestionModelobj=surveyQuestionModel;
            }
            if(fragmentList.size() > 0){
                if(fragmentList.size() > 1){
                    imgNextFragment.setVisibility(View.GONE);
                }
                one2OneViewPagerAdapter = new One2OneViewPagerAdapter(requireActivity().getSupportFragmentManager(), fragmentList);
//            vpQuestion.setClipChildren(false);
//            vpQuestion.setClipToPadding(false);
//            int margin = (int) getResources().getDimension(R.dimen.d_margin_large);//16dp
//            final int padding = (int) getResources().getDimension(R.dimen.d_margin_ex_large);//18dp
//            vpQuestion.setPageMargin(margin);
//            vpQuestion.setPadding(0, padding * 2, 0, padding * 2);
//            vpQuestion.setPageTransformer(false, new ViewPager.PageTransformer() {
//                @Override public void transformPage(View page, float position) {
//                    if (vpQuestion.getCurrentItem() == 0) {
//                        page.setTranslationY(-(padding));
//                    } else if (vpQuestion.getCurrentItem() == one2OneViewPagerAdapter.getCount() - 1) {
//                        page.setTranslationY(padding);
//                    } else {
//                        page.setTranslationY(0);
//                    }
////                    final int pageWidth = page.getWidth();
////                    final int pageHeight = page.getHeight();
////                    if (position < -1) {
////                        page.setAlpha(0);
////                    } else if (position <= 1) {
////                        page.setAlpha(1);
////                        page.setTranslationX(pageWidth * -position);
////                        float yPosition = position * pageHeight;
////                        page.setTranslationY(yPosition);
////                    } else {
////                        page.setAlpha(0);
////                    }
//                }
//            });
                vpQuestion.setAdapter(one2OneViewPagerAdapter);
                vpQuestion.addOnPageChangeListener(this);
                imgNextFragment.setTag(AppConstant.NEXT);

                if(surveyAnswerModelList.size()>0){
                    int count=0;int value=0;

                    for(SurveyQuestionModel Model : surveyQuestionModelList){
                        value++;
                        if(Model.getRcCode().equals(memberModel.getRcCode())){
                            value--;
                            break;
                        }
                    }

                    Log.d("count",""+count+","+value);
                    if (childFragmentPos != vpQuestion.getAdapter().getCount() - 1) {
                    /*if(rollback && count>=0){
                        vpQuestion.setCurrentItem(count);
                    } else {
                        vpQuestion.setCurrentItem(value);
                    }*/
                        //vpQuestion.setCurrentItem(value);

                        canGoBack = true;
                    }
                }

            /*if(UserSessionManager.getUserSessionManager().getOnetoOneFlag().equalsIgnoreCase("f")){
                final IndicatorBinder sample = new IndicatorBinder().bind(requireActivity(),
                        vpQuestion, llIndicatorContainer,
                        R.drawable.indicator_selected,
                        R.drawable.indicator_unselected);
                // Set whether you want a progress style
                sample.setProgressStyle(true);
            }*/
            } else {
                openTaskListFragment(memberModel);
            }


        }
    }

    @Override
    public void FetchAppCongigData(List<AppConfigModel> appConfigModels) {
        getUserSessionManager().setOnetoOneFlag(appConfigModels.get(0).getValue());
    }

    private String getDateTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.sss'Z'", Locale.US);
        dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        Date date = new Date();
        return dateFormat.format(date);
    }

    private String getLocalDateTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm", Locale.US);
//        dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        Date date = new Date();
        return dateFormat.format(date);
    }

    @Override
    public void FetchServiceEventData(List<ServiceEventModel> serviceEvents) {
        serviceEventModelList=serviceEvents;
    }

    @Override
    public void FetchUpdatetypeData(List<UpdateTypeModel> updateTypes) {
        updateTypeModelList=updateTypes;
    }

    @Override
    public void OnSurveyPosted(Long id, Integer backendid) {
        surveylocalid= Integer.parseInt(id.toString());
        surveyrollbackId=backendid;
        //Toast.makeText(getActivity(),""+surveylocalid, Toast.LENGTH_SHORT).show();
    }
}
