package com.app.fsm.ui.dash.main.asynctask;

import android.os.AsyncTask;

import androidx.annotation.NonNull;

import com.app.fsm.local_network.network.model.updatetypes.UpdateTypeModel;
import com.app.fsm.local_network.network.model.updatetypes.UpdateTypeModelDao;
import com.app.fsm.local_network.repository.OaasDatabase;
import com.app.fsm.ui.dash.main.UpdateTypeListner;

import java.util.List;

public class UpdateTypeAsyncTask extends AsyncTask<Void,Void, List<UpdateTypeModel>> {

    private OaasDatabase oaasDatabase;
    private UpdateTypeListner updateTypeListner;

    public UpdateTypeAsyncTask(@NonNull UpdateTypeListner updateTypeListner, @NonNull OaasDatabase oaasDatabase) {
        this.oaasDatabase = oaasDatabase;
        this.updateTypeListner = updateTypeListner;
    }
    @Override
    protected List<UpdateTypeModel> doInBackground(Void... voids) {
        UpdateTypeModelDao updateTypeModelDao=oaasDatabase.provideupdateTypeModelDao();
        return updateTypeModelDao.getUpdateTypes();
    }

    @Override
    protected void onPostExecute(List<UpdateTypeModel> updateTypes) {
        if (updateTypeListner != null)
            updateTypeListner.FetchUpdatetypeData(updateTypes);
    }
}