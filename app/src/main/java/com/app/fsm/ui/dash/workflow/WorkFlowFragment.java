package com.app.fsm.ui.dash.workflow;


import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatSpinner;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.fsm.BaseFragment;
import com.app.fsm.R;
import com.app.fsm.local_network.network.model.joinedtable.TaskListModel;
import com.app.fsm.local_network.network.model.member.MemberModel;
import com.app.fsm.local_network.network.model.usermodel.UserModel;
import com.app.fsm.local_network.network.model.workflow.WorkFlowCategory;
import com.app.fsm.local_network.network.model.workflow.WorkFlowHeader;
import com.app.fsm.local_network.network.model.workflow.WorkFlowLangauge;
import com.app.fsm.local_network.network.model.workflow.WorkFlowModel;
import com.app.fsm.local_network.network.model.workflow.WorkFlowPriority;
import com.app.fsm.local_network.network.model.workflow.WorkFlowStatus;
import com.app.fsm.ui.dash.workflow.adapter.CategoryAdapter;
import com.app.fsm.ui.dash.workflow.adapter.PriorityAdapter;
import com.app.fsm.ui.dash.workflow.adapter.StatusAdapter;
import com.app.fsm.ui.dash.workflow.adapter.UserModelAdapter;
import com.app.fsm.ui.dash.workflow.adapter.WorkFlowAdapter;
import com.app.fsm.ui.dash.workflow.asynctask.GetWorkFlowAssignedToAsyncTask;
import com.app.fsm.ui.dash.workflow.asynctask.GetWorkFlowAsyncTask;
import com.app.fsm.ui.dash.workflow.asynctask.GetWorkFlowCategoryAsyncTask;
import com.app.fsm.ui.dash.workflow.asynctask.GetWorkFlowHeaderAsyncTask;
import com.app.fsm.ui.dash.workflow.asynctask.GetWorkFlowLanguageAsyncTask;
import com.app.fsm.ui.dash.workflow.asynctask.GetWorkFlowPriorityAsyncTask;
import com.app.fsm.ui.dash.workflow.asynctask.GetWorkFlowStatusAsyncTask;
import com.app.fsm.ui.dash.workflow.asynctask.SetWorkFlowHeaderAsyncTask;
import com.app.fsm.ui.dash.workflow.asynctask.UpdateWorkFlowAsyncTask;
import com.app.fsm.utils.AppConstant;
import com.app.fsm.utils.CommonUtils;

import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class WorkFlowFragment extends BaseFragment implements OnWorkFlowFetchListener
        , AdapterView.OnItemSelectedListener {

    @BindView(R.id.txt_label_member)
    TextView txtLabelMember;
    @BindView(R.id.txt_member_name)
    TextView txtMemberName;
    @BindView(R.id.txt_label_workflow)
    TextView txtLabelWorkflow;
    @BindView(R.id.spinner_workflow)
    AppCompatSpinner spinnerWorkflow;
    @BindView(R.id.txt_label_category)
    TextView txtLabelCategory;
    @BindView(R.id.spinner_category)
    AppCompatSpinner spinnerCategory;
    @BindView(R.id.txt_label_follow_up)
    TextView txtLabelFollowUp;
    @BindView(R.id.img_calender)
    ImageView imgCalender;
    @BindView(R.id.txt_dob)
    TextView txtFollowupdate;
    @BindView(R.id.rl_dob)
    RelativeLayout rlDob;
    @BindView(R.id.txt_label_assigned_to)
    TextView txtLabelAssignedTo;
    @BindView(R.id.spinner_assign)
    AppCompatSpinner spinnerAssign;
    @BindView(R.id.txt_label_status)
    TextView txtLabelStatus;
    @BindView(R.id.spinner_status)
    AppCompatSpinner spinnerStatus;
    @BindView(R.id.txt_label_priority)
    TextView txtLabelPriority;
    @BindView(R.id.spinner_priority)
    AppCompatSpinner spinnerPriority;
    @BindView(R.id.txt_label_comment)
    TextView txtLabelComment;
    @BindView(R.id.edt_comment)
    AppCompatEditText edtComment;
    @BindView(R.id.ll_workflow)
    LinearLayout llWorkflow;
    Unbinder unbinder;
    @BindView(R.id.txt_title_child_fragment)
    TextView txtTitleChildFragment;
    @BindView(R.id.img_to_previous_fragment)
    ImageView imgToPreviousFragment;
    @BindView(R.id.ll_indicator_container)
    LinearLayout llIndicatorContainer;
    @BindView(R.id.img_to_next_fragment)
    ImageView imgToNextFragment;
    @BindView(R.id.txt_fragment_count)
    TextView txtFragmentCount;

    private MemberModel memberModel;
    private TaskListModel taskListModel;
    private Boolean isEditable = false;
    private WorkFlowModel workflowmodel;
    private UserModel userModel;
    private WorkFlowStatus workflowstatus;
    private WorkFlowCategory workflowcategory;
    private WorkFlowPriority workflowpriority;
    private Calendar calendar = Calendar.getInstance();

    public static WorkFlowFragment newInstance(Boolean isEditable, MemberModel memberModel
            , TaskListModel taskListModel) {
        Bundle bundle = new Bundle();
        bundle.putBoolean(AppConstant.IS_EDITABLE, isEditable);
        bundle.putSerializable(AppConstant.MEMBER_MODEL, memberModel);
        bundle.putSerializable(AppConstant.TASK_LIST, taskListModel);
        WorkFlowFragment workFlowFragment = new WorkFlowFragment();
        workFlowFragment.setArguments(bundle);
        return workFlowFragment;
    }

    DatePickerDialog.OnDateSetListener onDateSetListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.MONTH, month);
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            txtFollowupdate.setText(CommonUtils.updateDate(calendar.getTime()));
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_workflow, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (getArguments() != null) {
            memberModel = (MemberModel) getArguments().getSerializable(AppConstant.MEMBER_MODEL);
            isEditable = getArguments().getBoolean(AppConstant.IS_EDITABLE);
            taskListModel = (TaskListModel) getArguments().getSerializable(AppConstant.TASK_LIST);

        }
    }

    public void init() {

        if (isEditable) {
            setTitle(getString(R.string.update_work_flow));
            new GetWorkFlowHeaderAsyncTask(this, provideOaasDatabase(), taskListModel.getId()).execute();
        } else {
            setTitle(getString(R.string.work_flow));
            txtMemberName.setText(memberModel.getSerno());
            new GetWorkFlowAsyncTask(this, provideOaasDatabase()).execute();
        }
        imgToNextFragment.setImageResource(R.drawable.ic_done);
        imgToPreviousFragment.setVisibility(View.VISIBLE);
        imgToPreviousFragment.setImageResource(R.drawable.ic_cancel);
        txtTitleChildFragment.setText(R.string.work_flow);
        txtFragmentCount.setText("1/1");
        new GetWorkFlowLanguageAsyncTask(this, provideOaasDatabase()).execute();

    }

    @OnClick(R.id.img_to_previous_fragment)
    public void onCLickPreviousFragment() {
        requireActivity().getSupportFragmentManager().popBackStack();
    }

    @Override
    public void onUpdate() {
        requireActivity().getSupportFragmentManager().popBackStack();
    }

    @OnClick(R.id.img_to_next_fragment)
    public void onClickNextFragment() {
        if (checkValidation()) {
            WorkFlowHeader workFlowHeader = new WorkFlowHeader();

            if(isEditable){
                workFlowHeader.setMemberid(taskListModel.getMemberId());
                workFlowHeader.setServerId(this.workFlowHeader.getServerId());
                workFlowHeader.setLocalId(this.workFlowHeader.getLocalId());
            }else{
                workFlowHeader.setMemberid(memberModel.getServerId());
            }
            workFlowHeader.setAssignedto(userModel.getId());
            workFlowHeader.setWorkflowid(workflowmodel.getId());
            workFlowHeader.setWorkflowname(workflowmodel.getName());
            workFlowHeader.setWorkflowstatusid(workflowstatus.getId());
            workFlowHeader.setWorkflowcategoryid(workflowcategory.getId());
            workFlowHeader.setWorkflowpriorityid(workflowpriority.getId());
            workFlowHeader.setCreatedtime(CommonUtils.localDbDateFormat(Calendar.getInstance().getTime()));
            String followup = calendar.get(Calendar.YEAR) + "-" + (calendar.get(Calendar.MONTH) + 1) + "-" + calendar.get(Calendar.DAY_OF_MONTH) + "T00:00:00.000Z";
            workFlowHeader.setFollowupdate(followup);
            workFlowHeader.setComments(edtComment.getText().toString());
            workFlowHeader.setPushed(false);
            if (getUserSessionManager().getUser() != null)
                workFlowHeader.setCreatedby(getUserSessionManager().getUser().getId());

            if (isEditable)
                new UpdateWorkFlowAsyncTask(this, provideOaasDatabase(), workFlowHeader).execute();
            else
                new SetWorkFlowHeaderAsyncTask(this, provideOaasDatabase(), workFlowHeader).execute();
        }
    }

    private boolean checkValidation() {

        if (workflowmodel == null) {
            Toast.makeText(requireActivity(), "Please select WorkFlow", Toast.LENGTH_SHORT).show();
        } else if (workflowcategory == null) {
            Toast.makeText(requireActivity(), "Please select WorkFlow Category", Toast.LENGTH_SHORT).show();
        } else if (TextUtils.isEmpty(txtFollowupdate.getText())) {
            Toast.makeText(requireActivity(), "Follow up", Toast.LENGTH_SHORT).show();
        } else if (userModel == null) {
            Toast.makeText(requireActivity(), "Please select WorkFlow Assigned To", Toast.LENGTH_SHORT).show();
        } else if (workflowstatus == null) {
            Toast.makeText(requireActivity(), "Please select WorkFlow Status", Toast.LENGTH_SHORT).show();
        } else if (workflowpriority == null) {
            Toast.makeText(requireActivity(), "Please select WorkFlow Priority", Toast.LENGTH_SHORT).show();
        } else {
            return true;
        }
        return false;
    }

    @Override
    public void onWorkLanguageFetched(List<WorkFlowLangauge> workFlowLanguageList) {
        if (!workFlowLanguageList.isEmpty()) {
            WorkFlowLangauge workFlowLangauge = workFlowLanguageList.get(0);
            setLabelView(workFlowLangauge);
        }
    }

    @Override
    public void onWorkFlowFetched(List<WorkFlowModel> workFlowModelList) {
        WorkFlowModel workFlowModel = new WorkFlowModel();
        workFlowModel.setName(getString(R.string.select));
        workFlowModelList.add(workFlowModel);
        Log.e("workflow_model_size",""+workFlowModelList.size());
        spinnerWorkflow.setOnItemSelectedListener(this);
        WorkFlowAdapter workFlowAdapter = new WorkFlowAdapter(requireActivity(), workFlowModelList);
        spinnerWorkflow.setAdapter(workFlowAdapter);

        if (isEditable)
            for (int i = 0; i < workFlowModelList.size(); i++) {
                if (workFlowModelList.get(i).getId() != null && workFlowModelList.get(i).getId().equals(workFlowHeader.getWorkflowid().intValue())) {
                    spinnerWorkflow.setSelection(i);
                    this.workflowmodel = workFlowModelList.get(i);
                    onWorkFlowSelected(workFlowHeader.getWorkflowid());
                    return;
                }
            }
        spinnerWorkflow.setSelection(workFlowAdapter.getCount());
    }

    @Override
    public void onWorkFlowPriorityFetched(List<WorkFlowPriority> workFlowPriorityList) {
        WorkFlowPriority workFlowPriority = new WorkFlowPriority();
        workFlowPriority.setName(getString(R.string.select));
        workFlowPriorityList.add(workFlowPriority);
        spinnerPriority.setOnItemSelectedListener(this);
        PriorityAdapter priorityAdapter = new PriorityAdapter(requireActivity(), workFlowPriorityList);
        spinnerPriority.setAdapter(priorityAdapter);

        if (isEditable)
            for (int i = 0; i < workFlowPriorityList.size(); i++) {
                if (workFlowPriorityList.get(i).getId() != null && workFlowPriorityList.get(i).getId().equals(workFlowHeader.getWorkflowpriorityid())) {
                    this.workflowpriority = workFlowPriorityList.get(i);
                    spinnerPriority.setSelection(i);
                    return;
                }
            }
        spinnerPriority.setSelection(priorityAdapter.getCount());

    }

    @Override
    public void onWorkFlowCategoryFetched(List<WorkFlowCategory> workFlowCategoryList) {
        WorkFlowCategory workFlowCategory = new WorkFlowCategory();
        workFlowCategory.setName(getString(R.string.select));
        workFlowCategoryList.add(workFlowCategory);
        Log.e("workflow_cat_size",""+workFlowCategoryList.size());
        spinnerCategory.setOnItemSelectedListener(this);
        CategoryAdapter categoryAdapter = new CategoryAdapter(requireActivity(), workFlowCategoryList);
        spinnerCategory.setAdapter(categoryAdapter);

        if (isEditable)
            for (int i = 0; i < workFlowCategoryList.size(); i++) {
                if (workFlowCategoryList.get(i).getId() != null && workFlowCategoryList.get(i).getId().equals(workFlowHeader.getWorkflowcategoryid())) {
                    this.workflowcategory = workFlowCategoryList.get(i);
                    spinnerCategory.setSelection(i);
                    return;
                }
            }
        spinnerCategory.setSelection(categoryAdapter.getCount());
    }

    @Override
    public void onWorkFlowAssignedToFetched(List<UserModel> userModelList) {
        UserModel userModel = new UserModel();
        userModel.setUsername(getString(R.string.select));
        userModelList.add(userModel);
        Log.e("workflow_assign_size",""+userModelList.size());
        spinnerAssign.setOnItemSelectedListener(this);
        UserModelAdapter userModelAdapter = new UserModelAdapter(requireActivity(), userModelList);
        spinnerAssign.setAdapter(userModelAdapter);

        if (isEditable)
            for (int i = 0; i < userModelList.size(); i++) {
                if (userModelList.get(i).getId() != null && userModelList.get(i).getId().equals(workFlowHeader.getAssignedto())) {
                    this.userModel = userModelList.get(i);
                    spinnerAssign.setSelection(i);
                    return;
                }
            }

        if (getUserSessionManager().getUser() != null) {
            for (int i = 0; i < userModelList.size(); i++) {
                if (userModelList.get(i).getId() != null && userModelList.get(i).getId().equals(getUserSessionManager().getUser().getId())) {
                    this.userModel = userModelList.get(i);
                    spinnerAssign.setSelection(i);
                    return;
                }
            }
        }
        spinnerAssign.setSelection(userModelAdapter.getCount());
    }

    @Override
    public void onWorkFlowStatusFetched(List<WorkFlowStatus> workFlowStatusList) {
        WorkFlowStatus workFlowStatus = new WorkFlowStatus();
        workFlowStatus.setName(getString(R.string.select));
        workFlowStatusList.add(workFlowStatus);
        Log.e("workflow_status_size",""+workFlowStatusList.size());
        spinnerStatus.setOnItemSelectedListener(this);
        StatusAdapter statusAdapter = new StatusAdapter(requireActivity(), workFlowStatusList);
        spinnerStatus.setAdapter(statusAdapter);

        if (isEditable)
            for (int i = 0; i < workFlowStatusList.size(); i++) {
                if (workFlowStatusList.get(i).getId() != null && workFlowStatusList.get(i).getId().equals(workFlowHeader.getWorkflowstatusid())) {
                    this.workflowstatus = workFlowStatusList.get(i);
                    spinnerStatus.setSelection(i);
                    return;
                }
            }
        spinnerStatus.setSelection(statusAdapter.getCount());
    }

    public void setLabelView(WorkFlowLangauge workFlowLangauge) {
        txtLabelMember.setText(workFlowLangauge.getMember());
        txtLabelWorkflow.setText(workFlowLangauge.getWorkflowLabel());
        txtLabelCategory.setText(workFlowLangauge.getCategory());
        txtLabelFollowUp.setText(workFlowLangauge.getFollowup());
        txtLabelAssignedTo.setText(workFlowLangauge.getAssignedTo());
        txtLabelStatus.setText(workFlowLangauge.getStatus());
        txtLabelPriority.setText(workFlowLangauge.getPriority());
        txtLabelComment.setText(workFlowLangauge.getComments());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.rl_dob)
    public void openDatePicker() {
        DatePickerDialog datePickerDialog = new DatePickerDialog(requireContext(), onDateSetListener
                , calendar.get(Calendar.YEAR)
                , calendar.get(Calendar.MONTH)
                , calendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
        datePickerDialog.show();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        if (parent.getAdapter() instanceof WorkFlowAdapter) {
            WorkFlowAdapter workFlowAdapter = (WorkFlowAdapter) parent.getAdapter();
            WorkFlowModel workFlowModel = workFlowAdapter.getItem(position);
            if (workFlowModel != null && !workFlowModel.getName().contains(getString(R.string.select))) {
                this.workflowmodel = workFlowModel;
                this.workflowstatus = null;
                this.workflowpriority = null;
                this.workflowcategory = null;
                onWorkFlowSelected(workFlowModel.getId());
            }
            return;
        } else if (parent.getAdapter() instanceof UserModelAdapter) {
            UserModelAdapter userModelAdapter = (UserModelAdapter) parent.getAdapter();
            UserModel userModel = userModelAdapter.getItem(position);
            if (userModel != null && !userModel.getUsername().contains(getString(R.string.select))) {
                this.userModel = userModel;
            }
            return;
        } else if (parent.getAdapter() instanceof StatusAdapter) {
            StatusAdapter statusAdapter = (StatusAdapter) parent.getAdapter();
            WorkFlowStatus workFlowStatus = statusAdapter.getItem(position);
            if (workFlowStatus != null && !workFlowStatus.getName().contains(getString(R.string.select))) {
                this.workflowstatus = workFlowStatus;
            }
            return;
        } else if (parent.getAdapter() instanceof CategoryAdapter) {
            CategoryAdapter categoryAdapter = (CategoryAdapter) parent.getAdapter();
            WorkFlowCategory workFlowCategory = categoryAdapter.getItem(position);
            if (workFlowCategory != null && !workFlowCategory.getName().contains(getString(R.string.select))) {
                this.workflowcategory = workFlowCategory;
                calendar.add(Calendar.DATE, 7);
                txtFollowupdate.setText(CommonUtils.updateDate(calendar.getTime()));
            }
            return;
        } else if (parent.getAdapter() instanceof PriorityAdapter) {
            PriorityAdapter priorityAdapter = (PriorityAdapter) parent.getAdapter();
            WorkFlowPriority workFlowPriority = priorityAdapter.getItem(position);
            if (workFlowPriority != null && !workFlowPriority.getName().contains(getString(R.string.select)))
                this.workflowpriority = workFlowPriority;
            return;
        }
    }

    public void onWorkFlowSelected(int workFlowId) {
        Log.e("workflow_id",""+workFlowId);
        int workflow_id=workFlowId-1;
        new GetWorkFlowAssignedToAsyncTask(this, provideOaasDatabase(), workflow_id).execute();
        new GetWorkFlowStatusAsyncTask(this, provideOaasDatabase(), workflow_id).execute();
        new GetWorkFlowCategoryAsyncTask(this, provideOaasDatabase(), workflow_id).execute();
        new GetWorkFlowPriorityAsyncTask(this, provideOaasDatabase(), workflow_id).execute();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onComplete() {
        Toast.makeText(requireActivity(), "Work Flow Added Successfully.", Toast.LENGTH_SHORT).show();
        requireActivity().getSupportFragmentManager().popBackStack();
    }

    private WorkFlowHeader workFlowHeader;

    @Override
    public void onWorkFlowHeaderFetched(WorkFlowHeader workFlowHeader) {
        this.workFlowHeader = workFlowHeader;
        setViewWorkFlowHeader(workFlowHeader);
    }

    public void setViewWorkFlowHeader(WorkFlowHeader workFlowHeader) {
        txtMemberName.setText(taskListModel.getMemberName());
        txtFollowupdate.setText(taskListModel.getFollowUpdate() != null ? CommonUtils.getDateFromISO(taskListModel.getFollowUpdate()) : "--");
        edtComment.setText(workFlowHeader.getComments() != null ? workFlowHeader.getComments() : "");
        onWorkFlowSelected(workFlowHeader.getWorkflowid());
        calendar = CommonUtils.getCalender(workFlowHeader.getFollowupdate());
        new GetWorkFlowAsyncTask(this, provideOaasDatabase()).execute();
    }
}
