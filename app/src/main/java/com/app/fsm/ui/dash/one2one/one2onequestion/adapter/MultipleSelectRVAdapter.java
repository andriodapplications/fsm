package com.app.fsm.ui.dash.one2one.one2onequestion.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.fsm.R;
import com.app.fsm.ui.dash.one2one.one2onequestion.MultiSelectListener;
import com.app.fsm.ui.dash.one2one.one2onequestion.MultipleSelectionAnswerModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MultipleSelectRVAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<MultipleSelectionAnswerModel> multipleSelectionAnswerModelList;
    private MultiSelectListener multiSelectListener;

    public MultipleSelectRVAdapter(Context mContext, MultiSelectListener multiSelectListener, List<MultipleSelectionAnswerModel> multipleSelectionAnswerModelList) {
        this.multipleSelectionAnswerModelList = multipleSelectionAnswerModelList;
        this.multiSelectListener=multiSelectListener;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_spinner_multi_select_item, viewGroup, false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        if (viewHolder instanceof ItemViewHolder) {
            ItemViewHolder itemViewHolder = (ItemViewHolder) viewHolder;
            itemViewHolder.bindDataToVH(multipleSelectionAnswerModelList.get(i));
        }
    }

    @Override
    public int getItemCount() {
        return multipleSelectionAnswerModelList != null ? multipleSelectionAnswerModelList.size() : 0;
    }

    public List<MultipleSelectionAnswerModel> getList(){
        return this.multipleSelectionAnswerModelList;
    }

    /************************************{@ITEM_VIEW_HOLDER}***************************************/

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txt_title)
        TextView tvTitle;
        @BindView(R.id.iv_selection_state)
        ImageView ivSelectionSate;
        @BindView(R.id.rl_multiple_select_item_layout)
        RelativeLayout rlItemLayout;

        ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void bindDataToVH(final MultipleSelectionAnswerModel multipleSelectionAnswerModel) {
            if (multipleSelectionAnswerModel != null) {
                tvTitle.setText(multipleSelectionAnswerModel.getItemText());
                ivSelectionSate.setVisibility
                        (multipleSelectionAnswerModel.isSelected() ? View.VISIBLE : View.INVISIBLE);
            }
            rlItemLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (multipleSelectionAnswerModel != null) {
                        multipleSelectionAnswerModel.setSelected(!multipleSelectionAnswerModel.isSelected());
                    }
                    notifyDataSetChanged();

                }
            });
        }
    }
}
