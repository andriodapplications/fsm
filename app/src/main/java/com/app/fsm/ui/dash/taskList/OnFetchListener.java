package com.app.fsm.ui.dash.taskList;

import com.app.fsm.local_network.network.model.joinedtable.TaskListModel;
import com.app.fsm.local_network.network.model.joinedtable.TaskStatusModel;
import com.app.fsm.local_network.network.model.joinedtable.TaskTypeModel;

import java.util.List;

public interface OnFetchListener {

    void onComplete(List<TaskListModel> taskListModelList);

    void onTaskTypeList(List<TaskTypeModel> taskTypeModelList);

    void onTaskStatusFetched(List<TaskStatusModel> taskStatusModelList);
}
