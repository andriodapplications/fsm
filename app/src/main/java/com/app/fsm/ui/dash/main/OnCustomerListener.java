package com.app.fsm.ui.dash.main;

import com.app.fsm.local_network.network.model.customer.CustomerModel;

import java.util.List;

public interface OnCustomerListener {

    void onCustomerFetchCompleted(List<CustomerModel> customerModels);
}
