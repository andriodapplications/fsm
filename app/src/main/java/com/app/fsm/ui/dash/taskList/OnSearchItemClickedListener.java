package com.app.fsm.ui.dash.taskList;

import com.app.fsm.local_network.network.model.joinedtable.TaskListModel;

public interface OnSearchItemClickedListener {
    void provideTaskListModel(TaskListModel taskListModel);
}
