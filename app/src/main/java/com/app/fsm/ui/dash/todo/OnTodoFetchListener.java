package com.app.fsm.ui.dash.todo;

import com.app.fsm.local_network.network.model.todo.TodoLanguage;
import com.app.fsm.local_network.network.model.todo.TodoModel;
import com.app.fsm.local_network.network.model.todo.TodoStatusModel;
import com.app.fsm.local_network.network.model.usermodel.UserModel;
import com.app.fsm.ui.dash.main.OnMemberListener;
import com.app.fsm.ui.dash.one2one.one2onequestion.OnTodoTypeFetchedCallback;

import java.util.List;

public interface OnTodoFetchListener extends OnMemberListener, OnTodoTypeFetchedCallback {

    void assignToFetched(List<UserModel> userModelList);

    void todoStatusFetched(List<TodoStatusModel> todoStatusModelList);

    void onTodoLanguageFetched(List<TodoLanguage> todoLanguageList);

    void onTodoFetched(TodoModel todoModel);

    void onTodoUpdated();

    void onTodoCreated();

}
