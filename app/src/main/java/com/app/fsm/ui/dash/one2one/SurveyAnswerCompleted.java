package com.app.fsm.ui.dash.one2one;

public interface SurveyAnswerCompleted {
    void OnSurveyPosted(Long aLong, Integer backendid);
}
