package com.app.fsm.ui.dash.one2one;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.app.fsm.BaseActivity;
import com.app.fsm.R;
import com.budiyev.android.codescanner.AutoFocusMode;
import com.budiyev.android.codescanner.CodeScanner;
import com.budiyev.android.codescanner.CodeScannerView;
import com.budiyev.android.codescanner.DecodeCallback;
import com.budiyev.android.codescanner.ScanMode;
import com.google.zxing.Result;

public class ScanQRCode extends BaseActivity {

    private CodeScanner mCodeScanner;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan_qrcode);
        CodeScannerView scannerView = findViewById(R.id.scanner_view);
        mCodeScanner = new CodeScanner(this, scannerView);
        // Parameters (default values)
        mCodeScanner.setCamera(CodeScanner.CAMERA_BACK);  // or CAMERA_FRONT or specific camera id
        mCodeScanner.setFormats(CodeScanner.ALL_FORMATS) ;// list of type BarcodeFormat,
        // ex. listOf(BarcodeFormat.QR_CODE)
        mCodeScanner.setAutoFocusMode(AutoFocusMode.SAFE); // or CONTINUOUS
        mCodeScanner.setScanMode(ScanMode.SINGLE);// or CONTINUOUS or PREVIEW
        mCodeScanner.setDecodeCallback(new DecodeCallback() {
            @Override
            public void onDecoded( final Result result) {
                ScanQRCode.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        getUserSessionManager().setQRCode(result.getText());
//                        Toast.makeText(ScanQRCode.this, result.getText(), Toast.LENGTH_SHORT).show();
                        finish();
                    }
                });
            }
        });
        scannerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCodeScanner.startPreview();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        mCodeScanner.startPreview();
    }

    @Override
    protected void onPause() {
        mCodeScanner.releaseResources();
        super.onPause();
    }
}
