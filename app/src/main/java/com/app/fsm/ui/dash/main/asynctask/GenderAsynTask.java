package com.app.fsm.ui.dash.main.asynctask;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.app.fsm.local_network.network.model.gendermodel.GenderModel;
import com.app.fsm.local_network.repository.OaasDatabase;
import com.app.fsm.local_network.repository.OassDatabaseBuilder;
import com.app.fsm.local_network.repository.dao.GenderDao;

import java.lang.ref.WeakReference;
import java.util.List;

public final class GenderAsynTask extends AsyncTask<Void, Void, Boolean> {

    //Prevent leak
    private WeakReference<Context> weakActivity;
    private List<GenderModel> genderModelList;

    public GenderAsynTask(Context context, List<GenderModel> genderModelList) {
        weakActivity = new WeakReference<>(context);
        this.genderModelList = genderModelList;
    }

    @Override
    protected Boolean doInBackground(Void... params) {
        OaasDatabase oaasDatabase = OassDatabaseBuilder.provideOassDatabase(weakActivity.get());
        GenderDao genderDao = oaasDatabase.provideGenderDao();
        genderDao.insertAllGender(genderModelList);
        return true;
    }

    @Override
    protected void onPostExecute(Boolean agentsCount) {
        Context context = weakActivity.get();
        if (context == null) {
            return;
        }

        if (agentsCount) {
            Toast.makeText(context, "Done", Toast.LENGTH_SHORT).show();
        }
    }
}

