package com.app.fsm.ui.dash.main.asynctask;

import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.util.Log;

import com.app.fsm.local_network.network.ApiService;
import com.app.fsm.local_network.network.model.appconfig.AppConfigModel;
import com.app.fsm.local_network.network.model.category.CategoryModel;
import com.app.fsm.local_network.network.model.charts.ChartModel;
import com.app.fsm.local_network.network.model.customer.CustomerModel;
import com.app.fsm.local_network.network.model.educationmodel.EducationModel;
import com.app.fsm.local_network.network.model.gendermodel.GenderModel;
import com.app.fsm.local_network.network.model.member.MemberLanguageModel;
import com.app.fsm.local_network.network.model.member.MemberModel;
import com.app.fsm.local_network.network.model.occupationmodel.OccupationModel;
import com.app.fsm.local_network.network.model.one_one_question.QuestionTypeModel;
import com.app.fsm.local_network.network.model.organizationlevel.OrganizationalLevelModel;
import com.app.fsm.local_network.network.model.organizationlocation.OrganizationLocationModel;
import com.app.fsm.local_network.network.model.serviceevents.ServiceEventModel;
import com.app.fsm.local_network.network.model.subcategory.SubCategoryModel;
import com.app.fsm.local_network.network.model.survey.One2OneLanguage;
import com.app.fsm.local_network.network.model.survey.SurveyAnswerModel;
import com.app.fsm.local_network.network.model.survey.SurveyQuestionModel;
import com.app.fsm.local_network.network.model.todo.TodoLanguage;
import com.app.fsm.local_network.network.model.todo.TodoModel;
import com.app.fsm.local_network.network.model.todo.TodoStatusModel;
import com.app.fsm.local_network.network.model.todo.TodoType;
import com.app.fsm.local_network.network.model.updatetypes.UpdateTypeModel;
import com.app.fsm.local_network.network.model.usermodel.UserModel;
import com.app.fsm.local_network.network.model.workflow.WorkFlowCategory;
import com.app.fsm.local_network.network.model.workflow.WorkFlowHeader;
import com.app.fsm.local_network.network.model.workflow.WorkFlowLangauge;
import com.app.fsm.local_network.network.model.workflow.WorkFlowModel;
import com.app.fsm.local_network.network.model.workflow.WorkFlowPriority;
import com.app.fsm.local_network.network.model.workflow.WorkFlowStatus;
import com.app.fsm.local_network.repository.OaasDatabase;
import com.app.fsm.local_network.repository.dao.TodoDao;
import com.app.fsm.ui.dash.main.PullDataListener;
import com.app.fsm.utils.UserSessionManager;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

public class PullDataAsyncTask extends AsyncTask<Void, Void, Boolean> {
    private OaasDatabase oaasDatabase;
    private ApiService apiService;
    private UserSessionManager userSessionManager;
    private PullDataListener pullDataListener;

    public PullDataAsyncTask(@NonNull OaasDatabase oaasDatabase
            , @NonNull ApiService apiService
            , @NonNull UserSessionManager userSessionManager
            , @NonNull PullDataListener pullDataListener) {
        this.oaasDatabase = oaasDatabase;
        this.userSessionManager = userSessionManager;
        this.apiService = apiService;
        this.pullDataListener = pullDataListener;
    }

    @Override
    protected Boolean doInBackground(Void... voids) {
        try {
            getMembers();
            getGenders();
            getOccupationModel();
            getEducationModel();
            getOrganizationalLevel();
            getOrganizationLocation();
            getMemberLanguage();
            getQuestionType();
            getSurveyQuestion();
            getSurveyAnwer();
            getTodo();
            getTodoType();
            getOne2OneLanguage();
            getWorkFlowLanguage();
            getWorkFlow();
            getWorkFlowCategory();
            getWorkFlowHeader();
            getWorkFlowPriority();
            getWorkFlowStatus();
            getUserList();
            getTodoLangauge();
            getTodoStatus();
            getCustomers();
            getCategories();
            getSubCategories();
            getChartModelData();
            getAppconfigData();
            getUpdateType();
            getServiceEvent();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @Override
    protected void onPostExecute(Boolean aBoolean) {
        if (aBoolean) {
            pullDataListener.onSuccess();
        } else {
            pullDataListener.onError("Error occurred.Please try again");
        }

    }

    private void getMembers() throws IOException {

        Call<List<MemberModel>> call = apiService.getMembers(userSessionManager.getAccessToken(),Integer.valueOf(userSessionManager.getUser().getId()));
        Response<List<MemberModel>> response = call.execute();
        if (response.isSuccessful()) {
            List<MemberModel > memberModelList=response.body();
            oaasDatabase.provideMemberDao().insertAllMember(memberModelList);
        }

        List<MemberModel> memberModelList = oaasDatabase.provideMemberDao().getAll();
        for(MemberModel memberModel:memberModelList){
            Log.i("MemberModel PULL",""+memberModel.getSerno()+" : "+memberModel.getServerId());
        }
    }

    private void getGenders() throws IOException {
        Call<List<GenderModel>> call = apiService.getGender(userSessionManager.getAccessToken()
                , Integer.valueOf(userSessionManager.getUser().getLanguage()));
        Response<List<GenderModel>> response = call.execute();
        if (response.isSuccessful()) {
            oaasDatabase.provideGenderDao().insertAllGender(response.body());
        }
    }

    private void getOccupationModel() throws IOException {
        Call<List<OccupationModel>> call = apiService.getOccupationModel(userSessionManager.getAccessToken()
                , Integer.valueOf(userSessionManager.getUser().getLanguage()));
        Response<List<OccupationModel>> response = call.execute();
        if (response.isSuccessful())
            oaasDatabase.provideOccupationDao().insertAllOccupation(response.body());
    }

    private void getEducationModel() throws IOException {
        Call<List<EducationModel>> call = apiService.getEducationModel(userSessionManager.getAccessToken()
                , Integer.valueOf(userSessionManager.getUser().getLanguage()));
        Response<List<EducationModel>> response = call.execute();
        if (response.isSuccessful())
            oaasDatabase.provideEducationDao().insertAllEducation(response.body());
    }

    private void getOrganizationalLevel() throws IOException {
        Call<List<OrganizationalLevelModel>> call = apiService.getOrganizationalLevel(userSessionManager.getAccessToken());
        Response<List<OrganizationalLevelModel>> response = call.execute();
        if (response.isSuccessful())
            oaasDatabase.provideOrganizationLevelDao().insertAllOrganizationLevel(response.body());
    }

    private void getOrganizationLocation() throws IOException {
        Call<List<OrganizationLocationModel>> call = apiService.getOrganizationLocation(userSessionManager.getAccessToken());
        Response<List<OrganizationLocationModel>> response = call.execute();
        if (response.isSuccessful())
            oaasDatabase.provideOrganizationalLocationDao().insertAllOrganizationLocation(response.body());
    }

    private void getMemberLanguage() throws IOException {
        Call<List<MemberLanguageModel>> call = apiService.getMemberLanguage(userSessionManager.getAccessToken()
                , Integer.valueOf(userSessionManager.getUser().getLanguage()));
        Response<List<MemberLanguageModel>> response = call.execute();
        if (response.isSuccessful())
            oaasDatabase.provideMemberLanguageDao().insertAllMember(response.body());
    }

    private void getQuestionType() throws IOException {
        Call<List<QuestionTypeModel>> call = apiService.getQuestionType(userSessionManager.getAccessToken());
        Response<List<QuestionTypeModel>> response = call.execute();
        if (response.isSuccessful())
            oaasDatabase.provideQuestionTypeDao().insertAll(response.body());
    }

    private void getSurveyQuestion() throws IOException {
        Call<List<SurveyQuestionModel>> call = apiService.getSurveyQuestion(userSessionManager.getAccessToken());
        Response<List<SurveyQuestionModel>> response = call.execute();
        if (response.isSuccessful())
            oaasDatabase.provideSurveyQQuestiondao().insertAllSurveyQuestion(response.body());
    }

    private void getSurveyAnwer() throws IOException {
        Call<List<SurveyAnswerModel>> call = apiService.getSurveyAnswer(userSessionManager.getAccessToken());
        Response<List<SurveyAnswerModel>> response = call.execute();
        if (response.isSuccessful())
            oaasDatabase.provideSurveyAnswerDao().insertAllSurveyAnswer(response.body());
    }

    private void getTodo() throws IOException {
        Call<List<TodoModel>> call = apiService.getTodo(userSessionManager.getAccessToken());
        Response<List<TodoModel>> response = call.execute();
        if (response.isSuccessful()){
            TodoDao todoDao=oaasDatabase.provideTodoModelDao();
            todoDao.insertAllTodoModel(response.body());
        }
    }

    private void getTodoType() throws IOException {
        Call<List<TodoType>> call = apiService.getTodoType(userSessionManager.getAccessToken()
                , Integer.valueOf(userSessionManager.getUser().getLanguage()));
        Response<List<TodoType>> response = call.execute();
        if (response.isSuccessful())
            oaasDatabase.provideTodoTypeDao().insertAllTodoType(response.body());
    }

    private void getOne2OneLanguage() throws IOException {
        Call<List<One2OneLanguage>> call = apiService.getOneToOneQuestionLanguage(userSessionManager.getAccessToken()
                , Integer.valueOf(userSessionManager.getUser().getLanguage()));
        Response<List<One2OneLanguage>> response = call.execute();
        if (response.isSuccessful())
            oaasDatabase.provideSurveyQQuestiondao().insertOne2OneQuestionLanguage(response.body());
    }

    private void getWorkFlowLanguage() throws IOException {
        Call<List<WorkFlowLangauge>> call = apiService.getWorkFlowLangauge(userSessionManager.getAccessToken()
                , Integer.valueOf(userSessionManager.getUser().getLanguage()));
        Response<List<WorkFlowLangauge>> response = call.execute();
        if (response.isSuccessful())
            oaasDatabase.provideWorkFlowLanguageDao().insertAllWorkFlowLanguage(response.body());
    }

    private void getWorkFlowHeader() throws IOException {
        Call<List<WorkFlowHeader>> call = apiService.getWorkFlowHeader(userSessionManager.getAccessToken());
        Response<List<WorkFlowHeader>> response = call.execute();
        if (response.isSuccessful())
            oaasDatabase.provideWorkFlowHeaderDao().insertAllWorkFlowHeader(response.body());
    }

    private void getWorkFlowStatus() throws IOException {
        Call<List<WorkFlowStatus>> call = apiService.getWorkFlowStatus(userSessionManager.getAccessToken()
                , Integer.valueOf(userSessionManager.getUser().getLanguage()));
        Response<List<WorkFlowStatus>> response = call.execute();
        if (response.isSuccessful())
            oaasDatabase.provideWorkFlowStatusDao().insertAllStatus(response.body());
    }

    private void getWorkFlow() throws IOException {
        Call<List<WorkFlowModel>> call = apiService.getWorkFlows(userSessionManager.getAccessToken()
                , Integer.valueOf(userSessionManager.getUser().getLanguage()));
        Response<List<WorkFlowModel>> response = call.execute();
        if (response.isSuccessful())
            oaasDatabase.provideWorkFlowDao().insertAllWorkFlow(response.body());
    }

    private void getWorkFlowCategory() throws IOException {
        Call<List<WorkFlowCategory>> call = apiService.getWorkFlowCategory(userSessionManager.getAccessToken()
                , Integer.valueOf(userSessionManager.getUser().getLanguage()));
        Response<List<WorkFlowCategory>> response = call.execute();
        if (response.isSuccessful())
            oaasDatabase.provideWorkFlowCategoryDao().insertAllCategory(response.body());
    }

    private void getWorkFlowPriority() throws IOException {
        Call<List<WorkFlowPriority>> call = apiService.getWorkFlowPriority(userSessionManager.getAccessToken()
                , Integer.valueOf(userSessionManager.getUser().getLanguage()));
        Response<List<WorkFlowPriority>> response = call.execute();
        if (response.isSuccessful())
            oaasDatabase.provideWorkFlowPriorityDao().insertAllPriority(response.body());
    }

    private void getUserList() throws IOException {
        Call<List<UserModel>> call = apiService.getUserList(userSessionManager.getAccessToken());
        Response<List<UserModel>> response = call.execute();
        if (response.isSuccessful())
            oaasDatabase.provideUserDao().insertAll(response.body());
    }

    private void getTodoLangauge() throws IOException {
        Call<List<TodoLanguage>> call = apiService.getTodoLangauges(userSessionManager.getAccessToken()
                , Integer.valueOf(userSessionManager.getUser().getLanguage()));
        Response<List<TodoLanguage>> response = call.execute();
        if (response.isSuccessful())
            oaasDatabase.provideTodoModelDao().insertAllTodoLangauge(response.body());
    }

    private void getTodoStatus() throws IOException {
        Call<List<TodoStatusModel>> call = apiService.getTodoStatus(userSessionManager.getAccessToken()
                , Integer.valueOf(userSessionManager.getUser().getLanguage()));
        Response<List<TodoStatusModel>> response = call.execute();
        if (response.isSuccessful())
            oaasDatabase.provideTodoModelDao().insertAllTodoStatus(response.body());
    }

    private void getCustomers() throws IOException {
        Call<List<CustomerModel>> call = apiService.getCustomers(userSessionManager.getAccessToken());
        Response<List<CustomerModel>> response = call.execute();
        if (response.isSuccessful())
            oaasDatabase.provideCustomersDao().insertAllCustomers(response.body());
    }

    private void getCategories() throws IOException {
        Call<List<CategoryModel>> call = apiService.getCategories(userSessionManager.getAccessToken());
        Response<List<CategoryModel>> response = call.execute();
        if (response.isSuccessful())
            oaasDatabase.provideCategoryDao().insertAllCategories(response.body());
    }

    private void getSubCategories() throws IOException {
        Call<List<SubCategoryModel>> call = apiService.getSubCategories(userSessionManager.getAccessToken());
        Response<List<SubCategoryModel>> response = call.execute();
        if (response.isSuccessful())
            oaasDatabase.provideSubCategoryDao().insertAllSubCategories(response.body());
    }

    private void getChartModelData() throws IOException {
        Call<List<ChartModel>> call = apiService.getchartdata(userSessionManager.getAccessToken(),
                Integer.valueOf(userSessionManager.getUserId()));
        Response<List<ChartModel>> response = call.execute();
        if (response.isSuccessful())
            oaasDatabase.provideChartModelDao().insertChartData(response.body());
    }

    private void getAppconfigData() throws IOException {
        Call<List<AppConfigModel>> call = apiService.getAppConfigdata(userSessionManager.getAccessToken(),
                Integer.valueOf(6001));
        Response<List<AppConfigModel>> response = call.execute();
        if (response.isSuccessful())
            oaasDatabase.provideAppconfigDao().insertAppconfigData(response.body());
    }

    private void getServiceEvent() throws IOException {
        Call<List<ServiceEventModel>> call = apiService.getServiceEvent(userSessionManager.getAccessToken());
        Response<List<ServiceEventModel>> response = call.execute();
        if (response.isSuccessful())
            oaasDatabase.provideserviceEventModelDao().insertServiceEvents(response.body());
    }

    private void getUpdateType() throws IOException {
        Call<List<UpdateTypeModel>> call = apiService.getUpdateType(userSessionManager.getAccessToken());
        Response<List<UpdateTypeModel>> response = call.execute();
        if (response.isSuccessful())
            oaasDatabase.provideupdateTypeModelDao().insertUpdateEvents(response.body());
    }


}
