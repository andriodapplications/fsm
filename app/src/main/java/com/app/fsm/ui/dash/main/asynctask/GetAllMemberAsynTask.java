package com.app.fsm.ui.dash.main.asynctask;

import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.app.fsm.local_network.network.model.member.MemberModel;
import com.app.fsm.local_network.repository.OaasDatabase;
import com.app.fsm.local_network.repository.dao.MemberDao;
import com.app.fsm.ui.dash.main.OnMemberListener;

import java.util.List;

public final class GetAllMemberAsynTask extends AsyncTask<Void, Void, List<MemberModel>> {

    private OaasDatabase oaasDatabase;
    private OnMemberListener onMemberListener;
    private int pos;//pos ==0 is for editing the member.

    public GetAllMemberAsynTask(@NonNull OnMemberListener onMemberListener, @NonNull OaasDatabase oaasDatabase, int pos) {
        this.oaasDatabase = oaasDatabase;
        this.onMemberListener = onMemberListener;
        this.pos=pos;
    }

    @Override
    protected List<MemberModel> doInBackground(Void... params) {
        MemberDao memberDao = oaasDatabase.provideMemberDao();
        return memberDao.getAll();
    }

    @Override
    protected void onPostExecute(List<MemberModel> memberModelList) {
        if (onMemberListener != null)
            onMemberListener.onMemberFetchCompleted(memberModelList, pos);
    }
}

