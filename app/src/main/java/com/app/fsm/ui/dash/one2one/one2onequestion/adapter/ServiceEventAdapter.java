/*
 * Developed by Avinash Kumar singh on 24/1/19 3:49 PM
 * Last Modified 21/1/19 8:26 PM.
 *
 * Copyright (c) 2019.  All rights reserved.
 */

package com.app.fsm.ui.dash.one2one.one2onequestion.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.app.fsm.R;
import com.app.fsm.local_network.network.model.serviceevents.ServiceEventModel;

import java.util.List;

public class ServiceEventAdapter extends BaseAdapter {

    private LayoutInflater layoutInflater;
    private List<ServiceEventModel> serviceEventModels;

    public ServiceEventAdapter(@NonNull Context context, @NonNull List<ServiceEventModel> serviceEventModels) {
        this.serviceEventModels = serviceEventModels;
        this.layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return serviceEventModels.size()+1;
    }


    @Nullable
    @Override
    public Object getItem(int position) {
        return position == 0 ?  null : serviceEventModels.get(position - 1);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ItemVH itemVH;
        if (convertView != null) {
            itemVH = (ItemVH) convertView.getTag();
        } else {
            convertView = layoutInflater.inflate(R.layout.row_spinner_list, parent,false);
            itemVH = new ItemVH();
            itemVH.tvTitle=convertView.findViewById(R.id.txt_title);
            convertView.setTag(itemVH);
        }
        ServiceEventModel serviceEventModel = (ServiceEventModel) getItem(position);
        if (serviceEventModel != null) {
            itemVH.tvTitle.setText(serviceEventModel.getName());
        }else {
            itemVH.tvTitle.setText("Select ServiceEvent");
        }

        return convertView;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        ItemVH itemVH;
        if (convertView != null) {
            itemVH = (ItemVH) convertView.getTag();
        } else {
            convertView = layoutInflater.inflate(R.layout.row_spinner_list, parent,false);
            itemVH = new ItemVH();
            itemVH.tvTitle=convertView.findViewById(R.id.txt_title);
            convertView.setTag(itemVH);
        }
        ServiceEventModel serviceEventModel = (ServiceEventModel) getItem(position);
        if (serviceEventModel != null) {
            itemVH.tvTitle.setText(serviceEventModel.getName());
        }else {
            itemVH.tvTitle.setText("Select ServiceEvent");
        }
        return convertView;
    }

    static class ItemVH {
        TextView tvTitle;
    }
}