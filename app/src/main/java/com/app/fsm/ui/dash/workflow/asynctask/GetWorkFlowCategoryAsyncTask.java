package com.app.fsm.ui.dash.workflow.asynctask;

import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.app.fsm.local_network.network.model.workflow.WorkFlowCategory;
import com.app.fsm.local_network.repository.OaasDatabase;
import com.app.fsm.local_network.repository.dao.WorkFlowCategoryDao;
import com.app.fsm.ui.dash.workflow.OnWorkFlowFetchListener;

import java.util.List;

public final class GetWorkFlowCategoryAsyncTask extends AsyncTask<Void, Void, List<WorkFlowCategory>> {

    private OaasDatabase oaasDatabase;
    private OnWorkFlowFetchListener onWorkFlowFetchListener;
    private int workFlowId;

    public GetWorkFlowCategoryAsyncTask(@NonNull OnWorkFlowFetchListener onWorkFlowFetchListener, @NonNull OaasDatabase oaasDatabase
            , int workFlowId) {
        this.oaasDatabase = oaasDatabase;
        this.onWorkFlowFetchListener = onWorkFlowFetchListener;
        this.workFlowId = workFlowId;
    }

    @Override
    protected List<WorkFlowCategory> doInBackground(Void... params) {
        WorkFlowCategoryDao workFlowCategoryDao = oaasDatabase.provideWorkFlowCategoryDao();
        return workFlowCategoryDao.provideWorkFlowCategory(workFlowId);
    }

    @Override
    protected void onPostExecute(List<WorkFlowCategory> workFlowCategoryList) {
        if (onWorkFlowFetchListener != null) {
            onWorkFlowFetchListener.onWorkFlowCategoryFetched(workFlowCategoryList);
        }
    }
}

