package com.app.fsm.ui.document;

import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.app.fsm.local_network.network.model.survey.SurveyQuestionModel;
import com.app.fsm.local_network.repository.OaasDatabase;
import com.app.fsm.local_network.repository.dao.SurveyQuestionDao;
import com.app.fsm.ui.dash.one2one.OnFetchComplete;

import java.util.ArrayList;
import java.util.List;

public final class GetAllDocumentSurveyQuestionAsynTask extends AsyncTask<Void, Void, List<SurveyQuestionModel>> {

    private OaasDatabase oaasDatabase;
    private OnFetchComplete onFetchComplete;

    public GetAllDocumentSurveyQuestionAsynTask(@NonNull OnFetchComplete onFetchComplete,
                                                @NonNull OaasDatabase oaasDatabase) {
        this.oaasDatabase = oaasDatabase;
        this.onFetchComplete = onFetchComplete;
    }

    @Override
    protected List<SurveyQuestionModel> doInBackground(Void... params) {
        SurveyQuestionDao surveyQuestionDao= oaasDatabase.provideSurveyQQuestiondao();
        return surveyQuestionDao.provideSurveyQuestion();
    }

    @Override
    protected void onPostExecute(List<SurveyQuestionModel> surveyQuestionModelList) {

        List<SurveyQuestionModel> surveyQuestionModelList1 = new ArrayList<>();
        for (SurveyQuestionModel surveyQuestionModel : surveyQuestionModelList) {
            if (surveyQuestionModel.getRcCode() != null)
                if(surveyQuestionModel.getRcCode()==11 || surveyQuestionModel.getRcCode()==16){
                    surveyQuestionModelList1.add(surveyQuestionModel);
                }
        }

        if(onFetchComplete !=null)
            onFetchComplete.onComplete(surveyQuestionModelList1);
    }
}

