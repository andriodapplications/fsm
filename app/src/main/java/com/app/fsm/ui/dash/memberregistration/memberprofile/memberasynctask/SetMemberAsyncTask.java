package com.app.fsm.ui.dash.memberregistration.memberprofile.memberasynctask;

import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.app.fsm.local_network.network.model.member.MemberModel;
import com.app.fsm.local_network.repository.OaasDatabase;
import com.app.fsm.local_network.repository.dao.MemberDao;
import com.app.fsm.ui.OnCompleteListener;

public final class SetMemberAsyncTask extends AsyncTask<Void, Void, Long> {

    private OaasDatabase oaasDatabase;
    private MemberModel memberModel;
    private OnCompleteListener onCompleteListener;

    public SetMemberAsyncTask(@NonNull OnCompleteListener onCompleteListener, @NonNull OaasDatabase oaasDatabase
            , MemberModel memberModel) {
        this.onCompleteListener=onCompleteListener;
        this.oaasDatabase = oaasDatabase;
        this.memberModel = memberModel;
    }

    @Override
    protected Long doInBackground(Void... params) {
        MemberDao memberDao = oaasDatabase.provideMemberDao();

        if (memberDao.getCount() == 0) {
            memberModel.setServerId(System.currentTimeMillis());
        } else {
            MemberModel lastMemberModel = memberDao.getMember();
            memberModel.setServerId(System.currentTimeMillis());
        }
        return memberDao.insertMember(memberModel);
    }

    @Override
    protected void onPostExecute(Long longValue) {
        if(onCompleteListener!=null)
            onCompleteListener.onComplete();
    }
}

