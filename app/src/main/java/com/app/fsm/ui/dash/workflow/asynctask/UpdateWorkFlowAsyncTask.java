package com.app.fsm.ui.dash.workflow.asynctask;

import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.app.fsm.local_network.network.model.workflow.WorkFlowHeader;
import com.app.fsm.local_network.repository.OaasDatabase;
import com.app.fsm.local_network.repository.dao.WorkFlowHeaderDao;
import com.app.fsm.ui.dash.workflow.OnWorkFlowFetchListener;

public final class UpdateWorkFlowAsyncTask extends AsyncTask<Void, Void, Integer> {

    private OaasDatabase oaasDatabase;
    private WorkFlowHeader workFlowHeader;
    private OnWorkFlowFetchListener onWorkFlowFetchListener;

    public UpdateWorkFlowAsyncTask(@NonNull OnWorkFlowFetchListener onWorkFlowFetchListener, @NonNull OaasDatabase oaasDatabase
            , WorkFlowHeader workFlowHeader) {
        this.onWorkFlowFetchListener=onWorkFlowFetchListener;
        this.oaasDatabase = oaasDatabase;
        this.workFlowHeader = workFlowHeader;
    }

    @Override
    protected Integer doInBackground(Void... params) {
        WorkFlowHeaderDao workFlowHeaderDao= oaasDatabase.provideWorkFlowHeaderDao();
        return workFlowHeaderDao.updateWorkFlowHeader(workFlowHeader);
    }

    @Override
    protected void onPostExecute(Integer longValue) {
        if(onWorkFlowFetchListener!=null)
            onWorkFlowFetchListener.onUpdate();
    }
}

