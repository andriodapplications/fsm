package com.app.fsm.ui.dash.main.asynctask;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.app.fsm.local_network.network.model.organizationlevel.OrganizationalLevelModel;
import com.app.fsm.local_network.repository.OaasDatabase;
import com.app.fsm.local_network.repository.OassDatabaseBuilder;
import com.app.fsm.local_network.repository.dao.OrganizationalLevelDao;

import java.lang.ref.WeakReference;
import java.util.List;

public final class OrganizationLevelAsynTask extends AsyncTask<Void, Void, Boolean> {

    //Prevent leak
    private WeakReference<Context> weakActivity;
    private List<OrganizationalLevelModel> organizationalLevelModelList;

    public OrganizationLevelAsynTask(Context context, List<OrganizationalLevelModel> organizationalLevelModelList) {
        weakActivity = new WeakReference<>(context);
        this.organizationalLevelModelList = organizationalLevelModelList;
    }

    @Override
    protected Boolean doInBackground(Void... params) {
        OaasDatabase oaasDatabase = OassDatabaseBuilder.provideOassDatabase(weakActivity.get());
        OrganizationalLevelDao organizationalLevelDao = oaasDatabase.provideOrganizationLevelDao();
        organizationalLevelDao.insertAllOrganizationLevel(organizationalLevelModelList);
        return true;
    }

    @Override
    protected void onPostExecute(Boolean agentsCount) {
        Context context = weakActivity.get();
        if (context == null) {
            return;
        }
        if (agentsCount) {
            Toast.makeText(context, "Done", Toast.LENGTH_SHORT).show();
        }
    }
}

