package com.app.fsm.ui.done;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.fsm.R;
import com.app.fsm.local_network.network.model.member.MemberModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kanthi on 06/07/2020.
 */

public class Donerv_Adapter extends RecyclerView.Adapter<Donerv_Adapter.ViewHolder> {

    private List<MemberModel> mAdditem;
    private Context mContext;

    public Donerv_Adapter(Context context) {
        mAdditem=new ArrayList<>();
        mContext = context;
    }

    public void update(List<MemberModel> stringList) {
        mAdditem.clear();
        mAdditem.addAll(stringList);
        notifyDataSetChanged();
    }

    @Override
    public Donerv_Adapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_memberdone,
                parent, false);
        mContext = parent.getContext();
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        holder.tv_rccode.setText(mAdditem.get(position).getRcCode()==null?"":""+mAdditem.get(position).getRcCode());
        holder.tv_dispatchno.setText(mAdditem.get(position).getDispatchno()==null?"":mAdditem.get(position).getDispatchno());
    }

    @Override
    public int getItemCount() {
        return mAdditem.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView tv_dispatchno;

        public TextView tv_rccode;

        public ViewHolder(View view) {
            super(view);
            tv_dispatchno=view.findViewById(R.id.tv_dispatchno);
            tv_rccode=view.findViewById(R.id.tv_rccode);
        }
    }
}

