package com.app.fsm.ui.dash.one2one.one2oneasynctask;

import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.app.fsm.local_network.network.model.survey.SurveyAnswerModel;
import com.app.fsm.local_network.repository.OaasDatabase;
import com.app.fsm.local_network.repository.dao.SurveyAnswerDao;
import com.app.fsm.ui.OnCompleteListener;

public final class UpdateSurveyAnswersAsyncTask extends AsyncTask<Void, Void, Integer> {

    private OaasDatabase oaasDatabase;
    private SurveyAnswerModel answerModel;
    private OnCompleteListener onCompleteListener;

    public UpdateSurveyAnswersAsyncTask(@NonNull OnCompleteListener onCompleteListener, @NonNull OaasDatabase oaasDatabase
            , SurveyAnswerModel answerModel) {
        this.onCompleteListener=onCompleteListener;
        this.oaasDatabase = oaasDatabase;
        this.answerModel = answerModel;
    }

    @Override
    protected Integer doInBackground(Void... params) {
        SurveyAnswerDao answerDao = oaasDatabase.provideSurveyAnswerDao();
        return answerDao.updateSurveyAnswer(answerModel);
    }

    @Override
    protected void onPostExecute(Integer longValue) {
        if(onCompleteListener!=null){
            onCompleteListener.onUpdate();
        }
    }
}

