package com.app.fsm.ui.dash.main.asynctask;

import android.content.Context;
import android.os.AsyncTask;

import com.app.fsm.local_network.network.model.todo.TodoLanguage;
import com.app.fsm.local_network.repository.OaasDatabase;
import com.app.fsm.local_network.repository.OassDatabaseBuilder;
import com.app.fsm.local_network.repository.dao.TodoDao;

import java.lang.ref.WeakReference;
import java.util.List;

public final class SetTodoLangaugeAsyncTask extends AsyncTask<Void, Void, Boolean> {

    //Prevent leak
    private WeakReference<Context> weakActivity;
    private List<TodoLanguage> todoLanguageList;

    public SetTodoLangaugeAsyncTask(Context context, List<TodoLanguage> todoLanguageList) {
        weakActivity = new WeakReference<>(context);
        this.todoLanguageList = todoLanguageList;
    }

    @Override
    protected Boolean doInBackground(Void... params) {
        OaasDatabase oaasDatabase = OassDatabaseBuilder.provideOassDatabase(weakActivity.get());
        TodoDao todoDao = oaasDatabase.provideTodoModelDao();
        todoDao.insertAllTodoLangauge(todoLanguageList);
        return true;
    }

    @Override
    protected void onPostExecute(Boolean isInserted) {

    }
}

