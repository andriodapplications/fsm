package com.app.fsm.ui.dash.one2one.one2onequestion;

import com.app.fsm.local_network.network.model.todo.TodoType;

import java.util.List;

public interface OnTodoTypeFetchedCallback {
    void onTodoTypeFetchComplete(List<TodoType> todoTypeList);
}
