package com.app.fsm.ui.dash.main.asynctask;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.app.fsm.local_network.network.model.workflow.WorkFlowCategory;
import com.app.fsm.local_network.repository.OaasDatabase;
import com.app.fsm.local_network.repository.OassDatabaseBuilder;
import com.app.fsm.local_network.repository.dao.WorkFlowCategoryDao;

import java.lang.ref.WeakReference;
import java.util.List;

public final class SetWorkFlowCategoryAsyncTask extends AsyncTask<Void, Void, Boolean> {

    //Prevent leak
    private WeakReference<Context> weakActivity;
    private List<WorkFlowCategory> workFlowCategoryList;

    public SetWorkFlowCategoryAsyncTask(Context context, List<WorkFlowCategory> workFlowCategoryList) {
        weakActivity = new WeakReference<>(context);
        this.workFlowCategoryList = workFlowCategoryList;
    }

    @Override
    protected Boolean doInBackground(Void... params) {
        OaasDatabase oaasDatabase = OassDatabaseBuilder.provideOassDatabase(weakActivity.get());
        WorkFlowCategoryDao workFlowCategoryDao= oaasDatabase.provideWorkFlowCategoryDao();
        workFlowCategoryDao.insertAllCategory(workFlowCategoryList);
        return true;
    }

    @Override
    protected void onPostExecute(Boolean isInserted) {
        if (isInserted) {
            Toast.makeText(weakActivity.get(), "Done", Toast.LENGTH_SHORT).show();
        }
    }
}

