package com.app.fsm.ui.dash.main;

import com.app.fsm.local_network.network.model.category.CategoryModel;

import java.util.List;

public interface OnCategoryListner {
    void onCategoryFetchCompleted(List<CategoryModel> categoryModels);
}
