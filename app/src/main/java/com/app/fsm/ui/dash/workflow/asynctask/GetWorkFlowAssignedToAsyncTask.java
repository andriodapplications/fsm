package com.app.fsm.ui.dash.workflow.asynctask;

import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.app.fsm.local_network.network.model.usermodel.UserModel;
import com.app.fsm.local_network.repository.OaasDatabase;
import com.app.fsm.local_network.repository.dao.UserDao;
import com.app.fsm.ui.dash.workflow.OnWorkFlowFetchListener;

import java.util.List;

public final class GetWorkFlowAssignedToAsyncTask extends AsyncTask<Void, Void, List<UserModel>> {

    private OaasDatabase oaasDatabase;
    private OnWorkFlowFetchListener onWorkFlowFetchListener;
    private int workFlowId;

    public GetWorkFlowAssignedToAsyncTask(@NonNull OnWorkFlowFetchListener onWorkFlowFetchListener, @NonNull OaasDatabase oaasDatabase
            , int workFlowId) {
        this.oaasDatabase = oaasDatabase;
        this.onWorkFlowFetchListener = onWorkFlowFetchListener;
        this.workFlowId = workFlowId;
    }

    @Override
    protected List<UserModel> doInBackground(Void... params) {
        UserDao userDao = oaasDatabase.provideUserDao();
        return userDao.getAll();
    }

    @Override
    protected void onPostExecute(List<UserModel> userModelList) {
        if (onWorkFlowFetchListener != null) {
            onWorkFlowFetchListener.onWorkFlowAssignedToFetched(userModelList);
        }
    }
}

