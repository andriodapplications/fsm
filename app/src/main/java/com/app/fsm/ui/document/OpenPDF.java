package com.app.fsm.ui.document;

import com.app.fsm.local_network.network.model.survey.SurveyAnswerModel;

public interface OpenPDF {
    void onclickpdf(SurveyAnswerModel surveyAnswerModel);
}
