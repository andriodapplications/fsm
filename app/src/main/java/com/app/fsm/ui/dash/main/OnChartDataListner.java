package com.app.fsm.ui.dash.main;

import com.app.fsm.local_network.network.model.charts.ChartModel;
import java.util.List;

public interface OnChartDataListner {
    void onChartFetchCompleted(List<ChartModel> chartModels);
}
