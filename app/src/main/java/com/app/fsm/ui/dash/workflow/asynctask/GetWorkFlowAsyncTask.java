package com.app.fsm.ui.dash.workflow.asynctask;

import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.app.fsm.local_network.network.model.workflow.WorkFlowModel;
import com.app.fsm.local_network.repository.OaasDatabase;
import com.app.fsm.local_network.repository.dao.WorkFlowDao;
import com.app.fsm.ui.dash.workflow.OnWorkFlowFetchListener;

import java.util.List;

public final class GetWorkFlowAsyncTask extends AsyncTask<Void, Void, List<WorkFlowModel>> {

    private OaasDatabase oaasDatabase;
    private OnWorkFlowFetchListener onWorkFlowFetchListener;

    public GetWorkFlowAsyncTask(@NonNull OnWorkFlowFetchListener onWorkFlowFetchListener, @NonNull OaasDatabase oaasDatabase) {
        this.oaasDatabase = oaasDatabase;
        this.onWorkFlowFetchListener = onWorkFlowFetchListener;
    }

    @Override
    protected List<WorkFlowModel> doInBackground(Void... params) {
        WorkFlowDao  workFlowDao= oaasDatabase.provideWorkFlowDao();
        return workFlowDao.getAllWorkFlowModel();
    }

    @Override
    protected void onPostExecute(List<WorkFlowModel> workFlowModelList) {
        if (onWorkFlowFetchListener != null) {
            onWorkFlowFetchListener.onWorkFlowFetched(workFlowModelList);
        }
    }
}

