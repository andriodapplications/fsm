package com.app.fsm.ui.dash.one2one.one2oneasynctask;

import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.app.fsm.local_network.network.model.todo.TodoType;
import com.app.fsm.local_network.repository.OaasDatabase;
import com.app.fsm.local_network.repository.dao.TodoTypeDao;
import com.app.fsm.ui.dash.one2one.one2onequestion.OnTodoTypeFetchedCallback;

import java.util.List;

public final class GetAllTodoTypeAsyncTask extends AsyncTask<Void, Void, List<TodoType>> {

    private OaasDatabase oaasDatabase;
    private OnTodoTypeFetchedCallback onFetchComplete;

    public GetAllTodoTypeAsyncTask(@NonNull OnTodoTypeFetchedCallback onFetchComplete, @NonNull OaasDatabase oaasDatabase) {
        this.oaasDatabase = oaasDatabase;
        this.onFetchComplete = onFetchComplete;
    }

    @Override
    protected List<TodoType> doInBackground(Void... params) {
        TodoTypeDao todoTypeDao= oaasDatabase.provideTodoTypeDao();
        return todoTypeDao.provideTodoType();
    }

    @Override
    protected void onPostExecute(List<TodoType> todoTypeList) {
        if(onFetchComplete !=null)
            onFetchComplete.onTodoTypeFetchComplete(todoTypeList);
    }
}

