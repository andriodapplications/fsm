package com.app.fsm.ui.dash.addticket;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.fsm.R;
import com.app.fsm.local_network.network.model.member.MemberModel;
import com.app.fsm.local_network.network.model.organizationlocation.OrganizationLocationModel;
import com.app.fsm.local_network.network.model.survey.SurveyQuestionModel;
import com.app.fsm.local_network.network.model.todo.TodoType;
import com.app.fsm.local_network.repository.OaasDatabase;
import com.app.fsm.local_network.repository.OassDatabaseBuilder;
import com.app.fsm.ui.OnCompleteListener;
import com.app.fsm.ui.dash.main.OnCategoryListner;
import com.app.fsm.ui.dash.main.OnOrganizationLocationListener;
import com.app.fsm.ui.dash.main.asynctask.OrganizationLocationAsynTask;
import com.app.fsm.ui.dash.memberregistration.memberprofile.memberasynctask.SetMemberAsyncTask;
import com.app.fsm.ui.dash.one2one.one2onequestion.adapter.OptionAdapter;
import com.app.fsm.ui.dash.one2one.one2onequestion.adapter.ServiceEventAdapter;
import com.app.fsm.ui.dash.one2one.one2onequestion.adapter.TodoAdapter;
import com.app.fsm.ui.dash.one2one.one2onequestion.adapter.UpdateTypeAdapter;
import com.app.fsm.utils.GPSTracker;
import com.app.fsm.utils.UserSessionManager;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.lang.reflect.Array;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AddTicketActivity extends AppCompatActivity implements OnCompleteListener, OnOrganizationLocationListener, AdapterView.OnItemSelectedListener {


    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.title)
    TextView txtTitle;
    @BindView(R.id.rl_startdate)
    RelativeLayout rlStDate;
    @BindView(R.id.rl_enddate)
    RelativeLayout rlEndDate;
    @BindView(R.id.edt_dispatchno)
    AppCompatEditText edtDispatchNo;
    @BindView(R.id.edt_customer_number)
    AppCompatEditText edtCustomerNumber;
    @BindView(R.id.edt_customer_name)
    AppCompatEditText edtCustomerName;
    @BindView(R.id.edt_customer_contact_name)
    AppCompatEditText edtCustomerContactName;
    @BindView(R.id.edt_customer_contact_phone)
    AppCompatEditText edtCustomerContactPhone;
    @BindView(R.id.edt_customer_contact_email)
    AppCompatEditText edtCustomerContactEmail;
    @BindView(R.id.edt_customer_secondary_contact_name)
    AppCompatEditText edtCustomerSecondaryContactName;
    @BindView(R.id.edt_customer_secondary_contact_phone)
    AppCompatEditText edtCustomerSecondaryContactPhone;
    @BindView(R.id.spinner_state)
    AppCompatSpinner sp_state;
    @BindView(R.id.spinner_city)
    AppCompatSpinner sp_city;
    @BindView(R.id.txt_header_city)
    TextView txtHeaderCity;


    @BindView(R.id.edt_service_address_line_1)
    AppCompatEditText edtServiceAddressLine1;
    @BindView(R.id.edt_service_address_line_2)
    AppCompatEditText edtServiceAddressLine2;
    @BindView(R.id.edt_service_address_line_3)
    AppCompatEditText edtServiceAddressLine3;
    @BindView(R.id.edt_service_address_line_4)
    AppCompatEditText edtServiceAddressLine4;
    @BindView(R.id.img_done)
    ImageView imgCreate;

    private OaasDatabase oaasDatabase;
    private UserSessionManager userSessionManager;
    String stringLatitude,stringLongitude;

    private List<OrganizationLocationModel> stateModelList;
    private List<OrganizationLocationModel> cityModelList;

    StateAdapter stateAdapter;
    CityAdapter cityAdapter;

    int country=0,state=0,city=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_ticket);
        ButterKnife.bind(this);
        txtTitle.setText(getString(R.string.add_ticket));
        toolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        oaasDatabase = OassDatabaseBuilder.provideOassDatabase(this);

        // check if GPS enabled
        GPSTracker gpsTracker = new GPSTracker(this);

        if (gpsTracker.getIsGPSTrackingEnabled())
        {
            stringLatitude = String.valueOf(gpsTracker.latitude);
            stringLongitude = String.valueOf(gpsTracker.longitude);

            String addressLine = gpsTracker.getAddressLine(this);
            edtServiceAddressLine1.setMovementMethod(new ScrollingMovementMethod());
            edtServiceAddressLine1.setText(addressLine);
            edtServiceAddressLine1.setOnTouchListener(new View.OnTouchListener() {

                public boolean onTouch(View v, MotionEvent event) {
                    if (edtServiceAddressLine1.hasFocus()) {
                        v.getParent().requestDisallowInterceptTouchEvent(true);
                        switch (event.getAction() & MotionEvent.ACTION_MASK){
                            case MotionEvent.ACTION_SCROLL:
                                v.getParent().requestDisallowInterceptTouchEvent(false);
                                return true;
                        }
                    }
                    return false;
                }
            });
        }
        else
        {
            // can't get location
            // GPS or Network is not enabled
            // Ask user to enable GPS/network in settings
            gpsTracker.showSettingsAlert();
        }

        init();
    }

    public OaasDatabase provideOaasDatabase(){
        return OassDatabaseBuilder.provideOassDatabase(this);
    }

    private void init() {

        stateModelList=new ArrayList<>();
        cityModelList=new ArrayList<>();

        String orgStructure = UserSessionManager.getUserSessionManager().getUser().getOrgStructure();
        String[] orgStructureArray = orgStructure.split(";");
        String str_state="",str_city="";
        if(orgStructureArray.length>0){
            country = Integer.parseInt(orgStructureArray[0].split("-")[1]);
        }

        if(orgStructureArray.length>1){
            state = Integer.parseInt(orgStructureArray[1].split("-")[1].split(",")[0]);
            str_state = (orgStructureArray[1].split("-")[1]);
        }

        if(orgStructureArray.length>2){
            city = Integer.parseInt(orgStructureArray[2].split("-")[1].split(",")[0]);
            str_city = (orgStructureArray[2].split("-")[1]);
        }
        Log.e("statecity",state+","+city);
        new OrganizationLocationAsynTask(this, provideOaasDatabase(),new ArrayList<String>(Arrays.asList((str_state+","+str_city).split(",")))).execute();


//        rlStDate.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                // Get Current Date
//                final Calendar c = Calendar.getInstance();
//                int mYear = c.get(Calendar.YEAR);
//                int mMonth = c.get(Calendar.MONTH);
//                int mDay = c.get(Calendar.DAY_OF_MONTH);
//
//                DatePickerDialog datePickerDialog = new DatePickerDialog(AddTicketActivity.this,
//                        new DatePickerDialog.OnDateSetListener() {
//                            @Override
//                            public void onDateSet(DatePicker view, int year,
//                                                  int monthOfYear, int dayOfMonth) {
//                                txtCustomerNumber.setText(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);
//                            }
//                        }, mYear, mMonth, mDay);
//                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
//                datePickerDialog.show();
//            }
//        });

//        rlEndDate.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                // Get Current Date
//                final Calendar c = Calendar.getInstance();
//                int mYear = c.get(Calendar.YEAR);
//                int mMonth = c.get(Calendar.MONTH);
//                int mDay = c.get(Calendar.DAY_OF_MONTH);
//                DatePickerDialog datePickerDialog = new DatePickerDialog(AddTicketActivity.this,
//                        new DatePickerDialog.OnDateSetListener() {
//                            @Override
//                            public void onDateSet(DatePicker view, int year,
//                                                  int monthOfYear, int dayOfMonth) {
//                                txtCustomerName.setText(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);
//                            }
//                        }, mYear, mMonth, mDay);
//                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
//                datePickerDialog.show();
//            }
//        });

        imgCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(edtDispatchNo.getText())) {
                    Toast.makeText(AddTicketActivity.this, "Please enter DispatchNo", Toast.LENGTH_SHORT).show();
                }

//                else if (TextUtils.isEmpty(edtCustomerNumber.getText())) {
//                    Toast.makeText(AddTicketActivity.this, "Please enter Customer Number", Toast.LENGTH_SHORT).show();
//                } else if (TextUtils.isEmpty(edtCustomerName.getText())) {
//                    Toast.makeText(AddTicketActivity.this, "Please enter Customer Name", Toast.LENGTH_SHORT).show();
//                }
//                else if (TextUtils.isEmpty(edtCustomerContactName.getText())) {
//                    Toast.makeText(AddTicketActivity.this, "Please enter Customer Contact Name", Toast.LENGTH_SHORT).show();
//                }
//                else if (TextUtils.isEmpty(edtServiceAddressLine1.getText())) {
//                    Toast.makeText(AddTicketActivity.this, "Please enter Service Address Line 1", Toast.LENGTH_SHORT).show();
//                }
//                else if (TextUtils.isEmpty(edtCustomerContactEmail.getText())) {
//                    Toast.makeText(AddTicketActivity.this, "Please enter Customer Contact Email Address", Toast.LENGTH_SHORT).show();
//                } else if (TextUtils.isEmpty(edtCustomerSecondaryContactName.getText())) {
//                    Toast.makeText(AddTicketActivity.this, "Please enter Customer Secondary Contact Name", Toast.LENGTH_SHORT).show();
//                } else if (TextUtils.isEmpty(edtCustomerSecondaryContactPhone.getText())) {
//                    Toast.makeText(AddTicketActivity.this, "Please enter Customer Secondary Contact Phone Number", Toast.LENGTH_SHORT).show();
//                }
                else {
                    imgCreate.setEnabled(false);
                    insertMemberModel();
                }
            }
        });
    }

    public void insertMemberModel() {
        MemberModel memberModel=new MemberModel();
        memberModel.setRcCode(0);
        memberModel.setNextquestionId(0);
        memberModel.setInsertFlag(true);

        memberModel.setDispatchno(edtDispatchNo.getText().toString());
//        if (!TextUtils.isEmpty(edtCustomerName.getText())) {
//            memberModel.setCustomername("Name");
//        }
//        if (!TextUtils.isEmpty(edtCustomerNumber.getText())) {
//            memberModel.setCustomerno("9999999999");
//        }
        memberModel.setCustomername("Name");
        memberModel.setCustomerno("9999999999");
//        memberModel.setCustomercontactname(edtCustomerContactName.getText().toString());
//        memberModel.setCustomercontactphno(edtCustomerContactPhone.getText().toString());
//        memberModel.setCustomercontactemail(edtCustomerContactEmail.getText().toString());
//        memberModel.setCustomersecondarycontactname(edtCustomerSecondaryContactName.getText().toString());
//        memberModel.setCustomersecondarycontactphno(edtCustomerSecondaryContactPhone.getText().toString());
        if (!TextUtils.isEmpty(edtServiceAddressLine1.getText())){
            memberModel.setServiceaddress1(edtServiceAddressLine1.getText().toString());
        }
        if (!TextUtils.isEmpty(edtServiceAddressLine2.getText())){
            memberModel.setServiceaddress2(edtServiceAddressLine2.getText().toString());
        }
        if (!TextUtils.isEmpty(edtServiceAddressLine3.getText())){
            memberModel.setServiceaddress3(edtServiceAddressLine3.getText().toString());
        }
        if (!TextUtils.isEmpty(edtServiceAddressLine4.getText())){
            memberModel.setServiceaddress4(edtServiceAddressLine4.getText().toString());
        }


        memberModel.setLatitude(stringLatitude);
        memberModel.setLongitude(stringLongitude);
        memberModel.setCityId(city);
        memberModel.setPincodeId(0);
        memberModel.setState("");
        memberModel.setLob("");
        memberModel.setLastmodifiedby(UserSessionManager.getUserSessionManager().getUser().getId());
        memberModel.setLastmodifiedtime(getDateTime());
        memberModel.setLastmodifiedrole(UserSessionManager.getUserSessionManager().getUser().getRoleId());
        memberModel.setCreatedby(UserSessionManager.getUserSessionManager().getUser().getId());
        memberModel.setCreatedtime(getDateTime());
        memberModel.setCreatedrole(UserSessionManager.getUserSessionManager().getUser().getRoleId());
        memberModel.setDeleteflag(false);
        memberModel.setParentFlag(false);
        memberModel.setLanguage("1");

        String orgStructure = UserSessionManager.getUserSessionManager().getUser().getOrgStructure();
        String[] orgStructureArray = orgStructure.split(";");
        if(orgStructureArray.length>2){
            memberModel.setOrgstructure("99-"+country+";102-"+state+";105-"+city);
        }else{
            memberModel.setOrgstructure("99-"+country+";102-"+state);
        }

        memberModel.setAssignedto(UserSessionManager.getUserSessionManager().getUser().getId());
        memberModel.setCompletedFlag(false);
        memberModel.setRollbackStatus("N");
        memberModel.setAssignFlag(false);
        memberModel.setApprovalFlag(false);
        memberModel.setFieldassignFlag(false);
        memberModel.setRollbackFlag(false);
        memberModel.setCustomerId(UserSessionManager.getUserSessionManager().getUser().getCustomerId());
        memberModel.setCategoryId(UserSessionManager.getUserSessionManager().getUser().getCategoryId());
        memberModel.setSubcategoryId(UserSessionManager.getUserSessionManager().getUser().getSubcategoryId());
        memberModel.setCountryId(country);
        memberModel.setStateId(state);
        memberModel.setStatusId(1);
        memberModel.setServicecalldate("");
        memberModel.setServiceprovider("");
        memberModel.setDispatchstatus("OPEN");
        memberModel.setSysclassification("");
        memberModel.setEndservicewindow("");
        memberModel.setServicetag("");
        memberModel.setDsptype("");
        memberModel.setServicelevel("");
        memberModel.setCountry("");
        memberModel.setAgentdescription("");
        memberModel.setWarrantyinvoicedate("");
        memberModel.setWarrantyinvoiceno("");
        memberModel.setWarrantybillfromstate("");
        memberModel.setOriginalorderbuid("");
        memberModel.setServicepostalcode("");
        memberModel.setProducttypeid(0);
        memberModel.setServiceeventid(0);
        memberModel.setProcessflag(true);
        //memberModel.setStarttime(txtStDate.getText().toString()+"T00:00:00.000Z");
        //memberModel.setEndtime(txtEndDate.getText().toString()+"T00:00:00.000Z");
        memberModel.setPushed(false);
        new SetMemberAsyncTask(this, oaasDatabase, memberModel).execute();
    }


    private String getDateTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.sss'Z'", Locale.US);
        dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        Date date = new Date();
        return dateFormat.format(date);
    }


    @Override
    public void onComplete() {
        Toast.makeText(this, "Ticket Added Successfully", Toast.LENGTH_LONG).show();
        finish();
    }

    @Override
    public void onUpdate() {

    }

    @Override
    public void onOrganizationLocationFetchCompleted(List<OrganizationLocationModel> organizationLocationModels) {
        try {
            JsonArray benjson = new JsonArray();
//        try {
//            JsonParser parser = new JsonParser();
//            benjson = parser.parse(new Gson().toJson(
//                    organizationLocationModels, OrganizationLocationModel.class)).getAsJsonArray();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
        Log.e("orgLocationModels", String.valueOf(organizationLocationModels.size()));
            String orgStructure = UserSessionManager.getUserSessionManager().getUser().getOrgStructure();
            String[] orgStructureArray = orgStructure.split(";");
            String [] state,city;
            if(orgStructureArray.length>1){
                state = orgStructureArray[1].split("-")[1].split(",");
                for(int i=0;i<organizationLocationModels.size();i++){
                    if(Arrays.asList(state).contains(String.valueOf(organizationLocationModels.get(i).getId()))){
                        stateModelList.add(organizationLocationModels.get(i));
                    }
                }
            }else{
                sp_state.setVisibility(View.GONE);
                sp_city.setVisibility(View.GONE);
                txtHeaderCity.setVisibility(View.GONE);
            }

            if(orgStructureArray.length>2){
                city = orgStructureArray[2].split("-")[1].split(",");
                for(int i=0;i<organizationLocationModels.size();i++){
                    if(Arrays.asList(city).contains(String.valueOf(organizationLocationModels.get(i).getId()))){
                        cityModelList.add(organizationLocationModels.get(i));
                    }
                }
            }else{
                sp_city.setVisibility(View.GONE);
                txtHeaderCity.setVisibility(View.GONE);
            }

            sp_state.setOnItemSelectedListener(AddTicketActivity.this);
            stateAdapter = new StateAdapter(this, stateModelList);
            sp_state.setAdapter(stateAdapter);
            sp_state.setSelection(stateModelList.size());

            sp_city.setOnItemSelectedListener(this);
            cityAdapter = new CityAdapter(this, cityModelList);
            sp_city.setAdapter(cityAdapter);
            sp_city.setSelection(cityModelList.size());
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (parent.getAdapter() instanceof StateAdapter) {
            StateAdapter stateAdapter = (StateAdapter) parent.getAdapter();
            OrganizationLocationModel todoType = (OrganizationLocationModel) stateAdapter.getItem(position);
            if (todoType != null && !todoType.getName().contains("Select Todo's")) {
                this.state = todoType.getId();
            }
            return;
        }

        if (parent.getAdapter() instanceof CityAdapter) {
            CityAdapter cityAdapter = (CityAdapter) parent.getAdapter();
            OrganizationLocationModel todoType = (OrganizationLocationModel) cityAdapter.getItem(position);
            if (todoType != null && !todoType.getName().contains("Select Todo's")) {
                this.city = todoType.getId();
            }
            return;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}