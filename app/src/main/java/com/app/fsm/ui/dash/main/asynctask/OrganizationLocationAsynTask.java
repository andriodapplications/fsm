package com.app.fsm.ui.dash.main.asynctask;

import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.widget.Toast;

import com.app.fsm.local_network.network.model.category.CategoryModel;
import com.app.fsm.local_network.network.model.category.CategoryModelDao;
import com.app.fsm.local_network.network.model.organizationlocation.OrganizationLocationModel;
import com.app.fsm.local_network.network.model.organizationlocation.OrganizationLocationModelDao;
import com.app.fsm.local_network.repository.OaasDatabase;
import com.app.fsm.local_network.repository.OassDatabaseBuilder;
import com.app.fsm.local_network.repository.dao.OrganizationalLocationDao;
import com.app.fsm.ui.dash.main.OnCategoryListner;
import com.app.fsm.ui.dash.main.OnOrganizationLocationListener;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

public final class OrganizationLocationAsynTask extends AsyncTask<Void, Void, List<OrganizationLocationModel>> {

    private OaasDatabase oaasDatabase;
    private OnOrganizationLocationListener onOrganizationLocationListner;
    private ArrayList<String> LocationIds;

    public OrganizationLocationAsynTask(@NonNull OnOrganizationLocationListener onOrganizationLocationListner, @NonNull OaasDatabase oaasDatabase , ArrayList<String> LocationIds) {
        this.oaasDatabase = oaasDatabase;
        this.onOrganizationLocationListner = onOrganizationLocationListner;
        this.LocationIds = LocationIds;
    }
    @Override
    protected List<OrganizationLocationModel> doInBackground(Void... voids) {
        OrganizationLocationModelDao organizationLocationModelDao= oaasDatabase.provideOrganizationLocationModelDao();
        return organizationLocationModelDao.getFilteredLocations(this.LocationIds);
    }

    @Override
    protected void onPostExecute(List<OrganizationLocationModel> organizationLocationModels) {
        if (onOrganizationLocationListner != null)
            onOrganizationLocationListner.onOrganizationLocationFetchCompleted(organizationLocationModels);
    }
}

