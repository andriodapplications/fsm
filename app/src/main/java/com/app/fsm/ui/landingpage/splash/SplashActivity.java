/*
 * Developed by Avinash Kumar singh on 24/1/19 3:49 PM
 * Last Modified 16/1/19 5:57 PM.
 *
 * Copyright (c) 2019.  All rights reserved.
 */

package com.app.fsm.ui.landingpage.splash;

import android.os.Bundle;
import android.os.Handler;
import android.widget.ImageView;

import com.app.fsm.BaseActivity;
import com.app.fsm.R;
import com.app.fsm.ui.dash.main.DashboardActivity;
import com.app.fsm.ui.landingpage.login.LoginActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SplashActivity extends BaseActivity {


    @BindView(R.id.imageView)
    ImageView imageView;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        init();
    }

    public void init() {

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                if(getUserSessionManager().getAccessToken()!=null && getUserSessionManager().getAccessToken().length()==0){
                    startActivity(LoginActivity.newInstance(SplashActivity.this));
                }else{
                    startActivity(DashboardActivity.newInstance(SplashActivity.this));
                }
                finish();

            }
        }, 3000);
    }



}
