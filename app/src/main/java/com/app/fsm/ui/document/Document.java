package com.app.fsm.ui.document;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;


import com.app.fsm.BaseActivity;
import com.app.fsm.R;
import com.app.fsm.local_network.network.model.member.MemberModel;
import com.app.fsm.local_network.network.model.survey.SurveyAnswerModel;
import com.app.fsm.local_network.network.model.survey.SurveyQuestionModel;
import com.app.fsm.local_network.repository.OaasDatabase;
import com.app.fsm.local_network.repository.OassDatabaseBuilder;
import com.app.fsm.ui.dash.main.OnMemberListener;
import com.app.fsm.ui.dash.main.asynctask.GetAllMemberAsynTask;
import com.app.fsm.ui.dash.one2one.OnFetchAnswersComplete;
import com.app.fsm.ui.dash.one2one.OnFetchComplete;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Document extends BaseActivity implements OpenPDF,DownloadPdf,
        OnMemberListener , OnFetchComplete,OnFetchAnswersComplete {

    @BindView(R.id.title)
    TextView txtTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.rv_document)
    RecyclerView rvdocument;

    private Documentrv_Adapter documentrv_adapter;

    private List<SurveyQuestionModel> surveyquestionModelArrayList;
    private List<SurveyAnswerModel> surveyAnswerModelArrayList;
    private ArrayList<SurveyAnswerModel> surveyAnswerFilteredList;
    private List<MemberModel> memberModelList;

    private OaasDatabase oaasDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_document);
        ButterKnife.bind(this);

        txtTitle.setText("Document");
        toolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        oaasDatabase = OassDatabaseBuilder.provideOassDatabase(this);

        memberModelList=new ArrayList<>();
        surveyquestionModelArrayList=new ArrayList<>();
        surveyAnswerModelArrayList = new ArrayList<>();
        surveyAnswerFilteredList=new ArrayList<>();

        new GetAllMemberAsynTask(this, oaasDatabase, 0).execute();

        new GetAllDocumentSurveyQuestionAsynTask(this,oaasDatabase).execute();

        new GetAllDoucmentSurveyAnswersAsynTask(this, oaasDatabase).execute();

        documentrv_adapter = new Documentrv_Adapter(this,this,this);
        LinearLayoutManager manager1 = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rvdocument.setAdapter(documentrv_adapter);
        rvdocument.setLayoutManager(manager1);
        documentrv_adapter.notifyDataSetChanged();
    }

    @Override
    public void onclickdownload(SurveyAnswerModel surveyAnswerModel) {
        String url="https://idcamp-api.herokuapp.com/api/v1/containers/converbiz/download/"+surveyAnswerModel.getUploadedfiles();
        new DownloadTask(Document.this, url);
        //Toast.makeText(this, "coming soon", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onclickpdf(SurveyAnswerModel surveyAnswerModel) {
        startActivity(new Intent(this, PDFViewer.class)
                .putExtra("surveyanswers",surveyAnswerModel));
    }

    @Override
    public void onMemberFetchCompleted(List<MemberModel> memberModelList, int pos) {
        this.memberModelList=memberModelList;
        Log.d("memberlist",""+memberModelList.size());
    }

    @Override
    public void onComplete(List<SurveyQuestionModel> surveyQuestionModelList) {
        surveyquestionModelArrayList=surveyQuestionModelList;
        Log.d("surveyquestionlist",""+surveyquestionModelArrayList.size());
    }

    @Override
    public void onCompleteSurvey(List<SurveyAnswerModel> surveyAnswerModels) {
        Log.d("surveyanswerlist",""+surveyAnswerModels.size());

        for(MemberModel memberModel:memberModelList){
            for(SurveyQuestionModel questionModel:surveyquestionModelArrayList){
                for(SurveyAnswerModel answerModel:surveyAnswerModels){
                    if(answerModel.getMemberid().equals(memberModel.getServerId())
                            && answerModel.getQuestionid().equals(questionModel.getId())){
                        answerModel.setDispatchno(memberModel.getDispatchno());
                        surveyAnswerModelArrayList.add(answerModel);

                    }
                }
            }
        }

        for(SurveyAnswerModel surveyAnswerModel:surveyAnswerModelArrayList){
            Integer substring = Integer.valueOf(surveyAnswerModel.getAnswer().substring(Math.max(surveyAnswerModel.getAnswer().length() - 2, 0)));
            if(surveyAnswerModel.getUploadedfiles()!=null && substring==10){
                surveyAnswerFilteredList.add(surveyAnswerModel);
            }
        }

        documentrv_adapter.update(surveyAnswerFilteredList);

    }
}