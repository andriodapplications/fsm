package com.app.fsm.ui.dash.main;

import com.app.fsm.local_network.network.model.subcategory.SubCategoryModel;

import java.util.List;

public interface OnSubCategoryListener {
    void onSubCategoryFetchCompleted(List<SubCategoryModel> subCategoryModels);
}
