package com.app.fsm.ui.dash.addticket;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.app.fsm.R;
import com.app.fsm.local_network.network.model.organizationlocation.OrganizationLocationModel;
import com.app.fsm.local_network.network.model.serviceevents.ServiceEventModel;
import com.app.fsm.ui.dash.one2one.one2onequestion.adapter.ServiceEventAdapter;

import java.util.List;

public class StateAdapter extends BaseAdapter {

    private LayoutInflater layoutInflater;
    private List<OrganizationLocationModel> organizationLocationModels;

    public StateAdapter(@NonNull Context context, @NonNull List<OrganizationLocationModel> organizationLocationModels) {
        this.organizationLocationModels = organizationLocationModels;
        this.layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return organizationLocationModels.size()+1;
    }


    @Nullable
    @Override
    public Object getItem(int position) {
        return position == 0 ?  null : organizationLocationModels.get(position - 1);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        StateAdapter.ItemVH itemVH;
        if (convertView != null) {
            itemVH = (StateAdapter.ItemVH) convertView.getTag();
        } else {
            convertView = layoutInflater.inflate(R.layout.row_spinner_list, parent,false);
            itemVH = new StateAdapter.ItemVH();
            itemVH.tvTitle=convertView.findViewById(R.id.txt_title);
            convertView.setTag(itemVH);
        }
        OrganizationLocationModel organizationLocationModel = (OrganizationLocationModel) getItem(position);
        if (organizationLocationModel != null) {
            itemVH.tvTitle.setText(organizationLocationModel.getName());
        }else {
            itemVH.tvTitle.setText("Select State");
        }

        return convertView;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        StateAdapter.ItemVH itemVH;
        if (convertView != null) {
            itemVH = (StateAdapter.ItemVH) convertView.getTag();
        } else {
            convertView = layoutInflater.inflate(R.layout.row_spinner_list, parent,false);
            itemVH = new StateAdapter.ItemVH();
            itemVH.tvTitle=convertView.findViewById(R.id.txt_title);
            convertView.setTag(itemVH);
        }
        OrganizationLocationModel serviceEventModel = (OrganizationLocationModel) getItem(position);
        if (serviceEventModel != null) {
            itemVH.tvTitle.setText(serviceEventModel.getName());
        }else {
            itemVH.tvTitle.setText("Select State");
        }
        return convertView;
    }

    static class ItemVH {
        TextView tvTitle;
    }
}