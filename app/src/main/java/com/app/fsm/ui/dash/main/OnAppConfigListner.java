package com.app.fsm.ui.dash.main;

import com.app.fsm.local_network.network.model.appconfig.AppConfigModel;

import java.util.List;

public interface OnAppConfigListner {

    void FetchAppCongigData(List<AppConfigModel> appConfigModels);
}

