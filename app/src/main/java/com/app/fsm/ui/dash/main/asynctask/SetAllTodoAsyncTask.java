package com.app.fsm.ui.dash.main.asynctask;

import android.content.Context;
import android.os.AsyncTask;

import com.app.fsm.local_network.network.model.todo.TodoModel;
import com.app.fsm.local_network.repository.OaasDatabase;
import com.app.fsm.local_network.repository.OassDatabaseBuilder;
import com.app.fsm.local_network.repository.dao.TodoDao;

import java.lang.ref.WeakReference;
import java.util.List;

public final class SetAllTodoAsyncTask extends AsyncTask<Void, Void, Boolean> {

    //Prevent leak
    private WeakReference<Context> weakActivity;
    private List<TodoModel> todoModelList;

    public SetAllTodoAsyncTask(Context context, List<TodoModel> todoModelList) {
        weakActivity = new WeakReference<>(context);
        this.todoModelList = todoModelList;
    }

    @Override
    protected Boolean doInBackground(Void... params) {
        OaasDatabase oaasDatabase = OassDatabaseBuilder.provideOassDatabase(weakActivity.get());
        TodoDao todoDao = oaasDatabase.provideTodoModelDao();
        Long[] longTodo=todoDao.insertAllTodoModel(todoModelList);
        return true;
    }

    @Override
    protected void onPostExecute(Boolean isInserted) {

    }
}

