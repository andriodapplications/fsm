package com.app.fsm.ui.document;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.fsm.R;
import com.app.fsm.local_network.network.model.survey.SurveyAnswerModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kanthi on 06/07/2020.
 */

public class Documentrv_Adapter extends RecyclerView.Adapter<Documentrv_Adapter.ViewHolder> {
    private List<SurveyAnswerModel> mAdditem;
    private Context mContext;
    private OpenPDF openPDF;
    private DownloadPdf downloadPdf;

    public Documentrv_Adapter(Context context,OpenPDF openpdf,DownloadPdf download) {
        mAdditem=new ArrayList<>();
        mContext = context;
        openPDF=openpdf;
        downloadPdf=download;
    }

    public void update(List<SurveyAnswerModel> stringList) {
        mAdditem.clear();
        mAdditem.addAll(stringList);
        notifyDataSetChanged();
    }

    @Override
    public Documentrv_Adapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_document,
                parent, false);
        mContext = parent.getContext();
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        String dispatch=mAdditem.get(position).getDispatchno()==null?"":mAdditem.get(position).getDispatchno();
        String pdfname=mAdditem.get(position).getUploadedfiles()!=null
                ?mAdditem.get(position).getUploadedfiles():"NA";
        holder.tv_documentname.setText(pdfname);
        holder.tv_dispatchno.setText("Dispatch no: "+dispatch);
        holder.im_openpdf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openPDF.onclickpdf(mAdditem.get(position));
            }
        });

        holder.im_downloadpdf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                downloadPdf.onclickdownload(mAdditem.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return mAdditem.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView tv_documentname;

        public TextView tv_dispatchno;

        public ImageView im_openpdf;

        public ImageView im_downloadpdf;

        public ViewHolder(View view) {
            super(view);
            tv_documentname= (TextView) view.findViewById(R.id.tv_documentname);
            tv_dispatchno=view.findViewById(R.id.tv_dispatchno);
            im_openpdf= (ImageView) view.findViewById(R.id.im_openpdf);
            im_downloadpdf= (ImageView) view.findViewById(R.id.im_downloadpdf);
        }
    }
}

