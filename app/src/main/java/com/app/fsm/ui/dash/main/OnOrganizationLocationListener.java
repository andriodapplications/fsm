package com.app.fsm.ui.dash.main;

import com.app.fsm.local_network.network.model.category.CategoryModel;
import com.app.fsm.local_network.network.model.organizationlocation.OrganizationLocationModel;

import java.util.List;

public interface OnOrganizationLocationListener {
    void onOrganizationLocationFetchCompleted(List<OrganizationLocationModel> organizationLocationModels);
}
