package com.app.fsm.ui.dash.memberregistration.memberprofile;


import android.Manifest;
import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.AppCompatTextView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.fsm.R;
import com.app.fsm.SliderBaseFragment;
import com.app.fsm.local_network.network.model.educationmodel.EducationModel;
import com.app.fsm.local_network.network.model.gendermodel.GenderModel;
import com.app.fsm.local_network.network.model.member.MemberLanguageModel;
import com.app.fsm.local_network.network.model.member.MemberModel;
import com.app.fsm.local_network.network.model.occupationmodel.OccupationModel;
import com.app.fsm.ui.OnCompleteListener;
import com.app.fsm.ui.dash.memberregistration.memberprofile.adapter.EducationAdapter;
import com.app.fsm.ui.dash.memberregistration.memberprofile.adapter.GenderAdapter;
import com.app.fsm.ui.dash.memberregistration.memberprofile.adapter.OccupationAdapter;
import com.app.fsm.ui.dash.memberregistration.memberprofile.memberasynctask.GetAllEducationAsynTask;
import com.app.fsm.ui.dash.memberregistration.memberprofile.memberasynctask.GetAllGenderAsynTask;
import com.app.fsm.ui.dash.memberregistration.memberprofile.memberasynctask.GetAllOccupationAsynTask;
import com.app.fsm.ui.dash.memberregistration.memberprofile.memberasynctask.GetMemberLanguageAsyncTask;
import com.app.fsm.ui.dash.memberregistration.memberprofile.memberasynctask.SetMemberAsyncTask;
import com.app.fsm.ui.dash.memberregistration.memberprofile.memberasynctask.UpdateMemberAsyncTask;
import com.app.fsm.utils.AppConstant;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import butterknife.Unbinder;
import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.AppSettingsDialog;
import pub.devrel.easypermissions.EasyPermissions;

/**
 * A simple {@link Fragment} subclass.
 */
public class MemberRegistrationProfile extends SliderBaseFragment implements OnFetchCompleteListener
        , AdapterView.OnItemSelectedListener
        , EasyPermissions.PermissionCallbacks
        , EasyPermissions.RationaleCallbacks
        , OnSuccessListener
        , OnCompleteListener {

    private static final int RC_COARSE_LOCATION = 1001;
    private final String[] LOCATION_PERMISSION = {Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION};

    @BindView(R.id.edt_full_name)
    AppCompatEditText edtFullName;
    @BindView(R.id.edt_member_id)
    AppCompatEditText edtMemberId;
    @BindView(R.id.edt_age)
    AppCompatEditText edtAge;
    @BindView(R.id.img_calender)
    ImageView imgCalender;
    @BindView(R.id.rl_dob)
    RelativeLayout rlDob;
    @BindView(R.id.spinner_gender)
    AppCompatSpinner spinnerGender;
    @BindView(R.id.spinner_occupation)
    AppCompatSpinner spinnerOccupation;
    @BindView(R.id.spinner_education)
    AppCompatSpinner spinnerEducation;
    Unbinder unbinder;
    @BindView(R.id.txt_dob)
    AppCompatTextView txtDob;
    @BindView(R.id.txt_header_full_name)
    TextView txtHeaderFullName;
    @BindView(R.id.txt_header_member_id)
    TextView txtHeaderMemberId;
    @BindView(R.id.txt_header_age)
    TextView txtHeaderAge;
    @BindView(R.id.txt_header_gender)
    TextView txtHeaderGender;
    @BindView(R.id.txt_header_occupation)
    TextView txtHeaderOccupation;
    @BindView(R.id.txt_header_education)
    TextView txtHeaderEducation;
    @BindView(R.id.txt_header_dob)
    TextView txtHeaderDob;
    @BindView(R.id.txt_header_lat)
    TextView txtHeaderLat;
    @BindView(R.id.edt_lat)
    AppCompatEditText edtLat;
    @BindView(R.id.txt_header_long)
    TextView txtHeaderLong;
    @BindView(R.id.edt_long)
    AppCompatEditText edtLong;
    @BindView(R.id.txt_header_address)
    TextView txtHeaderAddress;
    @BindView(R.id.edt_address)
    AppCompatEditText edtAddress;
    @BindView(R.id.img_drop_point)
    ImageView imgDropPoint;
    @BindView(R.id.txt_update_member)
    TextView txtUpdateMember;

    private Calendar calendar = Calendar.getInstance();
    private Boolean isFromDob = false;
    private GenderModel genderModel;
    private OccupationModel occupationModel;
    private EducationModel educationModel;
    private FusedLocationProviderClient fusedLocationProviderClient;
    private Location location;
    private Boolean isEditable;
    private MemberModel memberModel;

    public static MemberRegistrationProfile newInstance(Boolean isEditable, MemberModel memberModel) {
        Bundle bundle = new Bundle();
        bundle.putBoolean(AppConstant.IS_EDITABLE, isEditable);
        bundle.putSerializable(AppConstant.MEMBER_MODEL, memberModel);
        MemberRegistrationProfile memberRegistrationProfile = new MemberRegistrationProfile();
        memberRegistrationProfile.setArguments(bundle);
        return memberRegistrationProfile;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_member_registration_profile, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (getArguments() != null) {
            isEditable = getArguments().getBoolean(AppConstant.IS_EDITABLE);
            memberModel = (MemberModel) getArguments().getSerializable(AppConstant.MEMBER_MODEL);
        }

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!isEditable)
            getLocationTask();
    }

    public void init() {
        if (isEditable) {
            setEditView(memberModel);
        }
        fetchFromRoom();
    }

    public void setEditView(MemberModel memberModel) {
        edtFullName.setText(memberModel.getSerno());
        //edtMemberId.setText(memberModel.getMemberId());
        //edtAge.setText(memberModel.getag);
        edtLat.setText(memberModel.getLatitude());
        edtLong.setText(memberModel.getLongitude());
        //edtAddress.setText(memberModel.getAddress());
        //txtDob.setText(CommonUtils.getDate(memberModel.getDob(), "dd-MM-yyyy"));
        //calendar = CommonUtils.getCalender(memberModel.getDob());
        //txtUpdateMember.setVisibility(View.VISIBLE);
    }

    LocationRequest locationRequest;

    @SuppressLint("MissingPermission")
    protected void createLocationRequest() {

        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(requireActivity());
        locationRequest = LocationRequest.create();
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(5000);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);

        SettingsClient client = LocationServices.getSettingsClient(requireActivity());
        Task<LocationSettingsResponse> task = client.checkLocationSettings(builder.build());

        task.addOnSuccessListener(new OnSuccessListener<LocationSettingsResponse>() {
            @Override
            public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                fusedLocationProviderClient.getLastLocation().addOnSuccessListener(MemberRegistrationProfile.this);
            }
        });
        task.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                int statusCode = ((ApiException) e).getStatusCode();
                switch (statusCode) {
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        try {
                            ResolvableApiException rae = (ResolvableApiException) e;
                            rae.startResolutionForResult(requireActivity(), AppConstant.GPS_REQUEST);
                        } catch (IntentSender.SendIntentException sie) {
                            Log.i("GPS", "PendingIntent unable to execute request.");
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        String errorMessage = "Location settings are inadequate, and cannot be " +
                                "fixed here. Fix in Settings.";
                        Toast.makeText(requireActivity(), errorMessage, Toast.LENGTH_LONG).show();

                }
            }
        });

    }

    public void fetchFromRoom() {
        new GetAllEducationAsynTask(this, provideOaasDatabase()).execute();
        new GetAllGenderAsynTask(this, provideOaasDatabase()).execute();
        new GetAllOccupationAsynTask(this, provideOaasDatabase()).execute();
        new GetMemberLanguageAsyncTask(this, provideOaasDatabase()).execute();
    }


    @OnTextChanged(value = R.id.edt_age, callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED)
    public void nameChanged(CharSequence text) {
        Log.i("true", "AGE");
        int age = Integer.valueOf(text.toString().length() > 0 ? text.toString() : "0");
        if (age == 0) {
            updateAge(calendar);
        } else if (age >= 1 && age < 100) {
            calendar = Calendar.getInstance();
            updateDate(age);
        } else {
            txtDob.setText("");
        }
    }

    public void updateAge(Calendar calendar) {
        String myFormat = "dd-MM-yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        txtDob.setText(sdf.format(calendar.getTime()));
    }

    public void updateDate(int age) {
        String myFormat = "dd-MM-yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        calendar.add(Calendar.YEAR, -age);
        txtDob.setText(sdf.format(calendar.getTime()));
    }

    DatePickerDialog.OnDateSetListener onDateSetListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.MONTH, month);
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            edtAge.setText(String.valueOf(setAge(calendar)));
        }
    };

    public int setAge(Calendar dob) {
        return Calendar.getInstance().get(Calendar.YEAR) - dob.get(Calendar.YEAR);
    }

    @OnClick(R.id.rl_dob)
    public void onRlDobClicked() {

        DatePickerDialog datePickerDialog = new DatePickerDialog(requireContext(), onDateSetListener
                , calendar.get(Calendar.YEAR)
                , calendar.get(Calendar.MONTH)
                , calendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        datePickerDialog.show();
    }

    @OnClick(R.id.img_drop_point)
    public void onDropPointClicked() {
        getLocationTask();
    }

    @OnClick(R.id.txt_update_member)
    public void onUpdateClick() {
        checkValidation(memberModel);
    }

    @Override
    public void onPause() {
        super.onPause();
        fusedLocationProviderClient = null;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onGenderFetchComplete(List<GenderModel> genderModelList) {
        spinnerGender.setOnItemSelectedListener(this);
        GenderModel promptGenderModel = new GenderModel();
        promptGenderModel.setName("Select Gender");
        genderModelList.add(promptGenderModel);
        GenderAdapter genderAdapter = new GenderAdapter(requireContext(), genderModelList);
        spinnerGender.setAdapter(genderAdapter);

        if (isEditable) {
            for (int i = 0; i < genderModelList.size(); i++) {
                /*if (memberModel.getGenderId().equals(genderModelList.get(i).getId())) {
                    spinnerGender.setSelection(i);
                    break;
                }*/
            }
            return;
        }
        spinnerGender.setSelection(genderAdapter.getCount());
    }

    @Override
    public void onOccupationFetchComplete(List<OccupationModel> occupationModelList) {
        spinnerOccupation.setOnItemSelectedListener(this);
        OccupationModel occupationModel = new OccupationModel();
        occupationModel.setName("Select Occupation");
        occupationModelList.add(occupationModel);
        OccupationAdapter occupationAdapter = new OccupationAdapter(requireContext(), occupationModelList);
        spinnerOccupation.setAdapter(occupationAdapter);
        if (isEditable) {
            for (int i = 0; i < occupationModelList.size(); i++) {
                /*if (memberModel.getOccupationId().equals(occupationModelList.get(i).getId())) {
                    spinnerOccupation.setSelection(i);
                    break;
                }*/
            }
            return;
        }

        spinnerOccupation.setSelection(occupationAdapter.getCount());
    }

    @Override
    public void onEducationFetchComplete(List<EducationModel> educationModelList) {
        spinnerEducation.setOnItemSelectedListener(this);
        EducationModel educationModel = new EducationModel();
        educationModel.setName("Select Education");
        educationModelList.add(educationModel);
        EducationAdapter educationAdapter = new EducationAdapter(requireContext(), educationModelList);
        spinnerEducation.setAdapter(educationAdapter);
        if (isEditable) {
            for (int i = 0; i < educationModelList.size(); i++) {
                /*if (memberModel.getGenderId().equals(educationModelList.get(i).getId())) {
                    spinnerEducation.setSelection(i);
                    break;
                }*/
            }
            return;
        }
        spinnerEducation.setSelection(educationAdapter.getCount());
    }

    @Override
    public void onMemberLanguageComplete(List<MemberLanguageModel> memberLanguageModelList) {

        if (!memberLanguageModelList.isEmpty()) {
            MemberLanguageModel memberLanguageModel = memberLanguageModelList.get(0);
            txtHeaderFullName.setText(memberLanguageModel.getFullname());
            txtHeaderMemberId.setText(memberLanguageModel.getMemberId());
            txtHeaderAge.setText(memberLanguageModel.getAge());
            txtHeaderDob.setText(memberLanguageModel.getDob());
            txtHeaderGender.setText(memberLanguageModel.getGender());
            txtHeaderOccupation.setText(memberLanguageModel.getOccupation());
            txtHeaderEducation.setText(memberLanguageModel.getEducation());
            txtHeaderLat.setText(memberLanguageModel.getLatitude());
            txtHeaderLong.setText(memberLanguageModel.getLongitude());
            txtHeaderAddress.setText(memberLanguageModel.getAddress());
        }

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        if (parent.getAdapter() instanceof GenderAdapter) {
            GenderAdapter genderAdapter = (GenderAdapter) parent.getAdapter();
            GenderModel genderModel = (GenderModel) genderAdapter.getItem(position);
            if (genderModel != null && !genderModel.getName().contains("Select Gender")) {
                this.genderModel = genderModel;
                // Toast.makeText(requireActivity(), genderModel.getName(), Toast.LENGTH_SHORT).show();
            }
        } else if (parent.getAdapter() instanceof OccupationAdapter) {
            OccupationAdapter occupationAdapter = (OccupationAdapter) parent.getAdapter();
            OccupationModel occupationModel = (OccupationModel) occupationAdapter.getItem(position);
            if (occupationModel != null && !occupationModel.getName().contains("Select Occupation")) {
                this.occupationModel = occupationModel;
                // Toast.makeText(requireActivity(), occupationModel.getName(), Toast.LENGTH_SHORT).show();
            }
        } else if (parent.getAdapter() instanceof EducationAdapter) {
            EducationAdapter educationAdapter = (EducationAdapter) parent.getAdapter();
            EducationModel educationModel = (EducationModel) educationAdapter.getItem(position);
            if (educationModel != null && !educationModel.getName().contains("Select Education")) {
                this.educationModel = educationModel;
                // Toast.makeText(requireActivity(), educationModel.getName(), Toast.LENGTH_SHORT).show();
            }
        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public boolean onNext() {
        return checkValidation(new MemberModel());
    }

    public boolean checkValidation(MemberModel memberModel) {

        if (TextUtils.isEmpty(edtFullName.getText())) {
            Toast.makeText(requireActivity(), "Please enter Name", Toast.LENGTH_SHORT).show();
        } else if (TextUtils.isEmpty(edtMemberId.getText())) {
            Toast.makeText(requireActivity(), "Please enter Member Id", Toast.LENGTH_SHORT).show();
        } else if (TextUtils.isEmpty(txtDob.getText())) {
            Toast.makeText(requireActivity(), "Please enter Date of Birth", Toast.LENGTH_SHORT).show();
        } else if (genderModel == null) {
            Toast.makeText(requireActivity(), "Please select your gender", Toast.LENGTH_SHORT).show();
        } else if (occupationModel == null) {
            Toast.makeText(requireActivity(), "Please select your occupation", Toast.LENGTH_SHORT).show();
        } else if (educationModel == null) {
            Toast.makeText(requireActivity(), "Please select your education", Toast.LENGTH_SHORT).show();
        } else if (TextUtils.isEmpty(edtAddress.getText())) {
            Toast.makeText(requireActivity(), "Please enter your address", Toast.LENGTH_SHORT).show();
        } else if (TextUtils.isEmpty(edtLat.getText()) || TextUtils.isEmpty(edtLong.getText())) {
            Toast.makeText(requireActivity(), "Please set your location", Toast.LENGTH_SHORT).show();
        } else {
            insertMemberModel(memberModel);
            return true;
        }
        return false;
    }

    public void insertMemberModel(MemberModel memberModel) {
        /*memberModel.setFullname(edtFullName.getText().toString().trim());
        memberModel.setMemberId(edtMemberId.getText().toString().trim());
        String dob = calendar.get(Calendar.YEAR) + "-" + calendar.get(Calendar.MONTH) + "-" + calendar.get(Calendar.DAY_OF_MONTH) + "T00:00:00.000Z";
        memberModel.setDob(dob);
        memberModel.setGenderId(genderModel.getId());
        memberModel.setEducationId(educationModel.getId());
        memberModel.setOccupationId(occupationModel.getId());
        memberModel.setLatitude(String.valueOf(edtLat.getText()));
        memberModel.setLongitude(String.valueOf(edtLong.getText()));
        memberModel.setAddress(String.valueOf(edtAddress.getText()));
        memberModel.setPushed(false);
        memberModel.setCreatedby(getUserSessionManager().getUser().getId());
        memberModel.setAge(String.valueOf(diffYear(txtDob.getText().toString().trim() + "T00:00:00.000Z")));
*/

        if (isEditable) {
            memberModel.setServerId(this.memberModel.getServerId());
            memberModel.setLocalId(this.memberModel.getLocalId());
            new UpdateMemberAsyncTask(this,provideOaasDatabase(),memberModel).execute();
        } else
            new SetMemberAsyncTask(this, provideOaasDatabase(), memberModel).execute();
    }


    /**
     * This method is use to find the Difference between two dates(AGE).
     * Current date and set date by user.
     */
    public int diffYear(String strDate) {//"dd-MM-yyyy'T'HH:mm:ss"
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy'T'HH:mm:ss");
        try {
            cal.setTime(df.parse(strDate));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return setAge(cal);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    private Boolean hasLocationPermission() {
        return EasyPermissions.hasPermissions(requireActivity(), LOCATION_PERMISSION);
    }

    @AfterPermissionGranted(RC_COARSE_LOCATION)
    public void getLocationTask() {

        if (hasLocationPermission()) {
            //On Permission Granted
            createLocationRequest();
        } else {
            EasyPermissions.requestPermissions(
                    this
                    , "Please allow location access."
                    , RC_COARSE_LOCATION
                    , LOCATION_PERMISSION);
        }

    }

    @Override
    public void onPermissionsGranted(int requestCode, @NonNull List<String> perms) {
        //On Permission Granted
        createLocationRequest();
    }

    @Override
    public void onPermissionsDenied(int requestCode, @NonNull List<String> perms) {
        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
            new AppSettingsDialog.Builder(this).build().show();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == AppSettingsDialog.DEFAULT_SETTINGS_REQ_CODE) {

        } else if (resultCode == AppConstant.GPS_REQUEST) {
            getLocationTask();
        }
    }

    @Override
    public void onRationaleAccepted(int requestCode) {
        getLocationTask();
    }

    @Override
    public void onRationaleDenied(int requestCode) {
        Toast.makeText(requireActivity(), "Rationale Denied", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onSuccess(Object o) {
        if (o instanceof Location) {
            this.location = (Location) o;
            edtLat.setText(String.valueOf(location.getLatitude()));
            edtLong.setText(String.valueOf(location.getLongitude()));
        }
    }

    @Override
    public void onUpdate() {
        Toast.makeText(requireActivity(), "Member Updated Successfully", Toast.LENGTH_LONG).show();
        requireActivity().getSupportFragmentManager().popBackStack();
    }

    @Override
    public void onComplete() {
        Toast.makeText(requireActivity(), "Member Added Successfully", Toast.LENGTH_LONG).show();
        requireActivity().getSupportFragmentManager().popBackStack();
    }
}
