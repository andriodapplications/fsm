package com.app.fsm.ui.dash.main;

public interface PullDataListener {

    void onSuccess();

    void onError(String msg);
}
