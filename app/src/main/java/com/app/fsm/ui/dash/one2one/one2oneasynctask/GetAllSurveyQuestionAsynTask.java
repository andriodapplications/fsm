package com.app.fsm.ui.dash.one2one.one2oneasynctask;

import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.app.fsm.local_network.network.model.survey.SurveyQuestionModel;
import com.app.fsm.local_network.repository.OaasDatabase;
import com.app.fsm.local_network.repository.dao.SurveyQuestionDao;
import com.app.fsm.ui.dash.one2one.OnFetchComplete;

import java.util.ArrayList;
import java.util.List;

public final class GetAllSurveyQuestionAsynTask extends AsyncTask<Void, Void, List<SurveyQuestionModel>> {

    private OaasDatabase oaasDatabase;
    private OnFetchComplete onFetchComplete;
    private Integer CustommerId;
    private Integer CategoryId;

    public GetAllSurveyQuestionAsynTask(@NonNull OnFetchComplete onFetchComplete,
                                        @NonNull OaasDatabase oaasDatabase,
                                        Integer customerId, Integer categoryId) {
        this.oaasDatabase = oaasDatabase;
        this.onFetchComplete = onFetchComplete;
        CustommerId=customerId;
        CategoryId=categoryId;
    }

    @Override
    protected List<SurveyQuestionModel> doInBackground(Void... params) {
        SurveyQuestionDao surveyQuestionDao= oaasDatabase.provideSurveyQQuestiondao();
        return surveyQuestionDao.provideSurveyQuestion();
    }

    @Override
    protected void onPostExecute(List<SurveyQuestionModel> surveyQuestionModelList) {

        List<SurveyQuestionModel> surveyQuestionModelList1 = new ArrayList<>();
        for (SurveyQuestionModel surveyQuestionModel : surveyQuestionModelList) {
            if (surveyQuestionModel.getQuestiontype() != null
                    && surveyQuestionModel.getCustomerId().equals(CustommerId)
                    && surveyQuestionModel.getCategoryId().equals(CategoryId))
                surveyQuestionModelList1.add(surveyQuestionModel);
        }

        if(onFetchComplete !=null)
            onFetchComplete.onComplete(surveyQuestionModelList1);
    }
}

