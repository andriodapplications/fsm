package com.app.fsm.ui.dash.main.asynctask;

import android.arch.persistence.db.SimpleSQLiteQuery;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import com.amazonaws.AmazonClientException;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.mobileconnectors.s3.transfermanager.TransferManager;
import com.amazonaws.mobileconnectors.s3.transfermanager.Upload;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.app.fsm.local_network.network.ApiService;
import com.app.fsm.local_network.network.model.Images.ImagesModelDao;
import com.app.fsm.local_network.network.model.Images.InsertImages;
import com.app.fsm.local_network.network.model.member.MemberModel;
import com.app.fsm.local_network.network.model.survey.SurveyAnswerModel;
import com.app.fsm.local_network.network.model.todo.TodoModel;
import com.app.fsm.local_network.network.model.workflow.WorkFlowHeader;
import com.app.fsm.local_network.repository.OaasDatabase;
import com.app.fsm.local_network.repository.dao.BaseDao;
import com.app.fsm.local_network.repository.dao.MemberDao;
import com.app.fsm.local_network.repository.dao.PushDataDao;
import com.app.fsm.local_network.repository.dao.SurveyAnswerDao;
import com.app.fsm.local_network.repository.dao.TodoDao;
import com.app.fsm.local_network.repository.dao.UpdateMemberIdDao;
import com.app.fsm.local_network.repository.dao.WorkFlowHeaderDao;
import com.app.fsm.ui.dash.main.OnPushSuccesslistener;
import com.app.fsm.utils.UserSessionManager;

import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

public class PushOutAsyncTask extends AsyncTask<Void, Void, Boolean> {
    private OaasDatabase oaasDatabase;
    private OnPushSuccesslistener onPushSuccesslistener;
    private ApiService apiService;
    private UserSessionManager userSessionManager;
    private PushDataDao pushDataDao;
    String Str_WorkerImageUpload="";
    int workerimagecount=0;
    long workerimagelength;
    File uploadFile2;
    private Boolean isImagepushed = false;
    private Context mContext;
    AsyncTask<String, Void, String> imageFileUploader;

    public PushOutAsyncTask(@NonNull OnPushSuccesslistener onPushSuccesslistener
            , @NonNull OaasDatabase oaasDatabase
            , @NonNull ApiService apiService
            , @NonNull UserSessionManager userSessionManager,Context context) {
        this.oaasDatabase = oaasDatabase;
        this.onPushSuccesslistener = onPushSuccesslistener;
        this.userSessionManager = userSessionManager;
        this.apiService = apiService;
        this.mContext = context;
    }

    @Override
    protected Boolean doInBackground(Void... voids) {
        this.pushDataDao = oaasDatabase.providePushDataDao();
        InsertImages();
//        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
//        this.mContext.registerReceiver(networkStateReceiver, filter);
        if(getCount(oaasDatabase.provideMemberDao()) > 0){
            uploadMember(pushDataDao.provideMember(false));
        } /*else if (getCount(oaasDatabase.provideMemberDao()) > 0){
            uploadMember(pushDataDao.provideMember(false));
        }*/
        //uploadMember(pushDataDao.provideMember(false));
        if (getCount(oaasDatabase.provideSurveyAnswerDao()) > 0)
            answercheck(pushDataDao.provideSurveyAnswer(false));
            //uploadSurveyAnswer(pushDataDao.provideSurveyAnswer(false));
        if (getCount(oaasDatabase.provideWorkFlowHeaderDao()) > 0)
            uploadWorkFlowHeader(pushDataDao.provideWorkFlowHeader(false));
        if (getCount(oaasDatabase.provideTodoModelDao()) > 0)
            uploadTodoModel(pushDataDao.provideTodo(false));
        //testing for images
        /*if (getCount(oaasDatabase.provideInsertImagesDao()) > 0)
            UploadImages(pushDataDao.provideImages(false));*/
        if (getCount(oaasDatabase.provideMemberDao()) == 0
                && getCount(oaasDatabase.provideSurveyAnswerDao()) == 0
                && getCount(oaasDatabase.provideWorkFlowHeaderDao()) == 0
                && getCount(oaasDatabase.provideTodoModelDao()) == 0)
        //&& getCount(oaasDatabase.provideInsertImagesDao()) == 0
        {

            //oaasDatabase.provideInsertImagesDao().deleteAllImages();
            oaasDatabase.provideMemberDao().deleteAllMember();
            SimpleSQLiteQuery query = new SimpleSQLiteQuery("DELETE FROM sqlite_sequence WHERE name=?",
                    new Object[]{"MemberModel"});


            Log.i("SEQ", oaasDatabase.provideMemberDao().deleteSequence(query) + "");


            oaasDatabase.provideTodoModelDao().deleteAllTodoModel();
            SimpleSQLiteQuery queryTodo = new SimpleSQLiteQuery("DELETE FROM SQLITE_SEQUENCE WHERE name=?",
                    new Object[]{"TodoModel"});
            oaasDatabase.provideMemberDao().deleteSequence(queryTodo);


            oaasDatabase.provideSurveyAnswerDao().deleteAllSurveyAnswer();
            SimpleSQLiteQuery querySurveyAnswer = new SimpleSQLiteQuery("DELETE FROM SQLITE_SEQUENCE WHERE name=?",
                    new Object[]{"SurveyAnswerModel"});
            oaasDatabase.provideMemberDao().deleteSequence(querySurveyAnswer);

            oaasDatabase.provideWorkFlowHeaderDao().deleteAllWorkFlowHeader();
            SimpleSQLiteQuery queryWorkFlow = new SimpleSQLiteQuery("DELETE FROM SQLITE_SEQUENCE WHERE name=?",
                    new Object[]{"WorkFlowHeader"});
            oaasDatabase.provideMemberDao().deleteSequence(queryWorkFlow);
        }

        try {
            getMembers();
            getSurveyAnswer();
            getTodo();
            getWorkFlowHeader();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return true;
    }

    @Override
    protected void onPostExecute(Boolean aBoolean) {
        onPushSuccesslistener.onPushSuccess();
    }

    private void answercheck(SurveyAnswerModel surveyAnswerModel){
        //Log.d("answermodel",""+surveyAnswerModel.getUpdated());
        if(surveyAnswerModel.getInserted()!=null &&surveyAnswerModel.getInserted()){
            uploadSurveyAnswer(pushDataDao.provideSurveyAnswer(false));
        }else{
            updateSurveyAnswers(surveyAnswerModel);
        }
    }

    BroadcastReceiver networkStateReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            boolean noConnectivity = intent.getBooleanExtra(ConnectivityManager.EXTRA_NO_CONNECTIVITY, false);

            if(!noConnectivity) {
                onConnectionFound();
            } else {
                onConnectionLost();
            }
        }
    };


    public void onConnectionLost() {
        Toast.makeText(this.mContext, "Connection lost OnPush", Toast.LENGTH_LONG).show();
        imageFileUploader.cancel(true);

    }

    public void onConnectionFound() {
        Toast.makeText(this.mContext, "Connection found", Toast.LENGTH_LONG).show();

    }

    public int getCount(BaseDao baseDao) {

        if (baseDao instanceof MemberDao) {
            return ((MemberDao) baseDao).provideMemberCountNotPushed(false);
        } else if (baseDao instanceof WorkFlowHeaderDao)
            return ((WorkFlowHeaderDao) baseDao).provideWorkFlowHeaderCountNotPushed(false);
        else if (baseDao instanceof TodoDao)
            return ((TodoDao) baseDao).provideTodoCountNotPushed(false);
        else if (baseDao instanceof SurveyAnswerDao)
            return ((SurveyAnswerDao) baseDao).provideSurveyAnswerCountNotPushed(false);
        else if (baseDao instanceof InsertImages)
            return ((ImagesModelDao) baseDao).provideImagesCountNotPushed(false);
        else
            return 0;
    }

    public void uploadMember(final MemberModel memberModel) {

        if(memberModel.getInsertFlag()!=null&&memberModel.getInsertFlag()){
        long serverId = memberModel.getServerId();
        memberModel.setServerId(null);

        Call<ResponseBody> responseBodyCall = apiService.setMember(userSessionManager.getAccessToken(), memberModel);
        try {
            Response<ResponseBody> response = responseBodyCall.execute();
            if (response.isSuccessful()) {
                JSONObject jsonObject = new JSONObject(response.body().string());
                int id = jsonObject.getInt("id");
                Log.i("JSON", jsonObject.toString());
                memberModel.setServerId(serverId);
                memberModel.setPushed(true);
                memberModel.setInsertFlag(false);
                updateMemberModel(memberModel, id);
            }
        } catch (Exception e) {
            Log.i("JSON", "ERROR");
            e.printStackTrace();
        }
    }else{
            long serverId = memberModel.getServerId();
            Call<ResponseBody> responseBodyCall = apiService.updateMember(userSessionManager.getAccessToken(), memberModel);
            try {
                Response<ResponseBody> response = responseBodyCall.execute();
                if (response.isSuccessful()) {
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    int id = jsonObject.getInt("id");
                    Log.i("JSON", jsonObject.toString());
                    memberModel.setServerId(serverId);
                    memberModel.setPushed(true);
                    updateMemberModel(memberModel, id);
                }
            } catch (Exception e) {
                Log.i("JSON", "ERROR");
                e.printStackTrace();
            }
        }
    }

    public void updateMemberModel(MemberModel memberModel, int newId) {
        Log.i("update", "member");
        memberModel.setPushed(true);
        oaasDatabase.provideMemberDao().updateMember(memberModel);//Update the Member after push
        UpdateMemberIdDao updateMemberIdDao = oaasDatabase.provideUpdateMemberIdDao();//Replace the changed Id to each table
        updateMemberIdDao.updateSurveyAnswerMemberId(newId, memberModel.getServerId(), false);
        updateMemberIdDao.updateTodoMemberId(newId, memberModel.getServerId(), false);
        updateMemberIdDao.updateWorkFlowHeaderMemberId(newId, memberModel.getServerId(), false);

        if (getCount(oaasDatabase.provideMemberDao()) > 0) {
            uploadMember(pushDataDao.provideMember(false));
        }
    }

    public void updateMember(final MemberModel memberModel) {
        Call<ResponseBody> responseBodyCall = apiService.updateMember(userSessionManager.getAccessToken(), memberModel);
        try {
            Response<ResponseBody> response = responseBodyCall.execute();
            if (response.isSuccessful()) {

            }
        } catch (Exception e) {
            Log.i("JSON", "ERROR");
            e.printStackTrace();
        }
    }

    public void updateSurveyAnswers(final SurveyAnswerModel surveyAnswerModel) {
        Call<ResponseBody> responseBodyCall = apiService.updateSurveyanswer(userSessionManager.getAccessToken(), surveyAnswerModel);
        try {
            Response<ResponseBody> response = responseBodyCall.execute();
            if (response.isSuccessful()) {
                updateSurveyAnswerModel(surveyAnswerModel);
            }
        } catch (Exception e) {
            Log.i("JSON", "ERROR");
            e.printStackTrace();
        }
    }

    public void uploadSurveyAnswer(SurveyAnswerModel surveyAnswerModel) {
        surveyAnswerModel.setBackendid(null);
        Call<ResponseBody> responseBodyCall = apiService.setSurveyAnswer(userSessionManager.getAccessToken(), surveyAnswerModel);
        try {
            ResponseBody responseBody = responseBodyCall.execute().body();
            JSONObject jsonObject = new JSONObject(responseBody.string());
            int id = jsonObject.getInt("id");
            Log.i("JSON SURVEY", jsonObject.toString());
            updateSurveyAnswerModel(surveyAnswerModel);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateSurveyAnswerModel(SurveyAnswerModel surveyAnswerModel) {
        surveyAnswerModel.setPushed(true);
        surveyAnswerModel.setInserted(false);
        SurveyAnswerDao surveyAnswerDao = oaasDatabase.provideSurveyAnswerDao();
        surveyAnswerDao.updateSurveyAnswer(surveyAnswerModel);

        if (getCount(surveyAnswerDao) > 0) {
            SurveyAnswerModel surveyAnswerToPush = pushDataDao.provideSurveyAnswer(false);
            if(surveyAnswerToPush.getInserted()!=null &&surveyAnswerToPush.getInserted()){
                uploadSurveyAnswer(pushDataDao.provideSurveyAnswer(false));
            }else{
                updateSurveyAnswers(surveyAnswerToPush);
            }
//            uploadSurveyAnswer(surveyAnswerToPush);
        }
    }

    private void uploadWorkFlowHeader(WorkFlowHeader workFlowHeader) {

        workFlowHeader.setServerId(null);

        Call<ResponseBody> responseBodyCall = apiService.setWorkFlowHeader(userSessionManager.getAccessToken(), workFlowHeader);
        try {
            ResponseBody responseBody = responseBodyCall.execute().body();
            JSONObject jsonObject = new JSONObject(responseBody.string());
            Log.i("JSON WORK", jsonObject.toString());

            updateWorkFlowHeader(workFlowHeader);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateWorkFlowHeader(WorkFlowHeader workFlowHeader) {
        workFlowHeader.setPushed(true);
        oaasDatabase.provideWorkFlowHeaderDao().updateWorkFlowHeader(workFlowHeader);
        if (getCount(oaasDatabase.provideWorkFlowHeaderDao()) > 0) {
            uploadWorkFlowHeader(pushDataDao.provideWorkFlowHeader(false));
        }
    }

    private void uploadTodoModel(TodoModel todoModel) {
        todoModel.setServerId(null);
        Call<ResponseBody> responseBodyCall = apiService.setTodo(userSessionManager.getAccessToken(), todoModel);
        try {
            ResponseBody responseBody = responseBodyCall.execute().body();
            JSONObject jsonObject = new JSONObject(responseBody.string());
            Log.i("JSON TODO", jsonObject.toString());
            updateTodoModel(todoModel);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void UploadImages(InsertImages insertImages){
        Log.d("imagecount",""+insertImages.getImagecount());
        InsertImages();
    }

    private void updateTodoModel(TodoModel todoModel) {
        todoModel.setPushed(true);
        oaasDatabase.provideTodoModelDao().updateTodo(todoModel);
        if (getCount(oaasDatabase.provideTodoModelDao()) > 0) {
            uploadTodoModel(pushDataDao.provideTodo(false));
        }
    }

    private void getSurveyAnswer() throws IOException {
        Call<List<SurveyAnswerModel>> call = apiService.getSurveyAnswer(userSessionManager.getAccessToken());
        Response<List<SurveyAnswerModel>> response = call.execute();
        if (response.isSuccessful()){
            int count = 0;
            List<SurveyAnswerModel> responseList = response.body();
            for (SurveyAnswerModel answerModel : responseList) {
                answerModel.setLocalid(++count);
            }
            oaasDatabase.provideSurveyAnswerDao().insertAllSurveyAnswer(responseList);
        }
    }

    private void getMembers() throws IOException {
        Call<List<MemberModel>> call = apiService.getMembers(userSessionManager.getAccessToken(),Integer.valueOf(userSessionManager.getUser().getId()));
        Response<List<MemberModel>> response = call.execute();
        if (response.isSuccessful()) {
            int count = 0;
            List<MemberModel> responseList = response.body();
            for (MemberModel memberModel : responseList) {
                memberModel.setLocalId(++count);
            }
            oaasDatabase.provideMemberDao().insertAllMember(responseList);
            List<MemberModel> memberModelList = oaasDatabase.provideMemberDao().getAll();
            for (MemberModel memberModel : memberModelList) {
                Log.i("MemberModel", "" + memberModel.getSerno() + " : " + memberModel.getServerId());
            }
        }
    }

    private void getTodo() throws IOException {
        Call<List<TodoModel>> call = apiService.getTodo(userSessionManager.getAccessToken());
        Response<List<TodoModel>> response = call.execute();
        if (response.isSuccessful()) {
            TodoDao todoDao = oaasDatabase.provideTodoModelDao();
            Long[] longs = todoDao.insertAllTodoModel(response.body());
        }
    }

    private void getWorkFlowHeader() throws IOException {
        Call<List<WorkFlowHeader>> call = apiService.getWorkFlowHeader(userSessionManager.getAccessToken());
        Response<List<WorkFlowHeader>> response = call.execute();
        if (response.isSuccessful())
            oaasDatabase.provideWorkFlowHeaderDao().insertAllWorkFlowHeader(response.body());
    }

    private void InsertImages(){
        try {
            String path = Environment.getExternalStorageDirectory().toString()+"/Conserve";
            Log.d("File_path", "Path: "+path);
            File directory = new File(path);
            File[] files = directory.listFiles();
            if(directory.listFiles()!=null){
                Log.d("Files_length", "Size: "+ files.length);
                if(files.length>0){
                    for (int i = 0; i < files.length; i++)
                    {
                        Log.d("Files_name", "FileName:" + files[i].getName());
                        Log.d("Files_absolutepath", "FileAbsolutePath:" + files[i].getAbsolutePath());
                        imageFileUploader = new ImageFileUploader().execute(files[i].getAbsolutePath(),files[i].getName());
                    }
                }else{
                    isImagepushed = true;
                    Log.e("push_success","success");
                }
            } else {
                isImagepushed = true;
                Log.e("push_success","success1");
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public class ImageFileUploader extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            try {
            String filePath = params[0];
            String imagename = params[1];
            TransferManager transferManager = new TransferManager(new
                    BasicAWSCredentials("AKIA5WXQGMRO46N2JMYD",
                    "vlbKErTsNUypaT7Y3+duAyroXpvNE786Rb6x0Plt"));

            AmazonS3Client mClient = new AmazonS3Client(new BasicAWSCredentials("AKIA5WXQGMRO46N2JMYD",
                    "vlbKErTsNUypaT7Y3+duAyroXpvNE786Rb6x0Plt"));
            List<S3ObjectSummary> summaries = mClient.listObjects("converbiz").getObjectSummaries();
            String[] keys = new String[summaries.size()];
            uploadFile2 = new File(filePath);
            workerimagelength = uploadFile2.length();
            workerimagelength = workerimagelength /1024;
//            if (workerimagelength < 800) {
                workerimagecount = 1;
                Upload upload = transferManager.upload("converbiz",imagename , uploadFile2);

                    //Or you can block and wait for the upload to finish
                    upload.waitForCompletion();
                    if (upload.isDone() == true) {
                        Str_WorkerImageUpload = "Uploaded";
                    } else {
                        Str_WorkerImageUpload = "Not Uploaded";
                    }
                    System.out.println("Upload complete.");
                } catch (AmazonClientException amazonClientException) {
                    isImagepushed=true;
                    System.out.println("Unable to upload file, upload was aborted.");
                    amazonClientException.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                imageFileUploader.cancel(true);
                }
//            }
            return null;
        }
        protected void onPostExecute(String result) {
            try {
                if (workerimagecount == 1) {
                    Log.e("Imageprocess","Success");
                    uploadFile2.delete();
                    Str_WorkerImageUpload="";
                    workerimagecount=0;
                    InsertImages();
                } else {
                    Log.e("ImageSize","Image Size is more than 800kb");
                    uploadFile2.delete();
                    Str_WorkerImageUpload="";
                    workerimagecount=0;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
