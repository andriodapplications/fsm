package com.app.fsm.ui.document;

import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.app.fsm.local_network.network.model.survey.SurveyAnswerModel;
import com.app.fsm.local_network.repository.OaasDatabase;
import com.app.fsm.local_network.repository.dao.SurveyAnswerDao;
import com.app.fsm.ui.dash.one2one.OnFetchAnswersComplete;

import java.util.List;

public final class GetAllDoucmentSurveyAnswersAsynTask extends AsyncTask<Void, Void, List<SurveyAnswerModel>> {

    private OaasDatabase oaasDatabase;
    private OnFetchAnswersComplete onFetchComplete;

    public GetAllDoucmentSurveyAnswersAsynTask(@NonNull OnFetchAnswersComplete onFetchComplete,
                                               @NonNull OaasDatabase oaasDatabase) {
        this.oaasDatabase = oaasDatabase;
        this.onFetchComplete = onFetchComplete;
    }

    @Override
    protected List<SurveyAnswerModel> doInBackground(Void... params) {
        SurveyAnswerDao surveyAnswerDao= oaasDatabase.provideSurveyAnswerDao();
        return surveyAnswerDao.provideSurveyAnswer();
    }

    @Override
    protected void onPostExecute(List<SurveyAnswerModel> surveyAnswerModels) {
        if(onFetchComplete !=null)
            onFetchComplete.onCompleteSurvey(surveyAnswerModels);
    }
}

