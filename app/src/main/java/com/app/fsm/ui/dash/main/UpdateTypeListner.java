package com.app.fsm.ui.dash.main;

import com.app.fsm.local_network.network.model.updatetypes.UpdateTypeModel;

import java.util.List;

public interface UpdateTypeListner {
    void FetchUpdatetypeData(List<UpdateTypeModel> updateTypes);
}
