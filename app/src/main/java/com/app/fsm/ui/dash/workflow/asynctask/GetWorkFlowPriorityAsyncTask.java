package com.app.fsm.ui.dash.workflow.asynctask;

import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.app.fsm.local_network.network.model.workflow.WorkFlowPriority;
import com.app.fsm.local_network.repository.OaasDatabase;
import com.app.fsm.local_network.repository.dao.WorkFlowPriorityDao;
import com.app.fsm.ui.dash.workflow.OnWorkFlowFetchListener;

import java.util.List;

public final class GetWorkFlowPriorityAsyncTask extends AsyncTask<Void, Void, List<WorkFlowPriority>> {

    private OaasDatabase oaasDatabase;
    private OnWorkFlowFetchListener onWorkFlowFetchListener;
    private int workFlowId;

    public GetWorkFlowPriorityAsyncTask(@NonNull OnWorkFlowFetchListener onWorkFlowFetchListener, @NonNull OaasDatabase oaasDatabase
            , int workFlowId) {
        this.oaasDatabase = oaasDatabase;
        this.onWorkFlowFetchListener = onWorkFlowFetchListener;
        this.workFlowId = workFlowId;
    }

    @Override
    protected List<WorkFlowPriority> doInBackground(Void... params) {
        WorkFlowPriorityDao workFlowPriorityDao = oaasDatabase.provideWorkFlowPriorityDao();
        return workFlowPriorityDao.provideWorkFlowPriority(workFlowId);
    }

    @Override
    protected void onPostExecute(List<WorkFlowPriority> workFlowPriorityList) {
        if (onWorkFlowFetchListener != null) {
            onWorkFlowFetchListener.onWorkFlowPriorityFetched(workFlowPriorityList);
        }
    }
}

