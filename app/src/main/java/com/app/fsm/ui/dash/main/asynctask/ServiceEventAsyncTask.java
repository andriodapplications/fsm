package com.app.fsm.ui.dash.main.asynctask;

import android.os.AsyncTask;

import androidx.annotation.NonNull;
import com.app.fsm.local_network.network.model.serviceevents.ServiceEventModel;
import com.app.fsm.local_network.network.model.serviceevents.ServiceEventModelDao;
import com.app.fsm.local_network.repository.OaasDatabase;
import com.app.fsm.ui.dash.main.ServiceEventListner;

import java.util.List;

public class ServiceEventAsyncTask extends AsyncTask<Void,Void, List<ServiceEventModel>> {

    private OaasDatabase oaasDatabase;
    private ServiceEventListner eventListner;

    public ServiceEventAsyncTask(@NonNull ServiceEventListner eventListner, @NonNull OaasDatabase oaasDatabase) {
        this.oaasDatabase = oaasDatabase;
        this.eventListner = eventListner;
    }
    @Override
    protected List<ServiceEventModel> doInBackground(Void... voids) {
        ServiceEventModelDao serviceEventModelDao=oaasDatabase.provideserviceEventModelDao();
        return serviceEventModelDao.getServiceEvent();
    }

    @Override
    protected void onPostExecute(List<ServiceEventModel> serviceEvents) {
        if (eventListner != null)
            eventListner.FetchServiceEventData(serviceEvents);
    }
}