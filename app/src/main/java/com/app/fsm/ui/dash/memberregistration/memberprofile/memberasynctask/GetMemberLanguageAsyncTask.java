package com.app.fsm.ui.dash.memberregistration.memberprofile.memberasynctask;

import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.app.fsm.local_network.network.model.member.MemberLanguageModel;
import com.app.fsm.local_network.repository.OaasDatabase;
import com.app.fsm.local_network.repository.dao.MemberLanguageDao;
import com.app.fsm.ui.dash.memberregistration.memberprofile.OnFetchCompleteListener;

import java.util.List;

public final class GetMemberLanguageAsyncTask extends AsyncTask<Void, Void, List<MemberLanguageModel>> {

    private OaasDatabase oaasDatabase;
    private OnFetchCompleteListener onFetchCompleteListener;

    public GetMemberLanguageAsyncTask(@NonNull OnFetchCompleteListener onFetchCompleteListener, @NonNull OaasDatabase oaasDatabase) {
        this.oaasDatabase = oaasDatabase;
        this.onFetchCompleteListener=onFetchCompleteListener;
    }

    @Override
    protected List<MemberLanguageModel> doInBackground(Void... params) {
        MemberLanguageDao memberLanguageDao= oaasDatabase.provideMemberLanguageDao();
        return memberLanguageDao.getAll();
    }

    @Override
    protected void onPostExecute(List<MemberLanguageModel> memberLanguageModelList) {
        if (onFetchCompleteListener != null)
            onFetchCompleteListener.onMemberLanguageComplete(memberLanguageModelList);
    }
}

