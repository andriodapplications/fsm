package com.app.fsm.ui.dash.one2one;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.List;

public class One2OneViewPagerAdapter extends FragmentStatePagerAdapter {

    private List<Fragment> fragmentList;

    public One2OneViewPagerAdapter(FragmentManager fm, List<Fragment> fragmentList) {
        super(fm);
        this.fragmentList=fragmentList;
    }

    public  Fragment getFragment(int pos){
        return fragmentList!=null?fragmentList.get(pos):null;
    }

    @Override
    public Fragment getItem(int i) {
        return fragmentList.get(i);
    }

    @Override
    public int getCount() {
        return fragmentList.size();
    }


}
