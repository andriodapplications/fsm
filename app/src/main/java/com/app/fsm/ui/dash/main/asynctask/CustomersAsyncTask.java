package com.app.fsm.ui.dash.main.asynctask;

import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.app.fsm.local_network.network.model.customer.CustomerModel;
import com.app.fsm.local_network.network.model.customer.CustomerModelDao;
import com.app.fsm.local_network.repository.OaasDatabase;
import com.app.fsm.ui.dash.main.OnCustomerListener;

import java.util.List;

public final class CustomersAsyncTask extends AsyncTask<Void, Void, List<CustomerModel>> {

    private OaasDatabase oaasDatabase;
    private OnCustomerListener onCustomerListener;

    public CustomersAsyncTask(@NonNull OnCustomerListener onCustomerListener, @NonNull OaasDatabase oaasDatabase) {
            this.oaasDatabase = oaasDatabase;
            this.onCustomerListener = onCustomerListener;
    }

    @Override
    protected List<CustomerModel> doInBackground(Void... params) {
            CustomerModelDao customersDao = oaasDatabase.provideCustomersDao();
            return customersDao.getCustomerData();
    }

    @Override
    protected void onPostExecute(List<CustomerModel> customerModelList) {
            if (onCustomerListener != null)
                onCustomerListener.onCustomerFetchCompleted(customerModelList);
    }
}