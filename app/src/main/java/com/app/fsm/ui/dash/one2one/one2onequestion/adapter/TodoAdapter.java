/*
 * Developed by Avinash Kumar singh on 24/1/19 3:49 PM
 * Last Modified 21/1/19 8:26 PM.
 *
 * Copyright (c) 2019.  All rights reserved.
 */

package com.app.fsm.ui.dash.one2one.one2onequestion.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.app.fsm.R;
import com.app.fsm.local_network.network.model.todo.TodoType;

import java.util.List;

public class TodoAdapter extends BaseAdapter {

    private LayoutInflater layoutInflater;
    private List<TodoType> todoTypeList;

    public TodoAdapter(@NonNull Context context, @NonNull List<TodoType> todoTypeList) {
        this.todoTypeList = todoTypeList;
        this.layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        int count = todoTypeList.size();
        return count > 0 ? count - 1 : count;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        ItemVH itemVH;


        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.row_spinner_list, parent,false);
            itemVH = new ItemVH();
            itemVH.tvTitle = convertView.findViewById(R.id.txt_title);
            convertView.setTag(itemVH);
        } else
            itemVH = (ItemVH) convertView.getTag();
        itemVH.tvTitle.setText(todoTypeList.get(position).getName());
        return convertView;
    }

    @Nullable
    @Override
    public Object getItem(int position) {
        return todoTypeList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    static class ItemVH {
        TextView tvTitle;
    }
}