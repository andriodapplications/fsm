package com.app.fsm.ui.landingpage.login;

import com.app.fsm.local_network.network.model.usermodel.UserModel;

import java.util.List;

public interface FetchAllUserListener {
    void onFetchingAllUserCompleted(List<UserModel> userModelList);
}
