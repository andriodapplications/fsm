package com.app.fsm.ui.dash.taskList;

public interface TaskListConsts {
    String TASK_ID="id";
    String TASK_NAME="taskName";
    String FOLLOW_UPDATE="followUpdate";
    String MEMBER_NAME="memberName";
    String TASK_STATUS_ID="statusId";
    String MEMBER_ID="memberId";
    String IS_TODO_TYPE="isTodoType";
}
