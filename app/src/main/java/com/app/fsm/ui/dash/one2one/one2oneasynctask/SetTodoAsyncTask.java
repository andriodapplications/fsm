package com.app.fsm.ui.dash.one2one.one2oneasynctask;

import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.app.fsm.local_network.network.model.todo.TodoModel;
import com.app.fsm.local_network.repository.OaasDatabase;
import com.app.fsm.local_network.repository.dao.TodoDao;
import com.app.fsm.ui.dash.todo.OnTodoFetchListener;

public final class SetTodoAsyncTask extends AsyncTask<Void, Void, Long> {

    private OaasDatabase oaasDatabase;
    private TodoModel todoModel;
    private OnTodoFetchListener onTodoFetchListener;

    public SetTodoAsyncTask(@NonNull OaasDatabase oaasDatabase
            , TodoModel todoModel) {
        this.oaasDatabase = oaasDatabase;
        this.todoModel = todoModel;
    }

    public SetTodoAsyncTask(@NonNull OaasDatabase oaasDatabase
            , TodoModel todoModel, @NonNull OnTodoFetchListener onTodoFetchListener) {
        this.oaasDatabase = oaasDatabase;
        this.todoModel = todoModel;
        this.onTodoFetchListener = onTodoFetchListener;
    }

    @Override
    protected Long doInBackground(Void... params) {
        TodoDao todoDao = oaasDatabase.provideTodoModelDao();

        if (todoDao.getCount() == 0) {
            todoModel.setServerId(1);
        } else {
            TodoModel lastTodoModel= todoDao.getTodoModel();
            todoModel.setServerId(lastTodoModel.getServerId() + 1);
        }
        return todoDao.insertTodo(todoModel);
    }

    @Override
    protected void onPostExecute(Long aLong) {
        if (onTodoFetchListener != null && aLong > -1) {
            onTodoFetchListener.onTodoCreated();
        }
    }
}

