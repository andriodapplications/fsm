package com.app.fsm.ui.dash.taskList.asynctask;

import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.app.fsm.local_network.network.model.joinedtable.TaskListModel;
import com.app.fsm.local_network.repository.OaasDatabase;
import com.app.fsm.local_network.repository.dao.WorkFlowHeaderDao;
import com.app.fsm.ui.dash.taskList.OnFetchListener;

import java.util.ArrayList;
import java.util.List;

public final class GetWorkFlowTodoJoinedAsyncTask extends AsyncTask<Void, Void, List<TaskListModel>> {

    private OaasDatabase oaasDatabase;
    private OnFetchListener onFetchListener;

    public GetWorkFlowTodoJoinedAsyncTask(@NonNull OnFetchListener onFetchListener
            , @NonNull OaasDatabase oaasDatabase) {
        this.oaasDatabase = oaasDatabase;
        this.onFetchListener=onFetchListener;
    }

    @Override
    protected List<TaskListModel> doInBackground(Void... params) {
        WorkFlowHeaderDao workFlowHeaderDao = oaasDatabase.provideWorkFlowHeaderDao();

        List<TaskListModel> taskListModelListWork=workFlowHeaderDao.provideWorkTaskList();
        for(TaskListModel taskListModel:taskListModelListWork){
            taskListModel.setTodoType(false);
        }
        List<TaskListModel> taskListModelListTodo=workFlowHeaderDao.provideTodoTaskList();
        for(TaskListModel taskListModel:taskListModelListTodo){
            taskListModel.setTodoType(true);
        }
        List<TaskListModel> taskListModelList = new ArrayList<>();
        taskListModelList.addAll(taskListModelListWork);
        taskListModelList.addAll(taskListModelListTodo);
        return taskListModelList;
    }

    @Override
    protected void onPostExecute(List<TaskListModel> taskListModelList) {
        if (onFetchListener != null) {
            onFetchListener.onComplete(taskListModelList);
        }
    }
}

