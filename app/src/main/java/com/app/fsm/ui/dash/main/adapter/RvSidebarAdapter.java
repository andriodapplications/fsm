package com.app.fsm.ui.dash.main.adapter;

import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.fsm.R;
import com.app.fsm.local_network.network.model.SideBar;
import com.app.fsm.ui.dash.main.OnSideBarClickListener;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.app.fsm.utils.UserSessionManager.getUserSessionManager;

public class RvSidebarAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<SideBar> sideBarList;
    private OnSideBarClickListener onSideBarClickListener;

    public RvSidebarAdapter(List<SideBar> sideBarList, OnSideBarClickListener onSideBarClickListener) {
        this.sideBarList = sideBarList;
        this.onSideBarClickListener = onSideBarClickListener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_sidebar_layout, parent, false);
        return new RvSidebarViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, final int position) {

        if (holder instanceof RvSidebarViewHolder) {
            ((RvSidebarViewHolder) holder).bind(sideBarList.get(position), onSideBarClickListener,position);
        }
    }

    @Override
    public int getItemCount() {
        return sideBarList.size();
    }


    class RvSidebarViewHolder extends RecyclerView.ViewHolder {



        @BindView(R.id.img_sidebar_icon)
        ImageView imgSidebarIcon;
        @BindView(R.id.txt_sidebar_element)
        TextView txtSidebarElement;
        @BindView(R.id.ll_sidebar_item)
        LinearLayout llSideBarItem;

        public RvSidebarViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(final SideBar sideBar, final OnSideBarClickListener onSideBarClickListener, final int pos) {
            txtSidebarElement.setText(sideBar.getSidebarElement());
            imgSidebarIcon.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                imgSidebarIcon.setImageDrawable(imgSidebarIcon.getContext().getDrawable(sideBar.getDrawable()));
            } else {
                imgSidebarIcon.setImageResource(sideBar.getDrawable());
            }
            llSideBarItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(getUserSessionManager().getIsDataFetched()||sideBar.getSidebarElement().equalsIgnoreCase("Get Data")){
                        onSideBarClickListener.onSideBarItemCLicked(pos);
                    }else{
                        Toast.makeText(imgSidebarIcon.getContext(),"Perform Get Data to Continue...",Toast.LENGTH_LONG);
                    }
                }
            });
        }
    }


    /*public RequestBuilder<Drawable> getThumnail(Context context, String thumbnailUrl) {
        RequestBuilder<Drawable> thumbnailRequest = GlideApp
                .with(context)
                .load(thumbnailUrl);
        return thumbnailRequest;
    }


    public CircularProgressDrawable getCircularProgressDrawable(Context context) {

        CircularProgressDrawable circularProgressDrawable = new CircularProgressDrawable(context);
        circularProgressDrawable.setStrokeWidth(5f);
        circularProgressDrawable.setCenterRadius(30f);
        circularProgressDrawable.start();
        return circularProgressDrawable;

    }*/
}
