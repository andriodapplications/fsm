/*
 * Developed by Avinash Kumar singh on 24/1/19 3:49 PM
 * Last Modified 16/1/19 5:57 PM.
 *
 * Copyright (c) 2019.  All rights reserved.
 */

package com.app.fsm.ui.landingpage.verifyotp;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.app.fsm.BaseActivity;
import com.app.fsm.local_network.network.ApiService;
import com.app.fsm.utils.AppConstant;
import com.app.fsm.utils.UserSessionManager;

import butterknife.ButterKnife;

public class VerifyOtpActivity extends BaseActivity {

    private ApiService apiService;
    private ProgressDialog progressDialog;
    private UserSessionManager userSessionManager;

    public static Intent newInstance(Context context, String phoneNumber, String isFrom) {

        Intent intent = new Intent(context, VerifyOtpActivity.class);
        intent.putExtra(AppConstant.PHONE, phoneNumber);
        intent.putExtra(AppConstant.IS_FROM, isFrom);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_verify_otp);
        ButterKnife.bind(this);
    }
}
