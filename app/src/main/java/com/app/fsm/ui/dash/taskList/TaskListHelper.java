package com.app.fsm.ui.dash.taskList;

import com.app.fsm.local_network.network.model.joinedtable.TaskListModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.app.fsm.ui.dash.taskList.TaskListConsts.FOLLOW_UPDATE;
import static com.app.fsm.ui.dash.taskList.TaskListConsts.IS_TODO_TYPE;
import static com.app.fsm.ui.dash.taskList.TaskListConsts.MEMBER_ID;
import static com.app.fsm.ui.dash.taskList.TaskListConsts.MEMBER_NAME;
import static com.app.fsm.ui.dash.taskList.TaskListConsts.TASK_ID;
import static com.app.fsm.ui.dash.taskList.TaskListConsts.TASK_NAME;
import static com.app.fsm.ui.dash.taskList.TaskListConsts.TASK_STATUS_ID;

public class TaskListHelper {


    public List<HashMap<String, String>> createList(List<TaskListModel> taskListModelList) {
        List<HashMap<String, String>> dataList = new ArrayList<>();
        for (TaskListModel taskListModel : taskListModelList) {
            HashMap<String, String> hashMap = new HashMap<>();
            hashMap.put(TASK_ID, String.valueOf(taskListModel.getId()));
            hashMap.put(TASK_NAME, taskListModel.getTaskName());
            hashMap.put(FOLLOW_UPDATE, taskListModel.getFollowUpdate());
            hashMap.put(MEMBER_NAME, taskListModel.getMemberName());
            hashMap.put(TASK_STATUS_ID, String.valueOf(taskListModel.getStatusId()));
            hashMap.put(MEMBER_ID, String.valueOf(taskListModel.getMemberId()));
            hashMap.put(IS_TODO_TYPE, String.valueOf(taskListModel.getTodoType()));
            dataList.add(hashMap);
        }
        return dataList;
    }

    public List<HashMap<String, String>> filterList(List<HashMap<String, String>> list, String taskSearch,
                                                    String taskType, int taskStatusId) {
        List<HashMap<String, String>> hashMapList = new ArrayList<>(0);
        for (HashMap<String, String> map : list) {
            String taskSearch1 = map.get(MEMBER_NAME);
            String taskType1 = map.get(TASK_NAME);
            int taskStatusId1 = Integer.parseInt(map.get(TASK_STATUS_ID));
            if ((taskSearch1!=null && taskSearch1.contains(taskSearch))
                    && (taskType1!=null&&taskType1.equalsIgnoreCase(taskType))
                    && taskStatusId1== taskStatusId) {
                hashMapList.add(map);
            } else if ((taskSearch1!=null && taskSearch1.contains(taskSearch)) &&
                    (taskType1!=null&&taskType1.equalsIgnoreCase(taskType))) {
                hashMapList.add(map);
            } else if ((taskSearch1!=null && taskSearch1.contains(taskSearch)) && taskStatusId1 == taskStatusId) {
                hashMapList.add(map);
            } else if ((taskType1!=null&&taskType1.equalsIgnoreCase(taskType)) && taskStatusId1 == taskStatusId) {
                hashMapList.add(map);
            } else if ((taskSearch1!=null && taskSearch1.contains(taskSearch))) {
                hashMapList.add(map);
            } else if ((taskType1!=null&&taskType1.equalsIgnoreCase(taskType))) {
                hashMapList.add(map);
            } else if (taskStatusId1 == taskStatusId) {
                hashMapList.add(map);
            }
        }
        return hashMapList;
    }


    public List<HashMap<String, String>> filterListByName(List<HashMap<String, String>> taskListModelList, String searchTask) {
        List<HashMap<String, String>> hashMapList = new ArrayList<>();
        for (HashMap<String, String> map : taskListModelList) {
            if (map != null && map.get(MEMBER_NAME) != null) {
                String memberName = map.get(MEMBER_NAME);
                if (memberName != null && memberName.toLowerCase().contains(searchTask.toLowerCase())) {
                    hashMapList.add(map);
                }
            }
        }
        return hashMapList;
    }

    public List<HashMap<String, String>> filterListByType(List<HashMap<String, String>> taskListModelList, String taskType) {
        List<HashMap<String, String>> hashMapList = new ArrayList<>();
        for (HashMap<String, String> map : taskListModelList) {
            if (map != null && map.get(TASK_NAME) != null) {
                String taskName = map.get(TASK_NAME);
                if (taskName != null && taskName.equalsIgnoreCase(taskType)) {
                    hashMapList.add(map);
                }
            }
        }
        return hashMapList;
    }

    public List<HashMap<String, String>> filterListByStatus(List<HashMap<String, String>> taskListModelList, int statusId) {
        List<HashMap<String, String>> hashMapList = new ArrayList<>();
        for (HashMap<String, String> map : taskListModelList) {
            if (map != null && map.get(TASK_NAME) != null) {
                int taskStatusId = Integer.parseInt(map.get(TASK_STATUS_ID));
                if (taskStatusId == statusId) {
                    hashMapList.add(map);
                }
            }
        }
        return hashMapList;
    }
}
