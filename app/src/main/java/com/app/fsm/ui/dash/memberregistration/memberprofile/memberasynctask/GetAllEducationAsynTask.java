package com.app.fsm.ui.dash.memberregistration.memberprofile.memberasynctask;

import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.app.fsm.local_network.network.model.educationmodel.EducationModel;
import com.app.fsm.local_network.repository.OaasDatabase;
import com.app.fsm.local_network.repository.dao.EducationDao;
import com.app.fsm.ui.dash.memberregistration.memberprofile.OnFetchCompleteListener;

import java.util.List;

public final class GetAllEducationAsynTask extends AsyncTask<Void, Void, List<EducationModel>> {

    private OaasDatabase oaasDatabase;
    private OnFetchCompleteListener onFetchCompleteListener;

    public GetAllEducationAsynTask(@NonNull OnFetchCompleteListener onFetchCompleteListener, @NonNull OaasDatabase oaasDatabase) {
        this.oaasDatabase = oaasDatabase;
        this.onFetchCompleteListener=onFetchCompleteListener;
    }

    @Override
    protected List<EducationModel> doInBackground(Void... params) {
        EducationDao educationDao = oaasDatabase.provideEducationDao();
        return educationDao.getAll();
    }

    @Override
    protected void onPostExecute(List<EducationModel> educationModelList) {
        if (onFetchCompleteListener != null)
            onFetchCompleteListener.onEducationFetchComplete(educationModelList);
    }
}

