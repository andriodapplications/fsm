package com.app.fsm.ui.dash.main.asynctask;

import android.os.AsyncTask;
import android.support.annotation.NonNull;
import com.app.fsm.local_network.network.model.subcategory.SubCategoryModel;
import com.app.fsm.local_network.network.model.subcategory.SubCategoryModelDao;
import com.app.fsm.local_network.repository.OaasDatabase;
import com.app.fsm.ui.dash.main.OnSubCategoryListener;

import java.util.List;

public class SubCategoryAsyncTask extends AsyncTask<Void,Void, List<SubCategoryModel>> {

    private OaasDatabase oaasDatabase;
    private OnSubCategoryListener onSubCategoryListener;

    public SubCategoryAsyncTask(@NonNull OnSubCategoryListener onSubCategoryListener, @NonNull OaasDatabase oaasDatabase) {
        this.oaasDatabase = oaasDatabase;
        this.onSubCategoryListener = onSubCategoryListener;
    }

    @Override
    protected List<SubCategoryModel> doInBackground(Void... voids) {
        SubCategoryModelDao subcategoryModel=oaasDatabase.provideSubCategoryDao();
        return subcategoryModel.getSubCategoryData();
    }

    @Override
    protected void onPostExecute(List<SubCategoryModel> subCategoryModels) {
        if (onSubCategoryListener != null)
            onSubCategoryListener.onSubCategoryFetchCompleted(subCategoryModels);
    }
}
