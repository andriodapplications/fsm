package com.app.fsm.ui.dash.one2one;

import com.app.fsm.local_network.network.model.survey.SurveyAnswerModel;

import java.util.List;

public interface OnFetchAnswersComplete {
    void onCompleteSurvey(List<SurveyAnswerModel> surveyAnswerModels);
}
