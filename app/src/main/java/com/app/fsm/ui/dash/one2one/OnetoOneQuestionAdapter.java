/*
 * Developed by Avinash Kumar singh on 24/1/19 3:49 PM
 * Last Modified 21/1/19 8:26 PM.
 *
 * Copyright (c) 2019.  All rights reserved.
 */

package com.app.fsm.ui.dash.one2one;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.app.fsm.R;
import com.app.fsm.local_network.network.model.survey.SurveyQuestionModel;

import java.util.List;

public class OnetoOneQuestionAdapter extends BaseAdapter {

    private LayoutInflater layoutInflater;
    private List<SurveyQuestionModel> taskStatusModelList;

    public OnetoOneQuestionAdapter(@NonNull Context context, @NonNull List<SurveyQuestionModel> taskStatusModelList) {
        this.taskStatusModelList = taskStatusModelList;
        this.layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        //int count = taskStatusModelList.size();
        //return count > 0 ? count-1  : count;
        return taskStatusModelList.size();
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        ItemVH itemVH;

        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.row_spinner_list, parent,false);
            itemVH = new ItemVH();
            itemVH.tvTitle = convertView.findViewById(R.id.txt_title);
            convertView.setTag(itemVH);
        } else
            itemVH = (ItemVH) convertView.getTag();
        itemVH.tvTitle.setText(taskStatusModelList.get(position).getQuestion());
        //itemVH.tvTitle.setEnabled(false);
        return convertView;
    }

    @Nullable
    @Override
    public Object getItem(int position) {
        return taskStatusModelList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    static class ItemVH {
        TextView tvTitle;
    }

    @Override
    public boolean isEnabled(int position) {
        SurveyQuestionModel questionModel = (SurveyQuestionModel) getItem(position);
        if (questionModel != null) {
            /*List<String> list = Arrays.asList(questionModel.getEnabledFor().split(","));
            Log.e("lstvalues", "" + list + "," + mIntStep);
            if (list.contains(String.valueOf(mIntStep))) {
                Log.e("spiinner", "true");
                return true;
            } else {
                Log.e("spiinner", "false");
                return false;
            }*/
            return true;
        } else {
            return true;
        }
    }
}