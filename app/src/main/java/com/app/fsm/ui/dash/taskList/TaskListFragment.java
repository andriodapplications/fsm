package com.app.fsm.ui.dash.taskList;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.fsm.BaseActivity;
import com.app.fsm.BaseFragment;
import com.app.fsm.R;
import com.app.fsm.local_network.network.model.joinedtable.TaskListModel;
import com.app.fsm.local_network.network.model.joinedtable.TaskStatusModel;
import com.app.fsm.local_network.network.model.joinedtable.TaskTypeModel;
import com.app.fsm.local_network.network.model.member.MemberModel;
import com.app.fsm.ui.dash.main.DashboardActivity;
import com.app.fsm.ui.dash.taskList.adapter.TaskListRVAdapter;
import com.app.fsm.ui.dash.taskList.adapter.TaskStatusAdapter;
import com.app.fsm.ui.dash.taskList.adapter.TaskTypeAdapter;
import com.app.fsm.ui.dash.taskList.asynctask.GetTaskStatusAsyncTask;
import com.app.fsm.ui.dash.taskList.asynctask.GetTaskTypeAsyncTask;
import com.app.fsm.ui.dash.taskList.asynctask.GetWorkFlowTodoJoinedAsyncTask;
import com.app.fsm.ui.dash.todo.TodoFragment;
import com.app.fsm.ui.dash.workflow.WorkFlowFragment;
import com.app.fsm.utils.AppConstant;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import butterknife.Unbinder;

public class TaskListFragment
        extends BaseFragment
        implements OnFetchListener
        , AdapterView.OnItemSelectedListener
        , OnSearchItemClickedListener {

    @BindView(R.id.rv_task_list)
    RecyclerView rvTaskList;
    @BindView(R.id.et_search_task)
    EditText edtSearchTask;
    @BindView(R.id.iv_search_task)
    ImageView ivSearchTask;
    @BindView(R.id.spinner_task_type)
    AppCompatSpinner spinnerTaskType;
    @BindView(R.id.spinner_status)
    AppCompatSpinner spinnerStatus;
    @BindView(R.id.ll_task_search_layout)
    LinearLayout llTaskSearchLayout;
    @BindView(R.id.iv_arrow_down)
    ImageView ivDownArrow;
    @BindView(R.id.tv_ticket)
    TextView tv_ticket;
    @BindView(R.id.tv_no_data)
    TextView tvNoData;

    private Unbinder mUnbinder;
    private TaskListRVAdapter taskListRVAdapter;
    private List<HashMap<String, String>> taskListModelList, searchTaskList, taskTypeList, taskStatusList;
    private MemberModel memberModel;
    private String searchTask = "", taskType = "", taskStatus = "";
    private TaskListHelper taskListHelper;
    private boolean isFragmentInBackground = false;
    private boolean isBackSpaceClicked = false;

    public static TaskListFragment newInstance(MemberModel memberModel) {
        TaskListFragment taskListFragment = new TaskListFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(AppConstant.MEMBER_MODEL, memberModel);
        taskListFragment.setArguments(bundle);
        return taskListFragment;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_task_list, container, false);
        mUnbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
        taskListHelper = new TaskListHelper();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (getArguments() != null) {
            memberModel = (MemberModel) getArguments().getSerializable(AppConstant.MEMBER_MODEL);
        }
    }


    public void init() {
        setTitle("Task List");
        new GetWorkFlowTodoJoinedAsyncTask(this, provideOaasDatabase()).execute();
        new GetTaskTypeAsyncTask(this, provideOaasDatabase()).execute();
        new GetTaskStatusAsyncTask(this, provideOaasDatabase()).execute();
        taskListRVAdapter = new TaskListRVAdapter(this);
        rvTaskList.setAdapter(taskListRVAdapter);
        rvTaskList.setLayoutManager(new LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false));
        ivDownArrow.setTag(false);
    }

    @OnTextChanged(R.id.et_search_task) void onTextChanged(Editable s,int count,int before){
        if (count < before) {
            isBackSpaceClicked = true;
        } else {
            isBackSpaceClicked = false;
        }

        if (isBackSpaceClicked && spinnerTaskType.getAdapter()!=null){
            spinnerTaskType.setSelection(spinnerTaskType.getAdapter().getCount());
            taskTypeList=null;
        }

        if (isBackSpaceClicked && spinnerStatus.getAdapter() != null) {
            spinnerStatus.setSelection(spinnerStatus.getAdapter().getCount());
            taskStatus=null;
        }

        if (taskListModelList != null) {
            searchTask = s.toString();
            if (searchTask.isEmpty()) {
                searchTaskList = taskListHelper.filterListByName(taskListModelList, searchTask);

            } else {
                if (taskTypeList != null && taskTypeList.size() > 0) {
                    searchTaskList = taskListHelper.filterListByName(taskTypeList, searchTask);
                } else if (taskStatusList != null && taskStatusList.size() > 0) {
                    searchTaskList = taskListHelper.filterListByName(taskStatusList, searchTask);
                } else {
                    searchTaskList = taskListHelper.filterListByName(taskListModelList, searchTask);
                }
            }
            setFilteredValue(searchTaskList, true);
        }


    }

    private void setRecyclerViews(List<TaskListModel> taskListModelList) {
        this.taskListModelList = taskListHelper.createList(taskListModelList);
        taskListRVAdapter.updateList(this.taskListModelList);
    }

    @OnClick(R.id.iv_arrow_down)
    void onExpandCollapseClicked() {
        Boolean isVisible = (Boolean) ivDownArrow.getTag();
        if (isVisible) {
            llTaskSearchLayout.setVisibility(View.GONE);
            ivDownArrow.setRotation(0f);
        } else {
            llTaskSearchLayout.setVisibility(View.VISIBLE);
            ivDownArrow.setRotation(180f);
        }
        ivDownArrow.setTag(!isVisible);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        isFragmentInBackground = true;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    /**
     * This method is called when workflowheader and todos table is joined to fetch the JOIN data
     */

    @Override
    public void onComplete(List<TaskListModel> taskListModelList) {
        setRecyclerViews(taskListModelList);

        if (memberModel != null) {
            edtSearchTask.setText(memberModel.getDispatchno());
            setFilteredValue(this.taskListModelList, false);
        }
    }

    /**
     * This method provide the Todos type and workflow type combined in list
     */

    @Override
    public void onTaskTypeList(List<TaskTypeModel> taskTypeModelList) {
        spinnerTaskType.setOnItemSelectedListener(this);
        spinnerTaskType.setAdapter(new TaskTypeAdapter(requireContext(), getTaskList(taskTypeModelList)));
        spinnerTaskType.setSelection(spinnerTaskType.getAdapter().getCount());

    }

    @Override
    public void onTaskStatusFetched(List<TaskStatusModel> taskStatusModelList) {
        spinnerStatus.setOnItemSelectedListener(this);
        spinnerStatus.setAdapter(new TaskStatusAdapter(requireContext(), getStatusList(taskStatusModelList)));
        spinnerStatus.setSelection(spinnerStatus.getAdapter().getCount());
    }


    public List<TaskStatusModel> getStatusList(List<TaskStatusModel> taskStatusModelList) {
        TaskStatusModel taskStatusModel = new TaskStatusModel();
        taskStatusModel.setStatusName("Select Status");
        taskStatusModelList.add(taskStatusModel);
        return taskStatusModelList;
    }

    public List<TaskTypeModel> getTaskList(List<TaskTypeModel> taskTypeModelList) {
        TaskTypeModel taskTypeModel = new TaskTypeModel();
        taskTypeModel.setName("Select Type");
        taskTypeModelList.add(taskTypeModel);
        return taskTypeModelList;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (parent.getAdapter() instanceof TaskTypeAdapter) {
            TaskTypeModel taskTypeModel = (TaskTypeModel) ((TaskTypeAdapter) parent.getAdapter()).getItem(position);
            if (!isFragmentInBackground) {
                if (taskTypeModel != null && !taskTypeModel.getName().contains("Select Type")) {
                    if (!searchTask.isEmpty() && searchTaskList.size() > 0) {
                        taskTypeList = taskListHelper.filterListByType(searchTaskList, taskTypeModel.getName());
                        setFilteredValue(taskTypeList, true);
                    } else if (taskStatusList != null && taskStatusList.size() > 0) {
                        taskTypeList = taskListHelper.filterListByType(taskStatusList, taskTypeModel.getName());
                        setFilteredValue(taskTypeList, false);
                    } else {
                        taskTypeList = taskListHelper.filterListByType(taskListModelList, taskTypeModel.getName());
                        setFilteredValue(taskTypeList, false);
                    }
                }
            }
        }

        if (parent.getAdapter() instanceof TaskStatusAdapter) {
            TaskStatusModel taskStatusModel = (TaskStatusModel) parent.getAdapter().getItem(position);
            if (!isFragmentInBackground) {
                if (taskStatusModel != null && !taskStatusModel.getStatusName().contains("Select Status")) {
                    if (!searchTask.isEmpty() && searchTaskList.size() > 0) {
                        taskStatusList = taskListHelper.filterListByStatus(searchTaskList, taskStatusModel.getStatusId());
                        setFilteredValue(taskStatusList, true);
                    } else if (taskTypeList != null && taskTypeList.size() > 0) {
                        taskStatusList = taskListHelper.filterListByStatus(taskTypeList, taskStatusModel.getStatusId());
                        setFilteredValue(taskStatusList, false);
                    } else {
                        taskStatusList = taskListHelper.filterListByStatus(taskListModelList, taskStatusModel.getStatusId());
                        setFilteredValue(taskStatusList, false);
                    }
                }
            }
        }
    }

    public void setFilteredValue(List<HashMap<String, String>> filteredValues, Boolean isFrom) {
        if (filteredValues.isEmpty()) {
            if (!isFrom) {
                edtSearchTask.setText("");
            }
            showEmptyLayout(true);
        } else
            showEmptyLayout(false);
        taskListRVAdapter.updateList(filteredValues);
    }

    private void showEmptyLayout(boolean show) {
        if (show) {
            rvTaskList.setVisibility(View.GONE);
            //tvNoData.setVisibility(View.VISIBLE);
            tv_ticket.setVisibility(View.VISIBLE);
            if(memberModel!=null){
                tv_ticket.setText(memberModel.getDispatchno()+"          "+"Completed");
            }
        } else {
            rvTaskList.setVisibility(View.VISIBLE);
            //tvNoData.setVisibility(View.GONE);
            tv_ticket.setVisibility(View.GONE);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void provideTaskListModel(TaskListModel taskListModel) {
        edtSearchTask.clearFocus();
        edtSearchTask.setText("");
        if (requireActivity() instanceof DashboardActivity) {
            BaseActivity baseActivity = (DashboardActivity) requireActivity();
            if (taskListModel.getTodoType())
                baseActivity.openFragment(TodoFragment.newInstance(true, taskListModel)
                        , TodoFragment.class.getSimpleName());
            else
                baseActivity.openFragment(WorkFlowFragment.newInstance(true, null, taskListModel)
                        , WorkFlowFragment.class.getSimpleName());
        }
    }

}
