package com.app.fsm.ui.dash.memberregistration.memberprofile.memberasynctask;

import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.app.fsm.local_network.network.model.gendermodel.GenderModel;
import com.app.fsm.local_network.repository.OaasDatabase;
import com.app.fsm.local_network.repository.dao.GenderDao;
import com.app.fsm.ui.dash.memberregistration.memberprofile.OnFetchCompleteListener;

import java.util.List;

public final class GetAllGenderAsynTask extends AsyncTask<Void, Void, List<GenderModel>> {

    private OaasDatabase oaasDatabase;
    private OnFetchCompleteListener onFetchCompleteListener;

    public GetAllGenderAsynTask(@NonNull OnFetchCompleteListener onFetchCompleteListener, @NonNull OaasDatabase oaasDatabase) {
        this.oaasDatabase = oaasDatabase;
        this.onFetchCompleteListener=onFetchCompleteListener;
    }

    @Override
    protected List<GenderModel> doInBackground(Void... params) {
        GenderDao genderDao = oaasDatabase.provideGenderDao();
        return genderDao.getAll();
    }

    @Override
    protected void onPostExecute(List<GenderModel> genderModelList) {
        if (onFetchCompleteListener != null)
            onFetchCompleteListener.onGenderFetchComplete(genderModelList);
    }
}

