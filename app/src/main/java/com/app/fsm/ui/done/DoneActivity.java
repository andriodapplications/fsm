package com.app.fsm.ui.done;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.app.fsm.BaseActivity;
import com.app.fsm.R;
import com.app.fsm.local_network.network.model.member.MemberModel;
import com.app.fsm.local_network.repository.OaasDatabase;
import com.app.fsm.local_network.repository.OassDatabaseBuilder;
import com.app.fsm.ui.dash.main.OnMemberListener;
import com.app.fsm.ui.dash.main.asynctask.GetAllMemberAsynTask;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DoneActivity extends BaseActivity implements OnMemberListener {

    @BindView(R.id.title)
    TextView txtTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.rv_done)
    RecyclerView rvdone;


    private List<MemberModel> mMemberModelList;
    private OaasDatabase oaasDatabase;
    private Donerv_Adapter donerv_adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_done);
        ButterKnife.bind(this);

        txtTitle.setText("Done");
        toolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        oaasDatabase = OassDatabaseBuilder.provideOassDatabase(this);

        mMemberModelList=new ArrayList<>();

        donerv_adapter = new Donerv_Adapter(this);
        LinearLayoutManager manager1 = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rvdone.setAdapter(donerv_adapter);
        rvdone.setLayoutManager(manager1);
        donerv_adapter.notifyDataSetChanged();

        new GetAllMemberAsynTask(this, oaasDatabase, 0).execute();
    }

    @Override
    public void onMemberFetchCompleted(List<MemberModel> memberModelList, int pos) {
        //this.memberModelList=memberModelList;
        Log.d("memberlist",""+memberModelList.size());
        for(MemberModel model:memberModelList){
//            if(model.getRcCode()==53 || model.getRcCode()==84){
                mMemberModelList.add(model);
//            }
        }
        donerv_adapter.update(mMemberModelList);
    }
}