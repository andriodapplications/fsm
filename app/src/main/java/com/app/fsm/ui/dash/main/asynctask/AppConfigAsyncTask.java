package com.app.fsm.ui.dash.main.asynctask;

import android.os.AsyncTask;

import androidx.annotation.NonNull;

import com.app.fsm.local_network.network.model.appconfig.AppConfigModel;
import com.app.fsm.local_network.network.model.appconfig.AppConfigModelDao;
import com.app.fsm.local_network.repository.OaasDatabase;
import com.app.fsm.ui.dash.main.OnAppConfigListner;

import java.util.List;

public class AppConfigAsyncTask extends AsyncTask<Void,Void, List<AppConfigModel>> {

    private OaasDatabase oaasDatabase;
    private OnAppConfigListner appConfigListner;

    public AppConfigAsyncTask(@NonNull OnAppConfigListner appConfigListner, @NonNull OaasDatabase oaasDatabase) {
        this.oaasDatabase = oaasDatabase;
        this.appConfigListner = appConfigListner;
    }
    @Override
    protected List<AppConfigModel> doInBackground(Void... voids) {
        AppConfigModelDao categoryModelDao=oaasDatabase.provideAppconfigDao();
        return categoryModelDao.getAppconfigData();
    }

    @Override
    protected void onPostExecute(List<AppConfigModel> appConfigModels) {
        if (appConfigListner != null)
            appConfigListner.FetchAppCongigData(appConfigModels);
    }
}