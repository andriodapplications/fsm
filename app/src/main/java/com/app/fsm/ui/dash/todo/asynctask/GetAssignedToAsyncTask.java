package com.app.fsm.ui.dash.todo.asynctask;

import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.app.fsm.local_network.network.model.usermodel.UserModel;
import com.app.fsm.local_network.repository.OaasDatabase;
import com.app.fsm.local_network.repository.dao.UserDao;
import com.app.fsm.ui.dash.todo.OnTodoFetchListener;

import java.util.List;

public final class GetAssignedToAsyncTask extends AsyncTask<Void, Void, List<UserModel>> {

    private OaasDatabase oaasDatabase;
    private OnTodoFetchListener onTodoFetchListener;

    public GetAssignedToAsyncTask(@NonNull OnTodoFetchListener onTodoFetchListener
            , @NonNull OaasDatabase oaasDatabase) {
        this.oaasDatabase = oaasDatabase;
        this.onTodoFetchListener = onTodoFetchListener;
    }

    @Override
    protected List<UserModel> doInBackground(Void... params) {
        UserDao userDao = oaasDatabase.provideUserDao();
        return userDao.getAll();
    }

    @Override
    protected void onPostExecute(List<UserModel> userModelList) {
        if (onTodoFetchListener != null) {
            onTodoFetchListener.assignToFetched(userModelList);
        }
    }
}

