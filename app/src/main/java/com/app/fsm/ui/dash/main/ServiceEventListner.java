package com.app.fsm.ui.dash.main;

import com.app.fsm.local_network.network.model.serviceevents.ServiceEventModel;

import java.util.List;

public interface ServiceEventListner {
    void FetchServiceEventData(List<ServiceEventModel> serviceEvents);
}
