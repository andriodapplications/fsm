package com.app.fsm.ui.dash.workflow;

import com.app.fsm.local_network.network.model.usermodel.UserModel;
import com.app.fsm.local_network.network.model.workflow.WorkFlowCategory;
import com.app.fsm.local_network.network.model.workflow.WorkFlowHeader;
import com.app.fsm.local_network.network.model.workflow.WorkFlowLangauge;
import com.app.fsm.local_network.network.model.workflow.WorkFlowModel;
import com.app.fsm.local_network.network.model.workflow.WorkFlowPriority;
import com.app.fsm.local_network.network.model.workflow.WorkFlowStatus;

import java.util.List;

public interface OnWorkFlowFetchListener {

    void onWorkLanguageFetched(List<WorkFlowLangauge> workFlowLanguageList);

    void onWorkFlowFetched(List<WorkFlowModel> workFlowModelList);

    void onWorkFlowPriorityFetched(List<WorkFlowPriority> workFlowPriorityList);

    void onWorkFlowCategoryFetched(List<WorkFlowCategory> workFlowCategoryList);

    void onWorkFlowAssignedToFetched(List<UserModel> userModelList);

    void onWorkFlowStatusFetched(List<WorkFlowStatus> workFlowStatusList);

    void onWorkFlowHeaderFetched(WorkFlowHeader workFlowHeader);

    void onComplete();

    void onUpdate();
}
