package com.app.fsm.ui.dash.main.asynctask;

import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.app.fsm.local_network.network.model.usermodel.UserModel;
import com.app.fsm.local_network.repository.OaasDatabase;
import com.app.fsm.local_network.repository.dao.UserDao;
import com.app.fsm.ui.landingpage.login.FetchAllUserListener;

import java.util.List;

public class GetAllUserAsyncTask extends AsyncTask<Void,Void, List<UserModel>> {

    private OaasDatabase oaasDatabase;
    private FetchAllUserListener fetchAllUserListener;

    public GetAllUserAsyncTask(@NonNull FetchAllUserListener fetchAllUserListener, @NonNull OaasDatabase oaasDatabase) {
        this.oaasDatabase = oaasDatabase;
        this.fetchAllUserListener = fetchAllUserListener;
    }

    @Override
    protected List<UserModel> doInBackground(Void... voids) {
        UserDao userDao=oaasDatabase.provideUserDao();
        return userDao.getAll();
    }

    @Override
    protected void onPostExecute(List<UserModel> userModels) {
        if (fetchAllUserListener!=null)
            fetchAllUserListener.onFetchingAllUserCompleted(userModels);
    }
}
