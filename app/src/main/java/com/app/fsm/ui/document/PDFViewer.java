package com.app.fsm.ui.document;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;


import com.app.fsm.R;
import com.app.fsm.local_network.network.model.survey.SurveyAnswerModel;
import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.listener.OnLoadCompleteListener;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PDFViewer extends AppCompatActivity {

    @BindView(R.id.title)
    TextView txtTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.pdfView)
    PDFView pdfView;

    ProgressDialog mObjDialog;

    SurveyAnswerModel answerModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_p_d_f_viewer);
        ButterKnife.bind(this);

        answerModel = (SurveyAnswerModel) getIntent().getSerializableExtra("surveyanswers");

        txtTitle.setText(answerModel.getUploadedfiles());
        toolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        Log.d("pdfurl","https://idcamp-api.herokuapp.com/api/v1/containers/converbiz/download/"+answerModel.getUploadedfiles());

        new Pdfviewer().execute("https://idcamp-api.herokuapp.com/api/v1/containers/converbiz/download/"+answerModel.getUploadedfiles());
    }

    class Pdfviewer extends AsyncTask<String, Void, InputStream> {
        @Override
        protected void onPreExecute() {
            mObjDialog = new ProgressDialog(PDFViewer.this);
            mObjDialog.setMessage("Loading pdf....");
            mObjDialog.setIndeterminate(true);
            mObjDialog.setCancelable(false);
            mObjDialog.show();
        }

        @Override
        protected InputStream doInBackground(String... params) {
            InputStream inputStream = null;
            try {
                URL url = new URL(params[0]);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                if (connection.getResponseCode() == 200) {
                    inputStream = new BufferedInputStream(connection.getInputStream());
                }
            } catch (IOException e) {
                e.printStackTrace();
                mObjDialog.dismiss();
                Toast.makeText(PDFViewer.this, "Network Error", Toast.LENGTH_SHORT).show();
                return null;
            }
            return inputStream;
        }

        @Override
        protected void onPostExecute(InputStream inputStream) {
            pdfView.fromStream(inputStream).enableSwipe(true).onLoad(new OnLoadCompleteListener() {
                @Override
                public void loadComplete(int nbPages) {
                    mObjDialog.dismiss();
                }
            }).load();
        }
    }
}