package com.app.fsm.ui.dash.workflow.asynctask;

import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.app.fsm.local_network.network.model.workflow.WorkFlowHeader;
import com.app.fsm.local_network.repository.OaasDatabase;
import com.app.fsm.local_network.repository.dao.WorkFlowHeaderDao;
import com.app.fsm.ui.dash.workflow.OnWorkFlowFetchListener;

public final class GetWorkFlowHeaderAsyncTask extends AsyncTask<Void, Void, WorkFlowHeader> {

    private OaasDatabase oaasDatabase;
    private OnWorkFlowFetchListener onWorkFlowFetchListener;
    private int id;

    public GetWorkFlowHeaderAsyncTask(@NonNull OnWorkFlowFetchListener onWorkFlowFetchListener
            , @NonNull OaasDatabase oaasDatabase, int id) {
        this.id = id;
        this.oaasDatabase = oaasDatabase;
        this.onWorkFlowFetchListener = onWorkFlowFetchListener;
    }

    @Override
    protected WorkFlowHeader doInBackground(Void... params) {
        WorkFlowHeaderDao workFlowHeaderDao = oaasDatabase.provideWorkFlowHeaderDao();
        return workFlowHeaderDao.provideWorkFlowHeader(id);
    }

    @Override
    protected void onPostExecute(WorkFlowHeader workFlowHeader) {
        if (onWorkFlowFetchListener != null) {
            onWorkFlowFetchListener.onWorkFlowHeaderFetched(workFlowHeader);
        }
    }
}

