package com.app.fsm.utils.searchdialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.app.fsm.R;
import com.app.fsm.local_network.network.model.category.CategoryModel;
import com.app.fsm.local_network.network.model.customer.CustomerModel;
import com.app.fsm.local_network.network.model.member.MemberModel;
import com.app.fsm.local_network.network.model.subcategory.SubCategoryModel;
import com.app.fsm.local_network.network.model.todo.TodoStatusModel;
import com.app.fsm.ui.dash.main.DashboardActivity;

import java.util.ArrayList;
import java.util.List;

public class SearchDialog implements OnSearchItemSelected,OnInfoItemSelectListner {

    private static final String TAG = "SearchDialog";
    private List<MemberModel> memberModelList;
    private Activity activity;
    private AlertDialog alertDialog,alertDialog1;

    private SearchAdapter searchAdapter;
    private RecyclerView recyclerView;
    private int pos;
    private List<CategoryModel> categoryModels;
    private List<SubCategoryModel> subcategoryModels;
    private List<CustomerModel> customerModels;
    private List<TodoStatusModel> todoStatusModels;
    private List<MemberModel> mMemberModelList;

    public SearchDialog(Activity activity, List<MemberModel> memberModelList, int pos
            , List<CustomerModel> mcustomerModels, List<CategoryModel> mcategoryModels,
                        List<SubCategoryModel> msubcategoryModels,
                        List<TodoStatusModel> mtodoStatusModels) {
        List<MemberModel> filteredMemberList = new ArrayList<>();
        for (MemberModel model : memberModelList) {
            if(model.getStatusId()!=5){
                filteredMemberList.add(model);
            }
        }
        this.memberModelList = filteredMemberList;
        this.activity = activity;
        this.pos = pos;
        categoryModels=mcategoryModels;
        customerModels=mcustomerModels;
        subcategoryModels=msubcategoryModels;
        todoStatusModels=mtodoStatusModels;
    }

    public void show() {
        Log.d("customer",""+customerModels.size());
        Log.d("category",""+categoryModels.size());
        Log.d("todostatus",""+todoStatusModels.size());
        for(MemberModel model:memberModelList){
            for(CustomerModel customerModel:customerModels){
                if(model.getCustomerId()==customerModel.getId()){
                    model.setCustomerMyName(customerModel.getName());
                }
            }
        }

        for(MemberModel model:memberModelList){
            for(CategoryModel categoryModel:categoryModels){
                if(model.getCategoryId()==categoryModel.getId()){
                    model.setCategoryName(categoryModel.getName());
                }
            }
        }

        for(MemberModel model:memberModelList){
            for(SubCategoryModel subCategoryModel:subcategoryModels){
                if(model.getSubcategoryId()==subCategoryModel.getId()){
                    model.setSubcategoryName(subCategoryModel.getName());
                }
            }
        }

        for(MemberModel model:memberModelList){
            for(TodoStatusModel todoStatusModel:todoStatusModels){
                if(model.getStatusId()==todoStatusModel.getId()){
                    model.setTodoStatusName(todoStatusModel.getName());
                }
            }
        }

        final AlertDialog.Builder adb = new AlertDialog.Builder(activity);
        View view = activity.getLayoutInflater().inflate(R.layout.search_dialog_layout, null);
        recyclerView =  view.findViewById(R.id.recyclerview);

        final EditText edtSearch =  view.findViewById(R.id.searchBox);
        adb.setView(view);
        alertDialog = adb.create();

        //Full screen but with bug
        ViewGroup.LayoutParams lp = alertDialog.getWindow().getAttributes();
        lp.width=WindowManager.LayoutParams.MATCH_PARENT;
        lp.height=WindowManager.LayoutParams.MATCH_PARENT;

        alertDialog.getWindow().setAttributes((android.view.WindowManager.LayoutParams)lp);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setCanceledOnTouchOutside(true);
        alertDialog.setCancelable(true);

        //Setting Recycler view
        searchAdapter = new SearchAdapter( activity,this,this);
        recyclerView.setLayoutManager(new LinearLayoutManager(activity,LinearLayoutManager.VERTICAL,false));
        recyclerView.setAdapter(searchAdapter);
        searchAdapter.update(memberModelList);

        edtSearch.setInputType(InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
        edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                //Setting filter
                List<MemberModel> filteredValues = new ArrayList<>();
                for (int i = 0; i < memberModelList.size(); i++) {
                    if (memberModelList.get(i) != null) {
                        MemberModel item = memberModelList.get(i);
                        if (item.getDispatchno()!=null&&item.getDispatchno().toLowerCase().trim().contains(editable.toString().toLowerCase().trim())) {
                            filteredValues.add(item);
                        }
                    }
                }

                if (filteredValues.isEmpty() && editable.length() == 0)
                    searchAdapter.update(memberModelList);
                else
                    searchAdapter.update(filteredValues);
            }
        });
        alertDialog.show();
    }

    @Override
    public void onClick(MemberModel memberModel) {
//        if(memberModel.getRcCode()!=93){
            if(activity instanceof DashboardActivity)
                ((DashboardActivity) activity).onSearchMemberSelected(memberModel, 0);
            alertDialog.dismiss();
            if(alertDialog1!=null){
                alertDialog1.dismiss();
            }
//        }
    }

    @Override
    public void onInfoClick(MemberModel memberModel) {
        final AlertDialog.Builder adb = new AlertDialog.Builder(activity);
        View view = activity.getLayoutInflater().inflate(R.layout.row_info, null);
        TextView tvstatus = view.findViewById(R.id.tv_status);
        TextView tvaddress = view.findViewById(R.id.tv_address);
        TextView tvcontact = view.findViewById(R.id.tv_contact);
        adb.setView(view);
        alertDialog1 = adb.create();
        alertDialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog1.setCanceledOnTouchOutside(true);
        alertDialog1.setCancelable(true);

        tvstatus.setText(memberModel.getTodoStatusName()==null?"Status :":"Status : "+memberModel.getTodoStatusName());
        tvaddress.setText(memberModel.getServiceaddress2()==null?"Address :":"Address : "+memberModel.getServiceaddress2());
        tvcontact.setText(memberModel.getCustomercontactphno()==null?"Phone Number :":"Phone Number : "+memberModel.getCustomercontactphno());
        alertDialog1.show();
    }

    @Override
    public void onContactClick(MemberModel memberModel) {
        if(memberModel.getContactno()!=null) {
            activity.startActivity(new Intent(Intent.ACTION_DIAL).setData(Uri.parse("tel:"+memberModel.getContactno())));
        } else {
            Toast.makeText(activity, "Not Found", Toast.LENGTH_SHORT).show();
        }
    }
}
