

package com.app.fsm.utils;

import android.content.Context;
import android.util.AttributeSet;

public class CustomHintSpinner extends android.support.v7.widget.AppCompatSpinner {

    private boolean mToggleFlag = true;

    public CustomHintSpinner(Context context, AttributeSet attrs, int defStyle, int mode) {
        super(context, attrs, defStyle, mode);
    }

    public CustomHintSpinner(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public CustomHintSpinner(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomHintSpinner(Context context, int mode) {
        super(context, mode);
    }

    public CustomHintSpinner(Context context) {
        super(context);
    }

    @Override
    public int getSelectedItemPosition() {
        if (!mToggleFlag) {
            return 0; // Gets to the first element
        }
        return super.getSelectedItemPosition();
    }

    @Override
    public boolean performClick() {
        mToggleFlag = false;
        boolean result = super.performClick();
        mToggleFlag = true;
        return result;
    }


}
