package com.app.fsm.utils.searchdialog;

import com.app.fsm.local_network.network.model.member.MemberModel;

public interface OnInfoItemSelectListner {

    void onInfoClick(MemberModel memberModel);

    void onContactClick(MemberModel memberModel);
}
