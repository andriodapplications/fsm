/*
 * Developed by Avinash Kumar singh on 24/1/19 3:49 PM
 * Last Modified 16/1/19 5:57 PM.
 *
 * Copyright (c) 2019.  All rights reserved.
 */

package com.app.fsm.utils;

import com.bumptech.glide.annotation.GlideModule;
import com.bumptech.glide.module.AppGlideModule;

// new since Glide v4
@GlideModule
public final class MyAppGlideModule extends AppGlideModule {
    // leave empty for now
}
