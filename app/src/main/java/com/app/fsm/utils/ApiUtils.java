/*
 * Developed by Avinash Kumar singh on 24/1/19 3:49 PM
 * Last Modified 16/1/19 5:57 PM.
 *
 * Copyright (c) 2019.  All rights reserved.
 */

package com.app.fsm.utils;


import com.app.fsm.local_network.network.ApiService;
import com.app.fsm.local_network.network.RetrofitClient;

public class ApiUtils {

    private ApiUtils() {
    }

    public static ApiService getAPIService() {
        return RetrofitClient.getClient(ApiService.BASE_URL).create(ApiService.class);
    }
}

