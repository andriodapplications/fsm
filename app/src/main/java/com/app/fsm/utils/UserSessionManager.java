/*
 * Developed by Avinash Kumar singh on 24/1/19 3:49 PM
 * Last Modified 16/1/19 5:57 PM.
 *
 * Copyright (c) 2019.  All rights reserved.
 */

package com.app.fsm.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.app.fsm.local_network.network.model.usermodel.UserModel;
import com.google.gson.Gson;

public class UserSessionManager {


    private static final String USER_ID = "user_id";
    private static final String TTL = "ttl";
    private static volatile UserSessionManager userSessionManager;
    private String ACCESS_TOKEN = "access_token";
    private String KEY_NAME = "name";
    private String PREF_NAME = "PowerGotha";
    private String ASSIGNED_TO="assignedto";
    private SharedPreferences sharedPreferences;

    private UserSessionManager(Context context) {

        if (userSessionManager != null) {
            throw new RuntimeException("Use getInstance method");
        }
        this.sharedPreferences = context.getSharedPreferences(this.PREF_NAME, 0);
    }

    public static UserSessionManager getUserSessionManager(Context context) {

        if (userSessionManager == null) {
            synchronized (UserSessionManager.class) {
                if (userSessionManager == null) {
                    userSessionManager = new UserSessionManager(context);
                }
            }
        }
        return userSessionManager;
    }

    public static UserSessionManager getUserSessionManager() {
        return userSessionManager;
    }

    public void createSession(String userId, String token, int ttl) {
        this.sharedPreferences.edit().putString(USER_ID, userId).apply();
        this.sharedPreferences.edit().putString(ACCESS_TOKEN, token).apply();
        this.sharedPreferences.edit().putInt(TTL, ttl).apply();
    }

    public String getAccessToken(){
        return this.sharedPreferences.getString(ACCESS_TOKEN,"");
    }

    public String getUserId(){
        return this.sharedPreferences.getString(USER_ID,"");
    }

    public String getName() {
        return this.sharedPreferences.getString(KEY_NAME, "");
    }

    public void setUser(UserModel userModel){
        this.sharedPreferences.edit().putString(AppConstant.USER,new Gson().toJson(userModel)).apply();
    }

    public UserModel getUser(){
        return new Gson().fromJson(this.sharedPreferences.getString(AppConstant.USER,null),UserModel.class);
    }

    public void setDataFetchingCompleted(boolean isFetchingCompleted) {
        this.sharedPreferences.edit().putBoolean(AppConstant.IS_DATA_FETCHED,isFetchingCompleted).apply();
    }

    public boolean getIsDataFetched(){
        return sharedPreferences.getBoolean(AppConstant.IS_DATA_FETCHED,false);
    }

    public void setQRCode(String qrCode) {
        this.sharedPreferences.edit().putString(AppConstant.QR_CODE,qrCode).apply();
    }

    public String getQRCode(){
        return sharedPreferences.getString(AppConstant.QR_CODE,null);
    }

    public void setQuestionSerialNumber(Integer questionsrno) {
        this.sharedPreferences.edit().putInt(AppConstant.QUESTION_SR_NO,questionsrno).apply();
    }

    public Integer getQuestionSerialNumber(){
        return sharedPreferences.getInt(AppConstant.QUESTION_SR_NO,0);
    }

    public void setOnetoOneFlag(String value) {
        this.sharedPreferences.edit().putString(AppConstant.ONETOONE_QUESTION_FLAG,value).apply();
    }

    public String getOnetoOneFlag(){
        return sharedPreferences.getString(AppConstant.ONETOONE_QUESTION_FLAG,"F");
    }
}

