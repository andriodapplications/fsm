package com.app.fsm.utils;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;

import com.app.fsm.ui.dash.one2one.One2OneViewPagerAdapter;

public class VerticalViewPager extends ViewPager {

    private Boolean mAnimStarted = false;
    public VerticalViewPager(Context context) {
        this(context, null);
    }

    public VerticalViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    @Override
    public boolean canScrollHorizontally(int direction) {
        return false;
    }

    @Override
    public boolean canScrollVertically(int direction) {
        return super.canScrollHorizontally(direction);
    }

    private void init() {
        setPageTransformer(true, new VerticalPageTransformer());
        setOverScrollMode(View.OVER_SCROLL_NEVER);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        final boolean toIntercept = super.onInterceptTouchEvent(flipXY(ev));
        flipXY(ev);
        return toIntercept;
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        final boolean toHandle = super.onTouchEvent(flipXY(ev));
        flipXY(ev);
        return toHandle;
    }

    private MotionEvent flipXY(MotionEvent ev) {
        final float width = getWidth();
        final float height = getHeight();
        final float x = (ev.getY() / height) * width;
        final float y = (ev.getX() / width) * height;
        ev.setLocation(x, y);
        return ev;
    }

    private static final class VerticalPageTransformer implements ViewPager.PageTransformer {
        @Override
        public void transformPage(View view, float position) {
            final int pageWidth = view.getWidth();
            final int pageHeight = view.getHeight();
            if (position < -1) {
                view.setAlpha(0);
            } else if (position <= 1) {
                view.setAlpha(1);
                view.setTranslationX(pageWidth * -position);
                float yPosition = position * pageHeight;
                view.setTranslationY(yPosition);
            } else {
                view.setAlpha(0);
            }
        }
    }

//    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
//    @Override
//    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
//        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
//
//        if(!mAnimStarted && null != getAdapter()) {
//            int height = 0;
//            View child = ((One2OneViewPagerAdapter) getAdapter()).getItem(getCurrentItem()).getView();
//            if (child != null) {
//                child.measure(widthMeasureSpec, MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED));
//                height = child.getMeasuredHeight();
////                if (VersionUtils.isJellyBean() && height < getMinimumHeight()) {
////                    height = getMinimumHeight();
////                }
//            }
//
//            // Not the best place to put this animation, but it works pretty good.
//            int newHeight = MeasureSpec.makeMeasureSpec(height, MeasureSpec.EXACTLY);
//            if (getLayoutParams().height != 0 && heightMeasureSpec != newHeight) {
//                final int targetHeight = height;
//                final int currentHeight = getLayoutParams().height;
//                final int heightChange = targetHeight - currentHeight;
//
//                Animation a = new Animation() {
//                    @Override
//                    protected void applyTransformation(float interpolatedTime, Transformation t) {
//                        if (interpolatedTime >= 1) {
//                            getLayoutParams().height = targetHeight;
//                        } else {
//                            int stepHeight = (int) (heightChange * interpolatedTime);
//                            getLayoutParams().height = currentHeight + stepHeight;
//                        }
//                        requestLayout();
//                    }
//
//                    @Override
//                    public boolean willChangeBounds() {
//                        return true;
//                    }
//                };
//
//                a.setAnimationListener(new Animation.AnimationListener() {
//                    @Override
//                    public void onAnimationStart(Animation animation) {
//                        mAnimStarted = true;
//                    }
//
//                    @Override
//                    public void onAnimationEnd(Animation animation) {
//                        mAnimStarted = false;
//                    }
//
//                    @Override
//                    public void onAnimationRepeat(Animation animation) {
//                    }
//                });
//
//                a.setDuration(1000);
//                startAnimation(a);
//                mAnimStarted = true;
//            } else {
//                heightMeasureSpec = newHeight;
//            }
//        }
//
//        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
//    }
}