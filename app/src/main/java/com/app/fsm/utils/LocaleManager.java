package com.app.fsm.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;


import com.app.fsm.local_network.network.model.languagemodel.LanguageDetail;

import java.util.Locale;

public class LocaleManager {

    public static final String LANGUAGE_ENGLISH   = "en";
    private static final String LANGUAGE_KEY       = "language_key";
    private static final String LANGUAGE_ID="language_id";
    private static final String LANGUAGE_NAME="language_name";

    public static Context setLocale(Context c) {
        return updateResources(c, getLanguage(c));
    }

    public static Context setNewLocale(@NonNull Context c, LanguageDetail languageDetail) {
        persistLanguage(c, languageDetail);
        return updateResources(c, languageDetail.getLanguageCode());
    }

    public static String getLanguage(Context c) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(c);
        return prefs.getString(LANGUAGE_KEY, LANGUAGE_ENGLISH);
    }

    @SuppressLint("ApplySharedPref")
    private static void persistLanguage(@NonNull Context c, LanguageDetail languageDetail) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(c);
        prefs.edit().putString(LANGUAGE_KEY, languageDetail.getLanguageCode()).commit();
        prefs.edit().putInt(LANGUAGE_ID,languageDetail.getId()).commit();
        prefs.edit().putString(LANGUAGE_NAME,languageDetail.getName()).commit();
    }

    public static String getLanguageName(@NonNull Context c){
        SharedPreferences preferences= PreferenceManager.getDefaultSharedPreferences(c);
        return preferences.getString(LANGUAGE_NAME,"English");
    }

    public static int getLanguageId(@NonNull Context context){
        SharedPreferences preferences= PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getInt(LANGUAGE_ID,2);
    }

    private static Context updateResources(Context context, String language) {
        Locale locale = new Locale(language);
        Locale.setDefault(locale);

        Resources res = context.getResources();
        Configuration config = new Configuration(res.getConfiguration());
        if (Build.VERSION.SDK_INT >= 17) {
            config.setLocale(locale);
            context = context.createConfigurationContext(config);
        } else {
            config.locale = locale;
            res.updateConfiguration(config, res.getDisplayMetrics());
        }
        return context;
    }

    public static Locale getLocale(Resources res) {
        Configuration config = res.getConfiguration();
        return Build.VERSION.SDK_INT >= 24 ? config.getLocales().get(0) : config.locale;
    }
}