package com.app.fsm.local_network.network.model.organizationlevel;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@Entity
public class OrganizationalLevelModel implements Serializable {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("parent")
    @Expose
    private Integer parent;
    @SerializedName("language")
    @Expose
    private Integer language;
    @SerializedName("languageparent")
    @Expose
    private Boolean languageparent;
    @SerializedName("languageparentid")
    @Expose
    private Integer languageparentid;
    @SerializedName("deleteflag")
    @Expose
    private Boolean deleteflag;

    @PrimaryKey
    @NonNull
    @SerializedName("id")
    @Expose
    private Integer id;
    private final static long serialVersionUID = 5591594042946932238L;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getParent() {
        return parent;
    }

    public void setParent(Integer parent) {
        this.parent = parent;
    }

    public Integer getLanguage() {
        return language;
    }

    public void setLanguage(Integer language) {
        this.language = language;
    }

    public Boolean getLanguageparent() {
        return languageparent;
    }

    public void setLanguageparent(Boolean languageparent) {
        this.languageparent = languageparent;
    }

    public Integer getLanguageparentid() {
        return languageparentid;
    }

    public void setLanguageparentid(Integer languageparentid) {
        this.languageparentid = languageparentid;
    }

    public Boolean getDeleteflag() {
        return deleteflag;
    }

    public void setDeleteflag(Boolean deleteflag) {
        this.deleteflag = deleteflag;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

}
