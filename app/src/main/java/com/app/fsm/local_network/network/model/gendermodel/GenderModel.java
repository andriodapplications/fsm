package com.app.fsm.local_network.network.model.gendermodel;


import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@Entity
public class GenderModel implements Serializable {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("baselang")
    @Expose
    private String baselang;
    @SerializedName("lang1")
    @Expose
    private String lang1;
    @SerializedName("lang2")
    @Expose
    private String lang2;
    @SerializedName("lang3")
    @Expose
    private String lang3;
    @SerializedName("lang4")
    @Expose
    private String lang4;
    @SerializedName("lang5")
    @Expose
    private String lang5;
    @SerializedName("lang7")
    @Expose
    private String lang7;
    @SerializedName("lang6")
    @Expose
    private String lang6;
    @SerializedName("lang8")
    @Expose
    private String lang8;
    @SerializedName("lang9")
    @Expose
    private String lang9;
    @SerializedName("lang10")
    @Expose
    private String lang10;
    @SerializedName("lastmodifiedby")
    @Expose
    private Integer lastmodifiedby;
    @SerializedName("lastmodifiedtime")
    @Expose
    private String lastmodifiedtime;
    @SerializedName("lastmodifiedrole")
    @Expose
    private Integer lastmodifiedrole;
    @SerializedName("createdby")
    @Expose
    private Integer createdby;
    @SerializedName("createdtime")
    @Expose
    private String createdtime;
    @SerializedName("createdrole")
    @Expose
    private Integer createdrole;
    @SerializedName("deleteflag")
    @Expose
    private Boolean deleteflag;
    @SerializedName("parentFlag")
    @Expose
    private Boolean parentFlag;
    @SerializedName("parentId")
    @Expose
    private String parentId;

    @PrimaryKey
    @NonNull
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("language")
    @Expose
    private String language;

    private final static long serialVersionUID = 3667120187918741416L;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBaselang() {
        return baselang;
    }

    public void setBaselang(String baselang) {
        this.baselang = baselang;
    }

    public String getLang1() {
        return lang1;
    }

    public void setLang1(String lang1) {
        this.lang1 = lang1;
    }

    public String getLang2() {
        return lang2;
    }

    public void setLang2(String lang2) {
        this.lang2 = lang2;
    }

    public String getLang3() {
        return lang3;
    }

    public void setLang3(String lang3) {
        this.lang3 = lang3;
    }

    public String getLang4() {
        return lang4;
    }

    public void setLang4(String lang4) {
        this.lang4 = lang4;
    }

    public String getLang5() {
        return lang5;
    }

    public void setLang5(String lang5) {
        this.lang5 = lang5;
    }

    public String getLang6() {
        return lang6;
    }

    public void setLang6(String lang6) {
        this.lang6 = lang6;
    }

    public String getLang7() {
        return lang7;
    }

    public void setLang7(String lang7) {
        this.lang7 = lang7;
    }

    public String getLang8() {
        return lang8;
    }

    public void setLang8(String lang8) {
        this.lang8 = lang8;
    }

    public String getLang9() {
        return lang9;
    }

    public void setLang9(String lang9) {
        this.lang9 = lang9;
    }

    public String getLang10() {
        return lang10;
    }

    public void setLang10(String lang10) {
        this.lang10 = lang10;
    }

    public Integer getLastmodifiedby() {
        return lastmodifiedby;
    }

    public void setLastmodifiedby(Integer lastmodifiedby) {
        this.lastmodifiedby = lastmodifiedby;
    }

    public String getLastmodifiedtime() {
        return lastmodifiedtime;
    }

    public void setLastmodifiedtime(String lastmodifiedtime) {
        this.lastmodifiedtime = lastmodifiedtime;
    }

    public Integer getLastmodifiedrole() {
        return lastmodifiedrole;
    }

    public void setLastmodifiedrole(Integer lastmodifiedrole) {
        this.lastmodifiedrole = lastmodifiedrole;
    }

    public Integer getCreatedby() {
        return createdby;
    }

    public void setCreatedby(Integer createdby) {
        this.createdby = createdby;
    }

    public String getCreatedtime() {
        return createdtime;
    }

    public void setCreatedtime(String createdtime) {
        this.createdtime = createdtime;
    }

    public Integer getCreatedrole() {
        return createdrole;
    }

    public void setCreatedrole(Integer createdrole) {
        this.createdrole = createdrole;
    }

    public Boolean getDeleteflag() {
        return deleteflag;
    }

    public void setDeleteflag(Boolean deleteflag) {
        this.deleteflag = deleteflag;
    }

    public Boolean getParentFlag() {
        return parentFlag;
    }

    public void setParentFlag(Boolean parentFlag) {
        this.parentFlag = parentFlag;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}