package com.app.fsm.local_network.network.model.joinedtable;

public class WorkFlowTodoJoined {

    private int workflowId = -1;
    private int workFlowStatusId = -1;
    private String workFlowTaskName = "";
    private String todoTypeName = "";
    private String workFollowUpdate = "";
    private String todoFollowUpdate = "";
    private int memberId = -1;
    private String memberName;
    private int todoId;
    private int todoStatusId;


    public String getTodoTypeName() {
        return todoTypeName;
    }

    public void setTodoTypeName(String todoTypeName) {
        this.todoTypeName = todoTypeName;
    }

    public int getWorkflowId() {
        return workflowId;
    }

    public void setWorkflowId(int workflowId) {
        this.workflowId = workflowId;
    }

    public int getWorkFlowStatusId() {
        return workFlowStatusId;
    }

    public void setWorkFlowStatusId(int workFlowStatusId) {
        this.workFlowStatusId = workFlowStatusId;
    }

    public int getMemberId() {
        return memberId;
    }

    public void setMemberId(int memberId) {
        this.memberId = memberId;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public int getTodoId() {
        return todoId;
    }

    public void setTodoId(int todoId) {
        this.todoId = todoId;
    }

    public int getTodoStatusId() {
        return todoStatusId;
    }

    public void setTodoStatusId(int todoStatusId) {
        this.todoStatusId = todoStatusId;
    }

    public String getWorkFlowTaskName() {
        return workFlowTaskName;
    }

    public void setWorkFlowTaskName(String workFlowTaskName) {
        this.workFlowTaskName = workFlowTaskName;
    }

    public String getWorkFollowUpdate() {
        return workFollowUpdate;
    }

    public void setWorkFollowUpdate(String workFollowUpdate) {
        this.workFollowUpdate = workFollowUpdate;
    }

    public String getTodoFollowUpdate() {
        return todoFollowUpdate;
    }

    public void setTodoFollowUpdate(String todoFollowUpdate) {
        this.todoFollowUpdate = todoFollowUpdate;
    }

}
