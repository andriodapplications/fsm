package com.app.fsm.local_network.repository.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.app.fsm.local_network.network.model.survey.SurveyAnswerModel;

import java.util.List;

@Dao
public interface SurveyAnswerDao extends BaseDao {

    @Query("SELECT * FROM surveyanswermodel")
    List<SurveyAnswerModel> provideSurveyAnswer();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAllSurveyAnswer(List<SurveyAnswerModel> surveyAnswerModel);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    long insertSurveyModel(SurveyAnswerModel surveyAnswerModel);

    @Update
    int updateSurveyAnswer(SurveyAnswerModel surveyAnswerModel);

    @Query("SELECT COUNT(isPushed) from surveyanswermodel where isPushed=:isPushed")
    int provideSurveyAnswerCountNotPushed(boolean isPushed);

    @Query("DELETE FROM surveyanswermodel")
    void deleteAllSurveyAnswer();

    @Query("SELECT * FROM SurveyAnswerModel ORDER BY serverId DESC LIMIT 1")
    SurveyAnswerModel getsurvey();

    @Query("SELECT COUNT(*) from surveyanswermodel")
    int getCount();

}
