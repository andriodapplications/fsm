package com.app.fsm.local_network.network.model.subcategory;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.app.fsm.local_network.repository.dao.BaseDao;

import java.util.List;

@Dao
public interface SubCategoryModelDao extends BaseDao {

    @Query("SELECT * FROM subcategorymodel")
    List<SubCategoryModel> getSubCategoryData();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAllSubCategories(List<SubCategoryModel> subCategoryModels);
}
