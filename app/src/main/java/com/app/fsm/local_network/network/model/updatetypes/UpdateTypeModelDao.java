package com.app.fsm.local_network.network.model.updatetypes;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.app.fsm.local_network.repository.dao.BaseDao;

import java.util.List;

@Dao
public interface UpdateTypeModelDao extends BaseDao {
    @Query("SELECT * FROM UpdateTypeModel")
    List<UpdateTypeModel> getUpdateTypes();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertUpdateEvents(List<UpdateTypeModel> updateTypes);
}
