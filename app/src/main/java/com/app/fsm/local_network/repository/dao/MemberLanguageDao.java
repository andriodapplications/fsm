package com.app.fsm.local_network.repository.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.app.fsm.local_network.network.model.member.MemberLanguageModel;

import java.util.List;

@Dao
public interface MemberLanguageDao extends BaseDao{

    @Query("SELECT * FROM memberlanguagemodel")
    List<MemberLanguageModel> getAll();

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertAllMember(List<MemberLanguageModel> memberLanguageModelList);

}
