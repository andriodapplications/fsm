package com.app.fsm.local_network.network.model.organizationlocation;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.app.fsm.local_network.network.model.Images.InsertImages;
import com.app.fsm.local_network.network.model.organizationlocation.OrganizationLocationModel;
import com.app.fsm.local_network.repository.dao.BaseDao;

import java.util.List;

@Dao
public interface OrganizationLocationModelDao extends BaseDao {


    @Query("SELECT * FROM organizationlocationmodel")
    List<OrganizationLocationModel> getCategoryData();

    @Query("SELECT * from organizationlocationmodel where id IN (:Id)")
    List<OrganizationLocationModel> getFilteredLocations(List<String> Id);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAllCategories(List<OrganizationLocationModel> categoryModels);
}
