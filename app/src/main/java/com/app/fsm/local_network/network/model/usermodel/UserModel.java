package com.app.fsm.local_network.network.model.usermodel;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@Entity
public class UserModel implements Serializable
{

    @SerializedName("language")
    @Expose
    private String language;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("roleId")
    @Expose
    private Integer roleId;
    @SerializedName("customerId")
    @Expose
    private Integer customerId;
    @SerializedName("categoryId")
    @Expose
    private Integer categoryId;
    @SerializedName("subcategoryId")
    @Expose
    private Integer subcategoryId;
    @SerializedName("mobile")
    @Expose
    private String mobile;
    @SerializedName("deleteflag")
    @Expose
    private String deleteflag;
    @SerializedName("lastmodifiedby")
    @Expose
    private Integer lastmodifiedby;
    @SerializedName("lastmodifiedtime")
    @Expose
    private String lastmodifiedtime;
    @SerializedName("syncintime")
    @Expose
    private String syncintime;
    @SerializedName("syncouttime")
    @Expose
    private String syncouttime;
    @SerializedName("location")
    @Expose
    private Integer location;
    @SerializedName("countryCode")
    @Expose
    private String countryCode;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;
    @SerializedName("onmobile")
    @Expose
    private Boolean onmobile;
    @SerializedName("level")
    @Expose
    private Integer level;
    @SerializedName("orgStructure")
    @Expose
    private String orgStructure;
    @SerializedName("realm")
    @Expose
    private String realm;
    @SerializedName("credentials")
    @Expose
    private String credentials;
    @SerializedName("challenges")
    @Expose
    private String challenges;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("emailVerified")
    @Expose
    private Boolean emailVerified;
    @SerializedName("verificationToken")
    @Expose
    private String verificationToken;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("created")
    @Expose
    private String created;
    @SerializedName("lastUpdated")
    @Expose
    private String lastUpdated;

    @PrimaryKey(autoGenerate = true)
    @NonNull
    @SerializedName("id")
    @Expose
    private Integer id;

    @SerializedName("modifiedBy")
    @Expose
    private String modifiedBy;

    @SerializedName("groupId")
    @Expose
    private String groupId;

    private final static long serialVersionUID = 7323090097622460343L;

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public Integer getSubcategoryId() {
        return subcategoryId;
    }

    public void setSubcategoryId(Integer subcategoryId) {
        this.subcategoryId = subcategoryId;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getDeleteflag() {
        return deleteflag;
    }

    public void setDeleteflag(String deleteflag) {
        this.deleteflag = deleteflag;
    }

    public Integer getLastmodifiedby() {
        return lastmodifiedby;
    }

    public void setLastmodifiedby(Integer lastmodifiedby) {
        this.lastmodifiedby = lastmodifiedby;
    }

    public String getLastmodifiedtime() {
        return lastmodifiedtime;
    }

    public void setLastmodifiedtime(String lastmodifiedtime) {
        this.lastmodifiedtime = lastmodifiedtime;
    }

    public String getSyncintime() {
        return syncintime;
    }

    public void setSyncintime(String syncintime) {
        this.syncintime = syncintime;
    }

    public String getSyncouttime() {
        return syncouttime;
    }

    public void setSyncouttime(String syncouttime) {
        this.syncouttime = syncouttime;
    }

    public Integer getLocation() {
        return location;
    }

    public void setLocation(Integer location) {
        this.location = location;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public Boolean getOnmobile() {
        return onmobile;
    }

    public void setOnmobile(Boolean onmobile) {
        this.onmobile = onmobile;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public String getOrgStructure() {
        return orgStructure;
    }

    public void setOrgStructure(String orgStructure) {
        this.orgStructure = orgStructure;
    }

    public String getRealm() {
        return realm;
    }

    public void setRealm(String realm) {
        this.realm = realm;
    }

    public String getCredentials() {
        return credentials;
    }

    public void setCredentials(String credentials) {
        this.credentials = credentials;
    }

    public String getChallenges() {
        return challenges;
    }

    public void setChallenges(String challenges) {
        this.challenges = challenges;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean getEmailVerified() {
        return emailVerified;
    }

    public void setEmailVerified(Boolean emailVerified) {
        this.emailVerified = emailVerified;
    }

    public String getVerificationToken() {
        return verificationToken;
    }

    public void setVerificationToken(String verificationToken) {
        this.verificationToken = verificationToken;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(String lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }
}
