package com.app.fsm.local_network.repository.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.app.fsm.local_network.network.model.joinedtable.TaskListModel;
import com.app.fsm.local_network.network.model.workflow.WorkFlowHeader;

import java.util.List;

@Dao
public interface WorkFlowHeaderDao extends BaseDao{

    @Query("SELECT * FROM workflowheader")
    List<WorkFlowHeader> getAllWorkFlowModel();

    @Query("SELECT * FROM workflowheader WHERE serverId=:id")
    WorkFlowHeader provideWorkFlowHeader(int id);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAllWorkFlowHeader(List<WorkFlowHeader> workFlowModelList);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    long insertWorkFlowHeader(WorkFlowHeader workFlowHeader);

    @Update
    int updateWorkFlowHeader(WorkFlowHeader workFlowHeader);

    @Query("SELECT COUNT(isPushed) from workflowheader where isPushed=:isPushed")
    int provideWorkFlowHeaderCountNotPushed(boolean isPushed);

  /*  @Query("SELECT MemberModel.id as memberId,MemberModel.fullname as memberName" +
            ",WorkFlowHeader.workflowid as workflowId " +
            ",WorkFlowHeader.workflowstatusid as workFlowStatusId " +
            ",WorkFlowHeader.workflowname as workFlowTaskName " +
            ",WorkFlowHeader.followupdate as workFollowUpdate " +
            ",TodoModel.followupdate as  todoFollowUpdate" +
            ",TodoType.name as  todoTypeName" +
            ",TodoModel.todostatusid as todoStatusId" +
            ",TodoModel.id as todoId" +
            " FROM membermodel" +
            " LEFT JOIN workflowheader on MemberModel.id=WorkFlowHeader.memberid " +
            " LEFT JOIN todomodel on MemberModel.id=TodoModel.memberid " +
            " JOIN todotype on TodoModel.todotypeid=TodoType.id ")
    List<WorkFlowTodoJoined> provide();*/

    @Query("SELECT MemberModel.serverId as memberId,MemberModel.serno as memberName" +
            ",WorkFlowHeader.serverId as id " +
            ",WorkFlowHeader.workflowstatusid as statusId  "+
            ",WorkFlowHeader.workflowname as taskName " +
            ",WorkFlowHeader.followupdate as followUpdate " +
            "FROM workflowheader " +
            "INNER JOIN membermodel on workflowheader.memberid=membermodel.serverId ")
    List<TaskListModel> provideWorkTaskList();

    @Query("SELECT MemberModel.serverId as memberId,MemberModel.serno as memberName" +
            ",TodoModel.serverId as id " +
            ",TodoModel.todostatusid as statusId  "+
            ",TodoType.name as taskName " +
            ",TodoModel.followupdate as followUpdate " +
            "FROM todomodel " +
            "INNER JOIN membermodel on todomodel.memberid=membermodel.serverId " +
            "JOIN todotype on TodoModel.todotypeid=TodoType.id ")
    List<TaskListModel> provideTodoTaskList();

    @Query("DELETE FROM workflowheader")
    void deleteAllWorkFlowHeader();

    @Query("SELECT COUNT(*) from workflowheader")
    int getCount();

    @Query("SELECT * FROM workflowheader ORDER BY serverId DESC LIMIT 1")
    WorkFlowHeader getWorkFlowHeader();

}
