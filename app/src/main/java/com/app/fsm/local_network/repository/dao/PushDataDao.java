package com.app.fsm.local_network.repository.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;

import com.app.fsm.local_network.network.model.Images.InsertImages;
import com.app.fsm.local_network.network.model.member.MemberModel;
import com.app.fsm.local_network.network.model.survey.SurveyAnswerModel;
import com.app.fsm.local_network.network.model.todo.TodoModel;
import com.app.fsm.local_network.network.model.workflow.WorkFlowHeader;

@Dao
public interface PushDataDao {

    @Query("SELECT * from surveyanswermodel where isPushed=:isPushed LIMIT 0,1")
    SurveyAnswerModel provideSurveyAnswer(boolean isPushed);

    @Query("SELECT * from todomodel where isPushed=:isPushed LIMIT 0,1")
    TodoModel provideTodo(boolean isPushed);

    @Query("SELECT * from workflowheader where isPushed=:isPushed LIMIT 0,1")
    WorkFlowHeader provideWorkFlowHeader(boolean isPushed);

    @Query("SELECT * from membermodel where isPushed=:isPushed LIMIT 0,1")
    MemberModel provideMember(boolean isPushed);

    @Query("SELECT * from insertimages where isPushed=:isPushed LIMIT 0,1")
    InsertImages provideImages(boolean isPushed);

}
