package com.app.fsm.local_network.repository.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.app.fsm.local_network.network.model.survey.One2OneLanguage;
import com.app.fsm.local_network.network.model.survey.SurveyQuestionModel;

import java.util.List;

@Dao
public interface SurveyQuestionDao {

    @Query("SELECT * FROM surveyquestionmodel")
    List<SurveyQuestionModel> provideSurveyQuestion();

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertAllSurveyQuestion(List<SurveyQuestionModel> surveyQuestionModelList);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    long insertSurveyQuestion(SurveyQuestionModel surveyQuestionModel);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertOne2OneQuestionLanguage(List<One2OneLanguage> one2OneLanguageList);

    @Query("SELECT * from one2onelanguage")
    List<One2OneLanguage> provideOne2One();

}
