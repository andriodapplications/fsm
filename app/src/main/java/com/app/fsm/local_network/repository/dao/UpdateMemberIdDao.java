package com.app.fsm.local_network.repository.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;

@Dao
public interface UpdateMemberIdDao extends BaseDao {

    @Query("UPDATE workflowheader SET memberid=:newId,isPushed=:isPushed WHERE memberid=:oldId")
    void updateWorkFlowHeaderMemberId(int newId, long oldId, boolean isPushed);

    @Query("UPDATE surveyanswermodel SET memberid=:newId,isPushed=:isPushed WHERE memberid=:oldId")
    void updateSurveyAnswerMemberId(int newId, long oldId, boolean isPushed);

    @Query("UPDATE todomodel SET memberid=:newId,isPushed=:isPushed WHERE memberid=:oldId")
    void updateTodoMemberId(int newId, long oldId, boolean isPushed);

}
