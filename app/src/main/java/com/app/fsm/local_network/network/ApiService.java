package com.app.fsm.local_network.network;

import com.app.fsm.local_network.network.model.appconfig.AppConfigModel;
import com.app.fsm.local_network.network.model.category.CategoryModel;
import com.app.fsm.local_network.network.model.charts.ChartModel;
import com.app.fsm.local_network.network.model.customer.CustomerModel;
import com.app.fsm.local_network.network.model.educationmodel.EducationModel;
import com.app.fsm.local_network.network.model.gendermodel.GenderModel;
import com.app.fsm.local_network.network.model.login.LoginRequest;
import com.app.fsm.local_network.network.model.login.LoginResponse;
import com.app.fsm.local_network.network.model.member.MemberLanguageModel;
import com.app.fsm.local_network.network.model.member.MemberModel;
import com.app.fsm.local_network.network.model.occupationmodel.OccupationModel;
import com.app.fsm.local_network.network.model.one_one_question.QuestionTypeModel;
import com.app.fsm.local_network.network.model.organizationlevel.OrganizationalLevelModel;
import com.app.fsm.local_network.network.model.organizationlocation.OrganizationLocationModel;
import com.app.fsm.local_network.network.model.serviceevents.ServiceEventModel;
import com.app.fsm.local_network.network.model.subcategory.SubCategoryModel;
import com.app.fsm.local_network.network.model.survey.One2OneLanguage;
import com.app.fsm.local_network.network.model.survey.SurveyAnswerModel;
import com.app.fsm.local_network.network.model.survey.SurveyQuestionModel;
import com.app.fsm.local_network.network.model.todo.TodoLanguage;
import com.app.fsm.local_network.network.model.todo.TodoModel;
import com.app.fsm.local_network.network.model.todo.TodoStatusModel;
import com.app.fsm.local_network.network.model.todo.TodoType;
import com.app.fsm.local_network.network.model.updatetypes.UpdateTypeModel;
import com.app.fsm.local_network.network.model.usermodel.UserModel;
import com.app.fsm.local_network.network.model.workflow.WorkFlowCategory;
import com.app.fsm.local_network.network.model.workflow.WorkFlowHeader;
import com.app.fsm.local_network.network.model.workflow.WorkFlowLangauge;
import com.app.fsm.local_network.network.model.workflow.WorkFlowModel;
import com.app.fsm.local_network.network.model.workflow.WorkFlowPriority;
import com.app.fsm.local_network.network.model.workflow.WorkFlowStatus;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Streaming;
import retrofit2.http.Url;

public interface ApiService {

    //String BASE_URL = "https://oaas-api.herokuapp.com/api/v1/";

    //TODO   Test Url   https://fsm-api.herokuapp.com/api/v1/
    String BASE_URL = "https://fsm-api.herokuapp.com/api/v1/";

    @POST("users/login")
    Call<LoginResponse> login(@Body LoginRequest loginRequest);
    //members api changed to rcdetails api
    @GET("rcdetails/")
    Call<List<MemberModel>> getMembers(@Query("access_token") String accessToken, @Query("filter[where][assignedto]") Integer assignedto);

    @GET("genders/")
    Call<List<GenderModel>> getGender(@Query("access_token") String accessToken, @Query("filter[where][language]") Integer Language);

    @GET("occupations/")
    Call<List<OccupationModel>> getOccupationModel(@Query("access_token") String accessToken, @Query("filter[where][language]") Integer Language);

    @GET("educations/")
    Call<List<EducationModel>> getEducationModel(@Query("access_token") String accessToken, @Query("filter[where][language]") Integer Language);

    @GET("organisationlevels/")
    Call<List<OrganizationalLevelModel>> getOrganizationalLevel(@Query("access_token") String accessToken);

    @GET("organisationlocations/")
    Call<List<OrganizationLocationModel>> getOrganizationLocation(@Query("access_token") String accessToken);

    @GET("memberlanguages/")
    Call<List<MemberLanguageModel>> getMemberLanguage(@Query("access_token") String accessToken, @Query("filter[where][language]") Integer Language);

    @GET("onetoonetypes")
    Call<List<QuestionTypeModel>> getQuestionType(@Query("access_token") String accessToken);

    @GET("surveyquestions")
    Call<List<SurveyQuestionModel>> getSurveyQuestion(@Query("access_token") String accessToken);

    @GET("surveyanswers")
    Call<List<SurveyAnswerModel>> getSurveyAnswer(@Query("access_token") String accessToken);

    @GET("todos")
    Call<List<TodoModel>> getTodo(@Query("access_token") String accessToken);


    @GET("users/{userid}")
    Call<UserModel> getUser(@Path("userid") Integer UserId, @Query("access_token") String accessToken);

    @GET("todotypes")
    Call<List<TodoType>> getTodoType(@Query("access_token") String accessToken, @Query("filter[where][language]") Integer Language);

    @GET("onetoonelanguages/")
    Call<List<One2OneLanguage>> getOneToOneQuestionLanguage(@Query("access_token") String accessToken, @Query("filter[where][language]") Integer Language);

    @GET("workflowlanguages/")
    Call<List<WorkFlowLangauge>> getWorkFlowLangauge(@Query("access_token") String accessToken, @Query("filter[where][language]") Integer Language);


    @GET("workflowheaders")
    Call<List<WorkFlowHeader>> getWorkFlowHeader(@Query("access_token") String accessToken);

    @GET("workflowstatuses")
    Call<List<WorkFlowStatus>> getWorkFlowStatus(@Query("access_token") String accessToken, @Query("filter[where][language]") Integer Language);

    @GET("workflows")
    Call<List<WorkFlowModel>> getWorkFlows(@Query("access_token") String accessToken, @Query("filter[where][language]") Integer Language);

    @GET("workflowcategories")
    Call<List<WorkFlowCategory>> getWorkFlowCategory(@Query("access_token") String accessToken, @Query("filter[where][language]") Integer Language);

    @GET("workflowpriorities")
    Call<List<WorkFlowPriority>> getWorkFlowPriority(@Query("access_token") String accessToken, @Query("filter[where][language]") Integer Language);

    @GET("users")
    Call<List<UserModel>> getUserList(@Query("access_token") String accessToken);

    @GET("todolanguages/")
    Call<List<TodoLanguage>> getTodoLangauges(@Query("access_token") String accessToken, @Query("filter[where][language]") Integer Language);

    @GET("todostatuses")
    Call<List<TodoStatusModel>> getTodoStatus(@Query("access_token") String accessToken, @Query("filter[where][language]") Integer Language);

    @GET("customers")
    Call<List<CustomerModel>> getCustomers(@Query("access_token") String accessToken);

    @GET("categories")
    Call<List<CategoryModel>> getCategories(@Query("access_token") String accessToken);

    @GET("subcategories")
    Call<List<SubCategoryModel>> getSubCategories(@Query("access_token") String accessToken);

    @GET("ticket_status_role_view")
    Call<List<ChartModel>> getchartdata(@Query("access_token") String accessToken, @Query("filter[where][assignedto]") Integer createdby);

    @GET("appconfigs")
    Call<List<AppConfigModel>> getAppConfigdata(@Query("access_token") String accessToken, @Query("filter[where][parameter]") Integer createdby);

    @GET("serviceevents")
    Call<List<ServiceEventModel>> getServiceEvent(@Query("access_token") String accessToken);

    @GET("updatetypes")
    Call<List<UpdateTypeModel>> getUpdateType(@Query("access_token") String accessToken);

    @GET()
    @Streaming
    Call<ResponseBody> downloadImage(@Url String fileUrl);
    /** Push data from local db to server*/


    @POST("workflowheaders")
    Call<ResponseBody> setWorkFlowHeader(@Query("access_token") String accessToken, @Body WorkFlowHeader workFlowHeader);

    @POST("todos")
    Call<ResponseBody> setTodo(@Query("access_token") String accessToken,@Body TodoModel todoModel);

    @POST("surveyanswers")
    Call<ResponseBody> setSurveyAnswer(@Query("access_token") String accessToken,
                                       @Body SurveyAnswerModel surveyAnswerModel);

    @POST("rcdetails")
    Call<ResponseBody> setMember(@Query("access_token") String accessToken, @Body MemberModel memberModel);

    @PUT("rcdetails")
    Call<ResponseBody> updateMember(@Query("access_token") String accessToken, @Body MemberModel memberModel);

    @PUT("surveyanswers")
    Call<ResponseBody> updateSurveyanswer(@Query("access_token") String accessToken, @Body SurveyAnswerModel answerModel);


}
