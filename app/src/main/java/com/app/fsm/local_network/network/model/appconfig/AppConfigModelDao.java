package com.app.fsm.local_network.network.model.appconfig;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.app.fsm.local_network.repository.dao.BaseDao;

import java.util.List;

@Dao
public interface AppConfigModelDao extends BaseDao {

    @Query("SELECT * FROM appconfigmodel")
    List<AppConfigModel> getAppconfigData();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAppconfigData(List<AppConfigModel> configModels);
}
