package com.app.fsm.local_network.network.model.charts;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity
public class ChartModel {

    @PrimaryKey
    @NonNull
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("total_open_count")
    @Expose
    private Integer totalOpenCount;
    @SerializedName("total_assign_count")
    @Expose
    private Integer totalAssignCount;
    @SerializedName("total_inprogress_count")
    @Expose
    private Integer totalInprogressCount;
    @SerializedName("total_completed_count")
    @Expose
    private Integer totalCompletedCount;
    @SerializedName("total_approval_pending_count")
    @Expose
    private Integer totalApprovalPendingCount;
    @SerializedName("cityId")
    @Expose
    private Integer cityId;
    @SerializedName("pincodeId")
    @Expose
    private Integer pincodeId;
    @SerializedName("customerId")
    @Expose
    private Integer customerId;
    @SerializedName("categoryId")
    @Expose
    private Integer categoryId;
    @SerializedName("subcategoryId")
    @Expose
    private Integer subcategoryId;
    @SerializedName("createdrole")
    @Expose
    private Integer createdrole;
    @SerializedName("createdby")
    @Expose
    private Integer createdby;
    @SerializedName("assignedto")
    @Expose
    private Integer assignedto;
    @SerializedName("todostatusId")
    @Expose
    private Integer todostatusId;
    @SerializedName("starttime")
    @Expose
    private String starttime;
    @SerializedName("fullname")
    @Expose
    private String fullname;
    @SerializedName("orgStructure")
    @Expose
    private String orgStructure;
    @SerializedName("deleteflag")
    @Expose
    private Boolean deleteflag;
    @SerializedName("approvalFlag")
    @Expose
    private Boolean approvalFlag;
    @SerializedName("assignFlag")
    @Expose
    private Boolean assignFlag;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;
    @SerializedName("customername")
    @Expose
    private String customername;
    @SerializedName("categoryname")
    @Expose
    private String categoryname;
    @SerializedName("subcategoryname")
    @Expose
    private String subcategoryname;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getTotalOpenCount() {
        return totalOpenCount;
    }

    public void setTotalOpenCount(Integer totalOpenCount) {
        this.totalOpenCount = totalOpenCount;
    }

    public Integer getTotalAssignCount() {
        return totalAssignCount;
    }

    public void setTotalAssignCount(Integer totalAssignCount) {
        this.totalAssignCount = totalAssignCount;
    }

    public Integer getTotalInprogressCount() {
        return totalInprogressCount;
    }

    public void setTotalInprogressCount(Integer totalInprogressCount) {
        this.totalInprogressCount = totalInprogressCount;
    }

    public Integer getTotalApprovalPendingCount() {
        return totalApprovalPendingCount;
    }

    public void setTotalApprovalPendingCount(Integer totalApprovalPendingCount) {
        this.totalApprovalPendingCount = totalApprovalPendingCount;
    }

    public Integer getTotalCompletedCount() {
        return totalCompletedCount;
    }

    public void setTotalCompletedCount(Integer totalCompletedCount) {
        this.totalCompletedCount = totalCompletedCount;
    }

    public Integer getCityId() {
        return cityId;
    }

    public void setCityId(Integer cityId) {
        this.cityId = cityId;
    }

    public Integer getPincodeId() {
        return pincodeId;
    }

    public void setPincodeId(Integer pincodeId) {
        this.pincodeId = pincodeId;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public Integer getSubcategoryId() {
        return subcategoryId;
    }

    public void setSubcategoryId(Integer subcategoryId) {
        this.subcategoryId = subcategoryId;
    }

    public Integer getCreatedrole() {
        return createdrole;
    }

    public void setCreatedrole(Integer createdrole) {
        this.createdrole = createdrole;
    }

    public Integer getCreatedby() {
        return createdby;
    }

    public void setCreatedby(Integer createdby) {
        this.createdby = createdby;
    }

    public Integer getAssignedto() {
        return assignedto;
    }

    public void setAssignedto(Integer assignedto) {
        this.assignedto = assignedto;
    }

    public Integer getTodostatusId() {
        return todostatusId;
    }

    public void setTodostatusId(Integer todostatusId) {
        this.todostatusId = todostatusId;
    }

    public String getStarttime() {
        return starttime;
    }

    public void setStarttime(String starttime) {
        this.starttime = starttime;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getOrgStructure() {
        return orgStructure;
    }

    public void setOrgStructure(String orgStructure) {
        this.orgStructure = orgStructure;
    }

    public Boolean getDeleteflag() {
        return deleteflag;
    }

    public void setDeleteflag(Boolean deleteflag) {
        this.deleteflag = deleteflag;
    }

    public Boolean getApprovalFlag() {
        return approvalFlag;
    }

    public void setApprovalFlag(Boolean approvalFlag) {
        this.approvalFlag = approvalFlag;
    }

    public Boolean getAssignFlag() {
        return assignFlag;
    }

    public void setAssignFlag(Boolean assignFlag) {
        this.assignFlag = assignFlag;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getCustomername() {
        return customername;
    }

    public void setCustomername(String customername) {
        this.customername = customername;
    }

    public String getCategoryname() {
        return categoryname;
    }

    public void setCategoryname(String categoryname) {
        this.categoryname = categoryname;
    }

    public String getSubcategoryname() {
        return subcategoryname;
    }

    public void setSubcategoryname(String subcategoryname) {
        this.subcategoryname = subcategoryname;
    }
}