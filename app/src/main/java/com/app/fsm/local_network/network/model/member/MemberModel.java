package com.app.fsm.local_network.network.model.member;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@Entity(indices = {@Index(value = {"serverId"},
        unique = true)})
//memebertable --> rcdetails
public class MemberModel implements Serializable {
    @SerializedName("serno")
    @Expose
    private String serno;
    @SerializedName("rcstatus")
    @Expose
    private String rcstatus;
    @SerializedName("rcCode")
    @Expose
    private Integer rcCode;
    @SerializedName("nextquestionId")
    @Expose
    private Integer nextquestionId;
    @SerializedName("prevquestionId")
    @Expose
    private Integer prevquestionId;
    @SerializedName("customername")
    @Expose
    private String customername;
    @SerializedName("customermiddlename")
    @Expose
    private String customermiddlename;
    @SerializedName("customerlastname")
    @Expose
    private String customerlastname;
    @SerializedName("customeraddress")
    @Expose
    private String customeraddress;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;
    @SerializedName("cityId")
    @Expose
    private Integer cityId;
    @SerializedName("pincodeId")
    @Expose
    private Integer pincodeId;
    @SerializedName("contactno")
    @Expose
    private String contactno;
    @SerializedName("contactpersonname")
    @Expose
    private String contactpersonname;
    @SerializedName("casedescription")
    @Expose
    private String casedescription;
    @SerializedName("customerrefnumber")
    @Expose
    private String customerrefnumber;
    @SerializedName("casenumber")
    @Expose
    private String casenumber;
    @SerializedName("dlpname")
    @Expose
    private String dlpname;
    @SerializedName("serreceivedate")
    @Expose
    private String serreceivedate;
    @SerializedName("serreceivetime")
    @Expose
    private String serreceivetime;
    @SerializedName("modelno")
    @Expose
    private String modelno;
    @SerializedName("countryzone")
    @Expose
    private String countryzone;
    @SerializedName("faxnocontactperson")
    @Expose
    private String faxnocontactperson;
    @SerializedName("fsd")
    @Expose
    private String fsd;
    @SerializedName("deferflag")
    @Expose
    private Boolean deferflag;
    @SerializedName("prosupport")
    @Expose
    private String prosupport;
    @SerializedName("keepyourharddrive")
    @Expose
    private String keepyourharddrive;
    @SerializedName("accidentaldamage")
    @Expose
    private String accidentaldamage;
    @SerializedName("attachfile")
    @Expose
    private String attachfile;
    @SerializedName("filedescription")
    @Expose
    private String filedescription;
    @SerializedName("modeldescription")
    @Expose
    private String modeldescription;
    @SerializedName("serialnoservicetag")
    @Expose
    private String serialnoservicetag;
    @SerializedName("sla")
    @Expose
    private String sla;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("dellmarks")
    @Expose
    private String dellmarks;
    @SerializedName("summary")
    @Expose
    private String summary;
    @SerializedName("worklocation")
    @Expose
    private String worklocation;
    @SerializedName("etatime")
    @Expose
    private String etatime;
    @SerializedName("engineer")
    @Expose
    private String engineer;
    @SerializedName("appointmentdatetime")
    @Expose
    private String appointmentdatetime;
    @SerializedName("callduration")
    @Expose
    private String callduration;
    @SerializedName("lob")
    @Expose
    private String lob;
    @SerializedName("hdcontactname")
    @Expose
    private String hdcontactname;
    @SerializedName("hdcontactno")
    @Expose
    private String hdcontactno;
    @SerializedName("orderno")
    @Expose
    private String orderno;
    @SerializedName("displayname")
    @Expose
    private String displayname;
    @SerializedName("partstatusatscm")
    @Expose
    private String partstatusatscm;
    @SerializedName("parthclintermediatedrpoint")
    @Expose
    private String parthclintermediatedrpoint;
    @SerializedName("partstatuscustomerplace")
    @Expose
    private String partstatuscustomerplace;
    @SerializedName("defectivepartreceived")
    @Expose
    private String defectivepartreceived;
    @SerializedName("alternativetelephoneno")
    @Expose
    private String alternativetelephoneno;
    @SerializedName("customeremialaddress")
    @Expose
    private String customeremialaddress;
    @SerializedName("alternatecontactno")
    @Expose
    private String alternatecontactno;
    @SerializedName("controlcode")
    @Expose
    private String controlcode;
    @SerializedName("controlcodedesc")
    @Expose
    private String controlcodedesc;
    @SerializedName("organizationId")
    @Expose
    private String organizationId;
    @SerializedName("lastmodifiedby")
    @Expose
    private Integer lastmodifiedby;
    @SerializedName("lastmodifiedtime")
    @Expose
    private String lastmodifiedtime;
    @SerializedName("lastmodifiedrole")
    @Expose
    private Integer lastmodifiedrole;
    @SerializedName("createdby")
    @Expose
    private Integer createdby;
    @SerializedName("createdtime")
    @Expose
    private String createdtime;
    @SerializedName("createdrole")
    @Expose
    private Integer createdrole;
    @SerializedName("deleteflag")
    @Expose
    private Boolean deleteflag;
    @SerializedName("parentFlag")
    @Expose
    private Boolean parentFlag;
    @SerializedName("parentId")
    @Expose
    private String parentId;
    @SerializedName("language")
    @Expose
    private String language;
    @SerializedName("orgstructure")
    @Expose
    private String orgstructure;
    @SerializedName("assignedto")
    @Expose
    private Integer assignedto;
    @SerializedName("completedFlag")
    @Expose
    private Boolean completedFlag;
    @SerializedName("starttime")
    @Expose
    private String starttime;
    @SerializedName("endtime")
    @Expose
    private String endtime;
    @SerializedName("rollbackStatus")
    @Expose
    private String rollbackStatus;
    @SerializedName("assignFlag")
    @Expose
    private Boolean assignFlag;
    @SerializedName("approvalFlag")
    @Expose
    private Boolean approvalFlag;
    @SerializedName("fieldassignFlag")
    @Expose
    private Boolean fieldassignFlag;
    @SerializedName("rollbackFlag")
    @Expose
    private Boolean rollbackFlag;
    @SerializedName("customerId")
    @Expose
    private Integer customerId;
    @SerializedName("categoryId")
    @Expose
    private Integer categoryId;
    @SerializedName("subcategoryId")
    @Expose
    private Integer subcategoryId;
    @SerializedName("countryId")
    @Expose
    private Integer countryId;
    @SerializedName("stateId")
    @Expose
    private Integer stateId;
    @SerializedName("statusId")
    @Expose
    private Integer statusId;
    @SerializedName("answeredQuestion")
    @Expose
    private String answeredQuestion;
    @SerializedName("dispatchno")
    @Expose
    private String dispatchno;
    @SerializedName("servicecalldate")
    @Expose
    private String  servicecalldate;
    @SerializedName("serviceprovider")
    @Expose
    private String serviceprovider;
    @SerializedName("logisticprovider")
    @Expose
    private String logisticprovider;
    @SerializedName("transport")
    @Expose
    private String transport;
    @SerializedName("dispatchstatus")
    @Expose
    private String dispatchstatus;
    @SerializedName("lobnew")
    @Expose
    private String lobnew;
    @SerializedName("sysclassification")
    @Expose
    private String sysclassification;
    @SerializedName("endservicewindow")
    @Expose
    private String endservicewindow;
    @SerializedName("servicetag")
    @Expose
    private String servicetag;
    @SerializedName("dsptype")
    @Expose
    private String dsptype;
    @SerializedName("calltype")
    @Expose
    private String calltype;
    @SerializedName("servicelevel")
    @Expose
    private String servicelevel;
    @SerializedName("customerno")
    @Expose
    private String customerno;
    @SerializedName("customernamenew")
    @Expose
    private String customernamenew;
    @SerializedName("customercontactname")
    @Expose
    private String customercontactname;
    @SerializedName("customercontactphno")
    @Expose
    private String customercontactphno;
    @SerializedName("customercontactemail")
    @Expose
    private String customercontactemail;
    @SerializedName("customersecondarycontactname")
    @Expose
    private String customersecondarycontactname;
    @SerializedName("customersecondarycontactphno")
    @Expose
    private String customersecondarycontactphno;
    @SerializedName("customersecondarycontactemail")
    @Expose
    private String customersecondarycontactemail;
    @SerializedName("country")
    @Expose
    private String country;
    @SerializedName("statenew")
    @Expose
    private String statenew;
    @SerializedName("citynew")
    @Expose
    private String citynew;
    @SerializedName("serviceaddress1")
    @Expose
    private String serviceaddress1;
    @SerializedName("serviceaddress2")
    @Expose
    private String serviceaddress2;
    @SerializedName("serviceaddress3")
    @Expose
    private String serviceaddress3;
    @SerializedName("serviceaddress4")
    @Expose
    private String serviceaddress4;
    @SerializedName("partnumber")
    @Expose
    private String partnumber;
    @SerializedName("partquantity")
    @Expose
    private String partquantity;
    @SerializedName("partstatus")
    @Expose
    private String partstatus;
    @SerializedName("partstatusdate")
    @Expose
    private String partstatusdate;
    @SerializedName("carriername")
    @Expose
    private String carriername;
    @SerializedName("waybillno")
    @Expose
    private String waybillno;
    @SerializedName("agentdescription")
    @Expose
    private String agentdescription;
    @SerializedName("closuredate")
    @Expose
    private String closuredate;
    @SerializedName("commentstovendor")
    @Expose
    private String commentstovendor;
    @SerializedName("warrantyinvoicedate")
    @Expose
    private String warrantyinvoicedate;
    @SerializedName("warrantyinvoiceno")
    @Expose
    private String warrantyinvoiceno;
    @SerializedName("warrantybillfromstate")
    @Expose
    private String warrantybillfromstate;
    @SerializedName("originalorderbuid")
    @Expose
    private String originalorderbuid;
    @SerializedName("servicepostalcode")
    @Expose
    private String servicepostalcode;
    @SerializedName("replycode")
    @Expose
    private String replycode;
    @SerializedName("engineerid")
    @Expose
    private String engineerid;
    @SerializedName("engineername")
    @Expose
    private String engineername;
    @SerializedName("producttypeid")
    @Expose
    private Integer producttypeid;
    @SerializedName("serviceeventid")
    @Expose
    private Integer serviceeventid;
    @SerializedName("serviceidentifier")
    @Expose
    private String serviceidentifier;
    @SerializedName("aspname")
    @Expose
    private String aspname;
    @SerializedName("processflag")
    @Expose
    private Boolean processflag;
    @SerializedName("couriername")
    @Expose
    private String couriername;
    @SerializedName("docket")
    @Expose
    private String docket;
    @SerializedName("dispatchdate")
    @Expose
    private String dispatchdate;

    @SerializedName("lastloginUserId")
    @Expose
    private Integer lastloginUserId;

    @SerializedName("timeslot")
    @Expose
    private String timeslot;
    @SerializedName("bandwidth")
    @Expose
    private String bandwidth;
    @SerializedName("radiususername")
    @Expose
    private String radiususername;
    @SerializedName("radiuspassword")
    @Expose
    private String radiuspassword;

    private String customerMyName;
    private String categoryName;
    private String subcategoryName;
    private String TodoStatusName;
    private String rollbackupdate;
    private Boolean updateflag;

    @SerializedName("insertFlag")
    private Boolean insertFlag;

    @SerializedName("isPushed")
    private Boolean isPushed;

    @ColumnInfo(name = "serverId")
    @SerializedName("id")
    @Expose
    private Long serverId;

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "localId")
    private int localId;


    private final static long serialVersionUID = 2980596607410047531L;

    public Boolean getPushed() {
        return isPushed;
    }

    public void setPushed(Boolean pushed) {
        isPushed = pushed;
    }

    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

    public String getRcstatus() {
        return rcstatus;
    }

    public void setRcstatus(String rcstatus) {
        this.rcstatus = rcstatus;
    }

    public String getCustomername() {
        return customername;
    }

    public void setCustomername(String customername) {
        this.customername = customername;
    }

    public String getCustomermiddlename() {
        return customermiddlename;
    }

    public void setCustomermiddlename(String customermiddlename) {
        this.customermiddlename = customermiddlename;
    }

    public String getCustomerlastname() {
        return customerlastname;
    }

    public void setCustomerlastname(String customerlastname) {
        this.customerlastname = customerlastname;
    }

    public String getCustomeraddress() {
        return customeraddress;
    }

    public void setCustomeraddress(String customeraddress) {
        this.customeraddress = customeraddress;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public Integer getCityId() {
        return cityId;
    }

    public void setCityId(Integer cityId) {
        this.cityId = cityId;
    }

    public Integer getPincodeId() {
        return pincodeId;
    }

    public void setPincodeId(Integer pincodeId) {
        this.pincodeId = pincodeId;
    }

    public String getContactno() {
        return contactno;
    }

    public void setContactno(String contactno) {
        this.contactno = contactno;
    }

    public String getContactpersonname() {
        return contactpersonname;
    }

    public void setContactpersonname(String contactpersonname) {
        this.contactpersonname = contactpersonname;
    }

    public String getCasedescription() {
        return casedescription;
    }

    public void setCasedescription(String casedescription) {
        this.casedescription = casedescription;
    }

    public String getCustomerrefnumber() {
        return customerrefnumber;
    }

    public void setCustomerrefnumber(String customerrefnumber) {
        this.customerrefnumber = customerrefnumber;
    }

    public String getCasenumber() {
        return casenumber;
    }

    public void setCasenumber(String casenumber) {
        this.casenumber = casenumber;
    }

    public String getDlpname() {
        return dlpname;
    }

    public void setDlpname(String dlpname) {
        this.dlpname = dlpname;
    }

    public String getSerreceivedate() {
        return serreceivedate;
    }

    public void setSerreceivedate(String serreceivedate) {
        this.serreceivedate = serreceivedate;
    }

    public String getSerreceivetime() {
        return serreceivetime;
    }

    public void setSerreceivetime(String serreceivetime) {
        this.serreceivetime = serreceivetime;
    }

    public String getModelno() {
        return modelno;
    }

    public void setModelno(String modelno) {
        this.modelno = modelno;
    }

    public String getCountryzone() {
        return countryzone;
    }

    public void setCountryzone(String countryzone) {
        this.countryzone = countryzone;
    }

    public String getFaxnocontactperson() {
        return faxnocontactperson;
    }

    public void setFaxnocontactperson(String faxnocontactperson) {
        this.faxnocontactperson = faxnocontactperson;
    }

    public String getFsd() {
        return fsd;
    }

    public void setFsd(String fsd) {
        this.fsd = fsd;
    }

    public Boolean getDeferflag() {
        return deferflag;
    }

    public void setDeferflag(Boolean deferflag) {
        this.deferflag = deferflag;
    }

    public String getProsupport() {
        return prosupport;
    }

    public void setProsupport(String prosupport) {
        this.prosupport = prosupport;
    }

    public String getKeepyourharddrive() {
        return keepyourharddrive;
    }

    public void setKeepyourharddrive(String keepyourharddrive) {
        this.keepyourharddrive = keepyourharddrive;
    }

    public String getAccidentaldamage() {
        return accidentaldamage;
    }

    public void setAccidentaldamage(String accidentaldamage) {
        this.accidentaldamage = accidentaldamage;
    }

    public String getAttachfile() {
        return attachfile;
    }

    public void setAttachfile(String attachfile) {
        this.attachfile = attachfile;
    }

    public String getFiledescription() {
        return filedescription;
    }

    public void setFiledescription(String filedescription) {
        this.filedescription = filedescription;
    }

    public String getModeldescription() {
        return modeldescription;
    }

    public void setModeldescription(String modeldescription) {
        this.modeldescription = modeldescription;
    }

    public String getSerialnoservicetag() {
        return serialnoservicetag;
    }

    public void setSerialnoservicetag(String serialnoservicetag) {
        this.serialnoservicetag = serialnoservicetag;
    }

    public String getSla() {
        return sla;
    }

    public void setSla(String sla) {
        this.sla = sla;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getDellmarks() {
        return dellmarks;
    }

    public void setDellmarks(String dellmarks) {
        this.dellmarks = dellmarks;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getWorklocation() {
        return worklocation;
    }

    public void setWorklocation(String worklocation) {
        this.worklocation = worklocation;
    }

    public String getEtatime() {
        return etatime;
    }

    public void setEtatime(String etatime) {
        this.etatime = etatime;
    }

    public String getEngineer() {
        return engineer;
    }

    public void setEngineer(String engineer) {
        this.engineer = engineer;
    }

    public String getAppointmentdatetime() {
        return appointmentdatetime;
    }

    public void setAppointmentdatetime(String appointmentdatetime) {
        this.appointmentdatetime = appointmentdatetime;
    }

    public String getCallduration() {
        return callduration;
    }

    public void setCallduration(String callduration) {
        this.callduration = callduration;
    }

    public String getLob() {
        return lob;
    }

    public void setLob(String lob) {
        this.lob = lob;
    }

    public String getHdcontactname() {
        return hdcontactname;
    }

    public void setHdcontactname(String hdcontactname) {
        this.hdcontactname = hdcontactname;
    }

    public String getHdcontactno() {
        return hdcontactno;
    }

    public void setHdcontactno(String hdcontactno) {
        this.hdcontactno = hdcontactno;
    }

    public String getOrderno() {
        return orderno;
    }

    public void setOrderno(String orderno) {
        this.orderno = orderno;
    }

    public String getDisplayname() {
        return displayname;
    }

    public void setDisplayname(String displayname) {
        this.displayname = displayname;
    }

    public String getPartstatusatscm() {
        return partstatusatscm;
    }

    public void setPartstatusatscm(String partstatusatscm) {
        this.partstatusatscm = partstatusatscm;
    }

    public String getParthclintermediatedrpoint() {
        return parthclintermediatedrpoint;
    }

    public void setParthclintermediatedrpoint(String parthclintermediatedrpoint) {
        this.parthclintermediatedrpoint = parthclintermediatedrpoint;
    }

    public String getPartstatuscustomerplace() {
        return partstatuscustomerplace;
    }

    public void setPartstatuscustomerplace(String partstatuscustomerplace) {
        this.partstatuscustomerplace = partstatuscustomerplace;
    }

    public String getDefectivepartreceived() {
        return defectivepartreceived;
    }

    public void setDefectivepartreceived(String defectivepartreceived) {
        this.defectivepartreceived = defectivepartreceived;
    }

    public String getAlternativetelephoneno() {
        return alternativetelephoneno;
    }

    public void setAlternativetelephoneno(String alternativetelephoneno) {
        this.alternativetelephoneno = alternativetelephoneno;
    }

    public String getCustomeremialaddress() {
        return customeremialaddress;
    }

    public void setCustomeremialaddress(String customeremialaddress) {
        this.customeremialaddress = customeremialaddress;
    }

    public String getAlternatecontactno() {
        return alternatecontactno;
    }

    public void setAlternatecontactno(String alternatecontactno) {
        this.alternatecontactno = alternatecontactno;
    }

    public String getControlcode() {
        return controlcode;
    }

    public void setControlcode(String controlcode) {
        this.controlcode = controlcode;
    }

    public String getControlcodedesc() {
        return controlcodedesc;
    }

    public void setControlcodedesc(String controlcodedesc) {
        this.controlcodedesc = controlcodedesc;
    }

    public String getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(String organizationId) {
        this.organizationId = organizationId;
    }

    public Integer getLastmodifiedby() {
        return lastmodifiedby;
    }

    public void setLastmodifiedby(Integer lastmodifiedby) {
        this.lastmodifiedby = lastmodifiedby;
    }

    public String getLastmodifiedtime() {
        return lastmodifiedtime;
    }

    public void setLastmodifiedtime(String lastmodifiedtime) {
        this.lastmodifiedtime = lastmodifiedtime;
    }

    public Integer getLastmodifiedrole() {
        return lastmodifiedrole;
    }

    public void setLastmodifiedrole(Integer lastmodifiedrole) {
        this.lastmodifiedrole = lastmodifiedrole;
    }

    public Integer getCreatedby() {
        return createdby;
    }

    public void setCreatedby(Integer createdby) {
        this.createdby = createdby;
    }

    public String getCreatedtime() {
        return createdtime;
    }

    public void setCreatedtime(String createdtime) {
        this.createdtime = createdtime;
    }

    public Integer getCreatedrole() {
        return createdrole;
    }

    public void setCreatedrole(Integer createdrole) {
        this.createdrole = createdrole;
    }

    public Boolean getDeleteflag() {
        return deleteflag;
    }

    public void setDeleteflag(Boolean deleteflag) {
        this.deleteflag = deleteflag;
    }

    public Boolean getParentFlag() {
        return parentFlag;
    }

    public void setParentFlag(Boolean parentFlag) {
        this.parentFlag = parentFlag;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getOrgstructure() {
        return orgstructure;
    }

    public void setOrgstructure(String orgstructure) {
        this.orgstructure = orgstructure;
    }

    public Integer getAssignedto() {
        return assignedto;
    }

    public void setAssignedto(Integer assignedto) {
        this.assignedto = assignedto;
    }

    public Boolean getCompletedFlag() {
        return completedFlag;
    }

    public void setCompletedFlag(Boolean completedFlag) {
        this.completedFlag = completedFlag;
    }

    public String getStarttime() {
        return starttime;
    }

    public void setStarttime(String starttime) {
        this.starttime = starttime;
    }

    public String getEndtime() {
        return endtime;
    }

    public void setEndtime(String endtime) {
        this.endtime = endtime;
    }

    public Boolean getAssignFlag() {
        return assignFlag;
    }

    public void setAssignFlag(Boolean assignFlag) {
        this.assignFlag = assignFlag;
    }

    public Boolean getApprovalFlag() {
        return approvalFlag;
    }

    public void setApprovalFlag(Boolean approvalFlag) {
        this.approvalFlag = approvalFlag;
    }

    public Boolean getFieldassignFlag() {
        return fieldassignFlag;
    }

    public void setFieldassignFlag(Boolean fieldassignFlag) {
        this.fieldassignFlag = fieldassignFlag;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public Integer getSubcategoryId() {
        return subcategoryId;
    }

    public void setSubcategoryId(Integer subcategoryId) {
        this.subcategoryId = subcategoryId;
    }

    public Integer getCountryId() {
        return countryId;
    }

    public void setCountryId(Integer countryId) {
        this.countryId = countryId;
    }

    public Integer getStateId() {
        return stateId;
    }

    public void setStateId(Integer stateId) {
        this.stateId = stateId;
    }

    public Integer getStatusId() {
        return statusId;
    }

    public void setStatusId(Integer statusId) {
        this.statusId = statusId;
    }

    public String getAnsweredQuestion() {
        return answeredQuestion;
    }

    public void setAnsweredQuestion(String answeredQuestion) {
        this.answeredQuestion = answeredQuestion;
    }

    public String getDispatchno() {
        return dispatchno;
    }

    public void setDispatchno(String dispatchno) {
        this.dispatchno = dispatchno;
    }

    public String getServicecalldate() {
        return servicecalldate;
    }

    public void setServicecalldate(String servicecalldate) {
        this.servicecalldate = servicecalldate;
    }

    public String getServiceprovider() {
        return serviceprovider;
    }

    public void setServiceprovider(String serviceprovider) {
        this.serviceprovider = serviceprovider;
    }

    public String getLogisticprovider() {
        return logisticprovider;
    }

    public void setLogisticprovider(String logisticprovider) {
        this.logisticprovider = logisticprovider;
    }

    public String getTransport() {
        return transport;
    }

    public void setTransport(String transport) {
        this.transport = transport;
    }

    public String getDispatchstatus() {
        return dispatchstatus;
    }

    public void setDispatchstatus(String dispatchstatus) {
        this.dispatchstatus = dispatchstatus;
    }

    public String getLobnew() {
        return lobnew;
    }

    public void setLobnew(String lobnew) {
        this.lobnew = lobnew;
    }

    public String getSysclassification() {
        return sysclassification;
    }

    public void setSysclassification(String sysclassification) {
        this.sysclassification = sysclassification;
    }

    public String getEndservicewindow() {
        return endservicewindow;
    }

    public void setEndservicewindow(String endservicewindow) {
        this.endservicewindow = endservicewindow;
    }

    public String getServicetag() {
        return servicetag;
    }

    public void setServicetag(String servicetag) {
        this.servicetag = servicetag;
    }

    public String getDsptype() {
        return dsptype;
    }

    public void setDsptype(String dsptype) {
        this.dsptype = dsptype;
    }

    public String getCalltype() {
        return calltype;
    }

    public void setCalltype(String calltype) {
        this.calltype = calltype;
    }

    public String getServicelevel() {
        return servicelevel;
    }

    public void setServicelevel(String servicelevel) {
        this.servicelevel = servicelevel;
    }

    public String getCustomerno() {
        return customerno;
    }

    public void setCustomerno(String customerno) {
        this.customerno = customerno;
    }

    public String getCustomernamenew() {
        return customernamenew;
    }

    public void setCustomernamenew(String customernamenew) {
        this.customernamenew = customernamenew;
    }

    public String getCustomercontactname() {
        return customercontactname;
    }

    public void setCustomercontactname(String customercontactname) {
        this.customercontactname = customercontactname;
    }

    public String getCustomercontactphno() {
        return customercontactphno;
    }

    public void setCustomercontactphno(String customercontactphno) {
        this.customercontactphno = customercontactphno;
    }

    public String getCustomercontactemail() {
        return customercontactemail;
    }

    public void setCustomercontactemail(String customercontactemail) {
        this.customercontactemail = customercontactemail;
    }

    public String getCustomersecondarycontactname() {
        return customersecondarycontactname;
    }

    public void setCustomersecondarycontactname(String customersecondarycontactname) {
        this.customersecondarycontactname = customersecondarycontactname;
    }

    public String getCustomersecondarycontactphno() {
        return customersecondarycontactphno;
    }

    public void setCustomersecondarycontactphno(String customersecondarycontactphno) {
        this.customersecondarycontactphno = customersecondarycontactphno;
    }

    public String getCustomersecondarycontactemail() {
        return customersecondarycontactemail;
    }

    public void setCustomersecondarycontactemail(String customersecondarycontactemail) {
        this.customersecondarycontactemail = customersecondarycontactemail;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getStatenew() {
        return statenew;
    }

    public void setStatenew(String statenew) {
        this.statenew = statenew;
    }

    public String getCitynew() {
        return citynew;
    }

    public void setCitynew(String citynew) {
        this.citynew = citynew;
    }

    public String getServiceaddress1() {
        return serviceaddress1;
    }

    public void setServiceaddress1(String serviceaddress1) {
        this.serviceaddress1 = serviceaddress1;
    }

    public String getServiceaddress2() {
        return serviceaddress2;
    }

    public void setServiceaddress2(String serviceaddress2) {
        this.serviceaddress2 = serviceaddress2;
    }

    public String getServiceaddress3() {
        return serviceaddress3;
    }

    public void setServiceaddress3(String serviceaddress3) {
        this.serviceaddress3 = serviceaddress3;
    }

    public String getServiceaddress4() {
        return serviceaddress4;
    }

    public void setServiceaddress4(String serviceaddress4) {
        this.serviceaddress4 = serviceaddress4;
    }

    public String getPartnumber() {
        return partnumber;
    }

    public void setPartnumber(String partnumber) {
        this.partnumber = partnumber;
    }

    public String getPartquantity() {
        return partquantity;
    }

    public void setPartquantity(String partquantity) {
        this.partquantity = partquantity;
    }

    public String getPartstatus() {
        return partstatus;
    }

    public void setPartstatus(String partstatus) {
        this.partstatus = partstatus;
    }

    public String getPartstatusdate() {
        return partstatusdate;
    }

    public void setPartstatusdate(String partstatusdate) {
        this.partstatusdate = partstatusdate;
    }

    public String getCarriername() {
        return carriername;
    }

    public void setCarriername(String carriername) {
        this.carriername = carriername;
    }

    public String getWaybillno() {
        return waybillno;
    }

    public void setWaybillno(String waybillno) {
        this.waybillno = waybillno;
    }

    public String getAgentdescription() {
        return agentdescription;
    }

    public void setAgentdescription(String agentdescription) {
        this.agentdescription = agentdescription;
    }

    public String getClosuredate() {
        return closuredate;
    }

    public void setClosuredate(String closuredate) {
        this.closuredate = closuredate;
    }

    public String getCommentstovendor() {
        return commentstovendor;
    }

    public void setCommentstovendor(String commentstovendor) {
        this.commentstovendor = commentstovendor;
    }

    public String getWarrantyinvoicedate() {
        return warrantyinvoicedate;
    }

    public void setWarrantyinvoicedate(String warrantyinvoicedate) {
        this.warrantyinvoicedate = warrantyinvoicedate;
    }

    public String getWarrantyinvoiceno() {
        return warrantyinvoiceno;
    }

    public void setWarrantyinvoiceno(String warrantyinvoiceno) {
        this.warrantyinvoiceno = warrantyinvoiceno;
    }

    public String getWarrantybillfromstate() {
        return warrantybillfromstate;
    }

    public void setWarrantybillfromstate(String warrantybillfromstate) {
        this.warrantybillfromstate = warrantybillfromstate;
    }

    public String getOriginalorderbuid() {
        return originalorderbuid;
    }

    public void setOriginalorderbuid(String originalorderbuid) {
        this.originalorderbuid = originalorderbuid;
    }

    public String getServicepostalcode() {
        return servicepostalcode;
    }

    public void setServicepostalcode(String servicepostalcode) {
        this.servicepostalcode = servicepostalcode;
    }

    public String getReplycode() {
        return replycode;
    }

    public void setReplycode(String replycode) {
        this.replycode = replycode;
    }

    public String getEngineerid() {
        return engineerid;
    }

    public void setEngineerid(String engineerid) {
        this.engineerid = engineerid;
    }

    public String getEngineername() {
        return engineername;
    }

    public void setEngineername(String engineername) {
        this.engineername = engineername;
    }

    public String getCustomerMyName() {
        return customerMyName;
    }

    public void setCustomerMyName(String customerMyName) {
        this.customerMyName = customerMyName;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getSubcategoryName() {
        return subcategoryName;
    }

    public void setSubcategoryName(String subcategoryName) {
        this.subcategoryName = subcategoryName;
    }

    public String getTodoStatusName() {
        return TodoStatusName;
    }

    public void setTodoStatusName(String todoStatusName) {
        TodoStatusName = todoStatusName;
    }

    public Boolean getUpdateflag() {
        return updateflag;
    }

    public void setUpdateflag(Boolean updateflag) {
        this.updateflag = updateflag;
    }

    public Boolean getInsertFlag() {
        return insertFlag;
    }

    public void setInsertFlag(Boolean insertFlag) {
        this.insertFlag = insertFlag;
    }

    public Integer getRcCode() {
        return rcCode;
    }

    public void setRcCode(Integer rcCode) {
        this.rcCode = rcCode;
    }

    public Integer getNextquestionId() {
        return nextquestionId;
    }

    public void setNextquestionId(Integer nextquestionId) {
        this.nextquestionId = nextquestionId;
    }

    public Integer getPrevquestionId() {
        return prevquestionId;
    }

    public void setPrevquestionId(Integer prevquestionId) {
        this.prevquestionId = prevquestionId;
    }

    public Boolean getRollbackFlag() {
        return rollbackFlag;
    }

    public void setRollbackFlag(Boolean rollbackFlag) {
        this.rollbackFlag = rollbackFlag;
    }

    public String getRollbackStatus() {
        return rollbackStatus;
    }

    public void setRollbackStatus(String rollbackStatus) {
        this.rollbackStatus = rollbackStatus;
    }

    public String getRollbackupdate() {
        return rollbackupdate;
    }

    public void setRollbackupdate(String rollbackupdate) {
        this.rollbackupdate = rollbackupdate;
    }

    public Integer getProducttypeid() {
        return producttypeid;
    }

    public void setProducttypeid(Integer producttypeid) {
        this.producttypeid = producttypeid;
    }

    public Integer getServiceeventid() {
        return serviceeventid;
    }

    public void setServiceeventid(Integer serviceeventid) {
        this.serviceeventid = serviceeventid;
    }

    public String getServiceidentifier() {
        return serviceidentifier;
    }

    public void setServiceidentifier(String serviceidentifier) {
        this.serviceidentifier = serviceidentifier;
    }

    public String getAspname() {
        return aspname;
    }

    public void setAspname(String aspname) {
        this.aspname = aspname;
    }

    public Boolean getProcessflag() {
        return processflag;
    }

    public void setProcessflag(Boolean processflag) {
        this.processflag = processflag;
    }

    public String getCouriername() {
        return couriername;
    }

    public void setCouriername(String couriername) {
        this.couriername = couriername;
    }

    public String getDocket() {
        return docket;
    }

    public void setDocket(String docket) {
        this.docket = docket;
    }

    public String getDispatchdate() {
        return dispatchdate;
    }

    public void setDispatchdate(String dispatchdate) {
        this.dispatchdate = dispatchdate;
    }

    public Integer getLastloginUserId() {
        return lastloginUserId;
    }

    public void setLastloginUserId(Integer lastloginUserId) {
        this.lastloginUserId = lastloginUserId;
    }

    public String getTimeslot() {
        return timeslot;
    }

    public void setTimeslot(String timeslot) {
        this.timeslot = timeslot;
    }

    public String getBandwidth() {
        return bandwidth;
    }

    public void setBandwidth(String bandwidth) {
        this.bandwidth = bandwidth;
    }

    public String getRadiususername() {
        return radiususername;
    }

    public void setRadiususername(String radiususername) {
        this.radiususername = radiususername;
    }

    public String getRadiuspassword() {
        return radiuspassword;
    }

    public void setRadiuspassword(String radiuspassword) {
        this.radiuspassword = radiuspassword;
    }

    public Long getServerId() {
        return serverId;
    }

    public void setServerId(Long serverId) {
        this.serverId = serverId;
    }

    public int getLocalId() {
        return localId;
    }

    public void setLocalId(int localId) {
        this.localId = localId;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }
}
