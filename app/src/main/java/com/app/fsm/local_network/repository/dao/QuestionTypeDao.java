package com.app.fsm.local_network.repository.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.app.fsm.local_network.network.model.one_one_question.QuestionTypeModel;

import java.util.List;

@Dao
public interface QuestionTypeDao {

    @Query("SELECT * FROM questiontypemodel")
    List<QuestionTypeModel> getAll();

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertAll(List<QuestionTypeModel> questionTypeModelList);

}
