package com.app.fsm.local_network.repository.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.app.fsm.local_network.network.model.educationmodel.EducationModel;

import java.util.List;

@Dao
public interface EducationDao extends BaseDao{

    @Query("SELECT * FROM educationmodel")
    List<EducationModel> getAll();

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertAllEducation(List<EducationModel> educationModelList);
}
