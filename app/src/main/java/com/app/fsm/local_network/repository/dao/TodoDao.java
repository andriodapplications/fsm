package com.app.fsm.local_network.repository.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.app.fsm.local_network.network.model.todo.TodoLanguage;
import com.app.fsm.local_network.network.model.todo.TodoModel;
import com.app.fsm.local_network.network.model.todo.TodoStatusModel;

import java.util.List;

@Dao
public interface TodoDao  extends BaseDao{

    @Query("SELECT * FROM todomodel")
    List<TodoModel> provideTodoModel();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Long[] insertAllTodoModel(List<TodoModel> todoModelList);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    long insertTodo(TodoModel todoModel);

    @Query("SELECT * FROM todomodel WHERE serverId=:id")
    TodoModel provideTodoModel(int id);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAllTodoLangauge(List<TodoLanguage> todoLanguageList);

    @Update
    int updateTodo(TodoModel todoModel);

    @Query("SELECT COUNT(isPushed) from todomodel where isPushed=:isPushed")
    int provideTodoCountNotPushed(boolean isPushed);

    @Query("SELECT * FROM todolanguage")
    List<TodoLanguage> provideAllTodoLangauge();

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertAllTodoStatus(List<TodoStatusModel> todoStatusModelList);

    @Query("SELECT * FROM TodoStatusModel")
    List<TodoStatusModel> provideAllTodoStatud();

    @Query("DELETE FROM todomodel")
    void deleteAllTodoModel();


    @Query("SELECT COUNT(*) from todomodel")
    int getCount();

    @Query("SELECT * FROM todomodel ORDER BY serverId DESC LIMIT 1")
    TodoModel getTodoModel();

}
