package com.app.fsm.local_network.network.model.Images;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

@Entity
public class InsertImages {
    @PrimaryKey
    @NonNull
    @SerializedName("id")
    private Integer id;

    @SerializedName("name")
    private String name;

    @SerializedName("imagecount")
    private Integer imagecount;

    @SerializedName("isPushed")
    private Boolean isPushed=true;

    @NonNull
    public Integer getId() {
        return id;
    }

    public void setId(@NonNull Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getImagecount() {
        return imagecount;
    }

    public void setImagecount(Integer imagecount) {
        this.imagecount = imagecount;
    }

    public Boolean getPushed() {
        return isPushed;
    }

    public void setPushed(Boolean pushed) {
        isPushed = pushed;
    }
}
