package com.app.fsm.local_network.repository.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.app.fsm.local_network.network.model.workflow.WorkFlowStatus;

import java.util.List;

@Dao
public interface WorkFlowStatusDao {

    @Query("SELECT * FROM workflowstatus WHERE workflowid=:workflowId")
    List<WorkFlowStatus> provideWorkFlowStatus(int workflowId);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertAllStatus(List<WorkFlowStatus> workFlowStatusList);

}
