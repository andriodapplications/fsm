package com.app.fsm.local_network.network.model.one_one_question;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


@Entity
public class QuestionLanguage implements Serializable {

    @SerializedName("language")
    @Expose
    private Integer language;
    @SerializedName("onetooneTitle")
    @Expose
    private String onetooneTitle;
    @SerializedName("history")
    @Expose
    private String history;
    @SerializedName("memberName")
    @Expose
    private String memberName;
    @SerializedName("memberID")
    @Expose
    private String memberID;
    @SerializedName("lessDetails")
    @Expose
    private String lessDetails;
    @SerializedName("moreDetails")
    @Expose
    private String moreDetails;
    @SerializedName("enableWrite")
    @Expose
    private Boolean enableWrite;
    @SerializedName("enableforroles")
    @Expose
    private String enableforroles;
    @SerializedName("progress")
    @Expose
    private String progress;
    @SerializedName("dateLabel")
    @Expose
    private String dateLabel;
    @SerializedName("question")
    @Expose
    private String question;
    @SerializedName("answer")
    @Expose
    private String answer;
    @SerializedName("todo")
    @Expose
    private String todo;
    @SerializedName("saveLabel")
    @Expose
    private String saveLabel;
    @SerializedName("updateLabel")
    @Expose
    private String updateLabel;
    @SerializedName("cancelLabel")
    @Expose
    private String cancelLabel;
    @SerializedName("showLabel")
    @Expose
    private String showLabel;
    @SerializedName("hideLabel")
    @Expose
    private String hideLabel;
    @SerializedName("searchLabel")
    @Expose
    private String searchLabel;
    @SerializedName("addLabel")
    @Expose
    private String addLabel;
    @SerializedName("organizationId")
    @Expose
    private String organizationId;
    @SerializedName("lastmodifiedby")
    @Expose
    private Integer lastmodifiedby;
    @SerializedName("lastmodifiedtime")
    @Expose
    private String lastmodifiedtime;
    @SerializedName("lastmodifiedrole")
    @Expose
    private Integer lastmodifiedrole;
    @SerializedName("createdby")
    @Expose
    private Integer createdby;
    @SerializedName("createdtime")
    @Expose
    private String createdtime;
    @SerializedName("createdrole")
    @Expose
    private Integer createdrole;
    @SerializedName("deleteflag")
    @Expose
    private Boolean deleteflag;
    @SerializedName("parentLanguage")
    @Expose
    private Boolean parentLanguage;

    @PrimaryKey
    @NonNull
    @SerializedName("id")
    @Expose
    private Integer id;
    private final static long serialVersionUID = -8159734379109266146L;

    public Integer getLanguage() {
        return language;
    }

    public void setLanguage(Integer language) {
        this.language = language;
    }

    public String getOnetooneTitle() {
        return onetooneTitle;
    }

    public void setOnetooneTitle(String onetooneTitle) {
        this.onetooneTitle = onetooneTitle;
    }

    public String getHistory() {
        return history;
    }

    public void setHistory(String history) {
        this.history = history;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public String getMemberID() {
        return memberID;
    }

    public void setMemberID(String memberID) {
        this.memberID = memberID;
    }

    public String getLessDetails() {
        return lessDetails;
    }

    public void setLessDetails(String lessDetails) {
        this.lessDetails = lessDetails;
    }

    public String getMoreDetails() {
        return moreDetails;
    }

    public void setMoreDetails(String moreDetails) {
        this.moreDetails = moreDetails;
    }

    public Boolean getEnableWrite() {
        return enableWrite;
    }

    public void setEnableWrite(Boolean enableWrite) {
        this.enableWrite = enableWrite;
    }

    public String getEnableforroles() {
        return enableforroles;
    }

    public void setEnableforroles(String enableforroles) {
        this.enableforroles = enableforroles;
    }

    public String getProgress() {
        return progress;
    }

    public void setProgress(String progress) {
        this.progress = progress;
    }

    public String getDateLabel() {
        return dateLabel;
    }

    public void setDateLabel(String dateLabel) {
        this.dateLabel = dateLabel;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getTodo() {
        return todo;
    }

    public void setTodo(String todo) {
        this.todo = todo;
    }

    public String getSaveLabel() {
        return saveLabel;
    }

    public void setSaveLabel(String saveLabel) {
        this.saveLabel = saveLabel;
    }

    public String getUpdateLabel() {
        return updateLabel;
    }

    public void setUpdateLabel(String updateLabel) {
        this.updateLabel = updateLabel;
    }

    public String getCancelLabel() {
        return cancelLabel;
    }

    public void setCancelLabel(String cancelLabel) {
        this.cancelLabel = cancelLabel;
    }

    public String getShowLabel() {
        return showLabel;
    }

    public void setShowLabel(String showLabel) {
        this.showLabel = showLabel;
    }

    public String getHideLabel() {
        return hideLabel;
    }

    public void setHideLabel(String hideLabel) {
        this.hideLabel = hideLabel;
    }

    public String getSearchLabel() {
        return searchLabel;
    }

    public void setSearchLabel(String searchLabel) {
        this.searchLabel = searchLabel;
    }

    public String getAddLabel() {
        return addLabel;
    }

    public void setAddLabel(String addLabel) {
        this.addLabel = addLabel;
    }

    public String getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(String organizationId) {
        this.organizationId = organizationId;
    }

    public Integer getLastmodifiedby() {
        return lastmodifiedby;
    }

    public void setLastmodifiedby(Integer lastmodifiedby) {
        this.lastmodifiedby = lastmodifiedby;
    }

    public String getLastmodifiedtime() {
        return lastmodifiedtime;
    }

    public void setLastmodifiedtime(String lastmodifiedtime) {
        this.lastmodifiedtime = lastmodifiedtime;
    }

    public Integer getLastmodifiedrole() {
        return lastmodifiedrole;
    }

    public void setLastmodifiedrole(Integer lastmodifiedrole) {
        this.lastmodifiedrole = lastmodifiedrole;
    }

    public Integer getCreatedby() {
        return createdby;
    }

    public void setCreatedby(Integer createdby) {
        this.createdby = createdby;
    }

    public String getCreatedtime() {
        return createdtime;
    }

    public void setCreatedtime(String createdtime) {
        this.createdtime = createdtime;
    }

    public Integer getCreatedrole() {
        return createdrole;
    }

    public void setCreatedrole(Integer createdrole) {
        this.createdrole = createdrole;
    }

    public Boolean getDeleteflag() {
        return deleteflag;
    }

    public void setDeleteflag(Boolean deleteflag) {
        this.deleteflag = deleteflag;
    }

    public Boolean getParentLanguage() {
        return parentLanguage;
    }

    public void setParentLanguage(Boolean parentLanguage) {
        this.parentLanguage = parentLanguage;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
