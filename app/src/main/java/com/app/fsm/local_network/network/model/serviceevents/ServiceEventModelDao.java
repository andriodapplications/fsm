package com.app.fsm.local_network.network.model.serviceevents;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.app.fsm.local_network.repository.dao.BaseDao;

import java.util.List;

@Dao
public interface ServiceEventModelDao extends BaseDao {
    @Query("SELECT * FROM ServiceEventModel")
    List<ServiceEventModel> getServiceEvent();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertServiceEvents(List<ServiceEventModel> serviceEvents);
}
