package com.app.fsm.local_network.network.model.todo;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@Entity
public class TodoModel implements Serializable {

    @SerializedName("memberid")
    @Expose
    private Long memberid;
    @SerializedName("todotypeid")
    @Expose
    private Integer todotypeid;
    @SerializedName("todostatusid")
    @Expose
    private Integer todostatusid;
    @SerializedName("followupdate")
    @Expose
    private String followupdate;
    @SerializedName("assignedto")
    @Expose
    private Integer assignedto;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("district")
    @Expose
    private String district;
    @SerializedName("districtId")
    @Expose
    private String districtId;
    @SerializedName("site")
    @Expose
    private String site;
    @SerializedName("syncouttime")
    @Expose
    private String syncouttime;
    @SerializedName("stress_data")
    @Expose
    private Boolean stressData;
    @SerializedName("organizationId")
    @Expose
    private String organizationId;
    @SerializedName("lastmodifiedby")
    @Expose
    private Integer lastmodifiedby;
    @SerializedName("lastmodifiedtime")
    @Expose
    private String lastmodifiedtime;
    @SerializedName("lastmodifiedrole")
    @Expose
    private Integer lastmodifiedrole;
    @SerializedName("createdby")
    @Expose
    private Integer createdby;
    @SerializedName("createdtime")
    @Expose
    private String createdtime;
    @SerializedName("createdrole")
    @Expose
    private Integer createdrole;
    @SerializedName("deleteflag")
    @Expose
    private Boolean deleteflag;
    @SerializedName("orgstructure")
    @Expose
    private String orgstructure;
    @SerializedName("isPushed")
    private Boolean isPushed;

    @ColumnInfo(name = "serverId")
    @SerializedName("id")
    @Expose
    private Integer serverId;

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "localId")
    private int localId;

    private final static long serialVersionUID = -7666426056210960441L;

    public Boolean getPushed() {
        return isPushed;
    }

    public void setPushed(Boolean pushed) {
        isPushed = pushed;
    }

    public Integer getTodotypeid() {
        return todotypeid;
    }

    public void setTodotypeid(Integer todotypeid) {
        this.todotypeid = todotypeid;
    }

    public Integer getTodostatusid() {
        return todostatusid;
    }

    public void setTodostatusid(Integer todostatusid) {
        this.todostatusid = todostatusid;
    }

    public String getFollowupdate() {
        return followupdate;
    }

    public void setFollowupdate(String followupdate) {
        this.followupdate = followupdate;
    }

    public Integer getAssignedto() {
        return assignedto;
    }

    public void setAssignedto(Integer assignedto) {
        this.assignedto = assignedto;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getDistrictId() {
        return districtId;
    }

    public void setDistrictId(String districtId) {
        this.districtId = districtId;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public String getSyncouttime() {
        return syncouttime;
    }

    public void setSyncouttime(String syncouttime) {
        this.syncouttime = syncouttime;
    }

    public Boolean getStressData() {
        return stressData;
    }

    public void setStressData(Boolean stressData) {
        this.stressData = stressData;
    }

    public String getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(String organizationId) {
        this.organizationId = organizationId;
    }

    public Integer getLastmodifiedby() {
        return lastmodifiedby;
    }

    public void setLastmodifiedby(Integer lastmodifiedby) {
        this.lastmodifiedby = lastmodifiedby;
    }

    public String getLastmodifiedtime() {
        return lastmodifiedtime;
    }

    public void setLastmodifiedtime(String lastmodifiedtime) {
        this.lastmodifiedtime = lastmodifiedtime;
    }

    public Integer getLastmodifiedrole() {
        return lastmodifiedrole;
    }

    public void setLastmodifiedrole(Integer lastmodifiedrole) {
        this.lastmodifiedrole = lastmodifiedrole;
    }

    public Integer getCreatedby() {
        return createdby;
    }

    public void setCreatedby(Integer createdby) {
        this.createdby = createdby;
    }

    public String getCreatedtime() {
        return createdtime;
    }

    public void setCreatedtime(String createdtime) {
        this.createdtime = createdtime;
    }

    public Integer getCreatedrole() {
        return createdrole;
    }

    public void setCreatedrole(Integer createdrole) {
        this.createdrole = createdrole;
    }

    public Boolean getDeleteflag() {
        return deleteflag;
    }

    public void setDeleteflag(Boolean deleteflag) {
        this.deleteflag = deleteflag;
    }

    public String getOrgstructure() {
        return orgstructure;
    }

    public void setOrgstructure(String orgstructure) {
        this.orgstructure = orgstructure;
    }

    public Long getMemberid() {
        return memberid;
    }


    public Integer getServerId() {
        return serverId;
    }

    public void setServerId(Integer serverId) {
        this.serverId = serverId;
    }

    public int getLocalId() {
        return localId;
    }

    public void setLocalId(int localId) {
        this.localId = localId;
    }

    public void setMemberid(Long memberid) {
        this.memberid = memberid;
    }
}