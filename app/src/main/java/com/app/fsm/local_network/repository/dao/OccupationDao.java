package com.app.fsm.local_network.repository.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.app.fsm.local_network.network.model.occupationmodel.OccupationModel;

import java.util.List;

@Dao
public interface OccupationDao extends BaseDao {

    @Query("SELECT * FROM occupationmodel")
    List<OccupationModel> getAll();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAllOccupation(List<OccupationModel> occupationModelList);
}
