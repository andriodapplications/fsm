/*
 * Developed by Avinash Kumar singh on 24/1/19 3:49 PM
 * Last Modified 21/1/19 8:26 PM.
 *
 * Copyright (c) 2019.  All rights reserved.
 */

package com.app.fsm;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.fsm.local_network.network.ApiService;
import com.app.fsm.local_network.repository.OaasDatabase;
import com.app.fsm.local_network.repository.OassDatabaseBuilder;
import com.app.fsm.utils.ApiUtils;
import com.app.fsm.utils.LocaleManager;
import com.app.fsm.utils.UserSessionManager;


public class BaseActivity extends AppCompatActivity {

    public ProgressDialog progressDialog;
    private ImageView ivLoadingCompleted;

    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            ConnectivityManager cm =
                    (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

            NetworkInfo activeNetwork;
            if (cm != null) {
                activeNetwork = cm.getActiveNetworkInfo();
                boolean isConnected = activeNetwork != null &&
                        activeNetwork.isConnected();
                onNetworkConnectionChanged(isConnected);
            }

        }
    };


    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleManager.setLocale(base));
    }


    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    public boolean isBackStackCountZero() {
        int count = getSupportFragmentManager().getBackStackEntryCount();
        return count == 0;
    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    protected void onResume() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(broadcastReceiver, filter);
        super.onResume();
    }

    public void openFragment(Fragment fragment, String tag) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fl_main, fragment, tag);
        fragmentTransaction.addToBackStack(tag);
        fragmentTransaction.commit();
    }

    public OaasDatabase provideOassDatabe(){
        return OassDatabaseBuilder.provideOassDatabase(this);
    }

    public void hideLoading() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.cancel();
        }
    }

    /** If the data is being fetched then send true
     *  to show the progress and false to hide the progress. */
    public void isFetching(Boolean isFetching,String msg){

        if(isFetching){
            hideLoading();
            progressDialog = showLoading(this);
            progressDialog.show();
            setMessageToProgressDialog(msg);
        }else{
            setMessageToProgressDialog(msg);
        }
    }


    public void showSuccessMsg(String strSuccessMsg){
        if (ivLoadingCompleted != null && txtViewMessage != null) {
            ivLoadingCompleted.setVisibility(View.VISIBLE);
        }
        setMessageToProgressDialog(strSuccessMsg);
    }



    public void setMessageToProgressDialog(String msg){
        if(txtViewMessage!=null){
            txtViewMessage.setText(msg);
        }
    }

    public UserSessionManager getUserSessionManager() {
        return UserSessionManager.getUserSessionManager();
    }

    public ApiService getApiService() {
        return ApiUtils.getAPIService();
    }

    public void onNetworkConnectionChanged(boolean isConnected) {

        if (isConnected) {
            //
            // showSnack(R.string.connected_net, R.color.app_green_color);
        } else {
            //showSnack(R.string.no_internet_connection, android.R.color.holo_red_dark);
        }
    }

    public void showSnack(int strId, int colorId) {
        View parentLayout = findViewById(android.R.id.content);
        Snackbar snackbar = Snackbar.make(parentLayout, getString(strId), Snackbar.LENGTH_LONG);
        snackbar.getView().setBackgroundColor(getResources().getColor(colorId));
        TextView mainTextView = (snackbar.getView()).findViewById(android.support.design.R.id.snackbar_text);
        //mainTextView.setTextColor(getResources().getColor(R.color.white));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            mainTextView.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        }
        snackbar.show();
    }


    @Override
    protected void onPause() {
        if (broadcastReceiver != null)
            unregisterReceiver(broadcastReceiver);
        super.onPause();
    }


    TextView txtViewMessage;

    public ProgressDialog showLoading(@NonNull Context context) {
        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.show();
        if (progressDialog.getWindow() != null) {
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        progressDialog.setContentView(R.layout.fetch_data_progress_dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        ivLoadingCompleted=progressDialog.findViewById(R.id.iv_loading_completed);
        txtViewMessage = progressDialog.findViewById(R.id.txt_message);
        return progressDialog;
    }
}
