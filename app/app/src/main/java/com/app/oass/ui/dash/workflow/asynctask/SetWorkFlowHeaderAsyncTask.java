package com.app.fsm.ui.dash.workflow.asynctask;

import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.app.fsm.local_network.network.model.workflow.WorkFlowHeader;
import com.app.fsm.local_network.repository.OaasDatabase;
import com.app.fsm.local_network.repository.dao.WorkFlowHeaderDao;
import com.app.fsm.ui.dash.workflow.OnWorkFlowFetchListener;

public final class SetWorkFlowHeaderAsyncTask extends AsyncTask<Void, Void, Long> {

    private OaasDatabase oaasDatabase;
    private WorkFlowHeader workFlowHeader;
    private OnWorkFlowFetchListener onWorkFlowFetchListener;

    public SetWorkFlowHeaderAsyncTask(@NonNull OnWorkFlowFetchListener onWorkFlowFetchListener, @NonNull OaasDatabase oaasDatabase
            , WorkFlowHeader workFlowHeader) {
        this.onWorkFlowFetchListener = onWorkFlowFetchListener;
        this.oaasDatabase = oaasDatabase;
        this.workFlowHeader = workFlowHeader;
    }

    @Override
    protected Long doInBackground(Void... params) {
        WorkFlowHeaderDao workFlowHeaderDao = oaasDatabase.provideWorkFlowHeaderDao();

        if (workFlowHeaderDao.getCount() == 0) {
            workFlowHeader.setServerId(1);
        } else {
            WorkFlowHeader lastWorkFlow= workFlowHeaderDao.getWorkFlowHeader();
            workFlowHeader.setServerId(lastWorkFlow.getServerId() + 1);
        }
        return workFlowHeaderDao.insertWorkFlowHeader(workFlowHeader);
    }

    @Override
    protected void onPostExecute(Long longValue) {
        if (onWorkFlowFetchListener != null)
            onWorkFlowFetchListener.onComplete();
    }
}

