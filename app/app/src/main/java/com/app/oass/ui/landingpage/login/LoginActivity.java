package com.app.fsm.ui.landingpage.login;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.AppCompatEditText;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.app.fsm.BaseActivity;
import com.app.fsm.R;
import com.app.fsm.local_network.network.model.login.LoginRequest;
import com.app.fsm.local_network.network.model.login.LoginResponse;
import com.app.fsm.local_network.network.model.usermodel.UserModel;
import com.app.fsm.ui.dash.main.DashboardActivity;
import com.app.fsm.ui.dash.main.asynctask.GetAllUserAsyncTask;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnFocusChange;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends BaseActivity implements FetchAllUserListener {

    @BindView(R.id.auto_complete_user_name)
    AutoCompleteTextView autoCompleteTextView;

    @BindView(R.id.edt_password)
    AppCompatEditText edtPassword;

    @BindView(R.id.txt_login)
    TextView txtLogin;

    @BindView(R.id.txt_forgot_password)
    TextView txtForgotPassword;

    private ProgressDialog progressDialog;

    public static Intent newInstance(Context context) {
        return new Intent(context, LoginActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        new GetAllUserAsyncTask(this, provideOassDatabe()).execute();

        //autoCompleteTextView.setText("sam");
        //edtPassword.setText("1234");
    }


    @OnFocusChange(R.id.auto_complete_user_name)
    public void onNameFocusChange(boolean isFocused) {
        onFocusChange(isFocused, autoCompleteTextView);
    }

    @OnFocusChange(R.id.edt_password)
    public void onPassword(boolean isFocused) {
        onFocusChange(isFocused, edtPassword);
    }

    public void onFocusChange(Boolean isFocused, View view) {
        if (isFocused) {
            view.setBackgroundResource(R.drawable.focused_edittext_stroke_1dp);
        } else {
            view.setBackgroundResource(R.drawable.unfocused_edittext_stroke_1dp);
        }
    }

    @OnClick(R.id.txt_login)
    public void onLogin() {

        if (TextUtils.isEmpty(autoCompleteTextView.getText())) {
            autoCompleteTextView.setError("Please enter name");
        } else if (TextUtils.isEmpty(edtPassword.getText())) {
            edtPassword.setError("Please enter password to login");
        } else if (edtPassword.getText().length() <= 3) {
            edtPassword.setError("Password should be 4 character");
        } else if (getUserSessionManager().getUser() != null && !autoCompleteTextView.getText().toString()
                .equalsIgnoreCase(getUserSessionManager().getUser().getUsername())) {
            showAlertDialog();
        } else {
            setLogin(new LoginRequest(edtPassword.getText().toString(), autoCompleteTextView.getText().toString()));
        }
    }

    public void showAlertDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.layout_forgot_password);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        Button btnOk = dialog.findViewById(R.id.btn_ok);
        TextView txtAlertMessage = dialog.findViewById(R.id.txt_alert_msg);
        txtAlertMessage.setText("Please clear the app data to login with other credential.");
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    @OnClick(R.id.txt_forgot_password) void onForgotPasswordClicked(){
        final Dialog dialog=new Dialog(this);
        dialog.setContentView(R.layout.layout_forgot_password);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        Button btnOk=dialog.findViewById(R.id.btn_ok);
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    /**
     * This progress dialog is just shown on the Login Activity
     */
    public void showProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Logging in.Please wait");
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
        progressDialog.show();
    }

    public void hideProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    /**
     * Login Api
     */
    public void setLogin(LoginRequest loginRequest) {
        showProgressDialog();
        getApiService().login(loginRequest)
                .enqueue(new Callback<LoginResponse>() {
                    @Override
                    public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                        if (response.isSuccessful()) {
                            LoginResponse loginResponse = response.body();
                            createSession(loginResponse);
                            getUser();
                        } else {

                            try {
                                JSONObject jsonObject = new JSONObject(response.errorBody().string());
                                JSONObject errorObject = jsonObject.getJSONObject("error");
                                Toast.makeText(LoginActivity.this, errorObject.getString("message"), Toast.LENGTH_SHORT).show();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            hideProgressDialog();
                        }
                    }

                    @Override
                    public void onFailure(Call<LoginResponse> call, Throwable t) {
                        hideProgressDialog();
                    }
                });
    }

    public void getUser() {
        Log.d("assignid",getUserSessionManager().getUserId());
        getApiService()
                .getUser(Integer.valueOf(getUserSessionManager().getUserId()), getUserSessionManager().getAccessToken())
                .enqueue(new Callback<UserModel>() {
                    @Override
                    public void onResponse(Call<UserModel> call, Response<UserModel> response) {
                        hideProgressDialog();
                        if (response.isSuccessful()) {
                            UserModel userModel = response.body();
                            getUserSessionManager().setUser(userModel);
                            openDashBoard();
                        } else {
                            hideProgressDialog();
                        }
                    }

                    @Override
                    public void onFailure(Call<UserModel> call, Throwable t) {
                        hideProgressDialog();
                    }
                });
    }

    public void openDashBoard() {
        startActivity(DashboardActivity.newInstance(this));
        finish();
    }

    public void createSession(LoginResponse loginResponse) {
        getUserSessionManager().createSession(loginResponse.getUserId()
                , loginResponse.getId()
                , loginResponse.getTtl());
    }

    @Override
    public void onFetchingAllUserCompleted(List<UserModel> userModelList) {
        if (userModelList.size() > 0) {
            List<String> userNameList = new ArrayList<>();
            UserModel user = getUserSessionManager().getUser();
            if (user != null) {
                userNameList.add(user.getUsername());
            }
            ArrayAdapter<String> adapter = new ArrayAdapter<>
                    (this,R.layout.row_auto_complete_text_view_item, userNameList);
            autoCompleteTextView.setThreshold(1);
            autoCompleteTextView.setAdapter(adapter);
            autoCompleteTextView.setDropDownVerticalOffset(10);
        }
    }
}
