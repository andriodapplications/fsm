package com.app.fsm.ui.dash.taskList.asynctask;

import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.app.fsm.local_network.network.model.joinedtable.TaskTypeModel;
import com.app.fsm.local_network.repository.OaasDatabase;
import com.app.fsm.local_network.repository.dao.TodoTypeDao;
import com.app.fsm.ui.dash.taskList.OnFetchListener;

import java.util.ArrayList;
import java.util.List;

public final class GetTaskTypeAsyncTask extends AsyncTask<Void, Void, List<TaskTypeModel>> {

    private OaasDatabase oaasDatabase;
    private OnFetchListener onFetchListener;

    public GetTaskTypeAsyncTask(@NonNull OnFetchListener onFetchListener
            , @NonNull OaasDatabase oaasDatabase) {
        this.oaasDatabase = oaasDatabase;
        this.onFetchListener = onFetchListener;
    }

    @Override
    protected List<TaskTypeModel> doInBackground(Void... params) {
        TodoTypeDao todoTypeDao = oaasDatabase.provideTodoTypeDao();
        List<TaskTypeModel> taskTypeModelList = todoTypeDao.provideWorkFlowType();
        List<TaskTypeModel> taskTypeModelList1 = todoTypeDao.provideTodoTypeModel();

        List<TaskTypeModel> finalTaskTypeModelList = new ArrayList<>();
        finalTaskTypeModelList.addAll(taskTypeModelList);
        finalTaskTypeModelList.addAll(taskTypeModelList1);
        return finalTaskTypeModelList;
    }

    @Override
    protected void onPostExecute(List<TaskTypeModel> taskTypeModelList) {
        if (onFetchListener != null) {
            onFetchListener.onTaskTypeList(taskTypeModelList);
        }
    }
}

