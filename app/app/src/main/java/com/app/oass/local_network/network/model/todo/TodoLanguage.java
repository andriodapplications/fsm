package com.app.fsm.local_network.network.model.todo;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


@Entity
public class TodoLanguage implements Serializable {

    @SerializedName("language")
    @Expose
    private Integer language;
    @SerializedName("todoTitle")
    @Expose
    private String todoTitle;
    @SerializedName("member")
    @Expose
    private String member;
    @SerializedName("todoLabel")
    @Expose
    private String todoLabel;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("followUp")
    @Expose
    private String followUp;
    @SerializedName("assignedTo")
    @Expose
    private String assignedTo;
    @SerializedName("enableWrite")
    @Expose
    private Boolean enableWrite;
    @SerializedName("enableforroles")
    @Expose
    private String enableforroles;
    @SerializedName("saveLabel")
    @Expose
    private String saveLabel;
    @SerializedName("updateLabel")
    @Expose
    private String updateLabel;
    @SerializedName("cancelLabel")
    @Expose
    private String cancelLabel;
    @SerializedName("showLabel")
    @Expose
    private String showLabel;
    @SerializedName("hideLabel")
    @Expose
    private String hideLabel;
    @SerializedName("searchLabel")
    @Expose
    private String searchLabel;
    @SerializedName("addLabel")
    @Expose
    private String addLabel;
    @SerializedName("organizationId")
    @Expose
    private String organizationId;
    @SerializedName("lastmodifiedby")
    @Expose
    private Integer lastmodifiedby;
    @SerializedName("lastmodifiedtime")
    @Expose
    private String lastmodifiedtime;
    @SerializedName("lastmodifiedrole")
    @Expose
    private Integer lastmodifiedrole;
    @SerializedName("createdby")
    @Expose
    private Integer createdby;
    @SerializedName("createdtime")
    @Expose
    private String createdtime;
    @SerializedName("createdrole")
    @Expose
    private Integer createdrole;
    @SerializedName("deleteflag")
    @Expose
    private Boolean deleteflag;
    @SerializedName("parentLanguage")
    @Expose
    private Boolean parentLanguage;

    @PrimaryKey(autoGenerate = true)
    @SerializedName("id")
    @Expose
    private Integer id;

    private final static long serialVersionUID = -6350071562685341000L;

    public Integer getLanguage() {
        return language;
    }

    public void setLanguage(Integer language) {
        this.language = language;
    }

    public String getTodoTitle() {
        return todoTitle;
    }

    public void setTodoTitle(String todoTitle) {
        this.todoTitle = todoTitle;
    }

    public String getMember() {
        return member;
    }

    public void setMember(String member) {
        this.member = member;
    }

    public String getTodoLabel() {
        return todoLabel;
    }

    public void setTodoLabel(String todoLabel) {
        this.todoLabel = todoLabel;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFollowUp() {
        return followUp;
    }

    public void setFollowUp(String followUp) {
        this.followUp = followUp;
    }

    public String getAssignedTo() {
        return assignedTo;
    }

    public void setAssignedTo(String assignedTo) {
        this.assignedTo = assignedTo;
    }

    public Boolean getEnableWrite() {
        return enableWrite;
    }

    public void setEnableWrite(Boolean enableWrite) {
        this.enableWrite = enableWrite;
    }

    public String getEnableforroles() {
        return enableforroles;
    }

    public void setEnableforroles(String enableforroles) {
        this.enableforroles = enableforroles;
    }

    public String getSaveLabel() {
        return saveLabel;
    }

    public void setSaveLabel(String saveLabel) {
        this.saveLabel = saveLabel;
    }

    public String getUpdateLabel() {
        return updateLabel;
    }

    public void setUpdateLabel(String updateLabel) {
        this.updateLabel = updateLabel;
    }

    public String getCancelLabel() {
        return cancelLabel;
    }

    public void setCancelLabel(String cancelLabel) {
        this.cancelLabel = cancelLabel;
    }

    public String getShowLabel() {
        return showLabel;
    }

    public void setShowLabel(String showLabel) {
        this.showLabel = showLabel;
    }

    public String getHideLabel() {
        return hideLabel;
    }

    public void setHideLabel(String hideLabel) {
        this.hideLabel = hideLabel;
    }

    public String getSearchLabel() {
        return searchLabel;
    }

    public void setSearchLabel(String searchLabel) {
        this.searchLabel = searchLabel;
    }

    public String getAddLabel() {
        return addLabel;
    }

    public void setAddLabel(String addLabel) {
        this.addLabel = addLabel;
    }

    public String getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(String organizationId) {
        this.organizationId = organizationId;
    }

    public Integer getLastmodifiedby() {
        return lastmodifiedby;
    }

    public void setLastmodifiedby(Integer lastmodifiedby) {
        this.lastmodifiedby = lastmodifiedby;
    }

    public String getLastmodifiedtime() {
        return lastmodifiedtime;
    }

    public void setLastmodifiedtime(String lastmodifiedtime) {
        this.lastmodifiedtime = lastmodifiedtime;
    }

    public Integer getLastmodifiedrole() {
        return lastmodifiedrole;
    }

    public void setLastmodifiedrole(Integer lastmodifiedrole) {
        this.lastmodifiedrole = lastmodifiedrole;
    }

    public Integer getCreatedby() {
        return createdby;
    }

    public void setCreatedby(Integer createdby) {
        this.createdby = createdby;
    }

    public String getCreatedtime() {
        return createdtime;
    }

    public void setCreatedtime(String createdtime) {
        this.createdtime = createdtime;
    }

    public Integer getCreatedrole() {
        return createdrole;
    }

    public void setCreatedrole(Integer createdrole) {
        this.createdrole = createdrole;
    }

    public Boolean getDeleteflag() {
        return deleteflag;
    }

    public void setDeleteflag(Boolean deleteflag) {
        this.deleteflag = deleteflag;
    }

    public Boolean getParentLanguage() {
        return parentLanguage;
    }

    public void setParentLanguage(Boolean parentLanguage) {
        this.parentLanguage = parentLanguage;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
