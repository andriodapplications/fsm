package com.app.fsm.ui.dash.main;

import com.app.fsm.local_network.network.model.member.MemberModel;

import java.util.List;

public interface OnMemberListener {

    void onMemberFetchCompleted(List<MemberModel> memberModelList, int pos);
}
