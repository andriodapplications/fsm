package com.app.fsm.ui.dash.main.asynctask;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.app.fsm.local_network.network.model.todo.TodoType;
import com.app.fsm.local_network.repository.OaasDatabase;
import com.app.fsm.local_network.repository.OassDatabaseBuilder;
import com.app.fsm.local_network.repository.dao.TodoTypeDao;

import java.lang.ref.WeakReference;
import java.util.List;

public final class SetAllTodoTypeAsyncTask extends AsyncTask<Void, Void, Boolean> {

    //Prevent leak
    private WeakReference<Context> weakActivity;
    private List<TodoType> todoTypeList;

    public SetAllTodoTypeAsyncTask(Context context, List<TodoType> todoTypeList) {
        weakActivity = new WeakReference<>(context);
        this.todoTypeList = todoTypeList;
    }

    @Override
    protected Boolean doInBackground(Void... params) {
        OaasDatabase oaasDatabase = OassDatabaseBuilder.provideOassDatabase(weakActivity.get());
        TodoTypeDao todoTypeDao = oaasDatabase.provideTodoTypeDao();
        todoTypeDao.insertAllTodoType(todoTypeList);
        return true;
    }

    @Override
    protected void onPostExecute(Boolean isInserted) {
        if (isInserted) {
            Toast.makeText(weakActivity.get(), "Done", Toast.LENGTH_SHORT).show();
        }
    }
}

