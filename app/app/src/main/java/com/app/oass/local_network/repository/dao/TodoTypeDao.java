package com.app.fsm.local_network.repository.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.app.fsm.local_network.network.model.joinedtable.TaskStatusModel;
import com.app.fsm.local_network.network.model.joinedtable.TaskTypeModel;
import com.app.fsm.local_network.network.model.todo.TodoType;

import java.util.List;

@Dao
public interface TodoTypeDao {

    @Query("SELECT * FROM todotype")
    List<TodoType> provideTodoType();

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertAllTodoType(List<TodoType> todoTypeList);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    long insertTodoType(TodoType todoType);

    @Query("SELECT TodoType.name as name" +
            ",TodoType.id as id FROM todotype")
    List<TaskTypeModel> provideTodoTypeModel();

    @Query("SELECT WorkFlowModel.name as name,WorkFlowModel.id as id from WorkFlowModel")
    List<TaskTypeModel> provideWorkFlowType();

    @Query("SELECT WorkFlowStatus.name as statusName,WorkFlowStatus.id as statusId from WorkFlowStatus")
    List<TaskStatusModel> provideWorKStatus();

    @Query("SELECT TodoStatusModel.name as statusName,TodoStatusModel.id as statusId from TodoStatusModel")
    List<TaskStatusModel> provideTodoStatus();


}
