package com.app.fsm.ui.dash.one2one.one2onequestion;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.location.Location;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatSpinner;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.app.fsm.PathUtils;
import com.app.fsm.R;
import com.app.fsm.local_network.network.model.Images.InsertImages;
import com.app.fsm.local_network.network.model.member.MemberModel;
import com.app.fsm.local_network.network.model.survey.SurveyAnswerModel;
import com.app.fsm.local_network.network.model.survey.SurveyQuestionModel;
import com.app.fsm.local_network.network.model.todo.TodoModel;
import com.app.fsm.local_network.network.model.todo.TodoType;
import com.app.fsm.local_network.network.model.usermodel.UserModel;
import com.app.fsm.local_network.repository.OaasDatabase;
import com.app.fsm.local_network.repository.OassDatabaseBuilder;
import com.app.fsm.ui.OnCompleteListener;
import com.app.fsm.ui.dash.main.OnMemberListener;
import com.app.fsm.ui.dash.main.asynctask.GetAllMemberAsynTask;
import com.app.fsm.ui.dash.one2one.OnetoOneQuestionAdapter;
import com.app.fsm.ui.dash.one2one.ScanQRCode;
import com.app.fsm.ui.dash.one2one.one2oneasynctask.GetAllTodoTypeAsyncTask;
import com.app.fsm.ui.dash.one2one.one2oneasynctask.SetInsertImagesAsyncTask;
import com.app.fsm.ui.dash.one2one.one2onequestion.adapter.OptionAdapter;
import com.app.fsm.ui.dash.one2one.one2onequestion.adapter.TodoAdapter;
import com.app.fsm.utils.AppConstant;
import com.app.fsm.utils.CommonUtils;
import com.app.fsm.utils.UserSessionManager;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.nex3z.flowlayout.FlowLayout;

import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.net.URISyntaxException;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.AppSettingsDialog;
import pub.devrel.easypermissions.EasyPermissions;

import static android.app.Activity.RESULT_OK;

public class One2OneQuestionFragment extends QuestionSliderCallbackFragment implements
        RadioGroup.OnCheckedChangeListener
        , EasyPermissions.PermissionCallbacks
        , EasyPermissions.RationaleCallbacks
        , OnSuccessListener
        , AdapterView.OnItemSelectedListener
        , CompoundButton.OnCheckedChangeListener
        , OnTodoTypeFetchedCallback
        , OnCompleteListener, OnMemberListener {

    @BindView(R.id.txt_question)
    TextView txtQuestion;

    @BindView(R.id.spinner_question)
    AppCompatSpinner spinnersurveyQuestions;

    @BindView(R.id.ll_answer_view_container)
    LinearLayout llAnswerViewContainer;

    @BindView(R.id.spinner_todo)
    AppCompatSpinner spinnerTodo;

    @BindView(R.id.txt_header_todo)
    TextView txtHeaderTodo;

    @BindView(R.id.ll_todo)
    LinearLayout llTodo;

    FlowLayout flowLayout;
    Boolean isSurveyAnswered = false;
    Boolean isTodoDone = false;
    EditText edtSingleLine;
    EditText edtMultiline;
    Button bt_Location;
    ImageButton im_scancode;
    TextView tv_location;
    String location_value=null,Upload=null,qrcode=null,dropdown=null;
    Integer rccode=0,nextquestionid=0,previousques_Id=0,dd_count=0;
    TodoType todoType;
    String surveyAnswer = "";
    Unbinder unbinder;
    TextView txtDate;
    Calendar calendar = Calendar.getInstance();
    DatePickerDialog.OnDateSetListener onDateSetListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.MONTH, month);
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            txtDate.setText(CommonUtils.updateDate(calendar.getTime()));
        }
    };
    private int previousQuestionId = -1;

    private Location location;
    private FusedLocationProviderClient fusedLocationProviderClient;
    private static final int RC_COARSE_LOCATION = 1001;
    private final String[] LOCATION_PERMISSION = {Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA};


    private ProgressDialog mObjDialog;
    private static final int REQUEST_IMAGE_CAPTURE=1;
    static String imageName;
    private static final int CHOOSE_IMAGE = 2;
    private static final int CHOOSE_PDF = 3;
    private static final int CHOOSE_XLS = 4;
    private Uri picUri,ImagePath;
    private File picFile;
    public static String imagePath;
    private static final float maxHeight = 920.0f;
    private static final float maxWidth = 920.0f;
    private AlertDialog bt_upload_dialog;
    private OaasDatabase oaasDatabase;

    List<TodoType> mtodoTypes;
    RadioButton radioButton;
    //qr code scanner object
    private TextView tv_scancode;
    private EditText et_remark;
    int dropdownpos=0;
    private SurveyQuestionModel surveyQuestionModel;
    List<SurveyQuestionModel> questionlist;
    List<SurveyQuestionModel> alllquestionlist;
    private View mainview;
    private List<MemberModel> memberModelList;
    private MemberModel memberModel;
    List<String> stringList;
    Context mcontext;


    public static One2OneQuestionFragment newInstance(
            SurveyQuestionModel surveyQuestionModel,
            List<SurveyQuestionModel> surveyQuestionModelList,
            List<SurveyQuestionModel> allsurveyQuestionModel) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(AppConstant.SURVEY_QUESTION_MODEL, surveyQuestionModel);
        bundle.putSerializable(AppConstant.SURVEY_QUESTION_LIST, (Serializable) surveyQuestionModelList);
        bundle.putSerializable(AppConstant.ALL_SURVEY_QUESTION_LIST, (Serializable) allsurveyQuestionModel);
        One2OneQuestionFragment one2OneQuestionFragment = new One2OneQuestionFragment();
        one2OneQuestionFragment.setArguments(bundle);
        //Log.d("surveylist",""+surveyQuestionModelList.size());
        return one2OneQuestionFragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (getArguments() != null) {
            surveyQuestionModel = (SurveyQuestionModel) getArguments().getSerializable(AppConstant.SURVEY_QUESTION_MODEL);
            questionlist = (List<SurveyQuestionModel>) getArguments().getSerializable(AppConstant.SURVEY_QUESTION_LIST);
            alllquestionlist = (List<SurveyQuestionModel>) getArguments().getSerializable(AppConstant.ALL_SURVEY_QUESTION_LIST);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mainview = inflater.inflate(R.layout.fragment_one2_one_question, container, false);
        unbinder = ButterKnife.bind(this, mainview);
        return mainview;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
    }

    @Override
    public void onResume() {
        super.onResume();
        getLocationTask();
        qrcode = UserSessionManager.getUserSessionManager().getQRCode();
        if(tv_scancode!=null && qrcode!=null){
            tv_scancode.setText(qrcode);
        }
    }

    private void init(){
        Log.d("calling","init");
        oaasDatabase = OassDatabaseBuilder.provideOassDatabase(getActivity());
        mcontext=requireContext();
        new GetAllMemberAsynTask(this, oaasDatabase, 0).execute();

        mObjDialog = new ProgressDialog(getActivity());
        mObjDialog.setMessage("Saving Image,Please Wait..");
        mObjDialog.setIndeterminate(true);
        mObjDialog.setCancelable(false);
    }

    @Override
    public void onMemberFetchCompleted(List<MemberModel> memberModelList, int pos) {
        for (MemberModel model : memberModelList) {
            if (model.getServerId().equals(surveyQuestionModel.getMemberid())) {
                memberModel = model;
                if(memberModel.getRollbackStatus()!=null && memberModel.getRollbackStatus().equalsIgnoreCase("A")){
                    memberModel.setRollbackupdate("A");
                } else {
                    memberModel.setRollbackupdate("N");
                }
                getalldata();
            }
        }
    }

    public void getalldata(){
        Log.d("calling","getdata");
        Log.d("surveyquestionssize",""+surveyQuestionModel.getQuestion());

        if(txtQuestion==null){
            txtQuestion=mainview.findViewById(R.id.txt_question);
        }
        if(spinnersurveyQuestions==null){
            spinnersurveyQuestions=mainview.findViewById(R.id.spinner_question);
        }
        if(surveyQuestionModel.getOnetooneFlag()){
            txtQuestion.setVisibility(View.GONE);
            spinnersurveyQuestions.setVisibility(View.VISIBLE);
        } else {
            txtQuestion.setVisibility(View.VISIBLE);
            spinnersurveyQuestions.setVisibility(View.GONE);
        }

        spinnersurveyQuestions.setAdapter(new OnetoOneQuestionAdapter(mcontext, questionlist));
        spinnersurveyQuestions.setOnItemSelectedListener(this);
        spinnersurveyQuestions.setSelection(surveyQuestionModel.getQuestioncount());

        Log.d("questionid",""+memberModel.getRcCode()+","+memberModel.getNextquestionId());

        /*for(SurveyQuestionModel questionModel:questionlist){
            if(memberModel.getRcCode()!=0 && memberModel.getNextquestionId()!=0){
                if(memberModel.getRcCode().equals(questionModel.getRcCode()) && memberModel.getNextquestionId().equals(questionModel.getNextquestionId())){
                    surveyQuestionModel=questionModel;
                }
            }
        }*/

        if (surveyQuestionModel != null && !surveyQuestionModel.getOnetooneFlag()) {
            //Here setting the todo's value
            Log.d("calling","insidegetdata"+surveyQuestionModel.getQuestion());

            txtQuestion.setText(surveyQuestionModel.getQuestion()+"-"+surveyQuestionModel.getRcCode());
            previousques_Id=surveyQuestionModel.getSerialno();
            switch (Integer.valueOf(surveyQuestionModel.getQuestiontype())) {
                case 1:
                    View viewSingleline = LayoutInflater.from(mcontext).inflate(R.layout.answer_single_line, llAnswerViewContainer);
                    edtSingleLine = viewSingleline.findViewById(R.id.edt_answer_single_line);
                    et_remark=viewSingleline.findViewById(R.id.et_remark);
                    if(surveyQuestionModel.getApplyskip()==null || surveyQuestionModel.getApplyskip().equalsIgnoreCase("")&&
                            surveyQuestionModel.getApplytomessage()==null || surveyQuestionModel.getApplytomessage().equalsIgnoreCase("")){
                        surveyQuestionModel.setApplyskip("no");
                        surveyQuestionModel.setApplytomessage("no");
                    }
                    //for edit
                    if(surveyQuestionModel.getSurveyanswer()!=null &&
                            !memberModel.getRollbackupdate().equalsIgnoreCase("A")){
                        edtSingleLine.setText(surveyQuestionModel.getSurveyanswer());
                        et_remark.setText(surveyQuestionModel.getRemarkvalue());
                        edtSingleLine.setEnabled(false);
                        et_remark.setEnabled(false);
                        /*if(memberModel.getRollbackupdate().equalsIgnoreCase("A")){
                            edtSingleLine.setEnabled(true);
                            et_remark.setEnabled(true);
                        }else{
                            edtSingleLine.setEnabled(false);
                            et_remark.setEnabled(false);
                        }*/
                    }
                    break;
                case 2:
                    setCheckBox();
                    if(surveyQuestionModel.getApplyskip()==null || surveyQuestionModel.getApplyskip().equalsIgnoreCase("")&&
                            surveyQuestionModel.getApplytomessage()==null || surveyQuestionModel.getApplytomessage().equalsIgnoreCase("")){
                        surveyQuestionModel.setApplyskip("no");
                        surveyQuestionModel.setApplytomessage("no");
                    }
                    break;
                case 3:
                    HorizontalScrollView scrollView = new HorizontalScrollView(mcontext);
                    scrollView.setSmoothScrollingEnabled(true);
                    scrollView.setVerticalScrollBarEnabled(false);
                    LinearLayout.LayoutParams scrollviewParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    scrollviewParams.gravity = Gravity.END;
                    scrollView.setLayoutParams(scrollviewParams);
                    RadioGroup rg = new RadioGroup(mcontext); //create the RadioGroup
                    rg.setOrientation(LinearLayout.HORIZONTAL);
                    rg.setOnCheckedChangeListener(this);
                    String[] radioOption = surveyQuestionModel.getAnsweroptions().split(",");

                    for (int i = 0; i < radioOption.length; i++) {
                        radioButton = new RadioButton(mcontext);
                        radioButton.setText(StringUtils.capitalize(radioOption[i]));
                        radioButton.setTextColor(ContextCompat.getColor(mcontext, R.color.text_color_white));
                        radioButton.setBackground(ContextCompat.getDrawable(mcontext, R.drawable.rg_selector_yes));
                        radioButton.setButtonDrawable(null);
                        radioButton.setPadding(40, 10, 40, 10);
                        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                        params.setMargins(20, 20, 20, 20);
                        radioButton.setLayoutParams(params);
                        rg.addView(radioButton);
                    }
                    scrollView.addView(rg);
                    llAnswerViewContainer.addView(scrollView);
                    //for edit
                    if(surveyQuestionModel.getSurveyanswer()!=null && !memberModel.getRollbackupdate().equalsIgnoreCase("A")){
                        for (int i=0;i<=radioOption.length;i++) {
                            if(surveyQuestionModel.getSurveyanswer().equalsIgnoreCase(radioOption[i])){
                                radioButton.setChecked(true);
                            }
                        }

                        /*if(memberModel.getRollbackupdate().equalsIgnoreCase("A")){
                            radioButton.setEnabled(true);
                        }else{
                            radioButton.setEnabled(false);
                        }*/

                        radioButton.setEnabled(false);
                        //edtSingleLine.setText(surveyQuestionModel.getSurveyanswer());
                    }

                    /*View view = LayoutInflater.from(mcontext).inflate(R.layout.answer_radio_group_yes_no_layput, llAnswerViewContainer);
                    radioGroup = view.findViewById(R.id.rg_selector);
                    String[] strOption = surveyQuestionModel.getAnsweroptions().split(",");
                    if (strOption.length == 2) {
                        RadioButton radioButton1 = view.findViewById(R.id.rb_first);
                        radioButton1.setText(strOption[0]);
                        RadioButton radioButton2 = view.findViewById(R.id.rb_second);
                        radioButton2.setText(strOption[1]);
                    }
                    radioGroup.setOnCheckedChangeListener(this);*/
                    if(surveyQuestionModel.getApplyskip()==null || surveyQuestionModel.getApplyskip().equalsIgnoreCase("")&&
                            surveyQuestionModel.getApplytomessage()==null || surveyQuestionModel.getApplytomessage().equalsIgnoreCase("")){
                        surveyQuestionModel.setApplyskip("no");
                        surveyQuestionModel.setApplytomessage("no");
                    }
                    break;
                case 4:
                    break;
                case 5:
                    View vMultiLine = LayoutInflater.from(mcontext).inflate(R.layout.answer_multi_line, llAnswerViewContainer);
                    edtMultiline = vMultiLine.findViewById(R.id.edt_answer_multi_line);
                    et_remark=vMultiLine.findViewById(R.id.et_remark);
                    if(surveyQuestionModel.getApplyskip()==null || surveyQuestionModel.getApplyskip().equalsIgnoreCase("")&&
                            surveyQuestionModel.getApplytomessage()==null || surveyQuestionModel.getApplytomessage().equalsIgnoreCase("")){
                        surveyQuestionModel.setApplyskip("no");
                        surveyQuestionModel.setApplytomessage("no");
                    }
                    //for edit
                    if(surveyQuestionModel.getSurveyanswer()!=null && !memberModel.getRollbackupdate().equalsIgnoreCase("A")){
                        edtMultiline.setText(surveyQuestionModel.getSurveyanswer());
                        edtMultiline.setEnabled(false);
                        et_remark.setText(surveyQuestionModel.getRemarkvalue());
                        et_remark.setEnabled(false);
                        /*if(memberModel.getRollbackupdate().equalsIgnoreCase("A")){
                            edtMultiline.setEnabled(true);
                            et_remark.setEnabled(true);
                        }else{
                            edtMultiline.setEnabled(false);
                            et_remark.setEnabled(false);
                        }*/
                    }
                    break;
                case 6:

                    if (surveyQuestionModel.getMulti().equalsIgnoreCase(AppConstant.YES)) {
                        //todo for edit multiview
                        setMultiView();
                        if(surveyQuestionModel.getApplyskip()==null || surveyQuestionModel.getApplyskip().equalsIgnoreCase("")&&
                                surveyQuestionModel.getApplytomessage()==null || surveyQuestionModel.getApplytomessage().equalsIgnoreCase("")){
                            surveyQuestionModel.setApplyskip("no");
                            surveyQuestionModel.setApplytomessage("no");
                        }
                    } else {
                        View viewDropdown = LayoutInflater.from(mcontext).inflate(R.layout.answer_drop_down_view, llAnswerViewContainer);
                        et_remark=viewDropdown.findViewById(R.id.et_remark);
                        setDropDown(viewDropdown);
                        if(surveyQuestionModel.getApplyskip()==null || surveyQuestionModel.getApplyskip().equalsIgnoreCase("")&&
                                surveyQuestionModel.getApplytomessage()==null || surveyQuestionModel.getApplytomessage().equalsIgnoreCase("")){
                            surveyQuestionModel.setApplyskip("no");
                            surveyQuestionModel.setApplytomessage("no");
                        }
                    }

                    break;
                case 7:
                    View viewDatePicker = LayoutInflater.from(mcontext).inflate(R.layout.answer_date_picker, llAnswerViewContainer);
                    RelativeLayout rlDatePicker = viewDatePicker.findViewById(R.id.rl_dob);
                    et_remark=viewDatePicker.findViewById(R.id.et_remark);
                    rlDatePicker.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            DatePickerDialog datePickerDialog = new DatePickerDialog(mcontext, onDateSetListener
                                    , calendar.get(Calendar.YEAR)
                                    , calendar.get(Calendar.MONTH)
                                    , calendar.get(Calendar.DAY_OF_MONTH));
                            datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
                            datePickerDialog.show();
                        }
                    });
                    if(surveyQuestionModel.getApplyskip()==null || surveyQuestionModel.getApplyskip().equalsIgnoreCase("")&&
                            surveyQuestionModel.getApplytomessage()==null || surveyQuestionModel.getApplytomessage().equalsIgnoreCase("")){
                        surveyQuestionModel.setApplyskip("no");
                        surveyQuestionModel.setApplytomessage("no");
                    }
                    txtDate = viewDatePicker.findViewById(R.id.txt_dob);
                    if(surveyQuestionModel.getSurveyanswer()!=null && !memberModel.getRollbackupdate().equalsIgnoreCase("A")){
                        txtDate.setText(CommonUtils.getDate(surveyQuestionModel.getSurveyanswer()));
                        txtDate.setEnabled(false);
                        et_remark.setText(surveyQuestionModel.getRemarkvalue());
                        et_remark.setEnabled(false);
                        /*if(memberModel.getRollbackupdate().equalsIgnoreCase("A")){
                            txtDate.setEnabled(true);
                            et_remark.setEnabled(true);
                        }else{
                            txtDate.setEnabled(false);
                            et_remark.setEnabled(false);
                        }*/
                    }
                    break;
                case 8:
                    View vbt_Location = LayoutInflater.from(mcontext).inflate(R.layout.answer_location_bt, llAnswerViewContainer);
                    bt_Location = vbt_Location.findViewById(R.id.bt_location);
                    tv_location=vbt_Location.findViewById(R.id.selectedlocation);
                    et_remark=vbt_Location.findViewById(R.id.et_remark);
                    if(surveyQuestionModel.getApplyskip()==null || surveyQuestionModel.getApplyskip().equalsIgnoreCase("")&&
                            surveyQuestionModel.getApplytomessage()==null || surveyQuestionModel.getApplytomessage().equalsIgnoreCase("")){
                        surveyQuestionModel.setApplyskip("no");
                        surveyQuestionModel.setApplytomessage("no");
                    }
                    bt_Location.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            setBt_Location();
                        }
                    });
                    // for edit
                    if(surveyQuestionModel.getSurveyanswer()!=null && !memberModel.getRollbackupdate().equalsIgnoreCase("A")){
                        location_value=surveyQuestionModel.getSurveyanswer();
                        tv_location.setText("Location :"+surveyQuestionModel.getSurveyanswer());
                        bt_Location.setEnabled(false);
                        et_remark.setText(surveyQuestionModel.getRemarkvalue());
                        et_remark.setEnabled(false);
                        /*if(memberModel.getRollbackupdate().equalsIgnoreCase("A")){
                            bt_Location.setEnabled(true);
                            et_remark.setEnabled(true);
                        }else{
                            bt_Location.setEnabled(false);
                            et_remark.setEnabled(false);
                        }*/
                    }
                    break;
                case 9:
                    View vbt_upload = LayoutInflater.from(mcontext).inflate(R.layout.answer_location_bt, llAnswerViewContainer);
                    bt_Location = vbt_upload.findViewById(R.id.bt_location);
                    bt_Location.setText("Upload");
                    tv_location=vbt_upload.findViewById(R.id.selectedlocation);
                    et_remark=vbt_upload.findViewById(R.id.et_remark);
                    if(surveyQuestionModel.getApplyskip()==null || surveyQuestionModel.getApplyskip().equalsIgnoreCase("")&&
                            surveyQuestionModel.getApplytomessage()==null || surveyQuestionModel.getApplytomessage().equalsIgnoreCase("")){
                        surveyQuestionModel.setApplyskip("no");
                        surveyQuestionModel.setApplytomessage("no");
                    }
                    // for edit
                    if(surveyQuestionModel.getSurveyanswer()!=null && !memberModel.getRollbackupdate().equalsIgnoreCase("A")){
                        Upload=surveyQuestionModel.getSurveyanswer();
                        tv_location.setText(surveyQuestionModel.getSurveyanswer());
                        bt_Location.setEnabled(false);
                        et_remark.setText(surveyQuestionModel.getRemarkvalue());
                        et_remark.setEnabled(false);

                        /*if(memberModel.getRollbackupdate().equalsIgnoreCase("A")){
                            bt_Location.setEnabled(true);
                            et_remark.setEnabled(true);
                        }else{
                            bt_Location.setEnabled(false);
                            et_remark.setEnabled(false);
                        }*/
                    }
                    bt_Location.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            setBt_Upload();
                        }
                    });
                    break;
                case 10:
                    View vbt_qrscan = LayoutInflater.from(mcontext).inflate(R.layout.layout_qr_scan, llAnswerViewContainer);
                    im_scancode = vbt_qrscan.findViewById(R.id.ib_scan);
                    tv_scancode=vbt_qrscan.findViewById(R.id.tv_qrcode);
                    et_remark=vbt_qrscan.findViewById(R.id.et_remark);
                    if(surveyQuestionModel.getApplyskip()==null ||
                            surveyQuestionModel.getApplyskip().equalsIgnoreCase("") &&
                            surveyQuestionModel.getApplytomessage()==null ||
                            surveyQuestionModel.getApplytomessage().equalsIgnoreCase("")){
                        surveyQuestionModel.setApplyskip("no");
                        surveyQuestionModel.setApplytomessage("no");
                    }
                    //for edit
                    if(surveyQuestionModel.getSurveyanswer()!=null && !memberModel.getRollbackupdate().equalsIgnoreCase("A")){
                        qrcode=surveyQuestionModel.getSurveyanswer();
                        tv_scancode.setText(surveyQuestionModel.getSurveyanswer());
                        im_scancode.setEnabled(false);
                        et_remark.setText(surveyQuestionModel.getRemarkvalue());
                        et_remark.setEnabled(false);
/*
                        if(memberModel.getRollbackupdate().equalsIgnoreCase("A")){
                            im_scancode.setEnabled(true);
                            et_remark.setEnabled(true);
                        }else{
                            im_scancode.setEnabled(false);
                            et_remark.setEnabled(false);
                        }*/
                    }
                    im_scancode.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            //todo
                            getActivity().startActivity(new Intent(getActivity(), ScanQRCode.class));
                        }
                    });
                    break;
                case 11:
                    View viewDropdown = LayoutInflater.from(mcontext).inflate(R.layout.answer_drop_down_view, llAnswerViewContainer);
                    et_remark=viewDropdown.findViewById(R.id.et_remark);
                    setDropDown(viewDropdown);
                    if(surveyQuestionModel.getApplyskip()==null || surveyQuestionModel.getApplyskip().equalsIgnoreCase("")&&
                            surveyQuestionModel.getApplytomessage()==null || surveyQuestionModel.getApplytomessage().equalsIgnoreCase("")){
                        surveyQuestionModel.setApplyskip("no");
                        surveyQuestionModel.setApplytomessage("no");
                    }
                    break;
                case 12:
                    View viewDropdownnew = LayoutInflater.from(mcontext).inflate(R.layout.answer_drop_down_view, llAnswerViewContainer);
                    et_remark=viewDropdownnew.findViewById(R.id.et_remark);
                    LinearLayout layout=viewDropdownnew.findViewById(R.id.upload_layout);
                    layout.setVisibility(View.VISIBLE);
                    bt_Location = viewDropdownnew.findViewById(R.id.bt_location);
                    bt_Location.setText("Upload");
                    tv_location=viewDropdownnew.findViewById(R.id.selectedlocation);
                    setDropDown(viewDropdownnew);
                    if(surveyQuestionModel.getApplyskip()==null ||
                            surveyQuestionModel.getApplyskip().equalsIgnoreCase("")&&
                            surveyQuestionModel.getApplytomessage()==null ||
                            surveyQuestionModel.getApplytomessage().equalsIgnoreCase("")){
                        surveyQuestionModel.setApplyskip("no");
                        surveyQuestionModel.setApplytomessage("no");
                    }
                    bt_Location.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            setBt_Upload();
                        }
                    });
                    break;
            }
        }
        //unmask for todo
        //checkTodoToShow();
    }


    private void setCheckBox() {
        String[] strOption = surveyQuestionModel.getAnsweroptions().split(",");
        CheckBox checkBox = null;
        for (String option : strOption) {
            checkBox = new CheckBox(requireActivity());
            checkBox.setText(option);
            llAnswerViewContainer.addView(checkBox);
        }
        /*TextView valueTV = new TextView(requireActivity());
        valueTV.setText("Remark");
        valueTV.setId(Integer.parseInt("5"));
        valueTV.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        llAnswerViewContainer.addView(valueTV);
        // add edittext
        et_remark = null;
        et_remark = new EditText(requireActivity());
        LinearLayout.LayoutParams p = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        et_remark.setLayoutParams(p);
        et_remark.setId(Integer.parseInt("6"));
        llAnswerViewContainer.addView(et_remark);*/

        //for edit
        if(surveyQuestionModel.getSurveyanswer()!=null && !memberModel.getRollbackupdate().equalsIgnoreCase("A")){
            String[] ansOption = surveyQuestionModel.getSurveyanswer().split(",");
            for(String option : strOption){
                for (String option1 : ansOption) {
                    if(option.equalsIgnoreCase(option1)){
                        checkBox.setChecked(true);
                    }
                }
            }
            checkBox.setEnabled(false);
            /*if(memberModel.getRollbackupdate().equalsIgnoreCase("A")){
                checkBox.setEnabled(true);
            }else{
                checkBox.setEnabled(false);
            }*/
            /*if(et_remark!=null){
                et_remark.setText(surveyQuestionModel.getRemarkvalue());
                et_remark.setEnabled(false);
            }*/
        }

    }

    private void setBt_Location(){
        tv_location.setText("Location :"+location.getLatitude()+" , "+location.getLongitude());
        location_value=location.getLatitude()+","+location.getLongitude();
        //getLocationTask();
    }

    private void setBt_Upload(){
        imagePath = createFile();
        AlertDialog.Builder builder=new AlertDialog.Builder(getActivity());
        View v=LayoutInflater.from(getActivity()).inflate(R.layout.answer_upload_file,null);
        ImageButton im_camera=v.findViewById(R.id.click_camera);
        ImageButton im_attach=v.findViewById(R.id.attach_file);
        Button bt_cancel=v.findViewById(R.id.cancel_popup);
        builder.setCancelable(false);
        builder.setTitle("Upload File");
        builder.setView(v);
        bt_upload_dialog=builder.create();
        //Upload="not null";
        im_camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                picFile = new File(imagePath);
                picUri = Uri.fromFile(picFile);
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, picUri);
                startActivityForResult(cameraIntent, REQUEST_IMAGE_CAPTURE);
                //bt_upload_dialog.dismiss();
            }
        });
        im_attach.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String[] mimeTypes =
                        {"image/*","application/pdf","application/vnd.ms-excel"};

                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.addCategory(Intent.CATEGORY_OPENABLE);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    intent.setType(mimeTypes.length == 1 ? mimeTypes[0] : "*/*");
                    if (mimeTypes.length > 0) {
                        intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
                    }
                } else {
                    String mimeTypesStr = "";
                    for (String mimeType : mimeTypes) {
                        mimeTypesStr += mimeType + "|";
                    }
                    intent.setType(mimeTypesStr.substring(0,mimeTypesStr.length() - 1));
                }

                picFile = new File(imagePath);
                ImagePath = Uri.fromFile(picFile);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, ImagePath);
                startActivityForResult(Intent.createChooser(intent,"ChooseFile"), CHOOSE_IMAGE);
            }
        });

        bt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bt_upload_dialog.dismiss();
            }
        });
        bt_upload_dialog.show();
    }


    private void setDropDown(View viewDropdown) {

        Log.d("allquestions",""+alllquestionlist.size());
        String[] strOptions = surveyQuestionModel.getAnsweroptions().split(",");
        stringList = new ArrayList<>();
        for (String str : strOptions) {
            for(SurveyQuestionModel model:alllquestionlist){
                if(String.valueOf(model.getRcCode()).equals(str)){
                    stringList.add(model.getQuestion()+"-"+model.getRcCode());
                }
            }
        }
        stringList.add("Select Options");
        Spinner spinnerOption = viewDropdown.findViewById(R.id.spinner_option);
        spinnerOption.setOnItemSelectedListener(this);
        OptionAdapter optionAdapter = new OptionAdapter(mcontext, stringList);
        spinnerOption.setAdapter(optionAdapter);
        spinnerOption.setSelection(optionAdapter.getCount());

        if(surveyQuestionModel.getSurveyanswer()!=null &&
                !memberModel.getRollbackupdate().equalsIgnoreCase("A")){
            dropdown=surveyQuestionModel.getSurveyanswer();
            et_remark.setText(surveyQuestionModel.getRemarkvalue());
            et_remark.setEnabled(false);
            if(surveyQuestionModel.getQuestiontype().equalsIgnoreCase("12")){
                if(tv_location!=null && surveyQuestionModel.getUploadfile()!=null){
                    Upload=surveyQuestionModel.getUploadfile();
                    tv_location.setText(surveyQuestionModel.getUploadfile());
                }
            }
            for (int i=0;i<=stringList.size()-1;i++) {
                Log.d("dropdownvalues",""+stringList.get(i));
                if(stringList.get(i).equalsIgnoreCase(surveyQuestionModel.getSurveyanswer())){
                    spinnerOption.setSelection(i);
                    spinnerOption.setEnabled(false);
                }
            }
            /*if(memberModel.getRollbackupdate().equalsIgnoreCase("A")){
                spinnerOption.setEnabled(true);
                et_remark.setEnabled(true);
            }else{
                spinnerOption.setEnabled(false);
                et_remark.setEnabled(false);
            }*/
        }
    }

    public void setMultiView() {
        TextView textView=new TextView(mcontext);
        textView.setText("Select Options");
        textView.setPadding(40, 30, 40, 30);
        textView.setBackgroundResource(R.drawable.focused_edittext_stroke_1dp);
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String[] strOptions = surveyQuestionModel.getAnsweroptions().split(",");
                List<MultipleSelectionAnswerModel> multipleSelectionAnswerModelList = new ArrayList<>();
                for (String str : strOptions) {
                    multipleSelectionAnswerModelList.add(new MultipleSelectionAnswerModel(str));
                }

                if(multipleSelectionDialog!=null){
                    multipleSelectionDialog.show();
                    return;
                }
                multipleSelectionDialog = new MultipleSelectionDialog(One2OneQuestionFragment.this,multipleSelectionAnswerModelList,getLayoutInflater());
                multipleSelectionDialog.show();
            }
        });
        llAnswerViewContainer.addView(textView);
    }

    MultipleSelectionDialog multipleSelectionDialog;

    public void updateMultiSelectedOptions(List<MultipleSelectionAnswerModel> multipleSelectionAnswerModelList) {
        setMultipleViewUI(multipleSelectionAnswerModelList);
    }

    void setMultipleViewUI(final List<MultipleSelectionAnswerModel> multipleSelectionAnswerModelList){
        if (flowLayout!=null){
            flowLayout.removeAllViews();
        }else{
            flowLayout = new FlowLayout(llAnswerViewContainer.getContext());
            llAnswerViewContainer.addView(flowLayout);
        }
        flowLayout.setRowSpacing(30.0f);
        flowLayout.setChildSpacing(30);
        flowLayout.setPadding(25, 25, 25, 25);
        flowLayout.setBackgroundResource(R.drawable.unfocused_edittext_stroke_1dp);


        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(0,50,0,0);
        flowLayout.setLayoutParams(layoutParams);

        for (MultipleSelectionAnswerModel strMultiSelect : multipleSelectionAnswerModelList) {
            if (strMultiSelect.isSelected())
                flowLayout.addView(getFlowableTextView(strMultiSelect.getItemText(), flowLayout.getContext()));
        }
        if (flowLayout.getChildCount() > 0) {
            flowLayout.setVisibility(View.VISIBLE);
        }
    }


    public TextView getFlowableTextView(@NonNull String str, @NonNull Context context) {
        final TextView txtMultiOption = new TextView(context);
        txtMultiOption.setText(str);
        txtMultiOption.setTextColor(ContextCompat.getColor(context,android.R.color.black));
        txtMultiOption.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_clear_new,0);
        txtMultiOption.setPadding(40, 10, 20, 10);
        txtMultiOption.setCompoundDrawablePadding(20);
        txtMultiOption.setGravity(Gravity.CENTER);
        txtMultiOption.setBackgroundResource(R.drawable.focused_edittext_stroke_1dp);
        txtMultiOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                flowLayout.removeView(v);
                if (flowLayout.getChildCount() == 0) {
                    flowLayout.setVisibility(View.GONE);
                }
            }
        });

        return txtMultiOption;
    }

    public String getMultiSelectAnswer(@NonNull LinearLayout linearLayout) {
        int childCount = linearLayout.getChildCount();

        for (int i = 0; i < childCount; i++) {
            if (linearLayout.getChildAt(i) instanceof FlowLayout) {
                return getFlowLayoutSelected((FlowLayout) linearLayout.getChildAt(i));
            }
        }
        return "";
    }

    public String getFlowLayoutSelected(@NonNull FlowLayout flowLayout) {
        int flowLayoutChild = flowLayout.getChildCount();
        List<String> stringList = new ArrayList<>();
        for (int j = 0; j < flowLayoutChild; j++) {
            if (flowLayout.getChildAt(j) instanceof TextView) {
                TextView txtSelected = (TextView) flowLayout.getChildAt(j);
                if ((boolean) txtSelected.getTag()) {
                    stringList.add(txtSelected.getText().toString());
                }
            }
        }
        return TextUtils.join(",", stringList);
    }

    public void checkTodoToShow() {
        if (surveyQuestionModel.getApplytomessage().equalsIgnoreCase(AppConstant.YES)) {
            if (surveyAnswer != null && surveyAnswer.contains(",")) {
                String[] strOption = surveyAnswer.split(",");
                for (String matchString : strOption) {
                    if (surveyAnswer.contains(matchString)) {
                        new GetAllTodoTypeAsyncTask(this, provideOaasDatabase()).execute();
                        break;
                    }
                }
            } else {
                if (surveyQuestionModel.getApplytodovalue().contains(surveyAnswer)) {
                    new GetAllTodoTypeAsyncTask(this, provideOaasDatabase()).execute();
                }
            }
        }
    }

    private void setSpinnerTodo(List<TodoType> todoTypeList) {
        TodoType todoType = new TodoType();
        todoType.setName("Select Todo's");
        todoTypeList.add(todoType);
        spinnerTodo.setOnItemSelectedListener(this);
        TodoAdapter todoAdapter = new TodoAdapter(requireActivity(), todoTypeList);
        spinnerTodo.setAdapter(todoAdapter);
        spinnerTodo.setSelection(todoAdapter.getCount());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        if (group.getCheckedRadioButtonId() != -1) {
            RadioButton radioButton = group.findViewById(group.getCheckedRadioButtonId());
            this.surveyAnswer = radioButton.getText().toString().trim();
            checkTodoToShow();//
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

        String answered = "";
        for (int i = 0; i < llAnswerViewContainer.getChildCount(); i++) {
            CheckBox checkBox = (CheckBox) llAnswerViewContainer.getChildAt(i);
            if (checkBox.isChecked()) {
                answered = answered + checkBox.getText().toString();
            }
        }
        this.surveyAnswer = answered;
        if (!answered.isEmpty())
            checkTodoToShow();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (parent.getAdapter() instanceof OptionAdapter) {
            OptionAdapter optionAdapter = (OptionAdapter) parent.getAdapter();
            String string = (String) optionAdapter.getItem(position);
            if (string != null && !string.contains("Select Options")) {
                this.surveyAnswer = string;
                dropdown=string;
                //previousques_Id=surveyQuestionModel.getSerialno();
                for(SurveyQuestionModel model:alllquestionlist){
                    if(model.getQuestion().equalsIgnoreCase(string.substring(0, string.length() - 3))){
                        rccode=model.getRcCode();
                        nextquestionid=model.getSerialno();
                    }
                }
                for(SurveyQuestionModel questionModel:questionlist){
                    if(questionModel.getQuestion().equalsIgnoreCase(string.substring(0, string.length() - 3))){
                        dd_count=questionModel.getQuestioncount();
                        break;
                    }
                }
                //rccode= Integer.valueOf(string.substring(string.length() - 2));
                /*for (int i=0;i<questionlist.size();i++){
                    if (rccode != 0) {
                        if(rccode.equals(questionlist.get(i).getRcCode())){
                            nextquestionid=questionlist.get(i).getId();
                        }
                    }
                }*/
                //checkTodoToShow();
                Log.d("dropdownvalue",""+rccode+","
                        +nextquestionid+","+dd_count+","+previousques_Id);
                /*if(surveyQuestionModel.getApplytodovalue()!=null){
                    String[] strOption = surveyQuestionModel.getApplytodovalue().split(",");
                    SetTodos(strOption,string);
                }*/
            }
            return;
        }
        if (parent.getAdapter() instanceof TodoAdapter) {
            TodoAdapter todoAdapter = (TodoAdapter) parent.getAdapter();
            TodoType todoType = (TodoType) todoAdapter.getItem(position);
            if (todoType != null && !todoType.getName().contains("Select Todo's")) {
                this.todoType = todoType;
                this.isTodoDone = true;
            }
            return;
        }
        if (parent.getAdapter() instanceof OnetoOneQuestionAdapter) {
            OnetoOneQuestionAdapter todoAdapter = (OnetoOneQuestionAdapter) parent.getAdapter();
            SurveyQuestionModel model = (SurveyQuestionModel) todoAdapter.getItem(position);
            if (model != null) {
                Log.d("questionpos",""+position);
                llAnswerViewContainer.removeAllViewsInLayout();
                //model.setQuestioncount(surveyQuestionModel.getQuestioncount()+position++);
                //questiondropdown(model);
            }
            return;
        }
    }

    private void questiondropdown(SurveyQuestionModel model){
        Log.d("dropdownquestion",""+model);
        //Log.d("dropdownpos",""+dropdownpos);
        switch (Integer.valueOf(model.getQuestiontype())) {
            case 1:
                View viewSingleline = LayoutInflater.from(mcontext).inflate(R.layout.answer_single_line, llAnswerViewContainer);
                edtSingleLine = viewSingleline.findViewById(R.id.edt_answer_single_line);
                et_remark=viewSingleline.findViewById(R.id.et_remark);
                if(model.getApplyskip()==null || model.getApplyskip().equalsIgnoreCase("")&&
                        model.getApplytomessage()==null || model.getApplytomessage().equalsIgnoreCase("")){
                    model.setApplyskip("no");
                    model.setApplytomessage("no");
                }
                //for edit
                if(model.getSurveyanswer()!=null){
                    edtSingleLine.setText(model.getSurveyanswer());
                    edtSingleLine.setEnabled(false);
                    et_remark.setText(model.getRemarkvalue());
                    et_remark.setEnabled(false);
                }
                break;
            case 2:
                setCheckBox();
                if(model.getApplyskip()==null || model.getApplyskip().equalsIgnoreCase("")&&
                        model.getApplytomessage()==null || model.getApplytomessage().equalsIgnoreCase("")){
                    model.setApplyskip("no");
                    model.setApplytomessage("no");
                }
                break;
            case 3:
                HorizontalScrollView scrollView = new HorizontalScrollView(mcontext);
                scrollView.setSmoothScrollingEnabled(true);
                scrollView.setVerticalScrollBarEnabled(false);
                LinearLayout.LayoutParams scrollviewParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                scrollviewParams.gravity = Gravity.END;
                scrollView.setLayoutParams(scrollviewParams);
                RadioGroup rg = new RadioGroup(mcontext); //create the RadioGroup
                rg.setOrientation(LinearLayout.HORIZONTAL);
                rg.setOnCheckedChangeListener(this);
                String[] radioOption = model.getAnsweroptions().split(",");

                for (int i = 0; i < radioOption.length; i++) {
                    radioButton = new RadioButton(mcontext);
                    radioButton.setText(StringUtils.capitalize(radioOption[i]));
                    radioButton.setTextColor(ContextCompat.getColor(mcontext, R.color.text_color_white));
                    radioButton.setBackground(ContextCompat.getDrawable(mcontext, R.drawable.rg_selector_yes));
                    radioButton.setButtonDrawable(null);
                    radioButton.setPadding(40, 10, 40, 10);
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    params.setMargins(20, 20, 20, 20);
                    radioButton.setLayoutParams(params);
                    rg.addView(radioButton);
                }
                scrollView.addView(rg);
                llAnswerViewContainer.addView(scrollView);
                //for edit
                if(model.getSurveyanswer()!=null){
                    for (int i=0;i<=radioOption.length;i++) {
                        if(model.getSurveyanswer().equalsIgnoreCase(radioOption[i])){
                            radioButton.setChecked(true);
                        }
                    }
                    radioButton.setEnabled(false);
                    //edtSingleLine.setText(surveyQuestionModel.getSurveyanswer());
                }

                    /*View view = LayoutInflater.from(mcontext).inflate(R.layout.answer_radio_group_yes_no_layput, llAnswerViewContainer);
                    radioGroup = view.findViewById(R.id.rg_selector);
                    String[] strOption = surveyQuestionModel.getAnsweroptions().split(",");
                    if (strOption.length == 2) {
                        RadioButton radioButton1 = view.findViewById(R.id.rb_first);
                        radioButton1.setText(strOption[0]);
                        RadioButton radioButton2 = view.findViewById(R.id.rb_second);
                        radioButton2.setText(strOption[1]);
                    }
                    radioGroup.setOnCheckedChangeListener(this);*/
                if(model.getApplyskip()==null || model.getApplyskip().equalsIgnoreCase("")&&
                        model.getApplytomessage()==null || model.getApplytomessage().equalsIgnoreCase("")){
                    model.setApplyskip("no");
                    model.setApplytomessage("no");
                }
                break;
            case 4:
                break;
            case 5:
                View vMultiLine = LayoutInflater.from(mcontext).inflate(R.layout.answer_multi_line, llAnswerViewContainer);
                edtMultiline = vMultiLine.findViewById(R.id.edt_answer_multi_line);
                et_remark=vMultiLine.findViewById(R.id.et_remark);
                if(model.getApplyskip()==null || model.getApplyskip().equalsIgnoreCase("")&&
                        model.getApplytomessage()==null || model.getApplytomessage().equalsIgnoreCase("")){
                    model.setApplyskip("no");
                    model.setApplytomessage("no");
                }
                //for edit
                if(model.getSurveyanswer()!=null){
                    edtMultiline.setText(model.getSurveyanswer());
                    edtMultiline.setEnabled(false);
                    et_remark.setText(model.getRemarkvalue());
                    et_remark.setEnabled(false);
                }
                break;
            case 6:

                if (model.getMulti().equalsIgnoreCase(AppConstant.YES)) {
                    //todo for edit multiview
                    setMultiView();
                    if(model.getApplyskip()==null || model.getApplyskip().equalsIgnoreCase("")&&
                            model.getApplytomessage()==null || model.getApplytomessage().equalsIgnoreCase("")){
                        model.setApplyskip("no");
                        model.setApplytomessage("no");
                    }
                } else {
                    View viewDropdown = LayoutInflater.from(mcontext).inflate(R.layout.answer_drop_down_view, llAnswerViewContainer);
                    et_remark=viewDropdown.findViewById(R.id.et_remark);
                    setDropDown(viewDropdown);
                    if(model.getApplyskip()==null || model.getApplyskip().equalsIgnoreCase("")&&
                            model.getApplytomessage()==null || model.getApplytomessage().equalsIgnoreCase("")){
                        model.setApplyskip("no");
                        model.setApplytomessage("no");
                    }
                }

                break;
            case 7:
                View viewDatePicker = LayoutInflater.from(mcontext).inflate(R.layout.answer_date_picker, llAnswerViewContainer);
                RelativeLayout rlDatePicker = viewDatePicker.findViewById(R.id.rl_dob);
                et_remark=viewDatePicker.findViewById(R.id.et_remark);
                rlDatePicker.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        DatePickerDialog datePickerDialog = new DatePickerDialog(mcontext, onDateSetListener
                                , calendar.get(Calendar.YEAR)
                                , calendar.get(Calendar.MONTH)
                                , calendar.get(Calendar.DAY_OF_MONTH));
                        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
                        datePickerDialog.show();
                    }
                });
                if(model.getApplyskip()==null || model.getApplyskip().equalsIgnoreCase("")&&
                        model.getApplytomessage()==null || model.getApplytomessage().equalsIgnoreCase("")){
                    model.setApplyskip("no");
                    model.setApplytomessage("no");
                }
                txtDate = viewDatePicker.findViewById(R.id.txt_dob);
                if(model.getSurveyanswer()!=null){
                    txtDate.setText(CommonUtils.getDate(model.getSurveyanswer()));
                    txtDate.setEnabled(false);

                    et_remark.setText(model.getRemarkvalue());
                    et_remark.setEnabled(false);
                }
                break;
            case 8:
                View vbt_Location = LayoutInflater.from(mcontext).inflate(R.layout.answer_location_bt, llAnswerViewContainer);
                bt_Location = vbt_Location.findViewById(R.id.bt_location);
                tv_location=vbt_Location.findViewById(R.id.selectedlocation);
                et_remark=vbt_Location.findViewById(R.id.et_remark);
                if(model.getApplyskip()==null || model.getApplyskip().equalsIgnoreCase("")&&
                        model.getApplytomessage()==null || model.getApplytomessage().equalsIgnoreCase("")){
                    model.setApplyskip("no");
                    model.setApplytomessage("no");
                }
                bt_Location.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        setBt_Location();
                    }
                });
                // for edit
                if(model.getSurveyanswer()!=null){
                    location_value=model.getSurveyanswer();
                    tv_location.setText("Location :"+model.getSurveyanswer());
                    bt_Location.setEnabled(false);

                    et_remark.setText(model.getRemarkvalue());
                    et_remark.setEnabled(false);
                }
                break;
            case 9:
                View vbt_upload = LayoutInflater.from(mcontext).inflate(R.layout.answer_location_bt, llAnswerViewContainer);
                bt_Location = vbt_upload.findViewById(R.id.bt_location);
                bt_Location.setText("Upload");
                tv_location=vbt_upload.findViewById(R.id.selectedlocation);
                et_remark=vbt_upload.findViewById(R.id.et_remark);
                if(model.getApplyskip()==null || model.getApplyskip().equalsIgnoreCase("")&&
                        model.getApplytomessage()==null || model.getApplytomessage().equalsIgnoreCase("")){
                    model.setApplyskip("no");
                    model.setApplytomessage("no");
                }
                // for edit
                if(model.getSurveyanswer()!=null){
                    Upload=model.getSurveyanswer();
                    tv_location.setText(model.getSurveyanswer());
                    bt_Location.setEnabled(false);
                    et_remark.setText(model.getRemarkvalue());
                    et_remark.setEnabled(false);
                }
                bt_Location.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        setBt_Upload();
                    }
                });
                break;
            case 10:
                View vbt_qrscan = LayoutInflater.from(mcontext).inflate(R.layout.layout_qr_scan, llAnswerViewContainer);
                im_scancode = vbt_qrscan.findViewById(R.id.ib_scan);
                tv_scancode=vbt_qrscan.findViewById(R.id.tv_qrcode);
                et_remark=vbt_qrscan.findViewById(R.id.et_remark);
                if(model.getApplyskip()==null || model.getApplyskip().equalsIgnoreCase("")&&
                        model.getApplytomessage()==null || model.getApplytomessage().equalsIgnoreCase("")){
                    model.setApplyskip("no");
                    model.setApplytomessage("no");
                }
                // for edit
                if(model.getSurveyanswer()!=null){
                    qrcode=model.getSurveyanswer();
                    tv_scancode.setText(model.getSurveyanswer());
                    im_scancode.setEnabled(false);
                    et_remark.setText(model.getRemarkvalue());
                    et_remark.setEnabled(false);
                }
                im_scancode.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //todo
                        getActivity().startActivity(new Intent(getActivity(), ScanQRCode.class));
                    }
                });
                break;
            case 11:
                View viewDropdown = LayoutInflater.from(mcontext).inflate(R.layout.answer_drop_down_view, llAnswerViewContainer);
                et_remark=viewDropdown.findViewById(R.id.et_remark);
                setDropDown(viewDropdown);
                if(surveyQuestionModel.getApplyskip()==null || surveyQuestionModel.getApplyskip().equalsIgnoreCase("")&&
                        surveyQuestionModel.getApplytomessage()==null || surveyQuestionModel.getApplytomessage().equalsIgnoreCase("")){
                    surveyQuestionModel.setApplyskip("no");
                    surveyQuestionModel.setApplytomessage("no");
                }
                break;
        }
    }

    private void SetTodos(String[] strOption, String selectedvalue) {
        for(String str:strOption){
            if(str.contains(selectedvalue)){
                for(int i=0;i <= mtodoTypes.size();i++){
                    try {
                        if(str.contains(String.valueOf(mtodoTypes.get(i).getId()))){
                            int todo_id=mtodoTypes.get(i).getId();
                            Log.d("todovalue1",""+todo_id);
                            spinnerTodo.setSelection(i);
                            break;
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public SurveyQuestionModel getSurveyQuestionModel() {
        return surveyQuestionModel;
    }

    @Override
    public boolean isSurveyQuestionAnswered() {
        checkQuestionAnswered();
        //init();
        return isSurveyAnswered;
    }

    @Override
    public SurveyAnswerModel getSurveyAnswerModel() {
        SurveyAnswerModel surveyAnswerModel = new SurveyAnswerModel();
        surveyAnswerModel.setAnswer(surveyAnswer);
        surveyAnswerModel.setQuestionid(surveyQuestionModel.getId());
        surveyAnswerModel.setPushed(false);
        surveyAnswerModel.setLastmodifiedtime(CommonUtils.localDbDateFormat(Calendar.getInstance().getTime()));
        surveyAnswerModel.setLastmodifiedby(String.valueOf(getUserSessionManager().getUser() != null
                ? getUserSessionManager().getUser().getId() : 0));
        surveyAnswerModel.setCreatedby(getUserSessionManager().getUser() != null
                ? getUserSessionManager().getUser().getId() : 0);
        surveyAnswerModel.setCreatedtime(CommonUtils.localDbDateFormat(Calendar.getInstance().getTime()));
        return surveyAnswerModel;
    }

    @Override
    public TodoModel getTodoModel() {
        TodoModel todoModel = new TodoModel();
        todoModel.setTodostatusid(1);
        todoModel.setTodotypeid(todoType.getId());
        Calendar calendar = Calendar.getInstance();
        todoModel.setCreatedtime(CommonUtils.localDbDateFormat(calendar.getTime()));
        if (todoType.getDue() != null)
            calendar.add(Calendar.DATE, Integer.parseInt(todoType.getDue()));
        todoModel.setFollowupdate(CommonUtils.localDbDateFormat(calendar.getTime()));
        UserModel userModel = getUserSessionManager().getUser();
        if (userModel != null) {
            todoModel.setAssignedto(userModel.getId());
            todoModel.setCreatedby(userModel.getId());
        }
        todoModel.setPushed(false);


        return todoModel;
    }

    @Override
    public boolean isTodoDone() {
        return isTodoDone;
    }

    @Override
    public int getPreviousQuestionId() {
        return previousQuestionId;
    }

    @Override
    public void setPreviousQuestionId(int previousQuestionId) {
        this.previousQuestionId = previousQuestionId;
    }

    public boolean checkQuestionAnswered() {

        switch (Integer.valueOf(surveyQuestionModel.getQuestiontype())) {
            case 1:
                if (edtSingleLine.getText().length() > 0) {
                    this.surveyAnswer = edtSingleLine.getText().toString().trim();
                    surveyQuestionModel.setAnswer(edtSingleLine.getText().toString().trim());
                    isSurveyAnswered = !TextUtils.isEmpty(surveyAnswer);
                    surveyQuestionModel.setRemarkvalue(et_remark.getText().toString());
                }
                break;
            case 2:
                int childCount = llAnswerViewContainer.getChildCount();
                List<String> stringList = new ArrayList<>();
                //remove the last index of , from string
                for (int i = 0; i < childCount; i++) {
                    CheckBox checkBox = (CheckBox) llAnswerViewContainer.getChildAt(i);
                    if (checkBox.isChecked()) {
                        stringList.add(checkBox.getText().toString().trim());
                    }
                }
                this.surveyAnswer = TextUtils.join(",", stringList);
                surveyQuestionModel.setAnswer(TextUtils.join(",", stringList));
                surveyQuestionModel.setRemarkvalue(et_remark.getText().toString());
                isSurveyAnswered = !stringList.isEmpty();
                break;
            case 3:
                this.isSurveyAnswered = !TextUtils.isEmpty(surveyAnswer);
                break;
            case 4:
                break;
            case 5:
                if (edtMultiline != null) {
                    this.surveyAnswer = edtMultiline.getText().toString().trim();
                    surveyQuestionModel.setAnswer(edtMultiline.getText().toString().trim());
                    surveyQuestionModel.setRemarkvalue(et_remark.getText().toString());
                    isSurveyAnswered = edtMultiline.getText().length() > 0;
                }
                break;
            case 6:
                if (surveyQuestionModel.getMulti().equalsIgnoreCase(AppConstant.YES)) {
                    if (!TextUtils.isEmpty(getFlowableChild())){
                        surveyQuestionModel.setAnswer(getFlowableChild());
                        surveyAnswer=getFlowableChild();
                        isSurveyAnswered=true;
                    }
                } else {
                    isSurveyAnswered = !surveyAnswer.isEmpty();
                }
                break;
            case 7:
                if (!TextUtils.isEmpty(txtDate.getText())) {
                    surveyAnswer = calendar.get(Calendar.YEAR) + "-" + calendar.get(Calendar.MONTH) + "-" + calendar.get(Calendar.DAY_OF_MONTH) + "T00:00:00.000Z";
                    surveyQuestionModel.setAnswer(calendar.get(Calendar.YEAR) + "-" + calendar.get(Calendar.MONTH) + "-" + calendar.get(Calendar.DAY_OF_MONTH) + "T00:00:00.000Z");
                    surveyQuestionModel.setRemarkvalue(et_remark.getText().toString());
                    isSurveyAnswered = !surveyAnswer.isEmpty();
                }
                break;
            case 8:
                if (location_value!=null) {
                    surveyAnswer = location_value;
                    surveyQuestionModel.setRemarkvalue(et_remark.getText().toString());
                    surveyQuestionModel.setAnswer(location_value);
                    isSurveyAnswered = !surveyAnswer.isEmpty();
                }
                break;
            case 9:
                if (Upload!=null) {
                    surveyAnswer = Upload;
                    surveyQuestionModel.setAnswer(Upload);
                    surveyQuestionModel.setRemarkvalue(et_remark.getText().toString());
                    isSurveyAnswered = !surveyAnswer.isEmpty();
                }
                break;
            case 10:
                if (qrcode!=null) {
                    surveyAnswer = qrcode;
                    surveyQuestionModel.setAnswer(qrcode);
                    surveyQuestionModel.setRemarkvalue(et_remark.getText().toString());
                    isSurveyAnswered = !surveyAnswer.isEmpty();
                }
                break;
            case 11:
                if (dropdown!=null) {
                    surveyAnswer = dropdown;
                    surveyQuestionModel.setRemarkvalue(et_remark.getText().toString());
                    surveyQuestionModel.setAnswer(dropdown);
                    surveyQuestionModel.setPreviousquestionId(previousques_Id);
                    surveyQuestionModel.setNextquestionId(nextquestionid);
                    surveyQuestionModel.setNextrccode(rccode);
                    surveyQuestionModel.setDdQuestioncount(dd_count==0?++dd_count+surveyQuestionModel.getQuestioncount():dd_count);
                    isSurveyAnswered = !surveyAnswer.isEmpty();
                }
                break;
            case 12:
                if (dropdown!=null) {
                    surveyAnswer = dropdown;
                    surveyQuestionModel.setRemarkvalue(et_remark.getText().toString());
                    surveyQuestionModel.setAnswer(dropdown);
                    surveyQuestionModel.setUploadfile(Upload);
                    surveyQuestionModel.setPreviousquestionId(previousques_Id);
                    surveyQuestionModel.setNextquestionId(nextquestionid);
                    surveyQuestionModel.setNextrccode(rccode);
                    surveyQuestionModel.setDdQuestioncount(dd_count==0?++dd_count+surveyQuestionModel.getQuestioncount():dd_count);
                    isSurveyAnswered = !surveyAnswer.isEmpty();
                }
                break;
        }
        return false;
    }

    private String getFlowableChild(){
        int count = flowLayout.getChildCount();
        if (count>0){
            List<String> stringList=new ArrayList<>();
            for (int i=0;i<count;i++){
                if (flowLayout.getChildAt(i) instanceof TextView){
                    stringList.add(((TextView) flowLayout.getChildAt(i)).getText().toString());
                }
            }
            return TextUtils.join(",",stringList);
        }
        return "";
    }

    @Override
    public void onTodoTypeFetchComplete(List<TodoType> todoTypeList) {
        mtodoTypes=todoTypeList;
        llTodo.setVisibility(View.VISIBLE);
        setSpinnerTodo(todoTypeList);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    private Boolean hasLocationPermission() {
        return EasyPermissions.hasPermissions(requireActivity(), LOCATION_PERMISSION);
    }

    @AfterPermissionGranted(RC_COARSE_LOCATION)
    public void getLocationTask() {

        if (hasLocationPermission()) {
            //On Permission Granted
            createLocationRequest();
        } else {
            EasyPermissions.requestPermissions(
                    this
                    , "Please allow location and storage access."
                    , RC_COARSE_LOCATION
                    , LOCATION_PERMISSION);
        }

    }

    @Override
    public void onPermissionsGranted(int requestCode, @NonNull List<String> perms) {
        //On Permission Granted
        createLocationRequest();
    }

    @Override
    public void onPermissionsDenied(int requestCode, @NonNull List<String> perms) {
        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
            new AppSettingsDialog.Builder(this).build().show();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == AppSettingsDialog.DEFAULT_SETTINGS_REQ_CODE) {

        } else if (resultCode == AppConstant.GPS_REQUEST) {
            getLocationTask();
        } else if(requestCode==REQUEST_IMAGE_CAPTURE && resultCode==RESULT_OK){
            mObjDialog.show();
            Log.d("imagepath",""+imagePath);
            new ImageCompression().execute(imagePath);
        } else if(resultCode==RESULT_OK && requestCode==CHOOSE_IMAGE){
            String imageName;
            String path = Environment.getExternalStorageDirectory().toString()+"/Conserve";
            String PathHolder = data.getData().getPath();
            //Log.d("PathHolder",PathHolder);
            if(PathHolder.contains(".pdf")){
                //Log.d("entered","pdf");
                imageName = "cbiz"+String.valueOf(CommonUtils.getDateandTimeString())+".pdf";
            } else if(PathHolder.contains(".xlsx")){
                //Log.d("entered","xlsx");
                imageName = "cbiz"+String.valueOf(CommonUtils.getDateandTimeString())+".xlsx";
            } else {
                //Log.d("entered","jpg");
                imageName = "cbiz"+String.valueOf(CommonUtils.getDateandTimeString())+".jpg";
            }
            File destination= new File(path, imageName);
            try {
                copyFile(new File(PathUtils.getPath(getActivity(),data.getData())), destination);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
            bt_upload_dialog.dismiss();
            if(Upload==null){
                Upload=imageName;
            }else{
                Upload=Upload+","+imageName;
            }
            tv_location.setText(Upload);
        }
    }

    private void copyFile(File sourceFile, File destFile) throws IOException {
        Log.d("sourcefile",""+sourceFile);
        if (!sourceFile.exists()) {
            return;
        }
        FileChannel source = null;
        FileChannel destination = null;
        source = new FileInputStream(sourceFile).getChannel();
        destination = new FileOutputStream(destFile).getChannel();
        if (destination != null && source != null) {
            destination.transferFrom(source, 0, source.size());
        }
        if (source != null) {
            source.close();
        }
        if (destination != null) {
            destination.close();
        }
        //TODO insert images count
        //InsertImagesCount();

    }


    @Override
    public void onRationaleAccepted(int requestCode) {
        getLocationTask();
    }

    @Override
    public void onRationaleDenied(int requestCode) {
        Toast.makeText(requireActivity(), "Rationale Denied", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onSuccess(Object o) {
        if (o instanceof Location) {
            this.location = (Location) o;
            //edtLat.setText(String.valueOf(location.getLatitude()));
            //edtLong.setText(String.valueOf(location.getLongitude()));
        }
    }

    LocationRequest locationRequest;

    @SuppressLint("MissingPermission")
    protected void createLocationRequest() {

        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(requireActivity());
        locationRequest = LocationRequest.create();
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(5000);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);

        SettingsClient client = LocationServices.getSettingsClient(requireActivity());
        Task<LocationSettingsResponse> task = client.checkLocationSettings(builder.build());

        task.addOnSuccessListener(new OnSuccessListener<LocationSettingsResponse>() {
            @Override
            public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                fusedLocationProviderClient.getLastLocation().addOnSuccessListener(One2OneQuestionFragment.this);
            }
        });
        task.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                int statusCode = ((ApiException) e).getStatusCode();
                switch (statusCode) {
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        try {
                            ResolvableApiException rae = (ResolvableApiException) e;
                            rae.startResolutionForResult(requireActivity(), AppConstant.GPS_REQUEST);
                        } catch (IntentSender.SendIntentException sie) {
                            Log.i("GPS", "PendingIntent unable to execute request.");
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        String errorMessage = "Location settings are inadequate, and cannot be " +
                                "fixed here. Fix in Settings.";
                        Toast.makeText(requireActivity(), errorMessage, Toast.LENGTH_LONG).show();

                }
            }
        });
    }

    @Override
    public void onComplete() {
        Log.d("insertimage","inserted");
    }

    @Override
    public void onUpdate() {

    }

    public class ImageCompression extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... strings) {
            if (strings.length == 0 || strings[0] == null)
                return null;
            return compressImage(strings[0]);
        }
        protected void onPostExecute(String imagePath) {
            mObjDialog.dismiss();
            bt_upload_dialog.dismiss();

            //todo insert image count
            //InsertImagesCount();

            Log.d("immagename",""+imagePath+"-"+imageName);
            if(Upload==null){
                Upload=imageName;
            } else {
                Upload=Upload+","+imageName;
            }
            tv_location.setText(Upload);
        }
    }

    private void InsertImagesCount(){
        if(oaasDatabase!=null){
            InsertImages images = new InsertImages();
            images.setName("insert");
            new SetInsertImagesAsyncTask(this,oaasDatabase,images).execute();
        }else {
            //Toast.makeText(getActivity(), "db not created", Toast.LENGTH_SHORT).show();
            Log.d("roomdb","notcreated");
        }

        /*InsertImages insertImages =new InsertImages();
        insertImages.setName("");

        ImagesModelDao imagesDao = oaasDatabase.provideInsertImagesDao();

        if (imagesDao.getImageCount() == 0) {
            insertImages.setImagecount(1);
            insertImages.setId(1);
        } else {
            InsertImages lastImageModel = imagesDao.getInsertImages();
            insertImages.setImagecount(lastImageModel.getImagecount()+1);
            insertImages.setId(lastImageModel.getId() + 1);
        }
        imagesDao.insertImage(insertImages);*/
    }

    public static String compressImage(String imagePath) {
        Bitmap scaledBitmap = null;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        Bitmap bitmap = BitmapFactory.decodeFile(imagePath, options);

        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;

        float imgRatio = (float) actualWidth / (float) actualHeight;
        float maxRatio = maxWidth / maxHeight;

        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) maxHeight;
            } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;
            }
        }
        options.inSampleSize = calculateSampleSize(options, actualWidth, actualHeight);
        options.inJustDecodeBounds = false;
        options.inDither = false;
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];
        try {
            bitmap = BitmapFactory.decodeFile(imagePath, options);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }
        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.RGB_565);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }
        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;
        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);
        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bitmap, middleX - bitmap.getWidth() / 2, middleY - bitmap.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));
        if (bitmap != null) {
            bitmap.recycle();
        }
        ExifInterface exif;
        try {
            exif = new ExifInterface(imagePath);
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 0);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
            } else if (orientation == 3) {
                matrix.postRotate(180);
            } else if (orientation == 8) {
                matrix.postRotate(270);
            }
            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);
        } catch (IOException e) {
            e.printStackTrace();
        }
        FileOutputStream out = null;
        String filepath = imagePath;
        try {
            out = new FileOutputStream(filepath);
            scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 80, out);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return filepath;
    }

    public static int calculateSampleSize(BitmapFactory.Options options, int requireWidth, int requireHeight) {
        final int actualHeight = options.outHeight;
        final int actualWidth = options.outWidth;
        int sampleSize = 1;

        if (actualHeight > requireHeight || actualWidth > requireWidth) {
            final int heightRatio = Math.round((float) actualHeight / (float) requireHeight);
            final int widthRatio = Math.round((float) actualWidth / (float) requireWidth);
            sampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        final float totalPixels = actualWidth * actualHeight;
        final float totalReqPixelsCap = requireWidth * requireHeight * 2;
        while (totalPixels / (sampleSize * sampleSize) > totalReqPixelsCap) {
            sampleSize++;
        }
        return sampleSize;
    }

    public static String createFile() {
        File imageFile = new File(Environment.getExternalStorageDirectory() + "/Conserve");
        if (!imageFile.exists()) {
            imageFile.mkdirs();
        }
        imageName = "cbiz"+String.valueOf(CommonUtils.getDateandTimeString())+".jpg";
        String uri = (imageFile.getAbsolutePath() + "/" + imageName);
        return uri;
    }
}
