/*
 * Developed by Avinash Kumar singh on 24/1/19 3:49 PM
 * Last Modified 16/1/19 5:57 PM.
 *
 * Copyright (c) 2019.  All rights reserved.
 */

package com.app.fsm.ui.dash.main.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.fsm.R;
import com.app.fsm.local_network.network.model.FeatureModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RvMemberAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private List<FeatureModel> featureModelList;

    public RvMemberAdapter(List<FeatureModel> featureModelList) {
        this.featureModelList = featureModelList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_member_data, parent, false);
        return new RvViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, final int position) {

        if (holder instanceof RvViewHolder) {
            ((RvViewHolder) holder).bind(featureModelList.get(position));

        }
    }

    @Override
    public int getItemCount() {
        return featureModelList.size();
    }


    class RvViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_number)
        TextView txtNumber;
        @BindView(R.id.txt_member_detail)
        TextView txtMemberDetail;

        public RvViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        /*public void bind(final FeatureModel featureModel) {
            txtNumber.setText(String.valueOf(100));
            txtMemberDetail.setText("Member Registered");
        }*/
        public void bind(final FeatureModel featureModel) {
            txtNumber.setText(String.valueOf(featureModel.getIc_option()));
            txtMemberDetail.setText(featureModel.getStrOption());
        }
    }

}
