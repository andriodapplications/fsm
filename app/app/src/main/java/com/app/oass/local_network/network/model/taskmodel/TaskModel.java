package com.app.fsm.local_network.network.model.taskmodel;

public class TaskModel {
    private String name,id,disease,date;

    public TaskModel(String name, String id, String disease, String date) {
        this.name = name;
        this.id = id;
        this.disease = disease;
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }

    public String getDisease() {
        return disease;
    }

    public String getDate() {
        return date;
    }
}
