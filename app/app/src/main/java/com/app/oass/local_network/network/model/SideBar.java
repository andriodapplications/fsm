package com.app.fsm.local_network.network.model;

public class SideBar {

    String strSidebarElement;

    int drawable;

    public SideBar(String strSidebarElement, int drawable) {
        this.strSidebarElement = strSidebarElement;
        this.drawable = drawable;
    }

    public String getSidebarElement() {
        return strSidebarElement;
    }

    public void setSidebarElement(String strSidebarElement) {
        this.strSidebarElement = strSidebarElement;
    }

    public int getDrawable() {
        return drawable;
    }

    public void setDrawable(int drawable) {
        this.drawable = drawable;
    }

}
