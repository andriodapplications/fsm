package com.app.fsm.ui.dash.main.asynctask;

import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.app.fsm.local_network.network.model.category.CategoryModel;
import com.app.fsm.local_network.network.model.category.CategoryModelDao;
import com.app.fsm.local_network.repository.OaasDatabase;
import com.app.fsm.ui.dash.main.OnCategoryListner;

import java.util.List;

public class CategoryAsyncTask extends AsyncTask<Void,Void, List<CategoryModel>> {

    private OaasDatabase oaasDatabase;
    private OnCategoryListner onCategoryListner;

    public CategoryAsyncTask(@NonNull OnCategoryListner onCategoryListner, @NonNull OaasDatabase oaasDatabase) {
        this.oaasDatabase = oaasDatabase;
        this.onCategoryListner = onCategoryListner;
    }
    @Override
    protected List<CategoryModel> doInBackground(Void... voids) {
        CategoryModelDao categoryModelDao=oaasDatabase.provideCategoryDao();
        return categoryModelDao.getCategoryData();
    }

    @Override
    protected void onPostExecute(List<CategoryModel> categoryModels) {
        if (onCategoryListner != null)
            onCategoryListner.onCategoryFetchCompleted(categoryModels);
    }
}
