package com.app.fsm.ui.dash.main;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.app.fsm.BaseActivity;
import com.app.fsm.CustomInfoWindowGoogleMap;
import com.app.fsm.MarkerInfo;
import com.app.fsm.R;
import com.app.fsm.local_network.network.model.FeatureModel;
import com.app.fsm.local_network.network.model.SideBar;
import com.app.fsm.local_network.network.model.appconfig.AppConfigModel;
import com.app.fsm.local_network.network.model.category.CategoryModel;
import com.app.fsm.local_network.network.model.charts.ChartModel;
import com.app.fsm.local_network.network.model.customer.CustomerModel;
import com.app.fsm.local_network.network.model.member.MemberModel;
import com.app.fsm.local_network.network.model.subcategory.SubCategoryModel;
import com.app.fsm.local_network.network.model.todo.TodoLanguage;
import com.app.fsm.local_network.network.model.todo.TodoModel;
import com.app.fsm.local_network.network.model.todo.TodoStatusModel;
import com.app.fsm.local_network.network.model.todo.TodoType;
import com.app.fsm.local_network.network.model.usermodel.UserModel;
import com.app.fsm.ui.dash.main.adapter.RvAdSHGAdapter;
import com.app.fsm.ui.dash.main.adapter.RvSidebarAdapter;
import com.app.fsm.ui.dash.main.asynctask.CategoryAsyncTask;
import com.app.fsm.ui.dash.main.asynctask.ChartAsyncTask;
import com.app.fsm.ui.dash.main.asynctask.CustomersAsyncTask;
import com.app.fsm.ui.dash.main.asynctask.GetAllMemberAsynTask;
import com.app.fsm.ui.dash.main.asynctask.PullDataAsyncTask;
import com.app.fsm.ui.dash.main.asynctask.PushOutAsyncTask;
import com.app.fsm.ui.dash.main.asynctask.SubCategoryAsyncTask;
import com.app.fsm.ui.dash.one2one.One2OneFragment;
import com.app.fsm.ui.dash.taskList.TaskListFragment;
import com.app.fsm.ui.dash.ticketlist.TicketList;
import com.app.fsm.ui.dash.todo.OnTodoFetchListener;
import com.app.fsm.ui.dash.todo.asynctask.GetTodoStatusAsyncTask;
import com.app.fsm.ui.landingpage.login.LoginActivity;
import com.app.fsm.utils.searchdialog.SearchDialog;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class DashboardActivity extends BaseActivity implements OnMapReadyCallback, GoogleMap.OnInfoWindowClickListener
        , OnMemberCallback,
        FragmentManager.OnBackStackChangedListener
        , OnSideBarClickListener
        , OnMemberListener
        , OnCategoryListner
        , OnCustomerListener
        , OnSubCategoryListener
        , OnTodoFetchListener
        , OnChartDataListner
        , PullDataListener
        , OnPushSuccesslistener {

    @BindView(R.id.title)
    TextView txtTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.fl_main)
    FrameLayout flMain;
    @BindView(R.id.nav_view)
    NavigationView navView;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    @BindView(R.id.rv_sidebar)
    RecyclerView rvSidebar;
    @BindView(R.id.img_user)
    ImageView imgUser;
    @BindView(R.id.txt_user_name)
    TextView txtname;
    @BindView(R.id.txt_user_email)
    TextView txtUserEmail;
    @BindView(R.id.img_down_arrow)
    AppCompatImageView imgDownArrow;
    @BindView(R.id.txt_push_data_date)
    TextView txtPushDataDate;
    @BindView(R.id.txt_push_data_time)
    TextView txtPushDataTime;
    @BindView(R.id.ll_push_data_info)
    LinearLayout llPushDataInfo;
    @BindView(R.id.txt_get_data_date)
    TextView txtGetDataDate;
    @BindView(R.id.txt_get_data_time)
    TextView txtGetDataTime;
    @BindView(R.id.ll_get_data_info)
    LinearLayout llGetDataInfo;
    @BindView(R.id.ll_push_get_info)
    LinearLayout llPushGetInfo;
    @BindView(R.id.scrollView)
    ScrollView scrollView;
    @BindView(R.id.spinner_status)
    AppCompatSpinner spinner;
    private GoogleMap mMap;
    @BindView(R.id.maplayout)
    RelativeLayout rl_map;
    @BindView(R.id.chartlayout)
    RelativeLayout rl_charts;
    @BindView(R.id.piechart)
    PieChart pieChart;

    /*@BindView(R.id.rv_grid)
    RecyclerView rvGrid;
    @BindView(R.id.rv_top)
    RecyclerView rvTop;
    @BindView(R.id.im_grid)
    ImageView rvGrid;*/

    private AlertDialog alertDialog;
    Location mLastLocation;

    private FusedLocationProviderClient fusedLocationClient;


    private RvAdSHGAdapter rvTopAdapter;
    private RvSidebarAdapter rvSidebarAdapter;
    private ActionBarDrawerToggle toggle;
    private List<CustomerModel> mcustomerModels;
    private List<CategoryModel> mcategoryModels;
    private List<SubCategoryModel> msubcategoryModels;
    private List<TodoStatusModel> mtodoStatusModels;
    private List<MemberModel> memberModelList;
    private List<AppConfigModel> appConfigModelList;
    private List<ChartModel> chartModelList;
    boolean onclick = false;
    String[] spinnerdata = {"Map", "Dashboard"};
    Integer open = 0, assign = 0, pending = 0, completed = 0;
    String titleName;
    Date current;

    public static Intent newInstance(Context context) {
        return new Intent(context, DashboardActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportFragmentManager().addOnBackStackChangedListener(this);

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        fetchLastLocation();

        memberModelList = new ArrayList<MemberModel>();
        appConfigModelList=new ArrayList<>();
        titleName = "Hi  " + getUserSessionManager().getUser().getUsername();
        txtTitle.setText(titleName);

        txtname.setText(getUserSessionManager().getUser().getUsername());
        txtUserEmail.setText(getUserSessionManager().getUser().getEmail());

        toggle = new ActionBarDrawerToggle(
                this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    Window window = getWindow();
                    window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                    window.setStatusBarColor(ContextCompat.getColor(DashboardActivity.this, R.color.colorPrimaryDark));
                }
                sidebar();
            }


            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    Window window = getWindow();
                    window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                    window.setStatusBarColor(ContextCompat.getColor(DashboardActivity.this, R.color.colorPrimaryDark));
                }
            }

            @Override
            public void onDrawerStateChanged(int newState) {
                super.onDrawerStateChanged(newState);
            }
        };
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();


        //Creating the ArrayAdapter instance having the country list
        ArrayAdapter aa = new ArrayAdapter(this, android.R.layout.simple_spinner_item, spinnerdata);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //Setting the ArrayAdapter data on the Spinner
        spinner.setAdapter(aa);

        Calendar calendar = Calendar.getInstance();
        //calendar.add(Calendar.DATE, -1);
        current = calendar.getTime();
        Log.d("currentdate",""+current);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (spinner.getSelectedItem().toString().equalsIgnoreCase("Map")) {
                    rl_map.setVisibility(View.VISIBLE);
                    rl_charts.setVisibility(View.GONE);
                } else if (spinner.getSelectedItem().toString().equalsIgnoreCase("Dashboard")) {
                    rl_charts.setVisibility(View.VISIBLE);
                    rl_map.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);


        init();

    }

    @OnClick(R.id.img_down_arrow)
    public void onClick() {
        Boolean isVisible = (Boolean) imgDownArrow.getTag();
        if (isVisible) {
            llPushGetInfo.setVisibility(View.GONE);
            imgDownArrow.setRotation(180f);
        } else {
            llPushGetInfo.setVisibility(View.VISIBLE);
            scrollView.scrollTo(0, llPushGetInfo.getBottom());
            imgDownArrow.setRotation(0);
        }
        imgDownArrow.setTag(!isVisible);
    }

    @OnClick(R.id.cv_search)
    public void onSearchClicked() {
        onclick = true;
        new GetAllMemberAsynTask(this, provideOassDatabe(), 0).execute();
    }

    public void init() {
        open=0;
        assign=0;
        pending=0;
        completed=0;
        imgDownArrow.setTag(false);
        final List<FeatureModel> stringList = new ArrayList<>();
        stringList.add(new FeatureModel(getString(R.string.register_member), R.drawable.ic_add_member));
        stringList.add(new FeatureModel(getString(R.string.ticket), R.drawable.ic_one_to_one));
        stringList.add(new FeatureModel(getString(R.string.work_flow), R.drawable.ic_workflow));
        stringList.add(new FeatureModel(getString(R.string.workplan), R.drawable.ic_work_plan));
        /*this.rvTopAdapter = new RvAdSHGAdapter(new ArrayList<>(stringList), this);
        this.rvTop.setLayoutManager(new GridLayoutManager(this, 4));
        this.rvTop.setAdapter(this.rvTopAdapter);
        this.rvTop.addItemDecoration(new SpacesItemDecoration(4, 8
                , false));*/
        setGridView();
    }

    public void setGridView() {
        final List<FeatureModel> stringList = new ArrayList<>();
        stringList.add(new FeatureModel(getString(R.string.no_of_registered_members), 0));
        stringList.add(new FeatureModel(getString(R.string.members_not_met), 0));
        stringList.add(new FeatureModel(getString(R.string.no_of_open_workflows), 0));
        stringList.add(new FeatureModel(getString(R.string.no_of_due_todos), 0));
        stringList.add(new FeatureModel(getString(R.string.no_of_overdue_todos), 0));
        stringList.add(new FeatureModel(getString(R.string.no_of_members_for_whom), 0));
        /*RvMemberAdapter rvTopAdapter = new RvMemberAdapter(new ArrayList<>(stringList));
        this.rvGrid.setLayoutManager(new GridLayoutManager(this, 2));
        this.rvGrid.setAdapter(rvTopAdapter);
        this.rvGrid.addItemDecoration(new SpacesItemDecoration(2, 1, false));*/
        //rvGrid.setBackgroundResource(R.drawable.landing_waterdrop);
        new GetAllMemberAsynTask(this, provideOassDatabe(), 0).execute();
        new CategoryAsyncTask(this, provideOassDatabe()).execute();
        new SubCategoryAsyncTask(this, provideOassDatabe()).execute();
        new CustomersAsyncTask(this, provideOassDatabe()).execute();
        new GetTodoStatusAsyncTask(this, provideOassDatabe()).execute();
        new ChartAsyncTask(this, provideOassDatabe()).execute();
    }



    public void sidebar() {

        final List<SideBar> sideBarList = new ArrayList<>();

        if (getUserSessionManager().getIsDataFetched())
            sideBarList.add(new SideBar(getString(R.string.push_data), R.drawable.ic_push_data));
        else
            sideBarList.add(new SideBar(getString(R.string.get_data), R.drawable.ic_push_data));
        sideBarList.add(new SideBar(getString(R.string.ticket), R.drawable.ic_one_to_one));
        //stringList.add(new FeatureModel(getString(R.string.ticket), R.drawable.ic_one_to_one));
        //sideBarList.add(new SideBar(getString(R.string.org_hierarchy), R.drawable.ic_add_intervention));
        //sideBarList.add(new SideBar(getString(R.string.todo), R.drawable.ic_add_intervention));
       /* sideBarList.add(new SideBar(getString(R.string.patient_records), R.drawable.ic_patient_records));
        sideBarList.add(new SideBar(getString(R.string.analytics), R.drawable.ic_analytics));
        sideBarList.add(new SideBar(getString(R.string.wash_mapping), R.drawable.ic_wash_mapping));
        sideBarList.add(new SideBar(getString(R.string.add_school), R.drawable.ic_add_school));
        sideBarList.add(new SideBar(getString(R.string.add_school_checklist), R.drawable.ic_add_school_checklist));
        sideBarList.add(new SideBar(getString(R.string.apply_for_scheme), R.drawable.ic_apply_scheme));
        sideBarList.add(new SideBar(getString(R.string.apply_for_document), R.drawable.ic_apply_documnetation));
        sideBarList.add(new SideBar(getString(R.string.resources), R.drawable.ic_resources));*/
        //sideBarList.add(new SideBar(getString(R.string.edit_profile), R.drawable.ic_profile));
        sideBarList.add(new SideBar(getString(R.string.logout), R.drawable.ic_log_out));

        this.rvSidebarAdapter = new RvSidebarAdapter(sideBarList, this);
        this.rvSidebar.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        this.rvSidebar.setAdapter(rvSidebarAdapter);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_item, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                if (toggle.isDrawerIndicatorEnabled()) {
                    drawerLayout.openDrawer(GravityCompat.START);
                }
                break;
            case R.id.menu_ticket:
                if (!(getSupportFragmentManager().findFragmentByTag(TicketList.class.getSimpleName()) instanceof TicketList)) {
                    openFragment(TicketList.newInstance(null), TicketList.class.getSimpleName());//OpenTicket List
                    disableDrawer();
                }
                break;
            /*case R.id.menu_document:
                if (!(getSupportFragmentManager().findFragmentByTag(TaskListFragment.class.getSimpleName()) instanceof TaskListFragment)) {
                    openFragment(TaskListFragment.newInstance(null), TaskListFragment.class.getSimpleName());//OpenTask List
                    disableDrawer();
                }
                break;*/
        }
        if (isBackStackCountZero()) {
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (isBackStackCountZero()) {
            //menu.findItem(R.id.menu_document).setVisible(true);
            menu.findItem(R.id.menu_ticket).setVisible(true);
        } else {
            //menu.findItem(R.id.menu_document).setVisible(false);
            menu.findItem(R.id.menu_ticket).setVisible(false);
        }
        return super.onPrepareOptionsMenu(menu);
    }

    /*Top Grid below search click handled*/
    @Override
    public void onMemberCallback(int pos) {
        Log.d("memberUpdated","updated");
        //todo before ooass
        /*switch (pos) {
            case 0:
                Toast.makeText(this, "Coming Soon", Toast.LENGTH_SHORT).show();
                //disableDrawer();
                //openFragment(MemberRegistration.newInstance(false, null), MemberRegistration.class.getSimpleName());
                break;
            case 1:
                new GetAllMemberAsynTask(this, provideOassDatabe(), pos).execute();
                break;
            case 2:
                Toast.makeText(this, "Coming Soon", Toast.LENGTH_SHORT).show();
                //new GetAllMemberAsynTask(this, provideOassDatabe(), pos).execute();
                break;
            default:
                Toast.makeText(this, "Coming Soon", Toast.LENGTH_SHORT).show();
                break;
        }*/
        //todo after ooass
        /*switch (pos) {
            case 0:
                new GetAllMemberAsynTask(this, provideOassDatabe(), pos).execute();
                break;
            case 1:
                new GetAllMemberAsynTask(this, provideOassDatabe(), pos).execute();
                break;
            case 2:
                Toast.makeText(this, "Coming Soon", Toast.LENGTH_SHORT).show();
                //new GetAllMemberAsynTask(this, provideOassDatabe(), pos).execute();
                break;
            default:
                Toast.makeText(this, "Coming Soon", Toast.LENGTH_SHORT).show();
                break;
        }*/
    }

    private void disableDrawer() {
        toggle.setDrawerIndicatorEnabled(false);
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
    }

    @Override
    public void onBackStackChanged() {
        if (isBackStackCountZero()) {
            if (getSupportActionBar() != null) {
                txtTitle.setText(titleName);
                getSupportActionBar().setHomeButtonEnabled(true);
                // Remove back button
                getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                //You must regain the power of swipe for the drawer.
                drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
                // Show hamburger
                toggle.setDrawerIndicatorEnabled(true);
//                invalidateOptionsMenu();
                init();
            }
        }
    }


    @Override
    public void onSideBarItemCLicked(int pos) {
        drawerLayout.closeDrawers();
        switch (pos) {
            case 0:
                pushOutData();
                break;
            case 1:
                onclick = true;
                new GetAllMemberAsynTask(this, provideOassDatabe(), pos).execute();
                //disableDrawer();
                break;
            case 2:
                logout();
                break;
            case 3:
                break;
            default:
                break;
        }
    }

    public void pushOutData() {
        if (getUserSessionManager().getIsDataFetched()) {
            isFetching(true, "Pushing data to server");
            //TODO Flush data to server from Local DB
            new PushOutAsyncTask(this, provideOassDatabe(), getApiService(), getUserSessionManager()).execute();
            init();
        } else {
            isFetching(true, "Fetching data from server");
            new PullDataAsyncTask(provideOassDatabe()
                    , getApiService()
                    , getUserSessionManager()
                    , this).execute();
            init();
            /*new GetAllMemberAsynTask(this, provideOassDatabe(), 0).execute();
            new CustomersAsyncTask(this, provideOassDatabe()).execute();
            new CategoryAsyncTask(this, provideOassDatabe()).execute();
            new SubCategoryAsyncTask(this, provideOassDatabe()).execute();
            new GetTodoStatusAsyncTask(this, provideOassDatabe()).execute();
            new ChartAsyncTask(this, provideOassDatabe()).execute();*/

        }
    }

    @Override
    public void onMemberFetchCompleted(List<MemberModel> mmemberModelList, int pos) {
        memberModelList = mmemberModelList;
        if (onclick) {
            onclick = false;
            SearchDialog searchDialog = new SearchDialog(this, mmemberModelList, pos,
                    mcustomerModels, mcategoryModels, msubcategoryModels, mtodoStatusModels);
            searchDialog.show();
            //disableDrawer();
        }
    }

    @Override
    public void onCustomerFetchCompleted(List<CustomerModel> customerModels) {
        mcustomerModels = customerModels;
        for (MemberModel model : memberModelList) {
            for (CustomerModel customerModel : customerModels) {
                if (model.getCustomerId().equals(customerModel.getId())) {
                    model.setCustomerMyName(customerModel.getName());
                }
            }
        }
        Log.d("customer", "" + mcustomerModels.size());
    }

    @Override
    public void onCategoryFetchCompleted(List<CategoryModel> categoryModels) {
        mcategoryModels = categoryModels;
        for (MemberModel model : memberModelList) {
            for (CategoryModel categoryModel : categoryModels) {
                if (model.getCategoryId().equals(categoryModel.getId())) {
                    model.setCategoryName(categoryModel.getName());
                }
            }
        }
        Log.d("category", "" + mcategoryModels.size());
    }

    @Override
    public void onSubCategoryFetchCompleted(List<SubCategoryModel> subCategoryModels) {
        msubcategoryModels = subCategoryModels;
        Log.d("subcategories", "" + msubcategoryModels.size());
        for (MemberModel model : memberModelList) {
            for (SubCategoryModel subCategoryModel : subCategoryModels) {
                if (model.getSubcategoryId().equals(subCategoryModel.getId())) {
                    model.setSubcategoryName(subCategoryModel.getName());
                }
            }
        }
    }


    public void onSearchMemberSelected(MemberModel memberModel, int pos) {
        disableDrawer();
        /*switch (pos) {
            case 0:
                openFragment(MemberRegistration.newInstance(true, memberModel), MemberRegistration.class.getSimpleName());
                break;
            case 1:
                openFragment(One2OneFragment.newInstance(memberModel), One2OneFragment.class.getSimpleName());
                break;
            case 2:
                openFragment(WorkFlowFragment.newInstance(false, memberModel, null), WorkFlowFragment.class.getSimpleName());
                break;
        }*/

        switch (pos) {
            case 0:
                openFragment(One2OneFragment.newInstance(memberModel), One2OneFragment.class.getSimpleName());
                break;
        }
    }

    @Override
    public void onSuccess() {
        getUserSessionManager().setDataFetchingCompleted(true);
        showSuccessMsg("Get Data Successful");
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                hideLoading();
            }
        }, 2000);
    }

    @Override
    public void onError(String msg) {
        isFetching(false, msg);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                hideLoading();
            }
        }, 2000);
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
            showAlertDialogOnLogout();
        } else {
            super.onBackPressed();
        }
    }

    public void showAlertDialogOnLogout() {
        new AlertDialog.Builder(this)
                .setTitle(getResources().getString(R.string.logout_confirm))
                .setNegativeButton(R.string.cancel, null) // dismisses by default
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        logout();
                    }
                })
                .create()
                .show();
    }


    public void logout() {
        getUserSessionManager().createSession("", "", 0);
        startActivity(LoginActivity.newInstance(this));
        finish();
    }

    @Override
    public void onPushSuccess() {
        showSuccessMsg("Data pushed successful");
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                hideLoading();

            }
        }, 2000);
    }

    @Override
    public void onPushError(String msg) {
        isFetching(false, "Some error occurred");
    }

    public void openTaskList(MemberModel memberModel) {
        getSupportFragmentManager().popBackStack();
        openFragment(TaskListFragment.newInstance(memberModel), TaskListFragment.class.getSimpleName());
    }

    @Override
    public void assignToFetched(List<UserModel> userModelList) {

    }

    @Override
    public void todoStatusFetched(List<TodoStatusModel> todoStatusModelList) {
        mtodoStatusModels = todoStatusModelList;
        for (MemberModel model : memberModelList) {
            for (TodoStatusModel todoStatusModel : todoStatusModelList) {
                if (model.getStatusId().equals(todoStatusModel.getId())) {
                    model.setTodoStatusName(todoStatusModel.getName());
                }
            }
        }

        if (mMap != null) {
            mMap.setOnInfoWindowClickListener(this);
            if (memberModelList != null && memberModelList.size() > 0) {
                Log.d("memberdata", "" + memberModelList.size());
                for (MemberModel model : memberModelList) {
                    String presentday=getpresentdate(model.getStarttime());
                    MarkerInfo info=new MarkerInfo();
                    info.setTitle(model.getSerno());
                    info.setCustomer(model.getCustomerMyName());
                    info.setCategory(model.getCategoryName());
                    info.setSubcategory(model.getSubcategoryName());
                    info.setStartdate(model.getStarttime());
                    info.setEnddate(model.getEndtime());
                    info.setStatus(model.getTodoStatusName());
                    info.setPhoneno(model.getContactno());
                    info.setAddress(model.getCustomeraddress());
                    info.setMemberid(model.getServerId());
                    info.setEtatime(model.getEtatime());

                    CustomInfoWindowGoogleMap customInfoWindow = new CustomInfoWindowGoogleMap(this);
                    mMap.setInfoWindowAdapter(customInfoWindow);

                    if(presentday.equalsIgnoreCase(String.valueOf(current).substring(0,11))){
                        Log.d("entered","date");
                        if (model.getLatitude() != null && model.getLatitude() != null) {
                            LatLng latLng1 = new LatLng(Double.parseDouble(model.getLatitude()), Double.parseDouble(model.getLongitude()));
                            MarkerOptions markerOptions = new MarkerOptions();
                            if(model.getStatusId() == 5) {

                                markerOptions.position(latLng1).icon(BitmapDescriptorFactory.fromResource(R.drawable.marker_green_new));

                                Marker m = mMap.addMarker(markerOptions);
                                m.setTag(info);
                                //mMap.addMarker(new MarkerOptions().position(latLng1).icon(BitmapDescriptorFactory.fromResource(R.drawable.marker_green_new)));
                            }if (model.getStatusId() == 4) {

                                markerOptions.position(latLng1).icon(BitmapDescriptorFactory.fromResource(R.drawable.marker_orange_new));

                                Marker m = mMap.addMarker(markerOptions);
                                m.setTag(info);
                                //mMap.addMarker(new MarkerOptions().position(latLng1).icon(BitmapDescriptorFactory.fromResource(R.drawable.marker_green_new)));
                            } else if(model.getStatusId() == 3){

                                markerOptions.position(latLng1).icon(BitmapDescriptorFactory.fromResource(R.drawable.marker_red_new));

                                Marker m = mMap.addMarker(markerOptions);
                                m.setTag(info);
                                //mMap.addMarker(new MarkerOptions().position(latLng1).icon(BitmapDescriptorFactory.fromResource(R.drawable.marker_red_new)));
                            }
                        }
                    }
                    /*if (model.getLatitude() != null && model.getLatitude() != null) {
                        //String name = model.getFullname();
                        //String customername =model.getCustomerName()==null?"":"-"+model.getCustomerName();
                        LatLng latLng1 = new LatLng(Double.parseDouble(model.getLatitude()), Double.parseDouble(model.getLongitude()));
                        MarkerOptions markerOptions = new MarkerOptions();
                        if (model.getStatusId() == 10) {

                            markerOptions.position(latLng1).icon(BitmapDescriptorFactory.fromResource(R.drawable.marker_green_new));

                            Marker m = mMap.addMarker(markerOptions);
                            m.setTag(info);
                            //mMap.addMarker(new MarkerOptions().position(latLng1).icon(BitmapDescriptorFactory.fromResource(R.drawable.marker_green_new)));
                        } else {

                            markerOptions.position(latLng1).icon(BitmapDescriptorFactory.fromResource(R.drawable.marker_red_new));

                            Marker m = mMap.addMarker(markerOptions);
                            m.setTag(info);
                            //mMap.addMarker(new MarkerOptions().position(latLng1).icon(BitmapDescriptorFactory.fromResource(R.drawable.marker_red_new)));
                        }
                    }*/
                }
            }
        }
        Log.d("todostatus", "" + mtodoStatusModels.size());
    }

    @Override
    public void onTodoLanguageFetched(List<TodoLanguage> todoLanguageList) {

    }

    @Override
    public void onTodoFetched(TodoModel todoModel) {

    }

    @Override
    public void onTodoUpdated() {

    }

    @Override
    public void onTodoCreated() {

    }

    @Override
    public void onTodoTypeFetchComplete(List<TodoType> todoTypeList) {

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mMap.setMyLocationEnabled(true);
        mMap.setOnInfoWindowClickListener(this);
    }

    private void fetchLastLocation() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    Activity#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for Activity#requestPermissions for more details.
//                    Toast.makeText(MainActivity.this, "Permission not granted, Kindly allow permission", Toast.LENGTH_LONG).show();
                showPermissionAlert();
                return;
            }
        }
        fusedLocationClient.getLastLocation()
                .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        // Got last known location. In some rare situations this can be null.
                        if (location != null) {
                            // Logic to handle location object
                            mLastLocation = location;
                            Log.e("LAST LOCATION: ", location.toString());
                            LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
                            if (mMap != null) {
                                if (ActivityCompat.checkSelfPermission(DashboardActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(DashboardActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                                    // TODO: Consider calling
                                    //    ActivityCompat#requestPermissions
                                    // here to request the missing permissions, and then overriding
                                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                    //                                          int[] grantResults)
                                    // to handle the case where the user grants the permission. See the documentation
                                    // for ActivityCompat#requestPermissions for more details.
                                    return;
                                }
                                mMap.setMyLocationEnabled(true);
                                //mMap.addMarker(new MarkerOptions().position(latLng).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));
                                mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                                mMap.animateCamera(CameraUpdateFactory.zoomTo( 17.0f ) );
                            }
                        }
                    }
                });

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 123: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                    // permission was denied, show alert to explain permission
                    showPermissionAlert();
                }else{
                    //permission is granted now start a background service
                    if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                            && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                        fetchLastLocation();
                    }
                }
            }
        }
    }

    private void showPermissionAlert(){
        if (ActivityCompat.checkSelfPermission(DashboardActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(DashboardActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(DashboardActivity.this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, 123);
        }
    }

    @Override
    public void onChartFetchCompleted(List<ChartModel> chartModels) {
        chartModelList=chartModels;
        Log.d("chartdata",""+chartModels.size());
        for(ChartModel model:chartModels){
            String presentday=getpresentdate(model.getStarttime());
            if(presentday.equalsIgnoreCase(String.valueOf(current).substring(0,11))){
                if(model.getTodostatusId()==1){
                    open=open+model.getTotalOpenCount();
                }else if(model.getTodostatusId()==3){
                    assign=assign+model.getTotalAssignCount();
                }else if(model.getTodostatusId()==4){
                    pending=pending+model.getTotalInprogressCount();
                }else if(model.getTodostatusId()==5){
                    completed=completed+model.getTotalCompletedCount();
                }
            }
        }
        PieDataSet pieDataSet = new PieDataSet(getData(),"");
        pieDataSet.setColors(ColorTemplate.COLORFUL_COLORS);
        pieDataSet.setColors(new int[]{ContextCompat.getColor(this, R.color.red),
                ContextCompat.getColor(this, R.color.orange),
                ContextCompat.getColor(this, R.color.green)});
        PieData pieData = new PieData(pieDataSet);
        pieChart.setData(pieData);
        pieChart.setDrawEntryLabels(false);
        pieChart.animateXY(3000, 3000);
        pieChart.invalidate();
    }

    private ArrayList getData(){
        ArrayList<PieEntry> entries = new ArrayList<>();
        //entries.add(new PieEntry(open, "Open"));
        entries.add(new PieEntry(assign, "Assign"));
        entries.add(new PieEntry(pending, "InProgress"));
        entries.add(new PieEntry(completed, "Completed"));
        return entries;
    }

    private String getpresentdate(String starttime){
        Date value = null;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        if (starttime != null && !starttime.equalsIgnoreCase("")) {
            Date givenDate = null;
            try {
                givenDate = sdf.parse(starttime);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Long l = givenDate.getTime();
            //create date object
            Date next = new Date(l);
            value=next;
            String data=String.valueOf(value).substring(0,11);
            Log.d("present_date",""+data);
        }
        return String.valueOf(value).substring(0,11);
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        MarkerInfo infoWindowData = (MarkerInfo) marker.getTag();
        /*for(MemberModel model : memberModelList){
            if(model.getServerId().equals(infoWindowData.getMemberid())
                    && model.getStatusId()!=5 && model.getStatusId()==3 || model.getStatusId()==4){
                LatLng latLng1 = new LatLng(Double.parseDouble(model.getLatitude()), Double.parseDouble(model.getLongitude()));
                if(model.getStateId()==5){
                    mMap.addMarker(new MarkerOptions().position(latLng1).icon(BitmapDescriptorFactory.fromResource(R.drawable.marker_green_new)));
                } else if(model.getStatusId()==4){
                    mMap.addMarker(new MarkerOptions().position(latLng1).icon(BitmapDescriptorFactory.fromResource(R.drawable.marker_orange_new)));
                } else if(model.getStatusId()==3){
                    mMap.addMarker(new MarkerOptions().position(latLng1).icon(BitmapDescriptorFactory.fromResource(R.drawable.marker_red_new)));
                }

                openFragment(One2OneFragment.newInstance(model), One2OneFragment.class.getSimpleName());
                disableDrawer();
            }
        }*/
    }
}
