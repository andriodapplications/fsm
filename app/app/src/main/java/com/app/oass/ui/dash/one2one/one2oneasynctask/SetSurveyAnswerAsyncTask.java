package com.app.fsm.ui.dash.one2one.one2oneasynctask;

import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.app.fsm.local_network.network.model.survey.SurveyAnswerModel;
import com.app.fsm.local_network.repository.OaasDatabase;
import com.app.fsm.local_network.repository.dao.SurveyAnswerDao;

public final class SetSurveyAnswerAsyncTask extends AsyncTask<Void, Void, Long> {

    private OaasDatabase oaasDatabase;
    private SurveyAnswerModel surveyAnswerModel;

    public SetSurveyAnswerAsyncTask(@NonNull OaasDatabase oaasDatabase
            , SurveyAnswerModel surveyAnswerModel) {
        this.oaasDatabase = oaasDatabase;
        this.surveyAnswerModel = surveyAnswerModel;
    }

    @Override
    protected Long doInBackground(Void... params) {
        SurveyAnswerDao surveyAnswerDao = oaasDatabase.provideSurveyAnswerDao();
        return surveyAnswerDao.insertSurveyModel(surveyAnswerModel);
    }
}

