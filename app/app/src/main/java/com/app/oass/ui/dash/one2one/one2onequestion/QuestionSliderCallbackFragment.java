package com.app.fsm.ui.dash.one2one.one2onequestion;

import com.app.fsm.BaseFragment;
import com.app.fsm.local_network.network.model.survey.SurveyAnswerModel;
import com.app.fsm.local_network.network.model.survey.SurveyQuestionModel;
import com.app.fsm.local_network.network.model.todo.TodoModel;

public abstract class QuestionSliderCallbackFragment extends BaseFragment {

    public abstract SurveyQuestionModel getSurveyQuestionModel();

    public abstract boolean isSurveyQuestionAnswered();

    public abstract boolean isTodoDone();

    public abstract SurveyAnswerModel getSurveyAnswerModel();

    public abstract TodoModel getTodoModel();

    public abstract int getPreviousQuestionId();

    public abstract void setPreviousQuestionId(int previousQuestionId);
}
