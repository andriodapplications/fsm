package com.app.fsm.ui.dash.one2one;


import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.fsm.BaseFragment;
import com.app.fsm.R;
import com.app.fsm.local_network.network.model.appconfig.AppConfigModel;
import com.app.fsm.local_network.network.model.member.MemberModel;
import com.app.fsm.local_network.network.model.survey.SurveyAnswerModel;
import com.app.fsm.local_network.network.model.survey.SurveyQuestionModel;
import com.app.fsm.local_network.network.model.todo.TodoModel;
import com.app.fsm.ui.OnCompleteListener;
import com.app.fsm.ui.dash.main.DashboardActivity;
import com.app.fsm.ui.dash.main.OnAppConfigListner;
import com.app.fsm.ui.dash.main.asynctask.AppConfigAsyncTask;
import com.app.fsm.ui.dash.memberregistration.memberprofile.memberasynctask.UpdateMemberAsyncTask;
import com.app.fsm.ui.dash.one2one.one2oneasynctask.GetAllSurveyAnswersAsynTask;
import com.app.fsm.ui.dash.one2one.one2oneasynctask.GetAllSurveyQuestionAsynTask;
import com.app.fsm.ui.dash.one2one.one2oneasynctask.SetSurveyAnswerAsyncTask;
import com.app.fsm.ui.dash.one2one.one2oneasynctask.SetTodoAsyncTask;
import com.app.fsm.ui.dash.one2one.one2onequestion.One2OneQuestionFragment;
import com.app.fsm.ui.dash.one2one.one2onequestion.QuestionSliderCallbackFragment;
import com.app.fsm.utils.AppConstant;
import com.app.fsm.utils.CommonUtils;
import com.app.fsm.utils.UserSessionManager;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class One2OneFragment extends BaseFragment implements
        ViewPager.OnPageChangeListener, OnCompleteListener
        , OnFetchComplete,OnFetchAnswersComplete, OnAppConfigListner {

    @BindView(R.id.layout_user_details)
    ViewGroup llUserDetails;
    @BindView(R.id.search_member)
    ViewGroup viewGroupSearchMember;/*
    @BindView(R.id.iv_arrow_down)
    ImageView ivArrowDown;*/
    @BindView(R.id.ll_expand_collapse)
    LinearLayout llExpandCollapse;
    @BindView(R.id.view_pager)
    ViewPager vpQuestion;
    @BindView(R.id.question_Layout)
    LinearLayout llQuestion;
    @BindView(R.id.txt_title_child_fragment)
    TextView txtTitleChildFragment;
    @BindView(R.id.img_to_previous_fragment)
    ImageView imgPreviousFragment;
    @BindView(R.id.ll_indicator_container)
    LinearLayout llIndicatorContainer;
    @BindView(R.id.img_to_next_fragment)
    ImageView imgNextFragment;
    @BindView(R.id.txt_fragment_count)
    TextView txtFragmentCount;
    @BindView(R.id.et_search_task)
    TextView etSearchTask;
    @BindView(R.id.iv_search_task)
    ImageView ivSearchTask;
    @BindView(R.id.txt_session)
    AppCompatTextView txtSession;
    @BindView(R.id.ll_search_user)
    LinearLayout llSearchUser;
    @BindView(R.id.cv_member_search)
    CardView cvMemberSearch;
    @BindView(R.id.ll_arrow)
    LinearLayout llArrow;
    @BindView(R.id.bt_rollback)
    Button btrollback;

    @BindView(R.id.txt_header_full_name)
    TextView txtHeaderFullName;
    @BindView(R.id.txt_full_name)
    TextView txtFullName;
    @BindView(R.id.txt_header_typology)
    TextView txtHeaderTypology;
    @BindView(R.id.txt_typology)
    TextView txtTypology;
    @BindView(R.id.txt_header_dob)
    TextView txtHeaderDob;
    @BindView(R.id.txt_dob)
    TextView txtDob;
    @BindView(R.id.txt_header_nick)
    TextView txtHeaderNick;
    @BindView(R.id.txt_nick)
    TextView txtNick;
    @BindView(R.id.txt_header_tl_id)
    TextView txtHeaderTlId;
    @BindView(R.id.txt_tl_id)
    TextView txtTlId;
    @BindView(R.id.txt_header_age)
    TextView txtHeaderAge;
    @BindView(R.id.txt_age)
    TextView txtAge;
    @BindView(R.id.rl_session_date)
    RelativeLayout rlSessionDate;

    private Unbinder mUnbinder;
    private One2OneViewPagerAdapter one2OneViewPagerAdapter;
    private MemberModel memberModel;
    private SurveyAnswerModel surveyAnswerModel;
    private TodoModel todoModel;
    private int childFragmentPos;
    public List<SurveyQuestionModel> AllsurveyQuestionModel;
    public List<SurveyQuestionModel> surveyQuestionModelList;
    public List<SurveyQuestionModel> dummysurveyQuestionModelList;
    public List<SurveyAnswerModel> surveyAnswerModelList;
    public List<SurveyAnswerModel> dummysurveyAnswerModelList;
    private boolean Inprogress = false;
    private int totalquestionsSize,totalsurveyanswered;
    private boolean rollback =false,buttondisable=false;
    private int questioncount=0,nextquestionid=0,previousquestionid=0,
            rccode=0,curr_position=0,rollback_count=0,lastid;
    private String lastanswer,lastremark,lastupload;

    public SurveyQuestionModel surveyQuestionModelobj;

    public static One2OneFragment newInstance(MemberModel memberModel) {
        One2OneFragment one2OneFragment = new One2OneFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(AppConstant.MEMBER_MODEL, memberModel);
        one2OneFragment.setArguments(bundle);
        return one2OneFragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_one2_one, container, false);
        mUnbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (getArguments() != null)
            memberModel = (MemberModel) getArguments().getSerializable(AppConstant.MEMBER_MODEL);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //ivArrowDown.setTag(false);
        setTitle("Ticket");
        init();
    }

    public void init() {

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        AllsurveyQuestionModel = new ArrayList<>();
        surveyQuestionModelList = new ArrayList<>();
        surveyAnswerModelList = new ArrayList<>();
        dummysurveyQuestionModelList = new ArrayList<>();
        dummysurveyAnswerModelList = new ArrayList<>();
        UserSessionManager.getUserSessionManager().setQRCode(null);

        new AppConfigAsyncTask(this,provideOaasDatabase()).execute();

        new GetAllSurveyQuestionAsynTask(this, provideOaasDatabase(),
                memberModel.getCustomerId(),memberModel.getCategoryId()).execute();

        setSliderView(memberModel);

        new GetAllSurveyAnswersAsynTask(this, provideOaasDatabase(),
                memberModel.getServerId()).execute();

        btrollback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(buttondisable && !memberModel.getRollbackStatus().equalsIgnoreCase("R")){
                    updateMemberrollback(memberModel);
                }
            }
        });

        if(memberModel.getRollbackStatus().equalsIgnoreCase("A")){
            rollback=true;
            memberModel.setRollbackupdate("A");
            imgNextFragment.setEnabled(true);
            imgPreviousFragment.setEnabled(true);
        } else if(memberModel.getRollbackStatus().equalsIgnoreCase("R")){
            memberModel.setRollbackupdate("N");
            imgNextFragment.setEnabled(false);
            imgPreviousFragment.setEnabled(false);
            btrollback.setBackground(getResources().getDrawable(R.drawable.background_rollback_red));
        } else if(memberModel.getRollbackStatus().equalsIgnoreCase("N")){
            memberModel.setRollbackupdate("N");
        }
        if(memberModel.getRcCode()==93){
            imgNextFragment.setEnabled(false);
            imgPreviousFragment.setEnabled(false);
        }
    }

    private void setSliderView(MemberModel memberModel) {
        txtSession.setText(CommonUtils.updateDate(calendar.getTime()));
        etSearchTask.setText(memberModel.getSerno());
        /*txtFullName.setText(memberModel.getFullname());
        txtTypology.setText("--");
        txtDob.setText(memberModel.getDob()==null?"--":CommonUtils.getDateFromISO(memberModel.getDob()));
        txtNick.setText(memberModel.getAddress());
        txtTlId.setText("--");
        txtAge.setText(memberModel.getAge());*/
    }



    /*@OnClick(R.id.iv_arrow_down)
    void onExpandCollapseClicked() {
        Boolean isVisible = (Boolean) ivArrowDown.getTag();
        *//*if(isVisible){
            llUserDetails.setVisibility(View.GONE);
            ivArrowDown.setRotation(0f);
            //overlayView.setVisibility(View.GONE);
            handleExpandCollapseView(false);
        } else {
            llUserDetails.setVisibility(View.VISIBLE);
            ivArrowDown.setRotation(180f);
            //overlayView.setVisibility(View.VISIBLE);
            handleExpandCollapseView(true);
        }*//*
        //animateLayout(!isVisible);
        ivArrowDown.setTag(!isVisible);
    }*/

    private void handleExpandCollapseView(boolean expand) {
        if (expand) {
            llExpandCollapse.setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.white));
        } else {
            llExpandCollapse.setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.background_light_grey));
        }
    }

    /*public void animateLayout(Boolean toAnimate) {
        if (toAnimate) {
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            llArrow.setLayoutParams(layoutParams);
        } else {
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            llArrow.setLayoutParams(layoutParams);
        }

    }*/

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mUnbinder.unbind();
    }

    @Override
    public void onPageScrolled(int i, float v, int i1) {

    }

    @Override
    public void onPageSelected(int i) {
        this.childFragmentPos = i;
        if (i == 0) {
            imgPreviousFragment.setVisibility(View.INVISIBLE);
        } else {
            imgPreviousFragment.setVisibility(View.VISIBLE);
        }

        if(getUserSessionManager().getOnetoOneFlag().equalsIgnoreCase("t")){
            imgPreviousFragment.setVisibility(View.INVISIBLE);
        }

        if (i == vpQuestion.getAdapter().getCount() - 1) {
            imgNextFragment.setImageResource(R.drawable.ic_done);
            imgNextFragment.setTag(AppConstant.DONE);
        } else {
            imgNextFragment.setImageResource(R.drawable.ic_next);
            imgNextFragment.setTag(AppConstant.NEXT);
        }
    }

    @Override
    public void onPageScrollStateChanged(int i) {

    }

    private Boolean canGoBack = false;

    @OnClick(R.id.img_to_previous_fragment)
    public void imgLeftClicked() {
        if (imgPreviousFragment.getVisibility() == View.VISIBLE) {
            QuestionSliderCallbackFragment questionSliderCallbackFragment = (QuestionSliderCallbackFragment) one2OneViewPagerAdapter.getFragment(childFragmentPos);
            questionSliderCallbackFragment.getPreviousQuestionId();
            if (questionSliderCallbackFragment.getPreviousQuestionId() != -1) {
                int previousFragmentPos = skipQuestion(questionSliderCallbackFragment.getPreviousQuestionId(), surveyQuestionModelList);
                if (previousFragmentPos != -1 && canGoBack) {
                    vpQuestion.setCurrentItem(previousFragmentPos);
                    canGoBack = false;
                    return;
                }
            }
            //for go back to the previous screens
            if (canGoBack) {
                vpQuestion.setCurrentItem(--childFragmentPos);
                canGoBack = true;
                return;
            } else {
                Toast.makeText(requireActivity(), "You can just edit one question.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @OnClick(R.id.img_to_next_fragment)
    public void imgRightClicked() {

        QuestionSliderCallbackFragment questionSliderCallbackFragment = (QuestionSliderCallbackFragment) one2OneViewPagerAdapter.getFragment(childFragmentPos);
        SurveyQuestionModel surveyQuestionModel = questionSliderCallbackFragment.getSurveyQuestionModel();
        totalsurveyanswered++;
        buttondisable=true;
        try {
            if (((String) imgNextFragment.getTag()).equalsIgnoreCase(AppConstant.DONE)) {
                if (questionSliderCallbackFragment.isSurveyQuestionAnswered() && surveyQuestionModel.getRemarkvalue().length()>0) {//Is survey question answered

                    rccode=surveyQuestionModel.getNextrccode();
                    nextquestionid=surveyQuestionModel.getNextquestionId();
                    previousquestionid=surveyQuestionModel.getPreviousquestionId();

                    surveyAnswerModel=new SurveyAnswerModel();

                    surveyAnswerModel.setAnswer(surveyQuestionModel.getAnswer());
                    surveyAnswerModel.setQuestionid(surveyQuestionModel.getId());
                    surveyAnswerModel.setMemberid(memberModel.getServerId());
                    surveyAnswerModel.setDeleteflag(false);
                    surveyAnswerModel.setUploadedfiles(surveyQuestionModel.getUploadfile());
                    surveyAnswerModel.setRemark(surveyQuestionModel.getRemarkvalue());
                    if (!surveyQuestionModel.getSkipquestion()
                            && surveyQuestionModel.getApplytomessage().equalsIgnoreCase(AppConstant.NO)) {
                        new SetSurveyAnswerAsyncTask(provideOaasDatabase(), surveyAnswerModel).execute();//Setting survey answer
                        if(totalquestionsSize==(totalsurveyanswered+dummysurveyAnswerModelList.size())){
                            updateMemberComplete(memberModel);
                        }
                        openTaskListFragment(memberModel);
                    } else if (!surveyQuestionModel.getSkipquestion()
                            && surveyQuestionModel.getApplytomessage().equalsIgnoreCase(AppConstant.YES)) {

                        if (questionSliderCallbackFragment.isTodoDone()) {//this condition do need stodo to be done
                            this.todoModel = questionSliderCallbackFragment.getTodoModel();
                            this.todoModel.setMemberid(memberModel.getServerId());
                            new SetSurveyAnswerAsyncTask(provideOaasDatabase(), surveyAnswerModel).execute();//Setting survey answer
                            new SetTodoAsyncTask(provideOaasDatabase(), todoModel).execute();
                            if(totalquestionsSize==surveyAnswerModelList.size()+dummysurveyAnswerModelList.size()){
                                updateMemberComplete(memberModel);
                            }
                            openTaskListFragment(memberModel);
                        } else {
                            Toast.makeText(requireActivity(), "ToDo not done", Toast.LENGTH_SHORT).show();
                        }

                    }
                } else {
                    Toast.makeText(requireActivity(), "Please Answer.", Toast.LENGTH_SHORT).show();
                }

            } else {
               completeSurveyQuestion(questionSliderCallbackFragment,surveyQuestionModel);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void openTaskListFragment(MemberModel memberModel) {
        if (requireActivity() instanceof DashboardActivity) {
            DashboardActivity dashboardActivity = (DashboardActivity) requireActivity();
            dashboardActivity.openTaskList(memberModel);
        }
    }

    public void completeSurveyQuestion(QuestionSliderCallbackFragment questionSliderCallbackFragment,SurveyQuestionModel surveyQuestionModel){
        Log.d("check","todo");
        if (questionSliderCallbackFragment.isSurveyQuestionAnswered() && surveyQuestionModel.getRemarkvalue().length()>0) {
            checkTodoDone(questionSliderCallbackFragment, surveyQuestionModel);
        } else {
            Toast.makeText(requireActivity(), "Please Answer.", Toast.LENGTH_SHORT).show();
        }
    }

    public void checkTodoDone(QuestionSliderCallbackFragment questionSliderCallbackFragment, SurveyQuestionModel surveyQuestionModel) {
        surveyAnswerModel=new SurveyAnswerModel();
        surveyAnswerModel = questionSliderCallbackFragment.getSurveyAnswerModel();
        surveyAnswerModel.setMemberid(memberModel.getServerId());
        surveyAnswerModel.setRemark(surveyQuestionModel.getRemarkvalue());
        surveyAnswerModel.setDeleteflag(false);
        rccode=surveyQuestionModel.getNextrccode();
        nextquestionid=surveyQuestionModel.getNextquestionId();
        previousquestionid=surveyQuestionModel.getPreviousquestionId();
        curr_position=surveyQuestionModel.getDdQuestioncount();
        surveyAnswerModel.setUploadedfiles(surveyQuestionModel.getUploadfile());
        Log.d("check","todo2");
        //For testing purpose I have written this
        if (!surveyQuestionModel.getSkipquestion()&& surveyQuestionModel.getApplytomessage()!=null
                && surveyQuestionModel.getApplytomessage().equalsIgnoreCase(AppConstant.NO)) {
            Log.d("check","todo3");
            if(surveyQuestionModel.getSurveyanswer()==null
                    || memberModel.getRollbackupdate().equalsIgnoreCase("A")){
                new SetSurveyAnswerAsyncTask(provideOaasDatabase(), surveyAnswerModel).execute();//Setting survey answer
                if(!Inprogress){
                    updateMemberProgress(memberModel);
                }
            }

            if(surveyQuestionModel.getQuestiontype().equalsIgnoreCase("11")
                    || surveyQuestionModel.getQuestiontype().equalsIgnoreCase("12")){
                //imgPreviousFragment.setVisibility(View.INVISIBLE);
                childFragmentPos=curr_position-1;
            }
            //new SetSurveyAnswerAsyncTask(provideOaasDatabase(), surveyAnswerModel).execute();//Setting survey answer
            if(rccode!=93){
                if (childFragmentPos != vpQuestion.getAdapter().getCount() - 1) {
                    vpQuestion.setCurrentItem(++childFragmentPos);
                    canGoBack = true;
                }
            }else{
                imgNextFragment.setEnabled(false);
            }
            // questionSliderCallbackFragment.isTodoDone(false, false);//this condition do not need stodo to be done
        } else if (!surveyQuestionModel.getSkipquestion()&& surveyQuestionModel.getApplytomessage()!=null
                && surveyQuestionModel.getApplytomessage().equalsIgnoreCase(AppConstant.YES)) {
            Log.d("check","todo4");
            if (questionSliderCallbackFragment.isTodoDone()) {//this condition do need stodo to be done
                this.todoModel = questionSliderCallbackFragment.getTodoModel();
                this.todoModel.setMemberid(memberModel.getServerId());
                if(surveyQuestionModel.getSurveyanswer()==null){
                    new SetSurveyAnswerAsyncTask(provideOaasDatabase(), surveyAnswerModel).execute();//Setting survey answer
                }
                //new SetSurveyAnswerAsyncTask(provideOaasDatabase(), surveyAnswerModel).execute();//Setting survey answer
                new SetTodoAsyncTask(provideOaasDatabase(), todoModel).execute();
                if (childFragmentPos != vpQuestion.getAdapter().getCount() - 1) {
                    vpQuestion.setCurrentItem(++childFragmentPos);
                    canGoBack = true;
                }
            } else {
                Toast.makeText(requireActivity(), "ToDo not done", Toast.LENGTH_SHORT).show();
            }

        } else if (surveyQuestionModel.getSkipquestion()&& surveyQuestionModel.getApplytomessage()!=null
                && surveyQuestionModel.getApplytomessage().equalsIgnoreCase(AppConstant.YES)) {
            Log.d("check","todo5");
            if (questionSliderCallbackFragment.isTodoDone()) {//this condition do need stodo to be done
                this.todoModel = questionSliderCallbackFragment.getTodoModel();
                this.todoModel.setMemberid(memberModel.getServerId());
                if(surveyQuestionModel.getSurveyanswer()==null){
                    new SetSurveyAnswerAsyncTask(provideOaasDatabase(), surveyAnswerModel).execute();//Setting survey answer
                }
                //new SetSurveyAnswerAsyncTask(provideOaasDatabase(), surveyAnswerModel).execute();//Setting survey answer
                new SetTodoAsyncTask(provideOaasDatabase(), todoModel).execute();
                int skipIndex=performQuestionSkip(surveyQuestionModel,surveyAnswerModel);
                if(skipIndex!=-1){
                    if (childFragmentPos != vpQuestion.getAdapter().getCount() - 1) {
                        vpQuestion.setCurrentItem(skipIndex);
                        canGoBack = true;
                    }
                }else{
                    if (childFragmentPos != vpQuestion.getAdapter().getCount() - 1) {
                        vpQuestion.setCurrentItem(++childFragmentPos);
                        canGoBack = true;
                    }
                }
            } else {
                Toast.makeText(requireActivity(), "ToDo not done", Toast.LENGTH_SHORT).show();
            }

        } else if (!surveyQuestionModel.getSkipquestion()&& surveyQuestionModel.getApplytomessage()==null) {//Remove this condition after testing
            Log.d("check","todo6");
            if(surveyQuestionModel.getSurveyanswer()==null){
                new SetSurveyAnswerAsyncTask(provideOaasDatabase(), surveyAnswerModel).execute();//Setting survey answer
            }
            if (childFragmentPos != vpQuestion.getAdapter().getCount() - 1) {
                vpQuestion.setCurrentItem(++childFragmentPos);
                canGoBack = true;
            }
            // questionSliderCallbackFragment.isTodoDone(false, false);//this condition do not need stodo to be done
        } else if (surveyQuestionModel.getSkipquestion()) {//Here there is no need to check sTodo
            Log.d("check","todo7");
            if(surveyQuestionModel.getSurveyanswer()==null){
                new SetSurveyAnswerAsyncTask(provideOaasDatabase(), surveyAnswerModel).execute();//Setting survey answer
            }
            //new SetSurveyAnswerAsyncTask(provideOaasDatabase(), surveyAnswerModel).execute();//Setting survey answer
            int skipIndex=performQuestionSkip(surveyQuestionModel,surveyAnswerModel);
            if(skipIndex!=-1){
                if (childFragmentPos != vpQuestion.getAdapter().getCount() - 1) {
                    vpQuestion.setCurrentItem(skipIndex);
                    canGoBack = true;
                }
            }else{
                if (childFragmentPos != vpQuestion.getAdapter().getCount() - 1) {
                    vpQuestion.setCurrentItem(++childFragmentPos);
                    canGoBack = true;
                }
            }
        }
    }

    public int performQuestionSkip(SurveyQuestionModel surveyQuestionModel, SurveyAnswerModel surveyAnswerModel) {

        if (surveyQuestionModel.getSkipquestionvalue().equalsIgnoreCase(surveyAnswerModel.getAnswer())) {
            int index=-1;
            for (SurveyQuestionModel surveyQuestionModel1 : surveyQuestionModelList) {
                if (surveyQuestionModel.getSkipquestionid().equalsIgnoreCase(String.valueOf(surveyQuestionModel1.getId()))) {
                    index= surveyQuestionModelList.indexOf(surveyQuestionModel1);
                    break;
                }
            }
            return index;
        }else{
            return -1;
        }
    }

    public int skipQuestion(int previousQuestionId, List<SurveyQuestionModel> surveyQuestionModelList) {
        for (SurveyQuestionModel surveyQuestionModel1 : surveyQuestionModelList) {
            if (previousQuestionId == surveyQuestionModel1.getId()) {
                return surveyQuestionModelList.indexOf(surveyQuestionModel1);
            }
        }
        return -1;
    }

    @Override
    public void onComplete(List<SurveyQuestionModel> surveyQuestionModel) {
        totalquestionsSize = surveyQuestionModel.size();
        AllsurveyQuestionModel = surveyQuestionModel;
        Log.d("surveyquestionstotal",""+totalquestionsSize);
        for(SurveyQuestionModel model : surveyQuestionModel){
            if(getUserSessionManager().getOnetoOneFlag().equalsIgnoreCase("t")){
                model.setOnetooneFlag(true);
            } else {
                model.setOnetooneFlag(false);
            }

            if(model.getPlatform().equalsIgnoreCase("mobile") ||
                    model.getPlatform().equalsIgnoreCase("web and mobile")){
                model.setQuestioncount(questioncount);
                model.setMemberid(memberModel.getServerId());
                ++questioncount;
                surveyQuestionModelList.add(model);
            } else {
                dummysurveyQuestionModelList.add(model);
            }
            //for rollbackprevious question
            /*if(memberModel.getPrevquestionId().equals(model.getSerialno())){
                Log.d("model",""+model.getQuestioncount());
                rollback=true;
            }*/

        }
        Log.d("rollbackflag",""+rollback);
        Log.d("surveyquestions",""+surveyQuestionModelList.size());
    }

    Calendar calendar = Calendar.getInstance();

    DatePickerDialog.OnDateSetListener onDateSetListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            calendar.set(Calendar.YEAR, year);//Here calender object is updated
            calendar.set(Calendar.MONTH, month);
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            txtSession.setText(CommonUtils.updateDate(calendar.getTime()));
        }
    };

    @OnClick(R.id.rl_session_date)
    public void onSessionDateClicked() {
        DatePickerDialog datePickerDialog = new DatePickerDialog(requireContext(), onDateSetListener
                , calendar.get(Calendar.YEAR)
                , calendar.get(Calendar.MONTH)
                , calendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        datePickerDialog.show();
    }

    public void updateMemberrollback(MemberModel memberModel) {
        Log.d("updaterollback","done");
        memberModel.setPushed(false);
        memberModel.setPrevquestionId(previousquestionid);
        memberModel.setRollbackFlag(true);
        memberModel.setRollbackStatus("R");
        memberModel.setUpdateflag(true);
        memberModel.setLastmodifiedtime(getDateTime());
        new UpdateMemberAsyncTask(this,provideOaasDatabase(),memberModel).execute();
        Toast.makeText(getActivity(), "RollBack Updated", Toast.LENGTH_SHORT).show();
        imgNextFragment.setEnabled(false);
        imgPreviousFragment.setEnabled(false);
        btrollback.setBackground(getResources().getDrawable(R.drawable.background_rollback_red));
    }

    public void updateMemberComplete(MemberModel memberModel) {
        Log.d("updatecomplete","done");
        memberModel.setPushed(false);
        memberModel.setUpdateflag(true);
        memberModel.setRcCode(rccode);
        memberModel.setNextquestionId(nextquestionid);
        memberModel.setStatusId(5);
        memberModel.setLastmodifiedtime(getDateTime());
        memberModel.setCompletedFlag(true);
        new UpdateMemberAsyncTask(this,provideOaasDatabase(),memberModel).execute();
    }

    public void updateMemberProgress(MemberModel memberModel) {
        Log.d("updateprogress","done"+memberModel);
        //to update once unmask Inprogress
        //Inprogress=true;
        if(memberModel.getRollbackStatus().equalsIgnoreCase("A")){
            memberModel.setRollbackupdate("N");
        }
        memberModel.setPushed(false);
        memberModel.setRcCode(rccode);
        memberModel.setLastmodifiedtime(getDateTime());
        memberModel.setNextquestionId(nextquestionid);
        memberModel.setUpdateflag(true);
        memberModel.setStatusId(4);
        new UpdateMemberAsyncTask(this,provideOaasDatabase(),memberModel).execute();
    }


    @Override
    public void onComplete() {

    }

    @Override
    public void onUpdate() {

    }

    @Override
    public void onCompleteSurvey(List<SurveyAnswerModel> surveyAnswerModels) {
        Log.d("surveyanswers",""+surveyAnswerModels.size());

        for(SurveyQuestionModel questionModel : surveyQuestionModelList){
            for(SurveyAnswerModel answerModel :surveyAnswerModels){
                if(questionModel.getId().equals(answerModel.getQuestionid()) && memberModel.getServerId().equals(answerModel.getMemberid())){
                    questionModel.setSurveyanswer(answerModel.getAnswer());
                    questionModel.setRemarkvalue(answerModel.getRemark());
                    questionModel.setUploadfile(answerModel.getUploadedfiles());
                    surveyAnswerModelList.add(answerModel);
                }
            }
        }

        totalsurveyanswered=surveyAnswerModelList.size();

        for(SurveyQuestionModel questionModel : dummysurveyQuestionModelList){
            for(SurveyAnswerModel answerModel :surveyAnswerModels){
                if(questionModel.getId().equals(answerModel.getQuestionid())){
                    dummysurveyAnswerModelList.add(answerModel);
                }
            }
        }
        Log.d("surveyanswersmobile",""+surveyAnswerModelList.size());
        Log.d("surveyanswersweb",""+dummysurveyAnswerModelList.size());

        if (!surveyQuestionModelList.isEmpty()) {
            List<Fragment> fragmentList = new ArrayList<>();
            for (SurveyQuestionModel surveyQuestionModel : surveyQuestionModelList) {
                if(surveyQuestionModel.getQuestiontype() != null) {
                    fragmentList.add(One2OneQuestionFragment.newInstance(surveyQuestionModel,
                            surveyQuestionModelList,AllsurveyQuestionModel));
                }
                surveyQuestionModelobj=surveyQuestionModel;
            }

            one2OneViewPagerAdapter = new One2OneViewPagerAdapter(requireActivity().getSupportFragmentManager(), fragmentList);
            vpQuestion.setClipChildren(false);
            vpQuestion.setClipToPadding(false);
            int margin = (int) getResources().getDimension(R.dimen.d_margin_large);//16dp
            final int padding = (int) getResources().getDimension(R.dimen.d_margin_ex_large);//18dp
            vpQuestion.setPageMargin(margin);
            vpQuestion.setPadding(padding * 2, 0, padding * 2, 0);
            vpQuestion.setPageTransformer(false, new ViewPager.PageTransformer() {
                @Override public void transformPage(View page, float position) {
                    if (mViewPager.getCurrentItem() == 0) {
                        page.setTranslationX(-(padding));
                    } else if (mViewPager.getCurrentItem() == one2OneViewPagerAdapter.getCount() - 1) {
                        page.setTranslationX(padding);
                    } else {
                        page.setTranslationX(0);
                    }
                }
            });
            vpQuestion.setAdapter(one2OneViewPagerAdapter);
            vpQuestion.addOnPageChangeListener(this);
            imgNextFragment.setTag(AppConstant.NEXT);

            if(surveyAnswerModelList.size()>0){
                int count=0;
                if(surveyAnswerModelList.size()>1){
                    buttondisable=true;
                }
                for(SurveyQuestionModel Model : surveyQuestionModelList){
                    if(Model.getSerialno().equals(memberModel.getPrevquestionId())){
                        count=Model.getQuestioncount();
                    }
                }
                Log.d("count",""+count);
                if (childFragmentPos != vpQuestion.getAdapter().getCount() - 1) {
                    if(rollback && count>=0){
                        vpQuestion.setCurrentItem(count);
                    } else {
                        vpQuestion.setCurrentItem(surveyAnswerModelList.size()-1);
                    }
                    canGoBack = true;
                }
            }

            /*if(UserSessionManager.getUserSessionManager().getOnetoOneFlag().equalsIgnoreCase("f")){
                final IndicatorBinder sample = new IndicatorBinder().bind(requireActivity(),
                        vpQuestion, llIndicatorContainer,
                        R.drawable.indicator_selected,
                        R.drawable.indicator_unselected);
                // Set whether you want a progress style
                sample.setProgressStyle(true);
            }*/

        }
    }

    @Override
    public void FetchAppCongigData(List<AppConfigModel> appConfigModels) {
        getUserSessionManager().setOnetoOneFlag(appConfigModels.get(0).getValue());
    }

    private String getDateTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.sss'Z'", Locale.US);
        dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        Date date = new Date();
        return dateFormat.format(date);
    }
}
