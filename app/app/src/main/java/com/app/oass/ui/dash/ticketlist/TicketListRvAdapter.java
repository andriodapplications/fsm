package com.app.fsm.ui.dash.ticketlist;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.fsm.R;
import com.app.fsm.local_network.network.model.member.MemberModel;
import com.app.fsm.utils.CommonUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TicketListRvAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int EMPTY_VIEW = 0;
    private final int MEMBER_VIEW = 1;

    private List<MemberModel> memberModelList;
    private Activity mactivity;

    public TicketListRvAdapter(Activity activity) {
        this.memberModelList = new ArrayList<>();
        mactivity=activity;

    }

    public void update(List<MemberModel> memberModelList){
        this.memberModelList.clear();
        this.memberModelList.addAll(memberModelList);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case MEMBER_VIEW:
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_ticket, parent, false);
                return new RvViewHolder(view);
            case EMPTY_VIEW:
            default:
                View view1 = LayoutInflater.from(parent.getContext()).inflate(R.layout.empty_layout, parent, false);
                return new EmptyViewHolder(view1);
        }

    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, final int position) {

        if (holder instanceof RvViewHolder) {
            ((RvViewHolder) holder).bind(memberModelList.get(position));
        }else if(holder instanceof EmptyViewHolder){
            ((EmptyViewHolder) holder).bind();
        }
    }

    @Override
    public int getItemCount() {
        return memberModelList.size();
    }

    @Override
    public int getItemViewType(int position) {
        if(memberModelList.isEmpty()){
            return EMPTY_VIEW;
        }else{
            return MEMBER_VIEW;
        }
    }

    class RvViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.txt_member_detail)
        TextView txtTicket;
        @BindView(R.id.txt_status)
        TextView txtStatus;
        @BindView(R.id.txt_customer)
        TextView txtCustomer;
        @BindView(R.id.txt_category)
        TextView txtCategory;
        @BindView(R.id.txt_subcategory)
        TextView txtSubCategory;
        @BindView(R.id.edt_customer_number)
        TextView txtStartDate;
        @BindView(R.id.edt_customer_name)
        TextView txtEndDate;
        @BindView(R.id.status_color)
        LinearLayout ll_color;

        public RvViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(final MemberModel memberModel) {

            String startdate_time;
            String enddate_time;
            if(memberModel.getStarttime()!=null){
                String starttime=memberModel.getStarttime();
                String st_time=starttime.substring(starttime.length()-5);
                int firsttwo= Integer.parseInt(st_time.substring(0,2));
                if(firsttwo>= 13 && firsttwo < 24){
                    st_time=st_time+" PM";
                }else{
                    st_time=st_time+" AM";
                }
                //startdate_time=CommonUtils.getDateEta(memberModel.getStarttime())+" , "+CommonUtils.getTimeEta(memberModel.getStarttime());
                startdate_time=CommonUtils.getDateEta(memberModel.getStarttime())+" , "+st_time;
            }else {
                startdate_time="";
            }

            if(memberModel.getEndtime()!=null){
                String endtime=memberModel.getEndtime();
                String ed_time=endtime.substring(endtime.length()-5);
                int firsttwo= Integer.parseInt(ed_time.substring(0,2));
                if(firsttwo>= 13 && firsttwo < 24){
                    ed_time=ed_time+" PM";
                }else{
                    ed_time=ed_time+" AM";
                }

                enddate_time=CommonUtils.getDateEta(memberModel.getEndtime())+" , "+ed_time;
            }else {
                enddate_time="";
            }

            txtTicket.setText(memberModel.getSerno()==null?"":memberModel.getSerno());
            txtStatus.setText(memberModel.getTodoStatusName()==null?"Status :":"Status : "+memberModel.getTodoStatusName());
            txtCustomer.setText(memberModel.getCustomerMyName()==null?"Customer : ":"Customer : "+memberModel.getCustomerMyName());
            txtCategory.setText(memberModel.getCategoryName()==null?"Category : ":"Category : "+memberModel.getCategoryName());
            txtSubCategory.setText(memberModel.getSubcategoryName()==null?"SubCategory : ":"SubCategory : "+memberModel.getSubcategoryName());
            txtStartDate.setText("Start Date :"+ startdate_time);
            txtEndDate.setText("End Date :"+enddate_time);

            if(memberModel.getStatusId()==5){
                ll_color.setBackgroundColor(mactivity.getResources().getColor(R.color.green));
            } else if(memberModel.getStatusId()==3){
                ll_color.setBackgroundColor(mactivity.getResources().getColor(R.color.red));
            } else if(memberModel.getStatusId()==4){
                ll_color.setBackgroundColor(mactivity.getResources().getColor(R.color.orange));
            }
        }
    }

    class EmptyViewHolder extends RecyclerView.ViewHolder {

        public EmptyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind() {

        }
    }
}
