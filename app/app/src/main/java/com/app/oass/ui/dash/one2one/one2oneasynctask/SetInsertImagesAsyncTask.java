package com.app.fsm.ui.dash.one2one.one2oneasynctask;

import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.app.fsm.local_network.network.model.Images.ImagesModelDao;
import com.app.fsm.local_network.network.model.Images.InsertImages;
import com.app.fsm.local_network.network.model.member.MemberModel;
import com.app.fsm.local_network.repository.OaasDatabase;
import com.app.fsm.local_network.repository.dao.MemberDao;
import com.app.fsm.ui.OnCompleteListener;

public final class SetInsertImagesAsyncTask extends AsyncTask<Void, Void, Long> {

    private OaasDatabase oaasDatabase;
    private InsertImages insertImages;
    private OnCompleteListener onCompleteListener;

    public SetInsertImagesAsyncTask(@NonNull OnCompleteListener onCompleteListener,
                                    @NonNull OaasDatabase oaasDatabase
            , InsertImages insertImages) {
        this.onCompleteListener=onCompleteListener;
        this.oaasDatabase = oaasDatabase;
        this.insertImages = insertImages;
    }

    @Override
    protected Long doInBackground(Void... params) {

        ImagesModelDao imagesDao = oaasDatabase.provideInsertImagesDao();

        if (imagesDao.getImageCount() == 0) {
            insertImages.setImagecount(1);
            insertImages.setId(1);
        } else {
            InsertImages lastImageModel = imagesDao.getInsertImages();
            insertImages.setImagecount(lastImageModel.getImagecount()+1);
            insertImages.setId(lastImageModel.getId() + 1);
        }
        return imagesDao.insertImage(insertImages);
    }

    @Override
    protected void onPostExecute(Long longValue) {
        if(onCompleteListener!=null)
            onCompleteListener.onComplete();
    }
}

