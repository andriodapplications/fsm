
/*
 * Developed by Avinash Kumar singh on 24/1/19 3:49 PM
 * Last Modified 16/1/19 5:57 PM.
 *
 * Copyright (c) 2019.  All rights reserved.
 */

package com.app.fsm.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.res.AssetManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.RelativeSizeSpan;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.app.fsm.R;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public final class CommonUtils {

    private static final String TAG = "CommonUtils";
    public static final String SERVER_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";

    private CommonUtils() {
        // This utility class is not publicly instantiable
    }

    public static ProgressDialog showLoadingDialog(@NonNull Context context) {
        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.show();
        if (progressDialog.getWindow() != null) {
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        progressDialog.setContentView(R.layout.progress_dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Fetching data");
        progressDialog.setCanceledOnTouchOutside(false);
        return progressDialog;
    }

    public static String errorMessage(int code) {

        if (code == 400) {
            return "Token not provided";
        } else if (code == 401) {
            return "Token Expired";
        } else if (code == 402) {
            return "402 error";
        } else if (code == 404) {
            return "Page not found";
        } else if (code == 408) {
            return "Request Timeout";
        } else if (code == 500) {
            return "Internal server error";
        } else if (code == 502) {
            return "Bad Gateway";
        } else if (code == 504) {
            return "Server timeout";
        } else if (code == 422) {
            return "Unprocessable Entity";
        } else {
            return "Error";
        }
    }

    public static void hideKeyboardFrom(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    @SuppressLint("all")
    public static String getDeviceId(Context context) {
        return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    public static boolean isEmailValid(String email) {
        Pattern pattern;
        Matcher matcher;
        final String EMAIL_PATTERN =
                "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                        + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public static String loadJSONFromAsset(Context context, String jsonFileName)
            throws IOException {

        AssetManager manager = context.getAssets();
        InputStream is = manager.open(jsonFileName);

        int size = is.available();
        byte[] buffer = new byte[size];
        is.read(buffer);
        is.close();

        return new String(buffer, "UTF-8");
    }

    public static int dpToPx(Context context, int dp) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }

    public static int pxToDp(Context context, int px) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return Math.round(px / (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }

    public static ContextWrapper changeLang(Context context, String lang_code){
        Locale sysLocale;

        Resources rs = context.getResources();
        Configuration config = rs.getConfiguration();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            sysLocale = config.getLocales().get(0);
        } else {
            sysLocale = config.locale;
        }
        if (!lang_code.equals("") && !sysLocale.getLanguage().equals(lang_code)) {
            Locale locale = new Locale(lang_code);
            Locale.setDefault(locale);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                config.setLocale(locale);
            } else {
                config.locale = locale;
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                context = context.createConfigurationContext(config);
            } else {
                context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());
            }
        }

        return new ContextWrapper(context);
    }

    public static String getDateandTimeString(){
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddhhmmss", Locale.US);
            dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
            Calendar calendar = Calendar.getInstance();
            dateFormat = new SimpleDateFormat("ddMMyyyyhhmmssSSS", Locale.US);
            return dateFormat.format(calendar.getTime());
        } catch (Exception e) {
            return "";
        }
    }

    public static String getDate(String strDate) {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");//2018-03-26
        Date date = null;
        try {
            date = sdf.parse(strDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return new SimpleDateFormat("MMM dd").format(date);
    }

    public static  String updateDate(Date date) {
        String myFormat = "dd-MM-yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        return sdf.format(date);
    }

    public static  String localDbDateFormat(Date date) {
        String myFormat = "yyyy-MM-dd'T'HH:mm:ss"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        return sdf.format(date);
    }

    public static String getDate(String strDate, String strFormat) {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");//2018-03-26
        Date date = null;
        try {
            date = sdf.parse(strDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return new SimpleDateFormat(strFormat).format(date);

    }

    public static long getTimestampFromDate(String date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(SERVER_DATE_FORMAT, Locale.ENGLISH);
        dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        try {
            Date date1 = dateFormat.parse(date);
            return date1.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return System.currentTimeMillis();
    }

    public static String getOnlyDateString(String date){
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat(SERVER_DATE_FORMAT, Locale.US);
            dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
            Date parsedDate = dateFormat.parse(date);
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(parsedDate.getTime());
            dateFormat = new SimpleDateFormat("dd-MMM-yyyy", Locale.US);
            return dateFormat.format(calendar.getTime());
        } catch (Exception e) {
            return "";
        }
    }

    public static String getDateEta(String date){
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm",Locale.US);
            //formatter.setTimeZone(TimeZone.getTimeZone("GMT"));
            Date parsedDate = (Date)formatter.parse(date);
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(parsedDate.getTime());
            formatter = new SimpleDateFormat("dd-MMM-yyyy", Locale.US);
            return formatter.format(calendar.getTime());
        } catch (Exception e) {
            return "";
        }
    }

    public static String getTimeEta(String date){
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm",Locale.US);
            //formatter.setTimeZone(TimeZone.getTimeZone("GMT"));
            Date parsedDate = (Date)formatter.parse(date);
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(parsedDate.getTime());
            formatter = new SimpleDateFormat("hhgi:mm", Locale.US);
            return formatter.format(calendar.getTime());
        } catch (Exception e) {
            return "";
        }
    }

    public static String getOnlyTimeString(String date){
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat(SERVER_DATE_FORMAT, Locale.US);
            dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
            Date parsedDate = dateFormat.parse(date);
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(parsedDate.getTime());
            dateFormat = new SimpleDateFormat("hh:mm a", Locale.US);
            return dateFormat.format(calendar.getTime());
        } catch (Exception e) {
            return "";
        }
    }

    public static Calendar getCalender(String strDate){
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        try {
            cal.setTime(df.parse(strDate));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return cal;
    }

    public static String getDateWithBackslash(String strDate) {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");//2018-03-26
        Date date = null;
        try {
            date = sdf.parse(strDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return new SimpleDateFormat("dd/MM/yyyy").format(date);
    }

    public static String getDateFromISO(String strDate) {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss");//2018-03-26T21:00Z
        Date date = null;
        try {
            date = sdf.parse(strDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return new SimpleDateFormat("dd-MM-yyyy").format(date);
    }


    public static SpannableString getSpannableStringOfRelativeSize(String str, int start, int end) {



        SpannableString spannableString = new SpannableString(str);
        spannableString.setSpan(new RelativeSizeSpan(0.6f), start, end, 0);
        return spannableString;
    }

    public static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }


    public static SpannableString getSpanAccordingToLocale(int languageId, String str, String strType) {

        if (languageId == 1 && strType.equalsIgnoreCase("ltr")) { //hindi
            return getSpannableStringOfRelativeSize(str,str.length()-2,str.length());
        } else if (languageId == 1 && strType.equalsIgnoreCase("rs")) {
            return getSpannableStringOfRelativeSize(str,str.length()-2,str.length());
        } else if (languageId == 1 && strType.equalsIgnoreCase("kg")) {
            return getSpannableStringOfRelativeSize(str,str.length()-4,str.length());
        }else if(languageId == 2 && strType.equalsIgnoreCase("ltr")){ //English
            return getSpannableStringOfRelativeSize(str,str.length()-3,str.length());
        }else if(languageId == 2 && strType.equalsIgnoreCase("rs")){
            return getSpannableStringOfRelativeSize(str,str.length()-2,str.length());
        }else if(languageId == 2 && strType.equalsIgnoreCase("kg")){
            return getSpannableStringOfRelativeSize(str,str.length()-2,str.length());
        }else if(languageId == 19 && strType.equalsIgnoreCase("ltr")){ //Marathi
            return getSpannableStringOfRelativeSize(str,str.length()-3,str.length());
        }else if(languageId == 19 && strType.equalsIgnoreCase("rs")){
            return getSpannableStringOfRelativeSize(str,str.length()-2,str.length());
        }else if(languageId == 19 && strType.equalsIgnoreCase("kg")){
            return getSpannableStringOfRelativeSize(str,str.length()-4,str.length());
        }else if(languageId == 21 && strType.equalsIgnoreCase("ltr")){ //Telugu
            return getSpannableStringOfRelativeSize(str,str.length()-3,str.length());
        }else if(languageId == 21 && strType.equalsIgnoreCase("rs")){
            return getSpannableStringOfRelativeSize(str,str.length()-3,str.length());
        }else if(languageId == 21 && strType.equalsIgnoreCase("kg")){
            return getSpannableStringOfRelativeSize(str,str.length()-3,str.length());
        }else{
           // return getSpannableStringOfRelativeSize(str,str.length(),str.length());
        }

        return null;
    }

}
