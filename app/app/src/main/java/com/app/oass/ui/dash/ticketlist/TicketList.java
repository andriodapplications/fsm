package com.app.fsm.ui.dash.ticketlist;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.app.fsm.BaseFragment;
import com.app.fsm.R;
import com.app.fsm.local_network.network.model.category.CategoryModel;
import com.app.fsm.local_network.network.model.customer.CustomerModel;
import com.app.fsm.local_network.network.model.member.MemberModel;
import com.app.fsm.local_network.network.model.subcategory.SubCategoryModel;
import com.app.fsm.local_network.network.model.todo.TodoLanguage;
import com.app.fsm.local_network.network.model.todo.TodoModel;
import com.app.fsm.local_network.network.model.todo.TodoStatusModel;
import com.app.fsm.local_network.network.model.todo.TodoType;
import com.app.fsm.local_network.network.model.usermodel.UserModel;
import com.app.fsm.ui.dash.main.OnCategoryListner;
import com.app.fsm.ui.dash.main.OnCustomerListener;
import com.app.fsm.ui.dash.main.OnMemberListener;
import com.app.fsm.ui.dash.main.OnSubCategoryListener;
import com.app.fsm.ui.dash.main.asynctask.CategoryAsyncTask;
import com.app.fsm.ui.dash.main.asynctask.CustomersAsyncTask;
import com.app.fsm.ui.dash.main.asynctask.GetAllMemberAsynTask;
import com.app.fsm.ui.dash.main.asynctask.SubCategoryAsyncTask;
import com.app.fsm.ui.dash.todo.OnTodoFetchListener;
import com.app.fsm.ui.dash.todo.asynctask.GetTodoStatusAsyncTask;
import com.app.fsm.utils.AppConstant;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class TicketList extends BaseFragment implements OnTodoFetchListener,
        OnMemberListener, AdapterView.OnItemSelectedListener, OnCategoryListner
        , OnCustomerListener
        , OnSubCategoryListener {



    @BindView(R.id.spinner_status)
    AppCompatSpinner spinnerStatus;
    @BindView(R.id.rv_ticketlist)
    RecyclerView rvTicketList;

    private List<TodoStatusModel> mtodoStatusModels;
    private List<MemberModel> memberModelList;
    private List<MemberModel> newmemberModelList;
    private List<CustomerModel> mcustomerModels;
    private List<CategoryModel> mcategoryModels;
    private List<SubCategoryModel> msubcategoryModels;

    private TicketListRvAdapter ticketListRvAdapter;

    public static TicketList newInstance(MemberModel memberModel) {
        TicketList ticketList = new TicketList();
        Bundle bundle = new Bundle();
        bundle.putSerializable(AppConstant.MEMBER_MODEL, memberModel);
        ticketList.setArguments(bundle);
        return ticketList;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =inflater.inflate(R.layout.fragment_ticket_list, container, false);
        ButterKnife.bind(this,view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
    }

    public void init() {
        setTitle("Ticket List");
        new GetAllMemberAsynTask(this, provideOaasDatabase(), 0).execute();
        new GetTodoStatusAsyncTask(this,provideOaasDatabase()).execute();
        new CategoryAsyncTask(this, provideOaasDatabase()).execute();
        new CustomersAsyncTask(this, provideOaasDatabase()).execute();
        new SubCategoryAsyncTask(this,provideOaasDatabase()).execute();

        newmemberModelList = new ArrayList<MemberModel>();
        //Setting Recycler view
        ticketListRvAdapter = new TicketListRvAdapter( getActivity());

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setReverseLayout(true);
        linearLayoutManager.setStackFromEnd(true);
        rvTicketList.setLayoutManager(linearLayoutManager);
        rvTicketList.setAdapter(ticketListRvAdapter);
    }

    @Override
    public void onMemberFetchCompleted(List<MemberModel> MemberModelList, int pos) {
        memberModelList=MemberModelList;
        Log.d("memberdata",""+memberModelList.size());

        Collections.sort(memberModelList, new Comparator<MemberModel>() {
            @Override
            public int compare(MemberModel m1, MemberModel m2) {
                if( m1.getStarttime() == null) {
                    return -1;
                }
                return m1.getStarttime().compareTo(m2.getStarttime());
            }
        });

    }

    @Override
    public void assignToFetched(List<UserModel> userModelList) {

    }

    @Override
    public void todoStatusFetched(List<TodoStatusModel> todoStatusModelList) {
        mtodoStatusModels=todoStatusModelList;
        /*for(MemberModel model:memberModelList){
            for(TodoStatusModel todoStatusModel:mtodoStatusModels){
                if(model.getStatusId()==todoStatusModel.getId()){
                    model.setTodoStatusName(todoStatusModel.getName());
                }
            }
        }

        //setting spinner
        spinnerStatus.setOnItemSelectedListener(this);
        spinnerStatus.setAdapter(new TicketStatusAdapter(requireContext(), getStatusList(mtodoStatusModels)));
        spinnerStatus.setSelection(spinnerStatus.getAdapter().getCount());

        ticketListRvAdapter.update(memberModelList);*/
    }

    public List<TodoStatusModel> getStatusList(List<TodoStatusModel> todoStatusModelList) {
        TodoStatusModel taskStatusModel = new TodoStatusModel();
        taskStatusModel.setName("Select Status");
        todoStatusModelList.add(taskStatusModel);
        return todoStatusModelList;
    }

    @Override
    public void onTodoLanguageFetched(List<TodoLanguage> todoLanguageList) {

    }

    @Override
    public void onTodoFetched(TodoModel todoModel) {

    }

    @Override
    public void onTodoUpdated() {

    }

    @Override
    public void onTodoCreated() {

    }

    @Override
    public void onTodoTypeFetchComplete(List<TodoType> todoTypeList) {

    }

    //spinner
    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        TodoStatusModel todoStatusModel = (TodoStatusModel) adapterView.getAdapter().getItem(i);
        newmemberModelList.clear();
        if (todoStatusModel != null && !todoStatusModel.getName().contains("Select Status")) {
            for(MemberModel model : memberModelList){
                if(model.getStatusId().equals(todoStatusModel.getId())){
                    newmemberModelList.add(model);
                }
            }
            ticketListRvAdapter.update(newmemberModelList);
        }
    }



    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    @Override
    public void onCategoryFetchCompleted(List<CategoryModel> categoryModels) {
        mcategoryModels=categoryModels;
    }

    @Override
    public void onCustomerFetchCompleted(List<CustomerModel> customerModels) {
        mcustomerModels=customerModels;
    }

    @Override
    public void onSubCategoryFetchCompleted(List<SubCategoryModel> subCategoryModels) {
        msubcategoryModels=subCategoryModels;

        for(MemberModel model:memberModelList){
            for(TodoStatusModel todoStatusModel:mtodoStatusModels){
                if(model.getStatusId()==todoStatusModel.getId()){
                    model.setTodoStatusName(todoStatusModel.getName());
                }
            }
        }

        for(MemberModel model:memberModelList){
            for(CustomerModel customerModel:mcustomerModels){
                if(model.getCustomerId()==customerModel.getId()){
                    model.setCustomerMyName(customerModel.getName());
                }
            }
        }

        for(MemberModel model:memberModelList){
            for(CategoryModel categoryModel:mcategoryModels){
                if(model.getCategoryId()==categoryModel.getId()){
                    model.setCategoryName(categoryModel.getName());
                }
            }
        }

        for(MemberModel model:memberModelList){
            for(SubCategoryModel subCategoryModel:msubcategoryModels){
                if(model.getSubcategoryId()==subCategoryModel.getId()){
                    model.setSubcategoryName(subCategoryModel.getName());
                }
            }
        }

        mtodoStatusModels.remove(0);
        mtodoStatusModels.remove(0);
        //setting spinner
        spinnerStatus.setOnItemSelectedListener(this);
        spinnerStatus.setAdapter(new TicketStatusAdapter(requireContext(), getStatusList(mtodoStatusModels)));
        spinnerStatus.setSelection(spinnerStatus.getAdapter().getCount());

        ticketListRvAdapter.update(memberModelList);
    }
}
