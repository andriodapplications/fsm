package com.app.fsm.ui.dash.main.asynctask;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.app.fsm.local_network.network.model.workflow.WorkFlowStatus;
import com.app.fsm.local_network.repository.OaasDatabase;
import com.app.fsm.local_network.repository.OassDatabaseBuilder;
import com.app.fsm.local_network.repository.dao.WorkFlowStatusDao;

import java.lang.ref.WeakReference;
import java.util.List;

public final class SetWorkFlowStatusAsyncTask extends AsyncTask<Void, Void, Boolean> {

    //Prevent leak
    private WeakReference<Context> weakActivity;
    private List<WorkFlowStatus> workFlowStatusList;

    public SetWorkFlowStatusAsyncTask(Context context, List<WorkFlowStatus> workFlowStatusList) {
        weakActivity = new WeakReference<>(context);
        this.workFlowStatusList = workFlowStatusList;
    }

    @Override
    protected Boolean doInBackground(Void... params) {
        OaasDatabase oaasDatabase = OassDatabaseBuilder.provideOassDatabase(weakActivity.get());
        WorkFlowStatusDao workFlowStatusDao = oaasDatabase.provideWorkFlowStatusDao();
        workFlowStatusDao.insertAllStatus(workFlowStatusList);
        return true;
    }

    @Override
    protected void onPostExecute(Boolean isInserted) {
        if (isInserted) {
            Toast.makeText(weakActivity.get(), "Done", Toast.LENGTH_SHORT).show();
        }
    }
}

