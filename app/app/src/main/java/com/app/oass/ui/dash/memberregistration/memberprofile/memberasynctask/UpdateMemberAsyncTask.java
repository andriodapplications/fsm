package com.app.fsm.ui.dash.memberregistration.memberprofile.memberasynctask;

import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.app.fsm.local_network.network.model.member.MemberModel;
import com.app.fsm.local_network.repository.OaasDatabase;
import com.app.fsm.local_network.repository.dao.MemberDao;
import com.app.fsm.ui.OnCompleteListener;

public final class UpdateMemberAsyncTask extends AsyncTask<Void, Void, Integer> {

    private OaasDatabase oaasDatabase;
    private MemberModel memberModel;
    private OnCompleteListener onCompleteListener;

    public UpdateMemberAsyncTask(@NonNull OnCompleteListener onCompleteListener, @NonNull OaasDatabase oaasDatabase
            , MemberModel memberModel) {
        this.onCompleteListener=onCompleteListener;
        this.oaasDatabase = oaasDatabase;
        this.memberModel = memberModel;
    }

    @Override
    protected Integer doInBackground(Void... params) {
        MemberDao memberDao = oaasDatabase.provideMemberDao();
        return memberDao.updateMember(memberModel);
    }

    @Override
    protected void onPostExecute(Integer longValue) {
        if(onCompleteListener!=null)
            onCompleteListener.onUpdate();
    }
}

