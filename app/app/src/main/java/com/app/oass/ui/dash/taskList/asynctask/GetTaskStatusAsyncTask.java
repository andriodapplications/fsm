package com.app.fsm.ui.dash.taskList.asynctask;

import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.app.fsm.local_network.network.model.joinedtable.TaskStatusModel;
import com.app.fsm.local_network.repository.OaasDatabase;
import com.app.fsm.local_network.repository.dao.TodoTypeDao;
import com.app.fsm.ui.dash.taskList.OnFetchListener;

import java.util.ArrayList;
import java.util.List;

public final class GetTaskStatusAsyncTask extends AsyncTask<Void, Void, List<TaskStatusModel>> {

    private OaasDatabase oaasDatabase;
    private OnFetchListener onFetchListener;

    public GetTaskStatusAsyncTask(@NonNull OnFetchListener onFetchListener
            , @NonNull OaasDatabase oaasDatabase) {
        this.oaasDatabase = oaasDatabase;
        this.onFetchListener = onFetchListener;
    }

    @Override
    protected List<TaskStatusModel> doInBackground(Void... params) {

        TodoTypeDao todoTypeDao = oaasDatabase.provideTodoTypeDao();
        List<TaskStatusModel> taskTypeModelList = todoTypeDao.provideWorKStatus();
        List<TaskStatusModel> taskTypeModelList1 = todoTypeDao.provideTodoStatus();

        List<TaskStatusModel> finalTaskTypeModelList = new ArrayList<>();
        finalTaskTypeModelList.addAll(taskTypeModelList);
        finalTaskTypeModelList.addAll(taskTypeModelList1);
        return finalTaskTypeModelList;
    }

    @Override
    protected void onPostExecute(List<TaskStatusModel> taskStatusModelList) {
        if (onFetchListener != null) {
            onFetchListener.onTaskStatusFetched(taskStatusModelList);
        }
    }
}

