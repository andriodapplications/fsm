package com.app.fsm.ui.dash.workflow.asynctask;

import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.app.fsm.local_network.network.model.workflow.WorkFlowLangauge;
import com.app.fsm.local_network.repository.OaasDatabase;
import com.app.fsm.local_network.repository.dao.WorkFlowLanguageDao;
import com.app.fsm.ui.dash.workflow.OnWorkFlowFetchListener;

import java.util.List;

public final class GetWorkFlowLanguageAsyncTask extends AsyncTask<Void, Void, List<WorkFlowLangauge>> {

    private OaasDatabase oaasDatabase;
    private OnWorkFlowFetchListener onWorkFlowFetchListener;

    public GetWorkFlowLanguageAsyncTask(@NonNull OnWorkFlowFetchListener onWorkFlowFetchListener, @NonNull OaasDatabase oaasDatabase) {
        this.oaasDatabase = oaasDatabase;
        this.onWorkFlowFetchListener = onWorkFlowFetchListener;
    }

    @Override
    protected List<WorkFlowLangauge> doInBackground(Void... params) {
        WorkFlowLanguageDao workFlowLanguageDao = oaasDatabase.provideWorkFlowLanguageDao();
        return workFlowLanguageDao.getAllWorkFlowlanguage();
    }

    @Override
    protected void onPostExecute(List<WorkFlowLangauge> workFlowLangaugeList) {
        if (onWorkFlowFetchListener != null) {
            onWorkFlowFetchListener.onWorkLanguageFetched(workFlowLangaugeList);
        }
    }
}

