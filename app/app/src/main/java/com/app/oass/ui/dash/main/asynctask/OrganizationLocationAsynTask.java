package com.app.fsm.ui.dash.main.asynctask;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.app.fsm.local_network.network.model.organizationlocation.OrganizationLocationModel;
import com.app.fsm.local_network.repository.OaasDatabase;
import com.app.fsm.local_network.repository.OassDatabaseBuilder;
import com.app.fsm.local_network.repository.dao.OrganizationalLocationDao;

import java.lang.ref.WeakReference;
import java.util.List;

public final class OrganizationLocationAsynTask extends AsyncTask<Void, Void, Boolean> {

    //Prevent leak
    private WeakReference<Context> weakActivity;
    private List<OrganizationLocationModel> organizationLocationModelList;

    public OrganizationLocationAsynTask(Context context, List<OrganizationLocationModel> organizationLocationModelList) {
        weakActivity = new WeakReference<>(context);
        this.organizationLocationModelList = organizationLocationModelList;
    }

    @Override
    protected Boolean doInBackground(Void... params) {
        OaasDatabase oaasDatabase = OassDatabaseBuilder.provideOassDatabase(weakActivity.get());
        OrganizationalLocationDao organizationalLocationDao = oaasDatabase.provideOrganizationalLocationDao();
        organizationalLocationDao.insertAllOrganizationLocation(organizationLocationModelList);
        return true;
    }

    @Override
    protected void onPostExecute(Boolean agentsCount) {
        Context context = weakActivity.get();
        if (context == null) {
            return;
        }
        if (agentsCount) {
            Toast.makeText(context, "Done", Toast.LENGTH_SHORT).show();
        }
    }
}

