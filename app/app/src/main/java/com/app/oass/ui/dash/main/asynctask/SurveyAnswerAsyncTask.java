package com.app.fsm.ui.dash.main.asynctask;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.app.fsm.local_network.network.model.survey.SurveyAnswerModel;
import com.app.fsm.local_network.repository.OaasDatabase;
import com.app.fsm.local_network.repository.OassDatabaseBuilder;
import com.app.fsm.local_network.repository.dao.SurveyAnswerDao;

import java.lang.ref.WeakReference;
import java.util.List;

public final class SurveyAnswerAsyncTask extends AsyncTask<Void, Void, Boolean> {

    //Prevent leak
    private WeakReference<Context> weakActivity;
    private List<SurveyAnswerModel> surveyAnswerModelList;

    public SurveyAnswerAsyncTask(Context context, List<SurveyAnswerModel> surveyAnswerModelList) {
        weakActivity = new WeakReference<>(context);
        this.surveyAnswerModelList =surveyAnswerModelList;
    }

    @Override
    protected Boolean doInBackground(Void... params) {
        OaasDatabase oaasDatabase = OassDatabaseBuilder.provideOassDatabase(weakActivity.get());
        SurveyAnswerDao surveyAnswerDao= oaasDatabase.provideSurveyAnswerDao();
        surveyAnswerDao.insertAllSurveyAnswer(surveyAnswerModelList);
        return true;
    }

    @Override
    protected void onPostExecute(Boolean isInserted) {
           Toast.makeText(weakActivity.get(), "Done", Toast.LENGTH_SHORT).show();

    }
}

