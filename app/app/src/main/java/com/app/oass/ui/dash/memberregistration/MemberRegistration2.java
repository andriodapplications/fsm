package com.app.fsm.ui.dash.memberregistration;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.fsm.BaseFragment;
import com.app.fsm.R;
import com.app.fsm.SliderBaseFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class MemberRegistration2 extends SliderBaseFragment {

    public static MemberRegistration2 newInstance() {
        return new MemberRegistration2();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_member_registration2, container, false);
    }

    @Override
    public boolean onNext() {
        return false;
    }
}
