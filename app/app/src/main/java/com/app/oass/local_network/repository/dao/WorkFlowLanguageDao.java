package com.app.fsm.local_network.repository.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.app.fsm.local_network.network.model.workflow.WorkFlowLangauge;

import java.util.List;

@Dao
public interface WorkFlowLanguageDao {

    @Query("SELECT * FROM workflowlangauge")
    List<WorkFlowLangauge> getAllWorkFlowlanguage();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAllWorkFlowLanguage(List<WorkFlowLangauge> workFlowLangaugeList);

}
