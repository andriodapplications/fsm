package com.app.fsm.local_network.network.model.customer;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.app.fsm.local_network.network.model.gendermodel.GenderModel;
import com.app.fsm.local_network.repository.dao.BaseDao;

import java.util.List;

@Dao
public interface CustomerModelDao extends BaseDao {

    @Query("SELECT * FROM customermodel")
    List<CustomerModel> getCustomerData();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAllCustomers(List<CustomerModel> customerModels);
}
