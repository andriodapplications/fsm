package com.app.fsm.ui.dash.main.asynctask;

import android.content.Context;
import android.os.AsyncTask;

import com.app.fsm.local_network.network.model.member.MemberLanguageModel;
import com.app.fsm.local_network.repository.OaasDatabase;
import com.app.fsm.local_network.repository.OassDatabaseBuilder;
import com.app.fsm.local_network.repository.dao.MemberLanguageDao;

import java.lang.ref.WeakReference;
import java.util.List;

public final class MemberLanguageAsyncTask extends AsyncTask<Void, Void, Boolean> {

    //Prevent leak
    private WeakReference<Context> weakActivity;
    private List<MemberLanguageModel> memberLanguageModelList;

    public MemberLanguageAsyncTask(Context context, List<MemberLanguageModel> memberLanguageModelList) {
        weakActivity = new WeakReference<>(context);
        this.memberLanguageModelList = memberLanguageModelList;
    }

    @Override
    protected Boolean doInBackground(Void... params) {
        OaasDatabase oaasDatabase = OassDatabaseBuilder.provideOassDatabase(weakActivity.get());
        MemberLanguageDao memberLanguageDao= oaasDatabase.provideMemberLanguageDao();
        memberLanguageDao.insertAllMember(memberLanguageModelList);
        return true;
    }

    @Override
    protected void onPostExecute(Boolean agentsCount) {
        Context context = weakActivity.get();
        if (context == null) {
            return;
        }
    }
}

