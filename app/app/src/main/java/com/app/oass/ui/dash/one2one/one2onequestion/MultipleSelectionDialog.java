package com.app.fsm.ui.dash.one2one.one2onequestion;

import android.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.app.fsm.R;
import com.app.fsm.ui.dash.one2one.one2onequestion.adapter.MultipleSelectRVAdapter;

import java.util.List;

public class MultipleSelectionDialog implements MultiSelectListener, View.OnClickListener {

    private static final String TAG = "SearchDialog";
    private List<MultipleSelectionAnswerModel> multipleSelectionAnswerModelList;
    private One2OneQuestionFragment mContext;
    private AlertDialog alertDialog;

    private MultipleSelectRVAdapter multipleSelectRVAdapter;
    private RecyclerView recyclerView;
    private int pos;
    private LayoutInflater layoutInflater;

    public MultipleSelectionDialog(One2OneQuestionFragment one2OneQuestionFragment, List<MultipleSelectionAnswerModel> multipleSelectionAnswerModelList,
                                   LayoutInflater layoutInflater) {
        this.multipleSelectionAnswerModelList = multipleSelectionAnswerModelList;
        this.mContext = one2OneQuestionFragment;
        this.layoutInflater = layoutInflater;
    }

    public void show() {
        final AlertDialog.Builder adb = new AlertDialog.Builder(mContext.requireActivity());
        View view = layoutInflater.inflate(R.layout.layout_multiple_selction_dialog, null);
        recyclerView = view.findViewById(R.id.recyclerview);

        final TextView textView = view.findViewById(R.id.tv_dialog_title);
        adb.setView(view);
        alertDialog = adb.create();

        Button btnOk=view.findViewById(R.id.btn_ok);
        btnOk.setOnClickListener(this);
        Button btnCancel=view.findViewById(R.id.btn_cancel);
        btnCancel.setOnClickListener(this);

        //Full screen but with bug
        ViewGroup.LayoutParams lp = alertDialog.getWindow().getAttributes();
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;

        alertDialog.getWindow().setAttributes((WindowManager.LayoutParams) lp);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setCanceledOnTouchOutside(true);
        alertDialog.setCancelable(true);

        //Setting Recycler view
        multipleSelectRVAdapter = new MultipleSelectRVAdapter(mContext.requireActivity(), this,multipleSelectionAnswerModelList);
        recyclerView.setLayoutManager(new LinearLayoutManager(mContext.requireActivity(), LinearLayoutManager.VERTICAL, false));
        recyclerView.setAdapter(multipleSelectRVAdapter);

        alertDialog.show();
    }

    public void dismiss(){
        if (alertDialog!=null){
            alertDialog.dismiss();
        }
    }

    @Override
    public void onItemSelectedListener(List<MultipleSelectionAnswerModel> multipleSelectionAnswerModelList) {

        mContext.updateMultiSelectedOptions(((MultipleSelectRVAdapter)recyclerView.getAdapter()).getList());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_ok:
                mContext.updateMultiSelectedOptions(((MultipleSelectRVAdapter)recyclerView.getAdapter()).getList());
                dismiss();
                break;
            case R.id.btn_cancel:
                dismiss();
                break;
        }
    }
}

