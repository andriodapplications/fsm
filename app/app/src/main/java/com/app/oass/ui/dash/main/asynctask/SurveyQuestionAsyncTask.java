package com.app.fsm.ui.dash.main.asynctask;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.app.fsm.local_network.network.model.survey.SurveyQuestionModel;
import com.app.fsm.local_network.repository.OaasDatabase;
import com.app.fsm.local_network.repository.OassDatabaseBuilder;
import com.app.fsm.local_network.repository.dao.SurveyQuestionDao;

import java.lang.ref.WeakReference;
import java.util.List;

public final class SurveyQuestionAsyncTask extends AsyncTask<Void, Void, Boolean> {

    //Prevent leak
    private WeakReference<Context> weakActivity;
    private List<SurveyQuestionModel> surveyQuestionModelList;

    public SurveyQuestionAsyncTask(Context context, List<SurveyQuestionModel> surveyQuestionModelList) {
        weakActivity = new WeakReference<>(context);
        this.surveyQuestionModelList =surveyQuestionModelList;
    }

    @Override
    protected Boolean doInBackground(Void... params) {

        OaasDatabase oaasDatabase = OassDatabaseBuilder.provideOassDatabase(weakActivity.get());
        SurveyQuestionDao surveyQuestionDao= oaasDatabase.provideSurveyQQuestiondao();
        surveyQuestionDao.insertAllSurveyQuestion(surveyQuestionModelList);
        return true;
    }

    @Override
    protected void onPostExecute(Boolean isInserted) {
       if(isInserted){
           Toast.makeText(weakActivity.get(), "Done", Toast.LENGTH_SHORT).show();
       }
    }
}

