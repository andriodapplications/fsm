package com.app.fsm.ui.dash.main;

public interface OnPushSuccesslistener {

    void onPushSuccess();

    void onPushError(String msg);

}
