package com.app.fsm;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;

import com.app.fsm.local_network.network.ApiService;
import com.app.fsm.local_network.repository.OaasDatabase;
import com.app.fsm.local_network.repository.OassDatabaseBuilder;
import com.app.fsm.utils.ApiUtils;
import com.app.fsm.utils.CommonUtils;
import com.app.fsm.utils.UserSessionManager;


public abstract class BaseFragment extends Fragment {

    private AppCompatActivity appCompatActivity;
    private ProgressDialog progressDialog;
    private TextView toolBarTitle;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        setUpToolbar();
        Log.i("Basefragment", this.toString());
    }

    public OaasDatabase provideOaasDatabase(){
        return OassDatabaseBuilder.provideOassDatabase(requireActivity());
    }

    private void setUpToolbar() {
        setHasOptionsMenu(true);
        AppCompatActivity appCompatActivity = (AppCompatActivity) requireActivity();
        if (appCompatActivity.getSupportActionBar() != null) {
            Toolbar toolbar = (requireActivity()).findViewById(R.id.toolbar);
            toolbar.setTitle("");
            toolBarTitle=toolbar.findViewById(R.id.title);
            (appCompatActivity.getSupportActionBar()).setHomeButtonEnabled(true);
            (appCompatActivity.getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
            appCompatActivity.setSupportActionBar(toolbar);
        }
    }

    public void setTitle(String title){
        if (toolBarTitle != null)
            toolBarTitle.setText(title);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId()==android.R.id.home){
            requireActivity().onBackPressed();
            return true;
        }
        return false;
    }

//    @Override
//    public void onPrepareOptionsMenu(Menu menu) {
//        if (menu != null) {
//            menu.removeItem(R.id.menu_document);
//        }
//    }

    @Override
    public void onDestroyView() {
        if(progressDialog!=null){
            progressDialog.dismiss();
        }
        super.onDestroyView();
    }

    public void showLoading() throws IllegalStateException {

        try{

            hideLoading();
            progressDialog = CommonUtils.showLoadingDialog(this.requireContext());
            progressDialog.show();

        }catch (Exception e){
            e.printStackTrace();
        }


    }

    public void hideLoading() throws IllegalStateException {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.cancel();
        }
    }

    public UserSessionManager getUserSessionManager() {
        return UserSessionManager.getUserSessionManager();
    }

    public ApiService getApiService() {
        return ApiUtils.getAPIService();
    }

}
