package com.app.fsm.utils.searchdialog;

import com.app.fsm.local_network.network.model.member.MemberModel;

/**
 * Created by ajithvgiri on 06/11/17.
 */

public interface OnSearchItemSelected {
    void onClick(MemberModel memberModel);
}
