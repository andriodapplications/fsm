package com.app.fsm.local_network.network.model;

public class FeatureModel {

    String strOption;

    int ic_option;

    public FeatureModel(String strOption, int ic_option) {
        this.strOption = strOption;
        this.ic_option = ic_option;
    }


    public String getStrOption() {
        return strOption;
    }

    public void setStrOption(String strOption) {
        this.strOption = strOption;
    }

    public int getIc_option() {
        return ic_option;
    }

    public void setIc_option(int ic_option) {
        this.ic_option = ic_option;
    }
}
