package com.app.fsm.ui;

public interface OnCompleteListener {
     void onComplete();

     void onUpdate();
}
