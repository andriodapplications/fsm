package com.app.fsm.local_network.repository.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.app.fsm.local_network.network.model.organizationlevel.OrganizationalLevelModel;

import java.util.List;

@Dao
public interface OrganizationalLevelDao {

    @Query("SELECT * FROM organizationallevelmodel")
    List<OrganizationalLevelModel> getAll();

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertAllOrganizationLevel(List<OrganizationalLevelModel> organizationalLevelModelList);
}
