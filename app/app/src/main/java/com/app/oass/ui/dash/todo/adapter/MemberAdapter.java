package com.app.fsm.ui.dash.todo.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.app.fsm.R;
import com.app.fsm.local_network.network.model.member.MemberModel;

import java.util.List;

public class MemberAdapter extends BaseAdapter {

    private LayoutInflater layoutInflater;
    private List<MemberModel> memberModelList;

    public MemberAdapter(@NonNull Context context, @NonNull List<MemberModel> memberModelList) {
        this.memberModelList = memberModelList;
        this.layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        int count = memberModelList.size();
        return count > 0 ? count - 1 : count;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        ItemVH itemVH;


        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.row_spinner_list, parent, false);
            itemVH = new ItemVH();
            itemVH.tvTitle = convertView.findViewById(R.id.txt_title);
            convertView.setTag(itemVH);
        } else
            itemVH = (ItemVH) convertView.getTag();
        itemVH.tvTitle.setText(memberModelList.get(position).getSerno());
        return convertView;
    }

    @Nullable
    @Override
    public MemberModel getItem(int position) {
        return memberModelList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    static class ItemVH {
        TextView tvTitle;
    }
}