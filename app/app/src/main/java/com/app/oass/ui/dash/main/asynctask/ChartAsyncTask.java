package com.app.fsm.ui.dash.main.asynctask;

import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.app.fsm.local_network.network.model.category.CategoryModel;
import com.app.fsm.local_network.network.model.category.CategoryModelDao;
import com.app.fsm.local_network.network.model.charts.ChartModel;
import com.app.fsm.local_network.network.model.charts.ChartModelDao;
import com.app.fsm.local_network.repository.OaasDatabase;
import com.app.fsm.ui.dash.main.OnCategoryListner;
import com.app.fsm.ui.dash.main.OnChartDataListner;

import java.util.List;

public class ChartAsyncTask extends AsyncTask<Void,Void, List<ChartModel>> {

    private OaasDatabase oaasDatabase;
    private OnChartDataListner onChartDataListner;

    public ChartAsyncTask(@NonNull OnChartDataListner onChartDataListner,
                          @NonNull OaasDatabase oaasDatabase) {
        this.oaasDatabase = oaasDatabase;
        this.onChartDataListner = onChartDataListner;
    }
    @Override
    protected List<ChartModel> doInBackground(Void... voids) {
        ChartModelDao chartModel=oaasDatabase.provideChartModelDao();
        return chartModel.getChartData();
    }

    @Override
    protected void onPostExecute(List<ChartModel> chartModels) {
        if (onChartDataListner != null)
            onChartDataListner.onChartFetchCompleted(chartModels);
    }
}
