package com.app.fsm.ui.dash.todo.asynctask;

import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.app.fsm.local_network.network.model.todo.TodoLanguage;
import com.app.fsm.local_network.repository.OaasDatabase;
import com.app.fsm.local_network.repository.dao.TodoDao;
import com.app.fsm.ui.dash.todo.OnTodoFetchListener;

import java.util.List;

public final class GetTodoLanguageAsyncTask extends AsyncTask<Void, Void, List<TodoLanguage>> {

    private OaasDatabase oaasDatabase;
    private OnTodoFetchListener onTodoFetchListener;

    public GetTodoLanguageAsyncTask(@NonNull OnTodoFetchListener onTodoFetchListener, @NonNull OaasDatabase oaasDatabase) {
        this.oaasDatabase = oaasDatabase;
        this.onTodoFetchListener = onTodoFetchListener;
    }

    @Override
    protected List<TodoLanguage> doInBackground(Void... params) {
        TodoDao todoDao = oaasDatabase.provideTodoModelDao();
        return todoDao.provideAllTodoLangauge();
    }

    @Override
    protected void onPostExecute(List<TodoLanguage> todoLanguageList) {
        if (onTodoFetchListener != null) {
            onTodoFetchListener.onTodoLanguageFetched(todoLanguageList);
        }
    }
}

