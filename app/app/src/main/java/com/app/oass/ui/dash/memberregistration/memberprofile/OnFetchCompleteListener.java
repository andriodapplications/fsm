package com.app.fsm.ui.dash.memberregistration.memberprofile;

import com.app.fsm.local_network.network.model.educationmodel.EducationModel;
import com.app.fsm.local_network.network.model.gendermodel.GenderModel;
import com.app.fsm.local_network.network.model.member.MemberLanguageModel;
import com.app.fsm.local_network.network.model.occupationmodel.OccupationModel;

import java.util.List;

public interface OnFetchCompleteListener {

    void onGenderFetchComplete(List<GenderModel> genderModelList);

    void onOccupationFetchComplete(List<OccupationModel> occupationModelList);

    void onEducationFetchComplete(List<EducationModel> educationModelList);

    void onMemberLanguageComplete(List<MemberLanguageModel> memberLanguageModelList);

}
