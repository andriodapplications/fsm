package com.app.fsm.ui.dash.todo.asynctask;

import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.app.fsm.local_network.network.model.todo.TodoStatusModel;
import com.app.fsm.local_network.repository.OaasDatabase;
import com.app.fsm.local_network.repository.dao.TodoDao;
import com.app.fsm.ui.dash.todo.OnTodoFetchListener;

import java.util.List;

public final class GetTodoStatusAsyncTask extends AsyncTask<Void, Void, List<TodoStatusModel>> {

    private OaasDatabase oaasDatabase;
    private OnTodoFetchListener onTodoFetchListener;

    public GetTodoStatusAsyncTask(@NonNull OnTodoFetchListener onTodoFetchListener, @NonNull OaasDatabase oaasDatabase) {
        this.oaasDatabase = oaasDatabase;
        this.onTodoFetchListener = onTodoFetchListener;
    }

    @Override
    protected List<TodoStatusModel> doInBackground(Void... params) {
        TodoDao todoDao = oaasDatabase.provideTodoModelDao();
        return todoDao.provideAllTodoStatud();
    }

    @Override
    protected void onPostExecute(List<TodoStatusModel> todoStatusModelList) {
        if (onTodoFetchListener != null) {
            onTodoFetchListener.todoStatusFetched(todoStatusModelList);
        }
    }
}

