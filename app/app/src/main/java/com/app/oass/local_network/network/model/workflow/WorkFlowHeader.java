package com.app.fsm.local_network.network.model.workflow;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@Entity
public class WorkFlowHeader implements Serializable {

    @SerializedName("memberid")//It's a member table serverId
    @Expose
    private Integer memberid;
    @SerializedName("assignedto")
    @Expose
    private Integer assignedto;
    @SerializedName("workflowid")
    @Expose
    private Integer workflowid;
    @SerializedName("workflowname")
    @Expose
    private String workflowname;
    @SerializedName("workflowstatusid")
    @Expose
    private Integer workflowstatusid;
    @SerializedName("workflowcategoryid")
    @Expose
    private Integer workflowcategoryid;
    @SerializedName("workflowpriorityid")
    @Expose
    private Integer workflowpriorityid;
    @SerializedName("followupdate")
    @Expose
    private String followupdate;
    @SerializedName("comments")
    @Expose
    private String comments;
    @SerializedName("organizationId")
    @Expose
    private String organizationId;
    @SerializedName("lastmodifiedby")
    @Expose
    private Integer lastmodifiedby;
    @SerializedName("lastmodifiedtime")
    @Expose
    private String lastmodifiedtime;
    @SerializedName("lastmodifiedrole")
    @Expose
    private Integer lastmodifiedrole;
    @SerializedName("createdby")
    @Expose
    private Integer createdby;
    @SerializedName("createdtime")
    @Expose
    private String createdtime;
    @SerializedName("createdrole")
    @Expose
    private Integer createdrole;
    @SerializedName("deleteflag")
    @Expose
    private Boolean deleteflag;
    @SerializedName("orgstructure")
    @Expose
    private String orgstructure;
    @SerializedName("isPushed")
    private Boolean isPushed;

    @ColumnInfo(name = "serverId")
    @SerializedName("id")
    @Expose
    private Integer serverId;

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "localId")
    private int localId;

    private final static long serialVersionUID = 95742057006783917L;

    public Integer getMemberid() {
        return memberid;
    }

    public void setMemberid(Integer memberid) {
        this.memberid = memberid;
    }

    public Integer getAssignedto() {
        return assignedto;
    }

    public void setAssignedto(Integer assignedto) {
        this.assignedto = assignedto;
    }

    public Integer getWorkflowid() {
        return workflowid;
    }

    public void setWorkflowid(Integer workflowid) {
        this.workflowid = workflowid;
    }

    public Integer getWorkflowstatusid() {
        return workflowstatusid;
    }

    public void setWorkflowstatusid(Integer workflowstatusid) {
        this.workflowstatusid = workflowstatusid;
    }

    public Integer getWorkflowcategoryid() {
        return workflowcategoryid;
    }

    public void setWorkflowcategoryid(Integer workflowcategoryid) {
        this.workflowcategoryid = workflowcategoryid;
    }

    public Integer getWorkflowpriorityid() {
        return workflowpriorityid;
    }

    public void setWorkflowpriorityid(Integer workflowpriorityid) {
        this.workflowpriorityid = workflowpriorityid;
    }

    public String getFollowupdate() {
        return followupdate;
    }

    public void setFollowupdate(String followupdate) {
        this.followupdate = followupdate;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(String organizationId) {
        this.organizationId = organizationId;
    }

    public Integer getLastmodifiedby() {
        return lastmodifiedby;
    }

    public void setLastmodifiedby(Integer lastmodifiedby) {
        this.lastmodifiedby = lastmodifiedby;
    }

    public String getLastmodifiedtime() {
        return lastmodifiedtime;
    }

    public void setLastmodifiedtime(String lastmodifiedtime) {
        this.lastmodifiedtime = lastmodifiedtime;
    }

    public String getWorkflowname() {
        return workflowname;
    }

    public void setWorkflowname(String workflowname) {
        this.workflowname = workflowname;
    }

    public Integer getLastmodifiedrole() {
        return lastmodifiedrole;
    }

    public void setLastmodifiedrole(Integer lastmodifiedrole) {
        this.lastmodifiedrole = lastmodifiedrole;
    }

    public Integer getCreatedby() {
        return createdby;
    }

    public void setCreatedby(Integer createdby) {
        this.createdby = createdby;
    }

    public String getCreatedtime() {
        return createdtime;
    }

    public void setCreatedtime(String createdtime) {
        this.createdtime = createdtime;
    }

    public Integer getCreatedrole() {
        return createdrole;
    }

    public void setCreatedrole(Integer createdrole) {
        this.createdrole = createdrole;
    }

    public Boolean getDeleteflag() {
        return deleteflag;
    }

    public void setDeleteflag(Boolean deleteflag) {
        this.deleteflag = deleteflag;
    }

    public String getOrgstructure() {
        return orgstructure;
    }

    public void setOrgstructure(String orgstructure) {
        this.orgstructure = orgstructure;
    }

    public Integer getServerId() {
        return serverId;
    }

    public void setServerId(Integer serverId) {
        this.serverId = serverId;
    }

    public int getLocalId() {
        return localId;
    }

    public void setLocalId(int localId) {
        this.localId = localId;
    }

    public Boolean getPushed() {
        return isPushed;
    }

    public void setPushed(Boolean pushed) {
        isPushed = pushed;
    }
}
