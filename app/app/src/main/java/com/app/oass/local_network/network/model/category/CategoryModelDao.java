package com.app.fsm.local_network.network.model.category;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.app.fsm.local_network.network.model.customer.CustomerModel;
import com.app.fsm.local_network.repository.dao.BaseDao;

import java.util.List;

@Dao
public interface CategoryModelDao extends BaseDao {


    @Query("SELECT * FROM categorymodel")
    List<CategoryModel> getCategoryData();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAllCategories(List<CategoryModel> categoryModels);
}
