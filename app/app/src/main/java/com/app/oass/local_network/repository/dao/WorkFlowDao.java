package com.app.fsm.local_network.repository.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.app.fsm.local_network.network.model.workflow.WorkFlowModel;

import java.util.List;

@Dao
public interface WorkFlowDao {

    @Query("SELECT * FROM workflowmodel")
    List<WorkFlowModel> getAllWorkFlowModel();

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertAllWorkFlow(List<WorkFlowModel> workFlowModelList);

}
