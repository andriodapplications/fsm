package com.app.fsm.ui.dash.main.asynctask;

import android.content.Context;
import android.os.AsyncTask;

import com.app.fsm.local_network.network.model.usermodel.UserModel;
import com.app.fsm.local_network.repository.OaasDatabase;
import com.app.fsm.local_network.repository.OassDatabaseBuilder;
import com.app.fsm.local_network.repository.dao.UserDao;

import java.lang.ref.WeakReference;
import java.util.List;

public final class SetUserListAsyncTask extends AsyncTask<Void, Void, Boolean> {

    //Prevent leak
    private WeakReference<Context> weakActivity;
    private List<UserModel> userModelList;

    public SetUserListAsyncTask(Context context, List<UserModel> userModelList) {
        weakActivity = new WeakReference<>(context);
        this.userModelList = userModelList;
    }

    @Override
    protected Boolean doInBackground(Void... params) {
        OaasDatabase oaasDatabase = OassDatabaseBuilder.provideOassDatabase(weakActivity.get());
        UserDao userDao = oaasDatabase.provideUserDao();
        userDao.insertAll(userModelList);
        return true;
    }
}

