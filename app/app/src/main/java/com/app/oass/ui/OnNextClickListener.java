package com.app.fsm.ui;

public interface OnNextClickListener {

    boolean onNextClick();
}
