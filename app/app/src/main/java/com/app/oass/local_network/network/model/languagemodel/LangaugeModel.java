package com.app.fsm.local_network.network.model.languagemodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class LangaugeModel {

    @SerializedName("data")
    @Expose
    private List<LanguageDetail> data = new ArrayList<LanguageDetail>();
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("status")
    @Expose
    private Integer status;

    public List<LanguageDetail> getData() {
        return data;
    }

    public void setData(List<LanguageDetail> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

}
