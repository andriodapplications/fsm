package com.app.fsm.ui.dash.main.asynctask;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.app.fsm.local_network.network.model.educationmodel.EducationModel;
import com.app.fsm.local_network.repository.OaasDatabase;
import com.app.fsm.local_network.repository.OassDatabaseBuilder;
import com.app.fsm.local_network.repository.dao.EducationDao;

import java.lang.ref.WeakReference;
import java.util.List;

public final class EducationAsynTask extends AsyncTask<Void, Void, Boolean> {

    //Prevent leak
    private WeakReference<Context> weakActivity;
    private List<EducationModel> educationModelList;

    public EducationAsynTask(Context context, List<EducationModel> educationModelList) {
        weakActivity = new WeakReference<>(context);
        this.educationModelList = educationModelList;
    }

    @Override
    protected Boolean doInBackground(Void... params) {
        OaasDatabase oaasDatabase = OassDatabaseBuilder.provideOassDatabase(weakActivity.get());
        EducationDao provideEducationdao = oaasDatabase.provideEducationDao();
        provideEducationdao.insertAllEducation(educationModelList);
        return true;
    }

    @Override
    protected void onPostExecute(Boolean agentsCount) {
        Context context = weakActivity.get();
        if (context == null) {
            return;
        }

        if (agentsCount) {
            Toast.makeText(context, "Done", Toast.LENGTH_SHORT).show();
        }
    }
}

