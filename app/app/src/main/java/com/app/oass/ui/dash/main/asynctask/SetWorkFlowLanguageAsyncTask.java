package com.app.fsm.ui.dash.main.asynctask;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.app.fsm.local_network.network.model.workflow.WorkFlowLangauge;
import com.app.fsm.local_network.repository.OaasDatabase;
import com.app.fsm.local_network.repository.OassDatabaseBuilder;
import com.app.fsm.local_network.repository.dao.WorkFlowLanguageDao;

import java.lang.ref.WeakReference;
import java.util.List;

public final class SetWorkFlowLanguageAsyncTask extends AsyncTask<Void, Void, Boolean> {

    //Prevent leak
    private WeakReference<Context> weakActivity;
    private List<WorkFlowLangauge> workFlowLangaugeList;

    public SetWorkFlowLanguageAsyncTask(Context context, List<WorkFlowLangauge> workFlowLangaugeList) {
        weakActivity = new WeakReference<>(context);
        this.workFlowLangaugeList = workFlowLangaugeList;
    }

    @Override
    protected Boolean doInBackground(Void... params) {
        OaasDatabase oaasDatabase = OassDatabaseBuilder.provideOassDatabase(weakActivity.get());
        WorkFlowLanguageDao workFlowLanguageDao= oaasDatabase.provideWorkFlowLanguageDao();
        workFlowLanguageDao.insertAllWorkFlowLanguage(workFlowLangaugeList);
        return true;
    }

    @Override
    protected void onPostExecute(Boolean isInserted) {
        if (isInserted) {
            Toast.makeText(weakActivity.get(), "Done", Toast.LENGTH_SHORT).show();
        }
    }
}

