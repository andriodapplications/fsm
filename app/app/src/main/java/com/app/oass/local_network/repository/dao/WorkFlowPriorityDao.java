package com.app.fsm.local_network.repository.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.app.fsm.local_network.network.model.workflow.WorkFlowPriority;

import java.util.List;

@Dao
public interface WorkFlowPriorityDao {

    @Query("SELECT * FROM workflowpriority WHERE workflowid = :workflowId")
    List<WorkFlowPriority> provideWorkFlowPriority(int workflowId);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertAllPriority(List<WorkFlowPriority> workFlowPriorityList);

}
