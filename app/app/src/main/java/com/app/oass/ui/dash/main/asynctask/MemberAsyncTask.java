package com.app.fsm.ui.dash.main.asynctask;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.app.fsm.local_network.network.model.member.MemberModel;
import com.app.fsm.local_network.repository.OaasDatabase;
import com.app.fsm.local_network.repository.OassDatabaseBuilder;
import com.app.fsm.local_network.repository.dao.MemberDao;

import java.lang.ref.WeakReference;
import java.util.List;

public final class MemberAsyncTask extends AsyncTask<Void, Void, Boolean> {

    //Prevent leak
    private WeakReference<Context> weakActivity;
    private List<MemberModel> memberModelList;

    public MemberAsyncTask(Context context, List<MemberModel> memberModelList) {
        weakActivity = new WeakReference<>(context);
        this.memberModelList=memberModelList;
    }

    @Override
    protected Boolean doInBackground(Void... params) {

        OaasDatabase oaasDatabase = OassDatabaseBuilder.provideOassDatabase(weakActivity.get());
        MemberDao memberDao = oaasDatabase.provideMemberDao();
        memberDao.insertAllMember(memberModelList);
        return true;
    }

    @Override
    protected void onPostExecute(Boolean agentsCount) {
        Context context = weakActivity.get();
        if (context == null) {
            return;
        }

        if (agentsCount) {
            Toast.makeText(context, "Done", Toast.LENGTH_SHORT).show();
        }
    }
}

