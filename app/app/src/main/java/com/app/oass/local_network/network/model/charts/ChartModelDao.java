package com.app.fsm.local_network.network.model.charts;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.app.fsm.local_network.network.model.category.CategoryModel;
import com.app.fsm.local_network.repository.dao.BaseDao;

import java.util.List;

@Dao
public interface ChartModelDao extends BaseDao {
    @Query("SELECT * FROM chartmodel")
    List<ChartModel> getChartData();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertChartData(List<ChartModel> chartModels);
}
