package com.app.fsm.ui.dash.todo.asynctask;

import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.app.fsm.local_network.network.model.todo.TodoModel;
import com.app.fsm.local_network.repository.OaasDatabase;
import com.app.fsm.local_network.repository.dao.TodoDao;
import com.app.fsm.ui.dash.todo.OnTodoFetchListener;

public final class UpdateTodoAsyncTask extends AsyncTask<Void, Void, Integer> {

    private OaasDatabase oaasDatabase;
    private TodoModel todoModel;
    private OnTodoFetchListener onTodoFetchListener;

    public UpdateTodoAsyncTask(@NonNull OaasDatabase oaasDatabase
            , TodoModel todoModel, @NonNull OnTodoFetchListener onTodoFetchListener) {
        this.oaasDatabase = oaasDatabase;
        this.todoModel = todoModel;
        this.onTodoFetchListener = onTodoFetchListener;
    }

    @Override
    protected Integer doInBackground(Void... params) {
        TodoDao todoDao = oaasDatabase.provideTodoModelDao();
        return todoDao.updateTodo(todoModel);
    }

    @Override
    protected void onPostExecute(Integer intSuccess) {
        if (onTodoFetchListener != null && intSuccess > -1) {
            onTodoFetchListener.onTodoUpdated();
        }
    }
}

