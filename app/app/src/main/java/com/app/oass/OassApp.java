
package com.app.fsm;

import android.app.Application;
import android.content.res.Configuration;

import com.app.fsm.local_network.repository.OassDatabaseBuilder;
import com.app.fsm.utils.UserSessionManager;

public class OassApp extends Application  {

    private static OassApp application;

    public void onCreate() {
        super.onCreate();
        application = this;
        UserSessionManager.getUserSessionManager(this);
        OassDatabaseBuilder.provideOassDatabase(this);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    public static OassApp getApplication() {
        return application;
    }

}
