package com.app.fsm.ui.dash.main.asynctask;

import android.content.Context;
import android.os.AsyncTask;

import com.app.fsm.local_network.network.model.workflow.WorkFlowHeader;
import com.app.fsm.local_network.repository.OaasDatabase;
import com.app.fsm.local_network.repository.OassDatabaseBuilder;
import com.app.fsm.local_network.repository.dao.WorkFlowHeaderDao;

import java.lang.ref.WeakReference;
import java.util.List;

public final class SetAllWorkFlowHeaderAsyncTask extends AsyncTask<Void, Void, Void> {

    //Prevent leak
    private WeakReference<Context> weakActivity;
    private List<WorkFlowHeader> workFlowHeaderList;

    public SetAllWorkFlowHeaderAsyncTask(Context context, List<WorkFlowHeader> workFlowHeaderList) {
        weakActivity = new WeakReference<>(context);
        this.workFlowHeaderList = workFlowHeaderList;
    }

    @Override
    protected Void doInBackground(Void... params) {
        OaasDatabase oaasDatabase = OassDatabaseBuilder.provideOassDatabase(weakActivity.get());
        WorkFlowHeaderDao workFlowHeaderDao= oaasDatabase.provideWorkFlowHeaderDao();
         workFlowHeaderDao.insertAllWorkFlowHeader(workFlowHeaderList);
         return null;
    }

    @Override
    protected void onPostExecute(Void voids) {

    }
}

