/*
 * Developed by Avinash Kumar singh on 24/1/19 3:49 PM
 * Last Modified 21/1/19 8:26 PM.
 *
 * Copyright (c) 2019.  All rights reserved.
 */

package com.app.fsm.ui.dash.taskList.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.app.fsm.R;
import com.app.fsm.local_network.network.model.joinedtable.TaskTypeModel;

import java.util.List;

public class TaskTypeAdapter extends BaseAdapter {

    private LayoutInflater layoutInflater;
    private List<TaskTypeModel> taskTypeModelList;

    public TaskTypeAdapter(@NonNull Context context, @NonNull List<TaskTypeModel> taskTypeModelList) {
        this.taskTypeModelList = taskTypeModelList;
        this.layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        int count = taskTypeModelList.size();
        return count > 0 ? count - 1 : count;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        ItemVH itemVH;

        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.row_spinner_list, parent,false);
            itemVH = new ItemVH();
            itemVH.tvTitle = convertView.findViewById(R.id.txt_title);
            convertView.setTag(itemVH);
        } else
            itemVH = (ItemVH) convertView.getTag();
        itemVH.tvTitle.setText(taskTypeModelList.get(position).getName());
        return convertView;
    }

    @Nullable
    @Override
    public Object getItem(int position) {
        return taskTypeModelList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    static class ItemVH {
        TextView tvTitle;
    }
}