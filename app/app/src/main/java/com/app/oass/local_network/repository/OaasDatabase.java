package com.app.fsm.local_network.repository;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.migration.Migration;

import com.app.fsm.local_network.network.model.Images.ImagesModelDao;
import com.app.fsm.local_network.network.model.Images.InsertImages;
import com.app.fsm.local_network.network.model.appconfig.AppConfigModel;
import com.app.fsm.local_network.network.model.appconfig.AppConfigModelDao;
import com.app.fsm.local_network.network.model.category.CategoryModel;
import com.app.fsm.local_network.network.model.category.CategoryModelDao;
import com.app.fsm.local_network.network.model.charts.ChartModel;
import com.app.fsm.local_network.network.model.charts.ChartModelDao;
import com.app.fsm.local_network.network.model.customer.CustomerModel;
import com.app.fsm.local_network.network.model.customer.CustomerModelDao;
import com.app.fsm.local_network.network.model.educationmodel.EducationModel;
import com.app.fsm.local_network.network.model.gendermodel.GenderModel;
import com.app.fsm.local_network.network.model.member.MemberLanguageModel;
import com.app.fsm.local_network.network.model.member.MemberModel;
import com.app.fsm.local_network.network.model.occupationmodel.OccupationModel;
import com.app.fsm.local_network.network.model.one_one_question.QuestionTypeModel;
import com.app.fsm.local_network.network.model.organizationlevel.OrganizationalLevelModel;
import com.app.fsm.local_network.network.model.organizationlocation.OrganizationLocationModel;
import com.app.fsm.local_network.network.model.subcategory.SubCategoryModel;
import com.app.fsm.local_network.network.model.subcategory.SubCategoryModelDao;
import com.app.fsm.local_network.network.model.survey.One2OneLanguage;
import com.app.fsm.local_network.network.model.survey.SurveyAnswerModel;
import com.app.fsm.local_network.network.model.survey.SurveyQuestionModel;
import com.app.fsm.local_network.network.model.todo.TodoLanguage;
import com.app.fsm.local_network.network.model.todo.TodoModel;
import com.app.fsm.local_network.network.model.todo.TodoStatusModel;
import com.app.fsm.local_network.network.model.todo.TodoType;
import com.app.fsm.local_network.network.model.user.User;
import com.app.fsm.local_network.network.model.usermodel.UserModel;
import com.app.fsm.local_network.network.model.workflow.WorkFlowCategory;
import com.app.fsm.local_network.network.model.workflow.WorkFlowHeader;
import com.app.fsm.local_network.network.model.workflow.WorkFlowLangauge;
import com.app.fsm.local_network.network.model.workflow.WorkFlowModel;
import com.app.fsm.local_network.network.model.workflow.WorkFlowPriority;
import com.app.fsm.local_network.network.model.workflow.WorkFlowStatus;
import com.app.fsm.local_network.repository.dao.EducationDao;
import com.app.fsm.local_network.repository.dao.GenderDao;
import com.app.fsm.local_network.repository.dao.MemberDao;
import com.app.fsm.local_network.repository.dao.MemberLanguageDao;
import com.app.fsm.local_network.repository.dao.OccupationDao;
import com.app.fsm.local_network.repository.dao.OrganizationalLevelDao;
import com.app.fsm.local_network.repository.dao.OrganizationalLocationDao;
import com.app.fsm.local_network.repository.dao.PushDataDao;
import com.app.fsm.local_network.repository.dao.QuestionTypeDao;
import com.app.fsm.local_network.repository.dao.SurveyAnswerDao;
import com.app.fsm.local_network.repository.dao.SurveyQuestionDao;
import com.app.fsm.local_network.repository.dao.TodoDao;
import com.app.fsm.local_network.repository.dao.TodoTypeDao;
import com.app.fsm.local_network.repository.dao.UpdateMemberIdDao;
import com.app.fsm.local_network.repository.dao.UserDao;
import com.app.fsm.local_network.repository.dao.WorkFlowCategoryDao;
import com.app.fsm.local_network.repository.dao.WorkFlowDao;
import com.app.fsm.local_network.repository.dao.WorkFlowHeaderDao;
import com.app.fsm.local_network.repository.dao.WorkFlowLanguageDao;
import com.app.fsm.local_network.repository.dao.WorkFlowPriorityDao;
import com.app.fsm.local_network.repository.dao.WorkFlowStatusDao;


@Database(entities = {User.class
        , MemberModel.class
        , MemberLanguageModel.class
        , GenderModel.class
        , UserModel.class
        , EducationModel.class
        , OccupationModel.class
        , OrganizationalLevelModel.class
        , OrganizationLocationModel.class
        , QuestionTypeModel.class
        , SurveyAnswerModel.class
        , SurveyQuestionModel.class
        , One2OneLanguage.class
        , TodoModel.class
        , TodoType.class
        , TodoLanguage.class
        , TodoStatusModel.class
        , WorkFlowCategory.class
        , WorkFlowLangauge.class
        , WorkFlowModel.class
        , WorkFlowPriority.class
        , WorkFlowStatus.class
        , WorkFlowHeader.class
        , CustomerModel.class
        , CategoryModel.class
        , SubCategoryModel.class
        , ChartModel.class
        , InsertImages.class
        , AppConfigModel.class}, version = 1, exportSchema = false)
public abstract class OaasDatabase extends RoomDatabase {

    public abstract UserDao provideUserDao();

    public abstract MemberDao provideMemberDao();

    public abstract MemberLanguageDao provideMemberLanguageDao();

    public abstract GenderDao provideGenderDao();

    public abstract EducationDao provideEducationDao();

    public abstract OccupationDao provideOccupationDao();

    public abstract OrganizationalLevelDao provideOrganizationLevelDao();

    public abstract OrganizationalLocationDao provideOrganizationalLocationDao();

    public abstract QuestionTypeDao provideQuestionTypeDao();

    public abstract SurveyAnswerDao provideSurveyAnswerDao();

    public abstract SurveyQuestionDao provideSurveyQQuestiondao();

    public abstract TodoTypeDao provideTodoTypeDao();

    public abstract TodoDao provideTodoModelDao();

    public abstract WorkFlowDao provideWorkFlowDao();

    public abstract WorkFlowHeaderDao provideWorkFlowHeaderDao();

    public abstract WorkFlowStatusDao provideWorkFlowStatusDao();

    public abstract WorkFlowPriorityDao provideWorkFlowPriorityDao();

    public abstract WorkFlowCategoryDao provideWorkFlowCategoryDao();

    public abstract WorkFlowLanguageDao provideWorkFlowLanguageDao();

    public abstract CustomerModelDao provideCustomersDao();

    public abstract CategoryModelDao provideCategoryDao();

    public abstract SubCategoryModelDao provideSubCategoryDao();

    public abstract ChartModelDao provideChartModelDao();

    public abstract ImagesModelDao provideInsertImagesDao();

    public abstract AppConfigModelDao provideAppconfigDao();

    public abstract PushDataDao providePushDataDao();

    public abstract UpdateMemberIdDao provideUpdateMemberIdDao();

    static final Migration MIGRATION_1_2 = new Migration(1,2) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {
            //database.execSQL("ALTER TABLE MemberModel RENAME COLUMN serverId TO id");
        }
    };

}
