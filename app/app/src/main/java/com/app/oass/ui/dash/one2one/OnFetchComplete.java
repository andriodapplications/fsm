package com.app.fsm.ui.dash.one2one;

import com.app.fsm.local_network.network.model.survey.SurveyQuestionModel;

import java.util.List;

public interface OnFetchComplete {
    void onComplete(List<SurveyQuestionModel> surveyQuestionModelList);
}
