package com.app.fsm.ui.dash.main;

import android.content.Context;

import com.app.fsm.local_network.network.ApiService;
import com.app.fsm.local_network.network.model.survey.SurveyQuestionModel;
import com.app.fsm.local_network.repository.OaasDatabase;
import com.app.fsm.ui.dash.one2one.OnFetchComplete;
import com.app.fsm.utils.UserSessionManager;

import java.util.List;

public class UploadDataToServer implements OnFetchComplete {
    private Context mContext;
    private OaasDatabase oaasDatabase;
    private ApiService apiService;
    private UserSessionManager userSessionManager;

    public UploadDataToServer(Context mContext,
                              OaasDatabase oaasDatabase, ApiService apiService,
                              UserSessionManager userSessionManager) {
        this.mContext = mContext;
        this.oaasDatabase = oaasDatabase;
        this.apiService = apiService;
        this.userSessionManager = userSessionManager;
    }

    public void startDataUploadToServer(){

    }

    @Override
    public void onComplete(List<SurveyQuestionModel> surveyQuestionModelList) {

    }
}
