
package com.app.fsm.utils;

public interface AppConstant {

    String IS_FROM = "isFrom";
    String PHONE = "phone";
    String DONE="done";
    String NEXT="next";
    String BACK="back";
    String USER="user";
    String IS_EDITABLE="is_editable";
    int GPS_REQUEST = 1002;
    String MEMBER_MODEL = "member_model";
    String SURVEY_QUESTION_MODEL = "survey_question_model";
    String SURVEY_QUESTION_LIST = "survey_question_list";
    String ALL_SURVEY_QUESTION_LIST = "all_survey_question_list";
    String NO="no";
    String YES="yes";
    String TASK_LIST = "task_list";
    String IS_DATA_FETCHED = "isDataFetched";
    String QR_CODE = "qrcode";
    String ONETOONE_QUESTION_FLAG = "onetoone_question_flag";

}
