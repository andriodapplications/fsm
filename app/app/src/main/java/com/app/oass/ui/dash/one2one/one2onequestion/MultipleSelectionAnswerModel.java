package com.app.fsm.ui.dash.one2one.one2onequestion;

public class MultipleSelectionAnswerModel {

    private String itemText;
    private boolean selected=false;

    MultipleSelectionAnswerModel(String itemText) {
        this.itemText = itemText;
    }

    public boolean isSelected()
    {
        return selected;
    }

    public void setSelected(boolean selected)
    {
        this.selected = selected;
    }

    public String getItemText() {
        return itemText;
    }
}
