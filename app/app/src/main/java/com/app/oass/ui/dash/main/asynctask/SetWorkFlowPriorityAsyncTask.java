package com.app.fsm.ui.dash.main.asynctask;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.app.fsm.local_network.network.model.workflow.WorkFlowPriority;
import com.app.fsm.local_network.repository.OaasDatabase;
import com.app.fsm.local_network.repository.OassDatabaseBuilder;
import com.app.fsm.local_network.repository.dao.WorkFlowPriorityDao;

import java.lang.ref.WeakReference;
import java.util.List;

public final class SetWorkFlowPriorityAsyncTask extends AsyncTask<Void, Void, Boolean> {

    //Prevent leak
    private WeakReference<Context> weakActivity;
    private List<WorkFlowPriority> workFlowPriorityList;

    public SetWorkFlowPriorityAsyncTask(Context context, List<WorkFlowPriority> workFlowPriorityList) {
        weakActivity = new WeakReference<>(context);
        this.workFlowPriorityList = workFlowPriorityList;
    }

    @Override
    protected Boolean doInBackground(Void... params) {
        OaasDatabase oaasDatabase = OassDatabaseBuilder.provideOassDatabase(weakActivity.get());
        WorkFlowPriorityDao workFlowPriorityDao = oaasDatabase.provideWorkFlowPriorityDao();
        workFlowPriorityDao.insertAllPriority(workFlowPriorityList);
        return true;
    }

    @Override
    protected void onPostExecute(Boolean isInserted) {
        if (isInserted) {
            Toast.makeText(weakActivity.get(), "Done", Toast.LENGTH_SHORT).show();
        }
    }
}

