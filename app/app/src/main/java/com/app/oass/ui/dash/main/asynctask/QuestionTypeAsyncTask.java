package com.app.fsm.ui.dash.main.asynctask;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.app.fsm.local_network.network.model.one_one_question.QuestionTypeModel;
import com.app.fsm.local_network.repository.OaasDatabase;
import com.app.fsm.local_network.repository.OassDatabaseBuilder;
import com.app.fsm.local_network.repository.dao.QuestionTypeDao;

import java.lang.ref.WeakReference;
import java.util.List;

public final class QuestionTypeAsyncTask extends AsyncTask<Void, Void, Boolean> {

    //Prevent leak
    private WeakReference<Context> weakActivity;
    private List<QuestionTypeModel> questionTypeModelList;

    public QuestionTypeAsyncTask(Context context, List<QuestionTypeModel> questionTypeModelList) {
        weakActivity = new WeakReference<>(context);
        this.questionTypeModelList = questionTypeModelList;
    }

    @Override
    protected Boolean doInBackground(Void... params) {
        OaasDatabase oaasDatabase = OassDatabaseBuilder.provideOassDatabase(weakActivity.get());
        QuestionTypeDao questionTypeDao = oaasDatabase.provideQuestionTypeDao();
        questionTypeDao.insertAll(questionTypeModelList);
        return true;
    }

    @Override
    protected void onPostExecute(Boolean isInserted) {
        Toast.makeText(weakActivity.get(), "Done", Toast.LENGTH_SHORT).show();
    }
}

