/*
 * Developed by Avinash Kumar singh on 24/1/19 3:49 PM
 * Last Modified 16/1/19 5:57 PM.
 *
 * Copyright (c) 2019.  All rights reserved.
 */

package com.app.fsm.ui.landingpage.signup;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.app.fsm.BaseActivity;
import com.app.fsm.local_network.network.ApiService;

import butterknife.ButterKnife;

public class SignUpActivity extends BaseActivity {

    private ApiService apiService;
    private ProgressDialog progressDialog;

    public static Intent newInstance(Context context) {
        return new Intent(context, SignUpActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_sign_up);
        ButterKnife.bind(this);
    }


}