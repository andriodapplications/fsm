package com.app.fsm.local_network.network.model.survey;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@Entity
public class SurveyQuestionModel implements Serializable {

    @SerializedName("question")
    @Expose
    private String question;
    @SerializedName("questiontype")
    @Expose
    private String questiontype;
    @SerializedName("answeroptions")
    @Expose
    private String answeroptions;
    @SerializedName("answer")
    @Expose
    private String answer;
    @SerializedName("surveyanswer")
    @Expose
    private String surveyanswer;
    @SerializedName("default")
    @Expose
    private String _default;
    @SerializedName("length")
    @Expose
    private Integer length;
    @SerializedName("serialno")
    @Expose
    private Integer serialno;
    @SerializedName("applyskip")
    @Expose
    private String applyskip;
    @SerializedName("applytomessage")
    @Expose
    private String applytomessage;
    @SerializedName("skipquestionid")
    @Expose
    private String skipquestionid;
    @SerializedName("skipquestionvalue")
    @Expose
    private String skipquestionvalue;
    @SerializedName("applytodovalueone")
    @Expose
    private String applytodovalueone;
    @SerializedName("applytodovalueonetodoid")
    @Expose
    private String applytodovalueonetodoid;
    @SerializedName("applytodovaluetwo")
    @Expose
    private String applytodovaluetwo;
    @SerializedName("applytodovaluetwotodoid")
    @Expose
    private String applytodovaluetwotodoid;
    @SerializedName("applytodovaluethree")
    @Expose
    private String applytodovaluethree;
    @SerializedName("applytodovaluethreetodoid")
    @Expose
    private String applytodovaluethreetodoid;
    @SerializedName("applytodovaluefour")
    @Expose
    private String applytodovaluefour;
    @SerializedName("applytodovaluefourtodoid")
    @Expose
    private String applytodovaluefourtodoid;
    @SerializedName("applytodovaluefive")
    @Expose
    private String applytodovaluefive;
    @SerializedName("applytodovaluefivetodoid")
    @Expose
    private String applytodovaluefivetodoid;
    @SerializedName("questionbaselang")
    @Expose
    private String questionbaselang;
    @SerializedName("questionlang1")
    @Expose
    private String questionlang1;
    @SerializedName("questionlang2")
    @Expose
    private String questionlang2;
    @SerializedName("questionlang3")
    @Expose
    private String questionlang3;
    @SerializedName("questionlang4")
    @Expose
    private String questionlang4;
    @SerializedName("questionlang5")
    @Expose
    private String questionlang5;
    @SerializedName("questionlang6")
    @Expose
    private String questionlang6;
    @SerializedName("questionlang7")
    @Expose
    private String questionlang7;
    @SerializedName("questionlang8")
    @Expose
    private String questionlang8;
    @SerializedName("questionlang9")
    @Expose
    private String questionlang9;
    @SerializedName("questionlang10")
    @Expose
    private String questionlang10;
    @SerializedName("valuebaselang")
    @Expose
    private String valuebaselang;
    @SerializedName("valuelang1")
    @Expose
    private String valuelang1;
    @SerializedName("valuelang2")
    @Expose
    private String valuelang2;
    @SerializedName("valuelang3")
    @Expose
    private String valuelang3;
    @SerializedName("valuelang4")
    @Expose
    private String valuelang4;
    @SerializedName("valuelang5")
    @Expose
    private String valuelang5;
    @SerializedName("valuelang6")
    @Expose
    private String valuelang6;
    @SerializedName("valuelang7")
    @Expose
    private String valuelang7;
    @SerializedName("valuelang8")
    @Expose
    private String valuelang8;
    @SerializedName("valuelang9")
    @Expose
    private String valuelang9;
    @SerializedName("valuelang10")
    @Expose
    private String valuelang10;
    @SerializedName("organizationId")
    @Expose
    private String organizationId;
    @SerializedName("lastmodifiedby")
    @Expose
    private Integer lastmodifiedby;
    @SerializedName("lastmodifiedtime")
    @Expose
    private String lastmodifiedtime;
    @SerializedName("lastmodifiedrole")
    @Expose
    private Integer lastmodifiedrole;
    @SerializedName("createdby")
    @Expose
    private Integer createdby;
    @SerializedName("createdtime")
    @Expose
    private String createdtime;
    @SerializedName("createdrole")
    @Expose
    private Integer createdrole;
    @SerializedName("deleteflag")
    @Expose
    private Boolean deleteflag;
    @SerializedName("parentFlag")
    @Expose
    private Boolean parentFlag;
    @SerializedName("parentId")
    @Expose
    private String parentId;
    @SerializedName("language")
    @Expose
    private String language;
    @SerializedName("multi")
    @Expose
    private String multi;
    @SerializedName("applytodovalue")
    @Expose
    private String applytodovalue;
    @SerializedName("skipquestion")
    @Expose
    private Boolean skipquestion;
    @SerializedName("customerId")
    @Expose
    private Integer customerId;
    @SerializedName("categoryId")
    @Expose
    private Integer categoryId;
    @SerializedName("subcategoryId")
    @Expose
    private Integer subcategoryId;
    @SerializedName("rcCode")
    @Expose
    private Integer rcCode;
    @SerializedName("fieldFlag")
    @Expose
    private Boolean fieldFlag;
    @SerializedName("webfieldFlag")
    @Expose
    private Boolean webfieldFlag;
    @SerializedName("platform")
    @Expose
    private String platform;

    private String remarkvalue;
    private String uploadfile;
    private Boolean onetooneFlag;
    private Integer questioncount;
    private Integer nextrccode;
    private Integer nextquestionId;
    private Integer previousquestionId;
    private Integer memberid;
    private Integer ddQuestioncount;

    @PrimaryKey
    @NonNull
    @SerializedName("id")
    @Expose
    private Integer id;
    private final static long serialVersionUID = 4952889992803527251L;

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getQuestiontype() {
        return questiontype;
    }

    public void setQuestiontype(String questiontype) {
        this.questiontype = questiontype;
    }

    public String getAnsweroptions() {
        return answeroptions;
    }

    public void setAnsweroptions(String answeroptions) {
        this.answeroptions = answeroptions;
    }

    public String getDefault() {
        return _default;
    }

    public void setDefault(String _default) {
        this._default = _default;
    }

    public Integer getLength() {
        return length;
    }

    public void setLength(Integer length) {
        this.length = length;
    }

    public Integer getSerialno() {
        return serialno;
    }

    public void setSerialno(Integer serialno) {
        this.serialno = serialno;
    }

    public String getApplyskip() {
        return applyskip;
    }

    public void setApplyskip(String applyskip) {
        this.applyskip = applyskip;
    }

    public String getApplytomessage() {
        return applytomessage;
    }

    public void setApplytomessage(String applytomessage) {
        this.applytomessage = applytomessage;
    }

    public String getSkipquestionid() {
        return skipquestionid;
    }

    public void setSkipquestionid(String skipquestionid) {
        this.skipquestionid = skipquestionid;
    }

    public String getSkipquestionvalue() {
        return skipquestionvalue;
    }

    public void setSkipquestionvalue(String skipquestionvalue) {
        this.skipquestionvalue = skipquestionvalue;
    }

    public String getApplytodovalueone() {
        return applytodovalueone;
    }

    public void setApplytodovalueone(String applytodovalueone) {
        this.applytodovalueone = applytodovalueone;
    }

    public String getApplytodovalueonetodoid() {
        return applytodovalueonetodoid;
    }

    public void setApplytodovalueonetodoid(String applytodovalueonetodoid) {
        this.applytodovalueonetodoid = applytodovalueonetodoid;
    }

    public String getApplytodovaluetwo() {
        return applytodovaluetwo;
    }

    public void setApplytodovaluetwo(String applytodovaluetwo) {
        this.applytodovaluetwo = applytodovaluetwo;
    }

    public String getApplytodovaluetwotodoid() {
        return applytodovaluetwotodoid;
    }

    public void setApplytodovaluetwotodoid(String applytodovaluetwotodoid) {
        this.applytodovaluetwotodoid = applytodovaluetwotodoid;
    }

    public String getApplytodovaluethree() {
        return applytodovaluethree;
    }

    public void setApplytodovaluethree(String applytodovaluethree) {
        this.applytodovaluethree = applytodovaluethree;
    }

    public String getApplytodovaluethreetodoid() {
        return applytodovaluethreetodoid;
    }

    public void setApplytodovaluethreetodoid(String applytodovaluethreetodoid) {
        this.applytodovaluethreetodoid = applytodovaluethreetodoid;
    }

    public String getApplytodovaluefour() {
        return applytodovaluefour;
    }

    public void setApplytodovaluefour(String applytodovaluefour) {
        this.applytodovaluefour = applytodovaluefour;
    }

    public String getApplytodovaluefourtodoid() {
        return applytodovaluefourtodoid;
    }

    public void setApplytodovaluefourtodoid(String applytodovaluefourtodoid) {
        this.applytodovaluefourtodoid = applytodovaluefourtodoid;
    }

    public String getApplytodovaluefive() {
        return applytodovaluefive;
    }

    public void setApplytodovaluefive(String applytodovaluefive) {
        this.applytodovaluefive = applytodovaluefive;
    }

    public String getApplytodovaluefivetodoid() {
        return applytodovaluefivetodoid;
    }

    public void setApplytodovaluefivetodoid(String applytodovaluefivetodoid) {
        this.applytodovaluefivetodoid = applytodovaluefivetodoid;
    }

    public String getQuestionbaselang() {
        return questionbaselang;
    }

    public void setQuestionbaselang(String questionbaselang) {
        this.questionbaselang = questionbaselang;
    }

    public String getQuestionlang1() {
        return questionlang1;
    }

    public void setQuestionlang1(String questionlang1) {
        this.questionlang1 = questionlang1;
    }

    public String getQuestionlang2() {
        return questionlang2;
    }

    public void setQuestionlang2(String questionlang2) {
        this.questionlang2 = questionlang2;
    }

    public String getQuestionlang3() {
        return questionlang3;
    }

    public void setQuestionlang3(String questionlang3) {
        this.questionlang3 = questionlang3;
    }

    public String getQuestionlang4() {
        return questionlang4;
    }

    public void setQuestionlang4(String questionlang4) {
        this.questionlang4 = questionlang4;
    }

    public String getQuestionlang5() {
        return questionlang5;
    }

    public void setQuestionlang5(String questionlang5) {
        this.questionlang5 = questionlang5;
    }

    public String getQuestionlang6() {
        return questionlang6;
    }

    public void setQuestionlang6(String questionlang6) {
        this.questionlang6 = questionlang6;
    }

    public String getQuestionlang7() {
        return questionlang7;
    }

    public void setQuestionlang7(String questionlang7) {
        this.questionlang7 = questionlang7;
    }

    public String getQuestionlang8() {
        return questionlang8;
    }

    public void setQuestionlang8(String questionlang8) {
        this.questionlang8 = questionlang8;
    }

    public String getQuestionlang9() {
        return questionlang9;
    }

    public void setQuestionlang9(String questionlang9) {
        this.questionlang9 = questionlang9;
    }

    public String getQuestionlang10() {
        return questionlang10;
    }

    public void setQuestionlang10(String questionlang10) {
        this.questionlang10 = questionlang10;
    }

    public String getValuebaselang() {
        return valuebaselang;
    }

    public void setValuebaselang(String valuebaselang) {
        this.valuebaselang = valuebaselang;
    }

    public String getValuelang1() {
        return valuelang1;
    }

    public void setValuelang1(String valuelang1) {
        this.valuelang1 = valuelang1;
    }

    public String getValuelang2() {
        return valuelang2;
    }

    public void setValuelang2(String valuelang2) {
        this.valuelang2 = valuelang2;
    }

    public String getValuelang3() {
        return valuelang3;
    }

    public void setValuelang3(String valuelang3) {
        this.valuelang3 = valuelang3;
    }

    public String getValuelang4() {
        return valuelang4;
    }

    public void setValuelang4(String valuelang4) {
        this.valuelang4 = valuelang4;
    }

    public String getValuelang5() {
        return valuelang5;
    }

    public void setValuelang5(String valuelang5) {
        this.valuelang5 = valuelang5;
    }

    public String getValuelang6() {
        return valuelang6;
    }

    public void setValuelang6(String valuelang6) {
        this.valuelang6 = valuelang6;
    }

    public String getValuelang7() {
        return valuelang7;
    }

    public void setValuelang7(String valuelang7) {
        this.valuelang7 = valuelang7;
    }

    public String getValuelang8() {
        return valuelang8;
    }

    public void setValuelang8(String valuelang8) {
        this.valuelang8 = valuelang8;
    }

    public String getValuelang9() {
        return valuelang9;
    }

    public void setValuelang9(String valuelang9) {
        this.valuelang9 = valuelang9;
    }

    public String getValuelang10() {
        return valuelang10;
    }

    public void setValuelang10(String valuelang10) {
        this.valuelang10 = valuelang10;
    }

    public String getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(String organizationId) {
        this.organizationId = organizationId;
    }

    public Integer getLastmodifiedby() {
        return lastmodifiedby;
    }

    public void setLastmodifiedby(Integer lastmodifiedby) {
        this.lastmodifiedby = lastmodifiedby;
    }

    public String getLastmodifiedtime() {
        return lastmodifiedtime;
    }

    public void setLastmodifiedtime(String lastmodifiedtime) {
        this.lastmodifiedtime = lastmodifiedtime;
    }

    public Integer getLastmodifiedrole() {
        return lastmodifiedrole;
    }

    public void setLastmodifiedrole(Integer lastmodifiedrole) {
        this.lastmodifiedrole = lastmodifiedrole;
    }

    public Integer getCreatedby() {
        return createdby;
    }

    public void setCreatedby(Integer createdby) {
        this.createdby = createdby;
    }

    public String getCreatedtime() {
        return createdtime;
    }

    public void setCreatedtime(String createdtime) {
        this.createdtime = createdtime;
    }

    public Integer getCreatedrole() {
        return createdrole;
    }

    public void setCreatedrole(Integer createdrole) {
        this.createdrole = createdrole;
    }

    public Boolean getDeleteflag() {
        return deleteflag;
    }

    public void setDeleteflag(Boolean deleteflag) {
        this.deleteflag = deleteflag;
    }

    public Boolean getParentFlag() {
        return parentFlag;
    }

    public void setParentFlag(Boolean parentFlag) {
        this.parentFlag = parentFlag;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getMulti() {
        return multi;
    }

    public void setMulti(String multi) {
        this.multi = multi;
    }

    public String getApplytodovalue() {
        return applytodovalue;
    }

    public void setApplytodovalue(String applytodovalue) {
        this.applytodovalue = applytodovalue;
    }

    public Boolean getSkipquestion() {
        return skipquestion;
    }

    public void setSkipquestion(Boolean skipquestion) {
        this.skipquestion = skipquestion;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public Integer getSubcategoryId() {
        return subcategoryId;
    }

    public void setSubcategoryId(Integer subcategoryId) {
        this.subcategoryId = subcategoryId;
    }

    public Integer getRcCode() {
        return rcCode;
    }

    public void setRcCode(Integer rcCode) {
        this.rcCode = rcCode;
    }

    public Boolean getFieldFlag() {
        return fieldFlag;
    }

    public void setFieldFlag(Boolean fieldFlag) {
        this.fieldFlag = fieldFlag;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getSurveyanswer() {
        return surveyanswer;
    }

    public void setSurveyanswer(String surveyanswer) {
        this.surveyanswer = surveyanswer;
    }

    public Boolean getOnetooneFlag() {
        return onetooneFlag;
    }

    public void setOnetooneFlag(Boolean onetooneFlag) {
        this.onetooneFlag = onetooneFlag;
    }

    public Integer getQuestioncount() {
        return questioncount;
    }

    public void setQuestioncount(Integer questioncount) {
        this.questioncount = questioncount;
    }

    public String getRemarkvalue() {
        return remarkvalue;
    }

    public void setRemarkvalue(String remarkvalue) {
        this.remarkvalue = remarkvalue;
    }

    public Boolean getWebfieldFlag() {
        return webfieldFlag;
    }

    public void setWebfieldFlag(Boolean webfieldFlag) {
        this.webfieldFlag = webfieldFlag;
    }

    public Integer getNextrccode() {
        return nextrccode;
    }

    public void setNextrccode(Integer nextrccode) {
        this.nextrccode = nextrccode;
    }

    public Integer getNextquestionId() {
        return nextquestionId;
    }

    public void setNextquestionId(Integer nextquestionId) {
        this.nextquestionId = nextquestionId;
    }

    public Integer getPreviousquestionId() {
        return previousquestionId;
    }

    public void setPreviousquestionId(Integer previousquestionId) {
        this.previousquestionId = previousquestionId;
    }

    public Integer getMemberid() {
        return memberid;
    }

    public void setMemberid(Integer memberid) {
        this.memberid = memberid;
    }

    public Integer getDdQuestioncount() {
        return ddQuestioncount;
    }

    public void setDdQuestioncount(Integer ddQuestioncount) {
        this.ddQuestioncount = ddQuestioncount;
    }

    public String getUploadfile() {
        return uploadfile;
    }

    public void setUploadfile(String uploadfile) {
        this.uploadfile = uploadfile;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

}
