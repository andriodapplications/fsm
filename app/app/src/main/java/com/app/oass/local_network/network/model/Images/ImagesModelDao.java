package com.app.fsm.local_network.network.model.Images;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.app.fsm.local_network.network.model.charts.ChartModel;
import com.app.fsm.local_network.network.model.member.MemberModel;
import com.app.fsm.local_network.repository.dao.BaseDao;

import java.util.List;

@Dao
public interface ImagesModelDao extends BaseDao {
    @Query("SELECT * FROM insertimages")
    List<InsertImages> getimagesData();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertImages(List<InsertImages> insertImages);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    long insertImage(InsertImages insertImages);

    @Query("DELETE FROM InsertImages")
    void deleteAllImages();

    @Query("SELECT COUNT(isPushed) from insertimages where isPushed=:isPushed")
    int provideImagesCountNotPushed(boolean isPushed);

    @Query("SELECT * FROM insertimages ORDER BY id DESC LIMIT 1")
    InsertImages getInsertImages();

    @Query("SELECT COUNT(*) from insertimages")
    int getImageCount();
}
