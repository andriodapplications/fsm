package com.app.fsm.ui.dash.main.asynctask;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.app.fsm.local_network.network.model.occupationmodel.OccupationModel;
import com.app.fsm.local_network.repository.OaasDatabase;
import com.app.fsm.local_network.repository.OassDatabaseBuilder;
import com.app.fsm.local_network.repository.dao.OccupationDao;

import java.lang.ref.WeakReference;
import java.util.List;

public final class OccupationAsynTask extends AsyncTask<Void, Void, Boolean> {

    //Prevent leak
    private WeakReference<Context> weakActivity;
    private List<OccupationModel> occupationModelList;

    public OccupationAsynTask(Context context, List<OccupationModel> occupationModelList) {
        weakActivity = new WeakReference<>(context);
        this.occupationModelList = occupationModelList;
    }

    @Override
    protected Boolean doInBackground(Void... params) {
        OaasDatabase oaasDatabase = OassDatabaseBuilder.provideOassDatabase(weakActivity.get());
        OccupationDao occupationDao= oaasDatabase.provideOccupationDao();
        occupationDao.insertAllOccupation(occupationModelList);
        return true;
    }

    @Override
    protected void onPostExecute(Boolean agentsCount) {
        Context context = weakActivity.get();
        if (context == null) {
            return;
        }
        if (agentsCount) {
            Toast.makeText(context, "Done", Toast.LENGTH_SHORT).show();
        }
    }
}

