package com.app.fsm.local_network.repository;

import android.arch.persistence.room.Room;
import android.content.Context;

import static com.app.fsm.local_network.repository.OaasDatabase.MIGRATION_1_2;

public class OassDatabaseBuilder {

    private static OaasDatabase oaasDatabase;

    public static OaasDatabase provideOassDatabase(Context context) {

        if ( oaasDatabase == null)
            return oaasDatabase = Room
                    .databaseBuilder(context, OaasDatabase.class, "oass")
                    .addMigrations(MIGRATION_1_2)
                    .fallbackToDestructiveMigration()
                    .build();
        else
            return oaasDatabase;
    }

}
