package com.app.fsm.ui.dash.todo;


import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatSpinner;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.fsm.BaseFragment;
import com.app.fsm.R;
import com.app.fsm.local_network.network.model.joinedtable.TaskListModel;
import com.app.fsm.local_network.network.model.member.MemberModel;
import com.app.fsm.local_network.network.model.todo.TodoLanguage;
import com.app.fsm.local_network.network.model.todo.TodoModel;
import com.app.fsm.local_network.network.model.todo.TodoStatusModel;
import com.app.fsm.local_network.network.model.todo.TodoType;
import com.app.fsm.local_network.network.model.usermodel.UserModel;
import com.app.fsm.ui.dash.main.asynctask.GetAllMemberAsynTask;
import com.app.fsm.ui.dash.one2one.one2oneasynctask.GetAllTodoTypeAsyncTask;
import com.app.fsm.ui.dash.one2one.one2oneasynctask.SetTodoAsyncTask;
import com.app.fsm.ui.dash.one2one.one2onequestion.adapter.TodoAdapter;
import com.app.fsm.ui.dash.todo.adapter.MemberAdapter;
import com.app.fsm.ui.dash.todo.adapter.TodoStatusAdapter;
import com.app.fsm.ui.dash.todo.asynctask.GetAssignedToAsyncTask;
import com.app.fsm.ui.dash.todo.asynctask.GetTodoLanguageAsyncTask;
import com.app.fsm.ui.dash.todo.asynctask.GetTodoModelAsyncTask;
import com.app.fsm.ui.dash.todo.asynctask.GetTodoStatusAsyncTask;
import com.app.fsm.ui.dash.todo.asynctask.UpdateTodoAsyncTask;
import com.app.fsm.ui.dash.workflow.adapter.UserModelAdapter;
import com.app.fsm.utils.AppConstant;
import com.app.fsm.utils.CommonUtils;

import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class TodoFragment extends BaseFragment implements OnTodoFetchListener, AdapterView.OnItemSelectedListener {

    @BindView(R.id.txt_title_child_fragment)
    TextView txtTitleChildFragment;
    @BindView(R.id.img_to_previous_fragment)
    ImageView imgToPreviousFragment;
    @BindView(R.id.ll_indicator_container)
    LinearLayout llIndicatorContainer;
    @BindView(R.id.img_to_next_fragment)
    ImageView imgToNextFragment;
    @BindView(R.id.txt_fragment_count)
    TextView txtFragmentCount;
    @BindView(R.id.txt_label_member)
    TextView txtLabelMember;
    @BindView(R.id.spinner_member)
    AppCompatSpinner spinnerMember;
    @BindView(R.id.txt_label_todo)
    TextView txtLabelTodo;
    @BindView(R.id.spinner_todo)
    AppCompatSpinner spinnerTodo;
    @BindView(R.id.txt_label_follow_up)
    TextView txtLabelFollowUp;
    @BindView(R.id.img_calender)
    ImageView imgCalender;
    @BindView(R.id.txt_follow_up)
    TextView txtFollowUpdate;
    @BindView(R.id.rl_dob)
    RelativeLayout rlDob;
    @BindView(R.id.txt_label_status)
    TextView txtLabelStatus;
    @BindView(R.id.spinner_status)
    AppCompatSpinner spinnerStatus;
    @BindView(R.id.txt_label_assigned_to)
    TextView txtLabelAssignedTo;
    @BindView(R.id.spinner_assign)
    AppCompatSpinner spinnerAssign;
    //    @BindView(R.id.txt_label_comment)
//    TextView txtLabelComment;
//    @BindView(R.id.edt_comment)
//    AppCompatEditText edtComment;
    @BindView(R.id.ll_workflow)
    LinearLayout llWorkflow;
    Unbinder unbinder;

    private TaskListModel taskListModel;
    boolean isEditable;
    private UserModel userModel;
    private TodoType todoType;
    private TodoStatusModel todoStatusModel;
    private MemberModel memberModel;
    private TodoModel todoModel;
    private Calendar calendar = Calendar.getInstance();

    public static TodoFragment newInstance(Boolean isEditable
            , TaskListModel taskListModel) {
        Bundle bundle = new Bundle();
        bundle.putBoolean(AppConstant.IS_EDITABLE, isEditable);
        bundle.putSerializable(AppConstant.TASK_LIST, taskListModel);
        TodoFragment todoFragment = new TodoFragment();
        todoFragment.setArguments(bundle);
        return todoFragment;
    }

    DatePickerDialog.OnDateSetListener onDateSetListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.MONTH, month);
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            txtFollowUpdate.setText(CommonUtils.updateDate(calendar.getTime()));
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_todo, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (getArguments() != null) {
            isEditable = getArguments().getBoolean(AppConstant.IS_EDITABLE);
            taskListModel = (TaskListModel) getArguments().getSerializable(AppConstant.TASK_LIST);
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
    }

    public void init() {
        if (isEditable) {
            setTitle("Edit Todo");
            new GetTodoModelAsyncTask(this, provideOaasDatabase(), taskListModel.getId()).execute();
        } else {
            new GetAllMemberAsynTask(this, provideOaasDatabase(), 0).execute();
        }
        imgToNextFragment.setImageResource(R.drawable.ic_done);
        imgToPreviousFragment.setVisibility(View.VISIBLE);
        imgToPreviousFragment.setImageResource(R.drawable.ic_cancel);
        txtTitleChildFragment.setText("Todo");
        txtFragmentCount.setText("1/1");
        new GetAllMemberAsynTask(this, provideOaasDatabase(), 0).execute();
        new GetTodoLanguageAsyncTask(this, provideOaasDatabase()).execute();

    }

    public void onMemberSet() {
        new GetTodoStatusAsyncTask(this, provideOaasDatabase()).execute();
        new GetAssignedToAsyncTask(this, provideOaasDatabase()).execute();
        new GetAllTodoTypeAsyncTask(this, provideOaasDatabase()).execute();
    }

    @OnClick(R.id.img_to_previous_fragment)
    public void onCLickPreviousFragment() {
        requireActivity().getSupportFragmentManager().popBackStack();
    }


    @OnClick(R.id.rl_dob)
    public void openDatePicker() {
        DatePickerDialog datePickerDialog = new DatePickerDialog(requireContext(), onDateSetListener
                , calendar.get(Calendar.YEAR)
                , calendar.get(Calendar.MONTH)
                , calendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
        datePickerDialog.show();
    }

    @OnClick(R.id.img_to_next_fragment)
    public void onClickNextFragment() {
        if (checkValidation()) {
            TodoModel todoModel = new TodoModel();

            if (isEditable) {
                todoModel.setMemberid(taskListModel.getMemberId());
                todoModel.setServerId(this.todoModel.getServerId());
                todoModel.setLocalId(this.todoModel.getLocalId());
            } else {
                todoModel.setMemberid(memberModel.getServerId());
            }
            todoModel.setAssignedto(userModel.getId());
            todoModel.setTodotypeid(todoType.getId());
            todoModel.setTodostatusid(todoStatusModel.getId());
            todoModel.setCreatedtime(CommonUtils.localDbDateFormat(Calendar.getInstance().getTime()));
            String followup = calendar.get(Calendar.YEAR) + "-" + (calendar.get(Calendar.MONTH) + 1) + "-" + calendar.get(Calendar.DAY_OF_MONTH) + "T00:00:00.000Z";
            todoModel.setFollowupdate(followup);
            todoModel.setPushed(false);

            if (isEditable)
                new UpdateTodoAsyncTask(provideOaasDatabase(), todoModel, this).execute();
            else
                new SetTodoAsyncTask(provideOaasDatabase(), todoModel, this).execute();
        }
    }

    @Override
    public void onTodoCreated() {
        Toast.makeText(requireActivity(), "Todo Created Successfully", Toast.LENGTH_SHORT).show();
        requireActivity().getSupportFragmentManager().popBackStack();
    }

    @Override
    public void onTodoUpdated() {
        requireActivity().getSupportFragmentManager().popBackStack();
    }


    @Override
    public void onTodoFetched(TodoModel todoModel) {
        this.todoModel = todoModel;
        new GetAllMemberAsynTask(this, provideOaasDatabase(), 0).execute();
    }

    private boolean checkValidation() {

        if (memberModel == null) {
            Toast.makeText(requireActivity(), "Please select Member", Toast.LENGTH_SHORT).show();
        } else if (todoType == null) {
            Toast.makeText(requireActivity(), "Please select Todo", Toast.LENGTH_SHORT).show();
        } else if (TextUtils.isEmpty(txtFollowUpdate.getText())) {
            Toast.makeText(requireActivity(), "Follow up", Toast.LENGTH_SHORT).show();
        } else if (userModel == null) {
            Toast.makeText(requireActivity(), "Please select WorkFlow Assigned To", Toast.LENGTH_SHORT).show();
        } else if (todoStatusModel == null) {
            Toast.makeText(requireActivity(), "Please select Todo Status", Toast.LENGTH_SHORT).show();
        } else {
            return true;
        }
        return false;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onMemberFetchCompleted(List<MemberModel> memberModelList, int pos) {

        MemberModel memberModel = new MemberModel();
        memberModel.setSerno(getString(R.string.select));
        memberModelList.add(memberModel);
        spinnerMember.setOnItemSelectedListener(this);
        MemberAdapter memberAdapter = new MemberAdapter(requireActivity(), memberModelList);
        spinnerMember.setAdapter(memberAdapter);
        if (isEditable)
            for (int i = 0; i < memberModelList.size(); i++) {
                if (memberModelList.get(i).getServerId() == todoModel.getMemberid()) {
                    this.memberModel = memberModelList.get(i);
                    spinnerMember.setSelection(i);
                    return;
                }
            }
        spinnerMember.setSelection(memberAdapter.getCount());
    }

    @Override
    public void assignToFetched(List<UserModel> userModelList) {
        UserModel userModel = new UserModel();
        userModel.setUsername(getString(R.string.select));
        userModelList.add(userModel);
        spinnerAssign.setOnItemSelectedListener(this);
        UserModelAdapter userModelAdapter = new UserModelAdapter(requireActivity(), userModelList);
        spinnerAssign.setAdapter(userModelAdapter);

        if (isEditable)
            for (int i = 0; i < userModelList.size(); i++) {
                if (userModelList.get(i).getId() != null && userModelList.get(i).getId().equals(todoModel.getAssignedto())) {
                    this.userModel = userModelList.get(i);
                    spinnerAssign.setSelection(i);
                    return;
                }
            }

        if (getUserSessionManager().getUser() != null) {
            for (int i = 0; i < userModelList.size(); i++) {
                if (userModelList.get(i).getId() != null && userModelList.get(i).getId().equals(getUserSessionManager().getUser().getId())) {
                    this.userModel = userModelList.get(i);
                    spinnerAssign.setSelection(i);
                    return;
                }
            }
        }
        spinnerAssign.setSelection(userModelAdapter.getCount());

    }

    @Override
    public void todoStatusFetched(List<TodoStatusModel> todoStatusModelList) {
        TodoStatusModel todoStatusModel = new TodoStatusModel();
        todoStatusModel.setName(getString(R.string.select));
        todoStatusModelList.add(todoStatusModel);
        spinnerStatus.setOnItemSelectedListener(this);
        TodoStatusAdapter todoStatusAdapter = new TodoStatusAdapter(requireActivity(), todoStatusModelList);
        spinnerStatus.setAdapter(todoStatusAdapter);

        if (isEditable)
            for (int i = 0; i < todoStatusModelList.size(); i++) {
                if (todoStatusModelList.get(i).getId() != null && todoStatusModelList.get(i).getId().equals(todoModel.getTodostatusid())) {
                    this.todoStatusModel = todoStatusModelList.get(i);
                    spinnerStatus.setSelection(i);
                    return;
                }
            }
        spinnerStatus.setSelection(todoStatusAdapter.getCount());
    }

    @Override
    public void onTodoTypeFetchComplete(List<TodoType> todoTypeList) {
        TodoType todoType = new TodoType();
        todoType.setName(getString(R.string.select));
        todoTypeList.add(todoType);
        spinnerTodo.setOnItemSelectedListener(this);
        TodoAdapter todoAdapter = new TodoAdapter(requireActivity(), todoTypeList);
        spinnerTodo.setAdapter(todoAdapter);
        if (isEditable)
            for (int i = 0; i < todoTypeList.size(); i++) {
                if (todoTypeList.get(i).getId() != null && todoTypeList.get(i).getId().equals(todoModel.getTodotypeid())) {
                    this.todoType = todoTypeList.get(i);
                    spinnerTodo.setSelection(i);
                    return;
                }
            }
        spinnerTodo.setSelection(todoAdapter.getCount());
    }

    @Override
    public void onTodoLanguageFetched(List<TodoLanguage> todoLanguageList) {
        if (!todoLanguageList.isEmpty())
            setLabelView(todoLanguageList.get(0));
    }

    public void setLabelView(TodoLanguage todoLanguage) {
        txtLabelMember.setText(todoLanguage.getMember());
        txtLabelFollowUp.setText(todoLanguage.getFollowUp());
        txtLabelAssignedTo.setText(todoLanguage.getAssignedTo());
        txtLabelStatus.setText(todoLanguage.getStatus());
        txtLabelTodo.setText(todoLanguage.getTodoLabel());
        setTitle("Todo");
        //txtLabelComment.setText(todoLanguage.getC());
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        if (parent.getAdapter() instanceof MemberAdapter) {
            MemberModel memberModel = ((MemberAdapter) parent.getAdapter()).getItem(position);
            if (memberModel != null && !memberModel.getSerno().contains(getString(R.string.select))) {
                this.memberModel = memberModel;
                onMemberSet();
            }
            return;
        }
        if (parent.getAdapter() instanceof TodoStatusAdapter) {
            TodoStatusModel todoStatusModel = ((TodoStatusAdapter) parent.getAdapter()).getItem(position);
            if (todoStatusModel != null && !todoStatusModel.getName().contains(getString(R.string.select))) {
                this.todoStatusModel = todoStatusModel;
            }
            return;
        }
        if (parent.getAdapter() instanceof TodoAdapter) {
            rlDob.setClickable(true);
            TodoAdapter todoAdapter = (TodoAdapter) parent.getAdapter();
            TodoType todoType = (TodoType) todoAdapter.getItem(position);
            if (todoType != null && !todoType.getName().contains(getString(R.string.select))) {
                this.calendar = Calendar.getInstance();
                if (todoType.getDue() != null)
                    this.calendar.add(Calendar.DATE, Integer.parseInt(todoType.getDue()));
                txtFollowUpdate.setText(CommonUtils.updateDate(calendar.getTime()));
                this.todoType = todoType;
            }
            return;
        }
        if (parent.getAdapter() instanceof UserModelAdapter) {
            UserModel userModel = ((UserModelAdapter) parent.getAdapter()).getItem(position);
            if (userModel != null && !userModel.getUsername().contains(getString(R.string.select))) {
                this.userModel = userModel;
            }
            return;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
