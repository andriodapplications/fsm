package com.app.fsm.local_network.network.model.organizationlocation;


import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@Entity
public class OrganizationLocationModel implements Serializable {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("level")
    @Expose
    private Integer level;
    @SerializedName("parent")
    @Expose
    private Integer parent;
    @SerializedName("parentlevel")
    @Expose
    private Integer parentlevel;
    @SerializedName("language")
    @Expose
    private String language;
    @SerializedName("languageparent")
    @Expose
    private Boolean languageparent;
    @SerializedName("languageparentid")
    @Expose
    private String languageparentid;
    @SerializedName("deleteflag")
    @Expose
    private Boolean deleteflag;

    @PrimaryKey
    @NonNull
    @SerializedName("id")
    @Expose
    private Integer id;
    private final static long serialVersionUID = -5254227675060116197L;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public Integer getParent() {
        return parent;
    }

    public void setParent(Integer parent) {
        this.parent = parent;
    }

    public Integer getParentlevel() {
        return parentlevel;
    }

    public void setParentlevel(Integer parentlevel) {
        this.parentlevel = parentlevel;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public Boolean getLanguageparent() {
        return languageparent;
    }

    public void setLanguageparent(Boolean languageparent) {
        this.languageparent = languageparent;
    }

    public String getLanguageparentid() {
        return languageparentid;
    }

    public void setLanguageparentid(String languageparentid) {
        this.languageparentid = languageparentid;
    }

    public Boolean getDeleteflag() {
        return deleteflag;
    }

    public void setDeleteflag(Boolean deleteflag) {
        this.deleteflag = deleteflag;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

}
