package com.app.fsm.local_network.repository.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.app.fsm.local_network.network.model.workflow.WorkFlowCategory;

import java.util.List;

@Dao
public interface WorkFlowCategoryDao {

    @Query("SELECT * FROM workflowcategory WHERE workflowid=:workflowId")
    List<WorkFlowCategory> provideWorkFlowCategory(int workflowId);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertAllCategory(List<WorkFlowCategory> workFlowCategoryList);

}
