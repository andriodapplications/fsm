package com.app.fsm.local_network.network.model.joinedtable;

import android.arch.persistence.room.Ignore;

import java.io.Serializable;

public class TaskListModel implements Serializable {

    private int id;
    private String taskName;
    private String followUpdate;
    private String memberName;
    private int statusId;
    private int memberId;

    @Ignore
    private Boolean isTodoType;//true is for todotype;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getFollowUpdate() {
        return followUpdate;
    }

    public void setFollowUpdate(String followUpdate) {
        this.followUpdate = followUpdate;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public int getMemberId() {
        return memberId;
    }

    public void setMemberId(int memberId) {
        this.memberId = memberId;
    }

    public Boolean getTodoType() {
        return isTodoType;
    }

    public void setTodoType(Boolean todoType) {
        isTodoType = todoType;
    }

    public int getStatusId() {
        return statusId;
    }

    public void setStatusId(int statusId) {
        this.statusId = statusId;
    }
}
