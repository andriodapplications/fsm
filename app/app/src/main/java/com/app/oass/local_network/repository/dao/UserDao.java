package com.app.fsm.local_network.repository.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.app.fsm.local_network.network.model.usermodel.UserModel;

import java.util.List;

@Dao
public interface UserDao {

    @Query("SELECT * FROM usermodel")
    List<UserModel> getAll();

    /*@Query("SELECT * FROM user WHERE uid IN (:userIds)")
    List<User> loadAllByIds(int[] userIds);

    @Query("SELECT * FROM user WHERE first_name LIKE :first AND " +
            "last_name LIKE :last LIMIT 1")
    User findByName(String first, String last);
*/
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertAll(List<UserModel> userModelList);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertUser(UserModel userModel);

    @Delete
    void delete(UserModel userModel);

}
