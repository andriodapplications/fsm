package com.app.fsm.ui.dash.memberregistration;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.fsm.BaseFragment;
import com.app.fsm.R;
import com.app.fsm.SliderBaseFragment;
import com.app.fsm.local_network.network.model.member.MemberModel;
import com.app.fsm.ui.dash.memberregistration.memberprofile.MemberRegistrationProfile;
import com.app.fsm.utils.AppConstant;
import com.avontell.pagerindicatorbinder.IndicatorBinder;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class MemberRegistration extends BaseFragment implements ViewPager.OnPageChangeListener {

    @BindView(R.id.txt_title_child_fragment)
    TextView txtTitleChildFragment;
    @BindView(R.id.txt_fragment_count)
    TextView txtFragmentCount;
    @BindView(R.id.rl_tab)
    LinearLayout rlTab;
    @BindView(R.id.vp_member_registration)
    ViewPager vpMemberRegistration;
    @BindView(R.id.ll_indicator_container)
    LinearLayout llIndicatorContainer;
    @BindView(R.id.img_to_previous_fragment)
    ImageView imgPreviousFragment;
    @BindView(R.id.img_to_next_fragment)
    ImageView imgNextFragment;

    private Unbinder unbinder;
    private MemberRegistrationAdapter memberRegistrationAdapter;
    private int childFragmentPos;
    private Boolean isMemberEditable = false;
    private MemberModel memberModel;

    public static MemberRegistration newInstance(Boolean isMemberEditable, MemberModel memberModel) {
        Bundle bundle = new Bundle();
        bundle.putBoolean(AppConstant.IS_EDITABLE, isMemberEditable);
        bundle.putSerializable(AppConstant.MEMBER_MODEL, memberModel);
        MemberRegistration memberRegistration = new MemberRegistration();
        memberRegistration.setArguments(bundle);
        return memberRegistration;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (getArguments() != null) {
            isMemberEditable = getArguments().getBoolean(AppConstant.IS_EDITABLE);
            memberModel = (MemberModel) getArguments().getSerializable(AppConstant.MEMBER_MODEL);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_member_registration, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
    }

    public void init() {

        List<Fragment> fragmentList = new ArrayList<>();
        if (isMemberEditable) {
            setTitle("Update Member");
            fragmentList.add(MemberRegistrationProfile.newInstance(isMemberEditable, memberModel));
        } else {
            setTitle("Member Registration");
            fragmentList.add(MemberRegistrationProfile.newInstance(false, null));
        }
        //fragmentList.add(new MemberRegistration2());
        //fragmentList.add(new MemberRegistrationFamilyProfile());
        memberRegistrationAdapter = new MemberRegistrationAdapter(requireActivity().getSupportFragmentManager(), fragmentList);
        vpMemberRegistration.setAdapter(memberRegistrationAdapter);
        vpMemberRegistration.addOnPageChangeListener(this);


        if (fragmentList.size() > 1) {
            imgNextFragment.setTag(AppConstant.NEXT);
        } else {
            imgNextFragment.setTag(AppConstant.DONE);
            imgNextFragment.setImageResource(R.drawable.ic_done);
            imgPreviousFragment.setTag(AppConstant.BACK);
            imgPreviousFragment.setImageResource(R.drawable.ic_cancel);
            imgPreviousFragment.setVisibility(View.VISIBLE);
        }

        final IndicatorBinder sample = new IndicatorBinder().bind(requireActivity(),
                vpMemberRegistration, llIndicatorContainer,
                R.drawable.indicator_selected,
                R.drawable.indicator_unselected);
        // Set whether you want a progress style
        sample.setProgressStyle(true);
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onPageScrolled(int i, float v, int i1) {

    }

    @Override
    public void onPageSelected(int i) {
        this.childFragmentPos = i;
        if (i == 0) {
            imgPreviousFragment.setVisibility(View.INVISIBLE);
        } else {
            imgPreviousFragment.setVisibility(View.VISIBLE);
        }

        if (i == vpMemberRegistration.getAdapter().getCount() - 1) {
            imgNextFragment.setImageResource(R.drawable.ic_done);
            imgNextFragment.setTag(AppConstant.DONE);
        } else {
            imgNextFragment.setImageResource(R.drawable.ic_next);
            imgNextFragment.setTag(AppConstant.NEXT);
        }
    }

    @Override
    public void onPageScrollStateChanged(int i) {

    }

    @OnClick(R.id.img_to_previous_fragment)
    public void imgLeftClicked() {
        if (imgPreviousFragment.getVisibility() == View.VISIBLE && ((String) imgPreviousFragment.getTag()).equalsIgnoreCase(AppConstant.BACK)) {
            requireActivity().getSupportFragmentManager().popBackStack();
        } else if (imgPreviousFragment.getVisibility() == View.VISIBLE) {
            vpMemberRegistration.setCurrentItem(--childFragmentPos);
        }
    }

    @OnClick(R.id.img_to_next_fragment)
    public void imgRightClicked() {
        SliderBaseFragment sliderBaseFragment = (SliderBaseFragment) memberRegistrationAdapter.getFragment(childFragmentPos);
        try {
            if (((String) imgNextFragment.getTag()).equalsIgnoreCase(AppConstant.DONE)) {
                //Call method of respective fragment in the viewpager on click.
                sliderBaseFragment.onNext();
            } else {
                if(sliderBaseFragment.onNext()){
                    if (childFragmentPos != vpMemberRegistration.getAdapter().getCount() - 1) {
                        vpMemberRegistration.setCurrentItem(++childFragmentPos);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
