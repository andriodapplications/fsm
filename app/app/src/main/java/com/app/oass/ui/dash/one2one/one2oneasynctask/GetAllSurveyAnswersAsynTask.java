package com.app.fsm.ui.dash.one2one.one2oneasynctask;

import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.app.fsm.local_network.network.model.survey.SurveyAnswerModel;
import com.app.fsm.local_network.network.model.survey.SurveyQuestionModel;
import com.app.fsm.local_network.repository.OaasDatabase;
import com.app.fsm.local_network.repository.dao.SurveyAnswerDao;
import com.app.fsm.local_network.repository.dao.SurveyQuestionDao;
import com.app.fsm.ui.dash.one2one.OnFetchAnswersComplete;
import com.app.fsm.ui.dash.one2one.OnFetchComplete;

import java.util.ArrayList;
import java.util.List;

public final class GetAllSurveyAnswersAsynTask extends AsyncTask<Void, Void, List<SurveyAnswerModel>> {

    private OaasDatabase oaasDatabase;
    private OnFetchAnswersComplete onFetchComplete;
    private Integer MemberId;

    public GetAllSurveyAnswersAsynTask(@NonNull OnFetchAnswersComplete onFetchComplete,
                                       @NonNull OaasDatabase oaasDatabase,
                                       Integer memberId) {
        this.oaasDatabase = oaasDatabase;
        this.onFetchComplete = onFetchComplete;
        MemberId=memberId;
    }

    @Override
    protected List<SurveyAnswerModel> doInBackground(Void... params) {
        SurveyAnswerDao surveyAnswerDao= oaasDatabase.provideSurveyAnswerDao();
        return surveyAnswerDao.provideSurveyAnswer();
    }

    @Override
    protected void onPostExecute(List<SurveyAnswerModel> surveyAnswerModels) {

        List<SurveyAnswerModel> surveyAnswerModels1 = new ArrayList<>();
        for (SurveyAnswerModel surveyAnswerModel : surveyAnswerModels) {
            if (surveyAnswerModel.getMemberid() != null
                    && surveyAnswerModel.getMemberid().equals(MemberId))
                surveyAnswerModels1.add(surveyAnswerModel);
        }

        if(onFetchComplete !=null)
            onFetchComplete.onCompleteSurvey(surveyAnswerModels1);
    }
}

