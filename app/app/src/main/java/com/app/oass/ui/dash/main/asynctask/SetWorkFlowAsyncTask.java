package com.app.fsm.ui.dash.main.asynctask;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.app.fsm.local_network.network.model.workflow.WorkFlowModel;
import com.app.fsm.local_network.repository.OaasDatabase;
import com.app.fsm.local_network.repository.OassDatabaseBuilder;
import com.app.fsm.local_network.repository.dao.WorkFlowDao;

import java.lang.ref.WeakReference;
import java.util.List;

public final class SetWorkFlowAsyncTask extends AsyncTask<Void, Void, Boolean> {

    //Prevent leak
    private WeakReference<Context> weakActivity;
    private List<WorkFlowModel> workFlowModelList;

    public SetWorkFlowAsyncTask(Context context, List<WorkFlowModel> workFlowModelList) {
        weakActivity = new WeakReference<>(context);
        this.workFlowModelList = workFlowModelList;
    }

    @Override
    protected Boolean doInBackground(Void... params) {
        OaasDatabase oaasDatabase = OassDatabaseBuilder.provideOassDatabase(weakActivity.get());
        WorkFlowDao workFlowDao = oaasDatabase.provideWorkFlowDao();
        workFlowDao.insertAllWorkFlow(workFlowModelList);
        return true;
    }

    @Override
    protected void onPostExecute(Boolean isInserted) {
        if (isInserted) {
            Toast.makeText(weakActivity.get(), "Done", Toast.LENGTH_SHORT).show();
        }
    }
}

