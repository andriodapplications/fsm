package com.app.fsm.local_network.network.model.member;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@Entity
public class MemberLanguageModel implements Serializable {

    @SerializedName("language")
    @Expose
    private Integer language;
    @SerializedName("memberLabel")
    @Expose
    private String memberLabel;
    @SerializedName("firstname")
    @Expose
    private String firstname;
    @SerializedName("lastname")
    @Expose
    private String lastname;
    @SerializedName("fullname")
    @Expose
    private String fullname;
    @SerializedName("memberId")
    @Expose
    private String memberId;
    @SerializedName("dob")
    @Expose
    private String dob;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("occupation")
    @Expose
    private String occupation;
    @SerializedName("education")
    @Expose
    private String education;
    @SerializedName("enableWrite")
    @Expose
    private Boolean enableWrite;
    @SerializedName("enableforroles")
    @Expose
    private String enableforroles;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("age")
    @Expose
    private String age;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;
    @SerializedName("stress")
    @Expose
    private String stress;
    @SerializedName("saveLabel")
    @Expose
    private String saveLabel;
    @SerializedName("updateLabel")
    @Expose
    private String updateLabel;
    @SerializedName("cancelLabel")
    @Expose
    private String cancelLabel;
    @SerializedName("showLabel")
    @Expose
    private String showLabel;
    @SerializedName("hideLabel")
    @Expose
    private String hideLabel;
    @SerializedName("searchLabel")
    @Expose
    private String searchLabel;
    @SerializedName("addLabel")
    @Expose
    private String addLabel;
    @SerializedName("organizationId")
    @Expose
    private String organizationId;
    @SerializedName("lastmodifiedby")
    @Expose
    private Integer lastmodifiedby;
    @SerializedName("lastmodifiedtime")
    @Expose
    private String lastmodifiedtime;
    @SerializedName("lastmodifiedrole")
    @Expose
    private Integer lastmodifiedrole;
    @SerializedName("createdby")
    @Expose
    private Integer createdby;
    @SerializedName("createdtime")
    @Expose
    private String createdtime;
    @SerializedName("createdrole")
    @Expose
    private Integer createdrole;
    @SerializedName("deleteflag")
    @Expose
    private Boolean deleteflag;
    @SerializedName("parentLanguage")
    @Expose
    private Boolean parentLanguage;

    @PrimaryKey(autoGenerate = true)
    @NonNull
    @SerializedName("id")
    @Expose
    private Integer id;
    private final static long serialVersionUID = -7767548991196842224L;

    public Integer getLanguage() {
        return language;
    }

    public void setLanguage(Integer language) {
        this.language = language;
    }

    public String getMemberLabel() {
        return memberLabel;
    }

    public void setMemberLabel(String memberLabel) {
        this.memberLabel = memberLabel;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public Boolean getEnableWrite() {
        return enableWrite;
    }

    public void setEnableWrite(Boolean enableWrite) {
        this.enableWrite = enableWrite;
    }

    public String getEnableforroles() {
        return enableforroles;
    }

    public void setEnableforroles(String enableforroles) {
        this.enableforroles = enableforroles;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getStress() {
        return stress;
    }

    public void setStress(String stress) {
        this.stress = stress;
    }

    public String getSaveLabel() {
        return saveLabel;
    }

    public void setSaveLabel(String saveLabel) {
        this.saveLabel = saveLabel;
    }

    public String getUpdateLabel() {
        return updateLabel;
    }

    public void setUpdateLabel(String updateLabel) {
        this.updateLabel = updateLabel;
    }

    public String getCancelLabel() {
        return cancelLabel;
    }

    public void setCancelLabel(String cancelLabel) {
        this.cancelLabel = cancelLabel;
    }

    public String getShowLabel() {
        return showLabel;
    }

    public void setShowLabel(String showLabel) {
        this.showLabel = showLabel;
    }

    public String getHideLabel() {
        return hideLabel;
    }

    public void setHideLabel(String hideLabel) {
        this.hideLabel = hideLabel;
    }

    public String getSearchLabel() {
        return searchLabel;
    }

    public void setSearchLabel(String searchLabel) {
        this.searchLabel = searchLabel;
    }

    public String getAddLabel() {
        return addLabel;
    }

    public void setAddLabel(String addLabel) {
        this.addLabel = addLabel;
    }

    public String getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(String organizationId) {
        this.organizationId = organizationId;
    }

    public Integer getLastmodifiedby() {
        return lastmodifiedby;
    }

    public void setLastmodifiedby(Integer lastmodifiedby) {
        this.lastmodifiedby = lastmodifiedby;
    }

    public String getLastmodifiedtime() {
        return lastmodifiedtime;
    }

    public void setLastmodifiedtime(String lastmodifiedtime) {
        this.lastmodifiedtime = lastmodifiedtime;
    }

    public Integer getLastmodifiedrole() {
        return lastmodifiedrole;
    }

    public void setLastmodifiedrole(Integer lastmodifiedrole) {
        this.lastmodifiedrole = lastmodifiedrole;
    }

    public Integer getCreatedby() {
        return createdby;
    }

    public void setCreatedby(Integer createdby) {
        this.createdby = createdby;
    }

    public String getCreatedtime() {
        return createdtime;
    }

    public void setCreatedtime(String createdtime) {
        this.createdtime = createdtime;
    }

    public Integer getCreatedrole() {
        return createdrole;
    }

    public void setCreatedrole(Integer createdrole) {
        this.createdrole = createdrole;
    }

    public Boolean getDeleteflag() {
        return deleteflag;
    }

    public void setDeleteflag(Boolean deleteflag) {
        this.deleteflag = deleteflag;
    }

    public Boolean getParentLanguage() {
        return parentLanguage;
    }

    public void setParentLanguage(Boolean parentLanguage) {
        this.parentLanguage = parentLanguage;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

}
