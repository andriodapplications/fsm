package com.app.fsm.ui.dash.workflow.asynctask;

import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.app.fsm.local_network.network.model.workflow.WorkFlowStatus;
import com.app.fsm.local_network.repository.OaasDatabase;
import com.app.fsm.local_network.repository.dao.WorkFlowStatusDao;
import com.app.fsm.ui.dash.workflow.OnWorkFlowFetchListener;

import java.util.List;

public final class GetWorkFlowStatusAsyncTask extends AsyncTask<Void, Void, List<WorkFlowStatus>> {

    private OaasDatabase oaasDatabase;
    private OnWorkFlowFetchListener onWorkFlowFetchListener;
    private int workFlowId;

    public GetWorkFlowStatusAsyncTask(@NonNull OnWorkFlowFetchListener onWorkFlowFetchListener, @NonNull OaasDatabase oaasDatabase
            , int workFlowId) {
        this.oaasDatabase = oaasDatabase;
        this.onWorkFlowFetchListener = onWorkFlowFetchListener;
        this.workFlowId = workFlowId;
    }

    @Override
    protected List<WorkFlowStatus> doInBackground(Void... params) {
        WorkFlowStatusDao workFlowStatusDao = oaasDatabase.provideWorkFlowStatusDao();
        return workFlowStatusDao.provideWorkFlowStatus(workFlowId);
    }

    @Override
    protected void onPostExecute(List<WorkFlowStatus> workFlowStatusList) {
        if (onWorkFlowFetchListener != null) {
            onWorkFlowFetchListener.onWorkFlowStatusFetched(workFlowStatusList);
        }
    }
}

