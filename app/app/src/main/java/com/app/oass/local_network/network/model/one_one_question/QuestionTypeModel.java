package com.app.fsm.local_network.network.model.one_one_question;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@Entity
public class QuestionTypeModel implements Serializable {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("organizationId")
    @Expose
    private String organizationId;
    @SerializedName("lastmodifiedby")
    @Expose
    private Integer lastmodifiedby;
    @SerializedName("lastmodifiedtime")
    @Expose
    private String lastmodifiedtime;
    @SerializedName("lastmodifiedrole")
    @Expose
    private Integer lastmodifiedrole;
    @SerializedName("createdby")
    @Expose
    private Integer createdby;
    @SerializedName("createdtime")
    @Expose
    private String createdtime;
    @SerializedName("createdrole")
    @Expose
    private Integer createdrole;
    @SerializedName("deleteflag")
    @Expose
    private Boolean deleteflag;
    @PrimaryKey
    @NonNull
    @SerializedName("id")
    @Expose
    private Integer id;
    private final static long serialVersionUID = 8236448130947720287L;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(String organizationId) {
        this.organizationId = organizationId;
    }

    public Integer getLastmodifiedby() {
        return lastmodifiedby;
    }

    public void setLastmodifiedby(Integer lastmodifiedby) {
        this.lastmodifiedby = lastmodifiedby;
    }

    public String getLastmodifiedtime() {
        return lastmodifiedtime;
    }

    public void setLastmodifiedtime(String lastmodifiedtime) {
        this.lastmodifiedtime = lastmodifiedtime;
    }

    public Integer getLastmodifiedrole() {
        return lastmodifiedrole;
    }

    public void setLastmodifiedrole(Integer lastmodifiedrole) {
        this.lastmodifiedrole = lastmodifiedrole;
    }

    public Integer getCreatedby() {
        return createdby;
    }

    public void setCreatedby(Integer createdby) {
        this.createdby = createdby;
    }

    public String getCreatedtime() {
        return createdtime;
    }

    public void setCreatedtime(String createdtime) {
        this.createdtime = createdtime;
    }

    public Integer getCreatedrole() {
        return createdrole;
    }

    public void setCreatedrole(Integer createdrole) {
        this.createdrole = createdrole;
    }

    public Boolean getDeleteflag() {
        return deleteflag;
    }

    public void setDeleteflag(Boolean deleteflag) {
        this.deleteflag = deleteflag;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

}