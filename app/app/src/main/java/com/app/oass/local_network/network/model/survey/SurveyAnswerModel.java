package com.app.fsm.local_network.network.model.survey;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@Entity
public class SurveyAnswerModel implements Serializable {

    @SerializedName("memberid")
    @Expose
    private Integer memberid;
    @SerializedName("questionid")
    @Expose
    private Integer questionid;
    @SerializedName("answer")
    @Expose
    private String answer;
    @SerializedName("lastupdatedtime")
    @Expose
    private String lastupdatedtime;
    @SerializedName("lastupdatedby")
    @Expose
    private String lastupdatedby;
    @SerializedName("deleteflag")
    @Expose
    private Boolean deleteflag;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("district")
    @Expose
    private String district;
    @SerializedName("site")
    @Expose
    private String site;
    @SerializedName("facility")
    @Expose
    private String facility;
    @SerializedName("documentflag")
    @Expose
    private Boolean documentflag;
    @SerializedName("documentid")
    @Expose
    private String documentid;
    @SerializedName("availeddate")
    @Expose
    private String availeddate;
    @SerializedName("referenceno")
    @Expose
    private String referenceno;
    @SerializedName("schemeid")
    @Expose
    private String schemeid;
    @SerializedName("syncouttime")
    @Expose
    private String syncouttime;
    @SerializedName("stress_data")
    @Expose
    private Boolean stressData;
    @SerializedName("organizationId")
    @Expose
    private String organizationId;
    @SerializedName("lastmodifiedby")
    @Expose
    private String lastmodifiedby;
    @SerializedName("lastmodifiedtime")
    @Expose
    private String lastmodifiedtime;
    @SerializedName("lastmodifiedrole")
    @Expose
    private Integer lastmodifiedrole;
    @SerializedName("createdby")
    @Expose
    private Integer createdby;
    @SerializedName("createdtime")
    @Expose
    private String createdtime;
    @SerializedName("createdrole")
    @Expose
    private Integer createdrole;

    @SerializedName("isPushed")
    private Boolean isPushed;

    @SerializedName("remark")
    @Expose
    private String remark;
    @SerializedName("uploadedfiles")
    @Expose
    private String uploadedfiles;

    @PrimaryKey(autoGenerate = true)
    @NonNull
    @SerializedName("id")
    private Integer id;
    private final static long serialVersionUID = -8233484162842308297L;

    public Boolean getPushed() {
        return isPushed;
    }

    public void setPushed(Boolean pushed) {
        isPushed = pushed;
    }



    public Integer getQuestionid() {
        return questionid;
    }

    public void setQuestionid(Integer questionid) {
        this.questionid = questionid;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getLastupdatedtime() {
        return lastupdatedtime;
    }

    public void setLastupdatedtime(String lastupdatedtime) {
        this.lastupdatedtime = lastupdatedtime;
    }

    public String getLastupdatedby() {
        return lastupdatedby;
    }

    public void setLastupdatedby(String lastupdatedby) {
        this.lastupdatedby = lastupdatedby;
    }

    public Boolean getDeleteflag() {
        return deleteflag;
    }

    public void setDeleteflag(Boolean deleteflag) {
        this.deleteflag = deleteflag;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public String getFacility() {
        return facility;
    }

    public void setFacility(String facility) {
        this.facility = facility;
    }

    public Boolean getDocumentflag() {
        return documentflag;
    }

    public void setDocumentflag(Boolean documentflag) {
        this.documentflag = documentflag;
    }

    public String getDocumentid() {
        return documentid;
    }

    public void setDocumentid(String documentid) {
        this.documentid = documentid;
    }

    public String getAvaileddate() {
        return availeddate;
    }

    public void setAvaileddate(String availeddate) {
        this.availeddate = availeddate;
    }

    public String getReferenceno() {
        return referenceno;
    }

    public void setReferenceno(String referenceno) {
        this.referenceno = referenceno;
    }

    public String getSchemeid() {
        return schemeid;
    }

    public void setSchemeid(String schemeid) {
        this.schemeid = schemeid;
    }

    public String getSyncouttime() {
        return syncouttime;
    }

    public void setSyncouttime(String syncouttime) {
        this.syncouttime = syncouttime;
    }

    public Boolean getStressData() {
        return stressData;
    }

    public void setStressData(Boolean stressData) {
        this.stressData = stressData;
    }

    public String getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(String organizationId) {
        this.organizationId = organizationId;
    }

    public String getLastmodifiedby() {
        return lastmodifiedby;
    }

    public void setLastmodifiedby(String lastmodifiedby) {
        this.lastmodifiedby = lastmodifiedby;
    }

    public String getLastmodifiedtime() {
        return lastmodifiedtime;
    }

    public void setLastmodifiedtime(String lastmodifiedtime) {
        this.lastmodifiedtime = lastmodifiedtime;
    }

    public Integer getLastmodifiedrole() {
        return lastmodifiedrole;
    }

    public void setLastmodifiedrole(Integer lastmodifiedrole) {
        this.lastmodifiedrole = lastmodifiedrole;
    }

    public Integer getCreatedby() {
        return createdby;
    }

    public void setCreatedby(Integer createdby) {
        this.createdby = createdby;
    }

    public String getCreatedtime() {
        return createdtime;
    }

    public void setCreatedtime(String createdtime) {
        this.createdtime = createdtime;
    }

    public Integer getCreatedrole() {
        return createdrole;
    }

    public void setCreatedrole(Integer createdrole) {
        this.createdrole = createdrole;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getUploadedfiles() {
        return uploadedfiles;
    }

    public void setUploadedfiles(String uploadedfiles) {
        this.uploadedfiles = uploadedfiles;
    }

    @NonNull
    public Integer getId() {
        return id;
    }

    public void setId(@NonNull Integer id) {
        this.id = id;
    }

    public Integer getMemberid() {
        return memberid;
    }

    public void setMemberid(Integer memberid) {
        this.memberid = memberid;
    }
}