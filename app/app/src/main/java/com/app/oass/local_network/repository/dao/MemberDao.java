package com.app.fsm.local_network.repository.dao;

import android.arch.persistence.db.SupportSQLiteQuery;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.RawQuery;
import android.arch.persistence.room.SkipQueryVerification;
import android.arch.persistence.room.Update;

import com.app.fsm.local_network.network.model.member.MemberModel;

import java.util.List;

@Dao
public interface MemberDao extends BaseDao{

    @Query("SELECT * FROM memberModel")
    List<MemberModel> getAll();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAllMember(List<MemberModel> memberModelList);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    long insertMember(MemberModel memberModel);

    @Update
    int updateMember(MemberModel memberModel);

    @Query("SELECT COUNT(isPushed) from membermodel where isPushed=:isPushed")
    int provideMemberCountNotPushed(boolean isPushed);

    @Query("DELETE FROM membermodel")
    void deleteAllMember();

    @RawQuery
    @SkipQueryVerification
    int deleteSequence(SupportSQLiteQuery supportSQLiteQuery);

    @Query("SELECT COUNT(*) from membermodel")
    int getCount();

    @Query("SELECT * FROM membermodel ORDER BY serverId DESC LIMIT 1")
    MemberModel getMember();



}
