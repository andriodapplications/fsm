package com.app.fsm.ui.dash.memberregistration.memberprofile.memberasynctask;

import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.app.fsm.local_network.network.model.occupationmodel.OccupationModel;
import com.app.fsm.local_network.repository.OaasDatabase;
import com.app.fsm.local_network.repository.dao.OccupationDao;
import com.app.fsm.ui.dash.memberregistration.memberprofile.OnFetchCompleteListener;

import java.util.List;

public final class GetAllOccupationAsynTask extends AsyncTask<Void, Void, List<OccupationModel>> {

    private OaasDatabase oaasDatabase;
    private OnFetchCompleteListener onFetchCompleteListener;

    public GetAllOccupationAsynTask(@NonNull OnFetchCompleteListener onFetchCompleteListener, @NonNull OaasDatabase oaasDatabase) {
        this.oaasDatabase = oaasDatabase;
        this.onFetchCompleteListener=onFetchCompleteListener;
    }

    @Override
    protected List<OccupationModel> doInBackground(Void... params) {
        OccupationDao occupationDao= oaasDatabase.provideOccupationDao();
        return occupationDao.getAll();
    }

    @Override
    protected void onPostExecute(List<OccupationModel> occupationModelList) {
        if (onFetchCompleteListener != null)
            onFetchCompleteListener.onOccupationFetchComplete(occupationModelList);
    }
}

