package com.app.fsm.local_network.network.model.member;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@Entity(indices = {@Index(value = {"serverId"},
        unique = true)})
//memebertable --> rcdetails
public class MemberModel implements Serializable {
    @SerializedName("serno")
    @Expose
    private String serno;
    @SerializedName("rcstatus")
    @Expose
    private String rcstatus;
    @SerializedName("rcCode")
    @Expose
    private Integer rcCode;
    @SerializedName("nextquestionId")
    @Expose
    private Integer nextquestionId;
    @SerializedName("prevquestionId")
    @Expose
    private Integer prevquestionId;
    @SerializedName("customername")
    @Expose
    private String customername;
    @SerializedName("customermiddlename")
    @Expose
    private String customermiddlename;
    @SerializedName("customerlastname")
    @Expose
    private String customerlastname;
    @SerializedName("customeraddress")
    @Expose
    private String customeraddress;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;
    @SerializedName("cityId")
    @Expose
    private Integer cityId;
    @SerializedName("pincodeId")
    @Expose
    private Integer pincodeId;
    @SerializedName("contactno")
    @Expose
    private String contactno;
    @SerializedName("contactpersonname")
    @Expose
    private String contactpersonname;
    @SerializedName("casedescription")
    @Expose
    private String casedescription;
    @SerializedName("customerrefnumber")
    @Expose
    private String customerrefnumber;
    @SerializedName("casenumber")
    @Expose
    private String casenumber;
    @SerializedName("dlpname")
    @Expose
    private String dlpname;
    @SerializedName("serreceivedate")
    @Expose
    private String serreceivedate;
    @SerializedName("serreceivetime")
    @Expose
    private String serreceivetime;
    @SerializedName("modelno")
    @Expose
    private String modelno;
    @SerializedName("countryzone")
    @Expose
    private String countryzone;
    @SerializedName("faxnocontactperson")
    @Expose
    private String faxnocontactperson;
    @SerializedName("fsd")
    @Expose
    private String fsd;
    @SerializedName("deferflag")
    @Expose
    private Boolean deferflag;
    @SerializedName("prosupport")
    @Expose
    private String prosupport;
    @SerializedName("keepyourharddrive")
    @Expose
    private String keepyourharddrive;
    @SerializedName("accidentaldamage")
    @Expose
    private String accidentaldamage;
    @SerializedName("attachfile")
    @Expose
    private String attachfile;
    @SerializedName("filedescription")
    @Expose
    private String filedescription;
    @SerializedName("modeldescription")
    @Expose
    private String modeldescription;
    @SerializedName("serialnoservicetag")
    @Expose
    private String serialnoservicetag;
    @SerializedName("sla")
    @Expose
    private String sla;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("dellmarks")
    @Expose
    private String dellmarks;
    @SerializedName("summary")
    @Expose
    private String summary;
    @SerializedName("worklocation")
    @Expose
    private String worklocation;
    @SerializedName("etatime")
    @Expose
    private String etatime;
    @SerializedName("engineer")
    @Expose
    private String engineer;
    @SerializedName("appointmentdatetime")
    @Expose
    private String appointmentdatetime;
    @SerializedName("callduration")
    @Expose
    private String callduration;
    @SerializedName("lob")
    @Expose
    private String lob;
    @SerializedName("hdcontactname")
    @Expose
    private String hdcontactname;
    @SerializedName("hdcontactno")
    @Expose
    private String hdcontactno;
    @SerializedName("orderno")
    @Expose
    private String orderno;
    @SerializedName("displayname")
    @Expose
    private String displayname;
    @SerializedName("partstatusatscm")
    @Expose
    private String partstatusatscm;
    @SerializedName("parthclintermediatedrpoint")
    @Expose
    private String parthclintermediatedrpoint;
    @SerializedName("partstatuscustomerplace")
    @Expose
    private String partstatuscustomerplace;
    @SerializedName("defectivepartreceived")
    @Expose
    private String defectivepartreceived;
    @SerializedName("alternativetelephoneno")
    @Expose
    private String alternativetelephoneno;
    @SerializedName("customeremialaddress")
    @Expose
    private String customeremialaddress;
    @SerializedName("alternatecontactno")
    @Expose
    private String alternatecontactno;
    @SerializedName("controlcode")
    @Expose
    private String controlcode;
    @SerializedName("controlcodedesc")
    @Expose
    private String controlcodedesc;
    @SerializedName("organizationId")
    @Expose
    private String organizationId;
    @SerializedName("lastmodifiedby")
    @Expose
    private Integer lastmodifiedby;
    @SerializedName("lastmodifiedtime")
    @Expose
    private String lastmodifiedtime;
    @SerializedName("lastmodifiedrole")
    @Expose
    private Integer lastmodifiedrole;
    @SerializedName("createdby")
    @Expose
    private Integer createdby;
    @SerializedName("createdtime")
    @Expose
    private String createdtime;
    @SerializedName("createdrole")
    @Expose
    private Integer createdrole;
    @SerializedName("deleteflag")
    @Expose
    private Boolean deleteflag;
    @SerializedName("parentFlag")
    @Expose
    private Boolean parentFlag;
    @SerializedName("parentId")
    @Expose
    private String parentId;
    @SerializedName("language")
    @Expose
    private String language;
    @SerializedName("orgstructure")
    @Expose
    private String orgstructure;
    @SerializedName("assignedto")
    @Expose
    private Integer assignedto;
    @SerializedName("completedFlag")
    @Expose
    private Boolean completedFlag;
    @SerializedName("starttime")
    @Expose
    private String starttime;
    @SerializedName("endtime")
    @Expose
    private String endtime;
    @SerializedName("rollbackStatus")
    @Expose
    private String rollbackStatus;
    @SerializedName("assignFlag")
    @Expose
    private Boolean assignFlag;
    @SerializedName("approvalFlag")
    @Expose
    private Boolean approvalFlag;
    @SerializedName("fieldassignFlag")
    @Expose
    private Boolean fieldassignFlag;
    @SerializedName("rollbackFlag")
    @Expose
    private Boolean rollbackFlag;
    @SerializedName("customerId")
    @Expose
    private Integer customerId;
    @SerializedName("categoryId")
    @Expose
    private Integer categoryId;
    @SerializedName("subcategoryId")
    @Expose
    private Integer subcategoryId;
    @SerializedName("countryId")
    @Expose
    private Integer countryId;
    @SerializedName("stateId")
    @Expose
    private Integer stateId;
    @SerializedName("statusId")
    @Expose
    private Integer statusId;

    private String customerMyName;
    private String categoryName;
    private String subcategoryName;
    private String TodoStatusName;
    private String rollbackupdate;
    private Boolean updateflag;

    @SerializedName("isPushed")
    private Boolean isPushed=true;

    @ColumnInfo(name = "serverId")
    @SerializedName("id")
    @Expose
    private Integer serverId;

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "localId")
    private int localId;


    private final static long serialVersionUID = 2980596607410047531L;

    public Boolean getPushed() {
        return isPushed;
    }

    public void setPushed(Boolean pushed) {
        isPushed = pushed;
    }

    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

    public String getRcstatus() {
        return rcstatus;
    }

    public void setRcstatus(String rcstatus) {
        this.rcstatus = rcstatus;
    }

    public String getCustomername() {
        return customername;
    }

    public void setCustomername(String customername) {
        this.customername = customername;
    }

    public String getCustomermiddlename() {
        return customermiddlename;
    }

    public void setCustomermiddlename(String customermiddlename) {
        this.customermiddlename = customermiddlename;
    }

    public String getCustomerlastname() {
        return customerlastname;
    }

    public void setCustomerlastname(String customerlastname) {
        this.customerlastname = customerlastname;
    }

    public String getCustomeraddress() {
        return customeraddress;
    }

    public void setCustomeraddress(String customeraddress) {
        this.customeraddress = customeraddress;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public Integer getCityId() {
        return cityId;
    }

    public void setCityId(Integer cityId) {
        this.cityId = cityId;
    }

    public Integer getPincodeId() {
        return pincodeId;
    }

    public void setPincodeId(Integer pincodeId) {
        this.pincodeId = pincodeId;
    }

    public String getContactno() {
        return contactno;
    }

    public void setContactno(String contactno) {
        this.contactno = contactno;
    }

    public String getContactpersonname() {
        return contactpersonname;
    }

    public void setContactpersonname(String contactpersonname) {
        this.contactpersonname = contactpersonname;
    }

    public String getCasedescription() {
        return casedescription;
    }

    public void setCasedescription(String casedescription) {
        this.casedescription = casedescription;
    }

    public String getCustomerrefnumber() {
        return customerrefnumber;
    }

    public void setCustomerrefnumber(String customerrefnumber) {
        this.customerrefnumber = customerrefnumber;
    }

    public String getCasenumber() {
        return casenumber;
    }

    public void setCasenumber(String casenumber) {
        this.casenumber = casenumber;
    }

    public String getDlpname() {
        return dlpname;
    }

    public void setDlpname(String dlpname) {
        this.dlpname = dlpname;
    }

    public String getSerreceivedate() {
        return serreceivedate;
    }

    public void setSerreceivedate(String serreceivedate) {
        this.serreceivedate = serreceivedate;
    }

    public String getSerreceivetime() {
        return serreceivetime;
    }

    public void setSerreceivetime(String serreceivetime) {
        this.serreceivetime = serreceivetime;
    }

    public String getModelno() {
        return modelno;
    }

    public void setModelno(String modelno) {
        this.modelno = modelno;
    }

    public String getCountryzone() {
        return countryzone;
    }

    public void setCountryzone(String countryzone) {
        this.countryzone = countryzone;
    }

    public String getFaxnocontactperson() {
        return faxnocontactperson;
    }

    public void setFaxnocontactperson(String faxnocontactperson) {
        this.faxnocontactperson = faxnocontactperson;
    }

    public String getFsd() {
        return fsd;
    }

    public void setFsd(String fsd) {
        this.fsd = fsd;
    }

    public Boolean getDeferflag() {
        return deferflag;
    }

    public void setDeferflag(Boolean deferflag) {
        this.deferflag = deferflag;
    }

    public String getProsupport() {
        return prosupport;
    }

    public void setProsupport(String prosupport) {
        this.prosupport = prosupport;
    }

    public String getKeepyourharddrive() {
        return keepyourharddrive;
    }

    public void setKeepyourharddrive(String keepyourharddrive) {
        this.keepyourharddrive = keepyourharddrive;
    }

    public String getAccidentaldamage() {
        return accidentaldamage;
    }

    public void setAccidentaldamage(String accidentaldamage) {
        this.accidentaldamage = accidentaldamage;
    }

    public String getAttachfile() {
        return attachfile;
    }

    public void setAttachfile(String attachfile) {
        this.attachfile = attachfile;
    }

    public String getFiledescription() {
        return filedescription;
    }

    public void setFiledescription(String filedescription) {
        this.filedescription = filedescription;
    }

    public String getModeldescription() {
        return modeldescription;
    }

    public void setModeldescription(String modeldescription) {
        this.modeldescription = modeldescription;
    }

    public String getSerialnoservicetag() {
        return serialnoservicetag;
    }

    public void setSerialnoservicetag(String serialnoservicetag) {
        this.serialnoservicetag = serialnoservicetag;
    }

    public String getSla() {
        return sla;
    }

    public void setSla(String sla) {
        this.sla = sla;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getDellmarks() {
        return dellmarks;
    }

    public void setDellmarks(String dellmarks) {
        this.dellmarks = dellmarks;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getWorklocation() {
        return worklocation;
    }

    public void setWorklocation(String worklocation) {
        this.worklocation = worklocation;
    }

    public String getEtatime() {
        return etatime;
    }

    public void setEtatime(String etatime) {
        this.etatime = etatime;
    }

    public String getEngineer() {
        return engineer;
    }

    public void setEngineer(String engineer) {
        this.engineer = engineer;
    }

    public String getAppointmentdatetime() {
        return appointmentdatetime;
    }

    public void setAppointmentdatetime(String appointmentdatetime) {
        this.appointmentdatetime = appointmentdatetime;
    }

    public String getCallduration() {
        return callduration;
    }

    public void setCallduration(String callduration) {
        this.callduration = callduration;
    }

    public String getLob() {
        return lob;
    }

    public void setLob(String lob) {
        this.lob = lob;
    }

    public String getHdcontactname() {
        return hdcontactname;
    }

    public void setHdcontactname(String hdcontactname) {
        this.hdcontactname = hdcontactname;
    }

    public String getHdcontactno() {
        return hdcontactno;
    }

    public void setHdcontactno(String hdcontactno) {
        this.hdcontactno = hdcontactno;
    }

    public String getOrderno() {
        return orderno;
    }

    public void setOrderno(String orderno) {
        this.orderno = orderno;
    }

    public String getDisplayname() {
        return displayname;
    }

    public void setDisplayname(String displayname) {
        this.displayname = displayname;
    }

    public String getPartstatusatscm() {
        return partstatusatscm;
    }

    public void setPartstatusatscm(String partstatusatscm) {
        this.partstatusatscm = partstatusatscm;
    }

    public String getParthclintermediatedrpoint() {
        return parthclintermediatedrpoint;
    }

    public void setParthclintermediatedrpoint(String parthclintermediatedrpoint) {
        this.parthclintermediatedrpoint = parthclintermediatedrpoint;
    }

    public String getPartstatuscustomerplace() {
        return partstatuscustomerplace;
    }

    public void setPartstatuscustomerplace(String partstatuscustomerplace) {
        this.partstatuscustomerplace = partstatuscustomerplace;
    }

    public String getDefectivepartreceived() {
        return defectivepartreceived;
    }

    public void setDefectivepartreceived(String defectivepartreceived) {
        this.defectivepartreceived = defectivepartreceived;
    }

    public String getAlternativetelephoneno() {
        return alternativetelephoneno;
    }

    public void setAlternativetelephoneno(String alternativetelephoneno) {
        this.alternativetelephoneno = alternativetelephoneno;
    }

    public String getCustomeremialaddress() {
        return customeremialaddress;
    }

    public void setCustomeremialaddress(String customeremialaddress) {
        this.customeremialaddress = customeremialaddress;
    }

    public String getAlternatecontactno() {
        return alternatecontactno;
    }

    public void setAlternatecontactno(String alternatecontactno) {
        this.alternatecontactno = alternatecontactno;
    }

    public String getControlcode() {
        return controlcode;
    }

    public void setControlcode(String controlcode) {
        this.controlcode = controlcode;
    }

    public String getControlcodedesc() {
        return controlcodedesc;
    }

    public void setControlcodedesc(String controlcodedesc) {
        this.controlcodedesc = controlcodedesc;
    }

    public String getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(String organizationId) {
        this.organizationId = organizationId;
    }

    public Integer getLastmodifiedby() {
        return lastmodifiedby;
    }

    public void setLastmodifiedby(Integer lastmodifiedby) {
        this.lastmodifiedby = lastmodifiedby;
    }

    public String getLastmodifiedtime() {
        return lastmodifiedtime;
    }

    public void setLastmodifiedtime(String lastmodifiedtime) {
        this.lastmodifiedtime = lastmodifiedtime;
    }

    public Integer getLastmodifiedrole() {
        return lastmodifiedrole;
    }

    public void setLastmodifiedrole(Integer lastmodifiedrole) {
        this.lastmodifiedrole = lastmodifiedrole;
    }

    public Integer getCreatedby() {
        return createdby;
    }

    public void setCreatedby(Integer createdby) {
        this.createdby = createdby;
    }

    public String getCreatedtime() {
        return createdtime;
    }

    public void setCreatedtime(String createdtime) {
        this.createdtime = createdtime;
    }

    public Integer getCreatedrole() {
        return createdrole;
    }

    public void setCreatedrole(Integer createdrole) {
        this.createdrole = createdrole;
    }

    public Boolean getDeleteflag() {
        return deleteflag;
    }

    public void setDeleteflag(Boolean deleteflag) {
        this.deleteflag = deleteflag;
    }

    public Boolean getParentFlag() {
        return parentFlag;
    }

    public void setParentFlag(Boolean parentFlag) {
        this.parentFlag = parentFlag;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getOrgstructure() {
        return orgstructure;
    }

    public void setOrgstructure(String orgstructure) {
        this.orgstructure = orgstructure;
    }

    public Integer getAssignedto() {
        return assignedto;
    }

    public void setAssignedto(Integer assignedto) {
        this.assignedto = assignedto;
    }

    public Boolean getCompletedFlag() {
        return completedFlag;
    }

    public void setCompletedFlag(Boolean completedFlag) {
        this.completedFlag = completedFlag;
    }

    public String getStarttime() {
        return starttime;
    }

    public void setStarttime(String starttime) {
        this.starttime = starttime;
    }

    public String getEndtime() {
        return endtime;
    }

    public void setEndtime(String endtime) {
        this.endtime = endtime;
    }

    public Boolean getAssignFlag() {
        return assignFlag;
    }

    public void setAssignFlag(Boolean assignFlag) {
        this.assignFlag = assignFlag;
    }

    public Boolean getApprovalFlag() {
        return approvalFlag;
    }

    public void setApprovalFlag(Boolean approvalFlag) {
        this.approvalFlag = approvalFlag;
    }

    public Boolean getFieldassignFlag() {
        return fieldassignFlag;
    }

    public void setFieldassignFlag(Boolean fieldassignFlag) {
        this.fieldassignFlag = fieldassignFlag;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public Integer getSubcategoryId() {
        return subcategoryId;
    }

    public void setSubcategoryId(Integer subcategoryId) {
        this.subcategoryId = subcategoryId;
    }

    public Integer getCountryId() {
        return countryId;
    }

    public void setCountryId(Integer countryId) {
        this.countryId = countryId;
    }

    public Integer getStateId() {
        return stateId;
    }

    public void setStateId(Integer stateId) {
        this.stateId = stateId;
    }

    public Integer getStatusId() {
        return statusId;
    }

    public void setStatusId(Integer statusId) {
        this.statusId = statusId;
    }

    public String getCustomerMyName() {
        return customerMyName;
    }

    public void setCustomerMyName(String customerMyName) {
        this.customerMyName = customerMyName;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getSubcategoryName() {
        return subcategoryName;
    }

    public void setSubcategoryName(String subcategoryName) {
        this.subcategoryName = subcategoryName;
    }

    public String getTodoStatusName() {
        return TodoStatusName;
    }

    public void setTodoStatusName(String todoStatusName) {
        TodoStatusName = todoStatusName;
    }

    public Boolean getUpdateflag() {
        return updateflag;
    }

    public void setUpdateflag(Boolean updateflag) {
        this.updateflag = updateflag;
    }

    public Integer getRcCode() {
        return rcCode;
    }

    public void setRcCode(Integer rcCode) {
        this.rcCode = rcCode;
    }

    public Integer getNextquestionId() {
        return nextquestionId;
    }

    public void setNextquestionId(Integer nextquestionId) {
        this.nextquestionId = nextquestionId;
    }

    public Integer getPrevquestionId() {
        return prevquestionId;
    }

    public void setPrevquestionId(Integer prevquestionId) {
        this.prevquestionId = prevquestionId;
    }

    public Boolean getRollbackFlag() {
        return rollbackFlag;
    }

    public void setRollbackFlag(Boolean rollbackFlag) {
        this.rollbackFlag = rollbackFlag;
    }

    public String getRollbackStatus() {
        return rollbackStatus;
    }

    public void setRollbackStatus(String rollbackStatus) {
        this.rollbackStatus = rollbackStatus;
    }

    public String getRollbackupdate() {
        return rollbackupdate;
    }

    public void setRollbackupdate(String rollbackupdate) {
        this.rollbackupdate = rollbackupdate;
    }

    public Integer getServerId() {
        return serverId;
    }

    public void setServerId(Integer serverId) {
        this.serverId = serverId;
    }

    public int getLocalId() {
        return localId;
    }

    public void setLocalId(int localId) {
        this.localId = localId;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }
}
