package com.app.fsm.ui.dash.taskList.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.fsm.R;
import com.app.fsm.local_network.network.model.joinedtable.TaskListModel;
import com.app.fsm.ui.dash.taskList.OnSearchItemClickedListener;
import com.app.fsm.utils.CommonUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.app.fsm.ui.dash.taskList.TaskListConsts.FOLLOW_UPDATE;
import static com.app.fsm.ui.dash.taskList.TaskListConsts.IS_TODO_TYPE;
import static com.app.fsm.ui.dash.taskList.TaskListConsts.MEMBER_ID;
import static com.app.fsm.ui.dash.taskList.TaskListConsts.MEMBER_NAME;
import static com.app.fsm.ui.dash.taskList.TaskListConsts.TASK_ID;
import static com.app.fsm.ui.dash.taskList.TaskListConsts.TASK_NAME;
import static com.app.fsm.ui.dash.taskList.TaskListConsts.TASK_STATUS_ID;

public class TaskListRVAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<HashMap<String,String>> taskListModelList;
    private OnSearchItemClickedListener onSearchItemClickedListener;

    public TaskListRVAdapter(OnSearchItemClickedListener onSearchItemClickedListener) {
        this.taskListModelList = new ArrayList<>();
        this.onSearchItemClickedListener = onSearchItemClickedListener;
    }

    public void updateList(List<HashMap<String,String>> taskListModelList) {
        this.taskListModelList.clear();
        this.taskListModelList.addAll(taskListModelList);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_task_layout, viewGroup, false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        if (viewHolder instanceof ItemViewHolder) {
            ItemViewHolder itemViewHolder = (ItemViewHolder) viewHolder;
            itemViewHolder.bindDataToVH(taskListModelList.get(i));
        }
    }



    public List<HashMap<String,String>> getTaskListModel() {
        return taskListModelList;
    }


    @Override
    public int getItemCount() {
        return taskListModelList != null ? taskListModelList.size() : 0;
    }

    /************************************{@ITEM_VIEW_HOLDER}***************************************/

    class ItemViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_name) TextView tvName;
        @BindView(R.id.tv_id) TextView tvID;
        @BindView(R.id.tv_disease) TextView tvDisease;
        @BindView(R.id.tv_date) TextView tvDate;
        @BindView(R.id.rl_task)
        RelativeLayout rlTask;
        @BindView(R.id.iv_task_type)
        ImageView imgTaskType;

        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bindDataToVH(final HashMap<String, String> hashMap) {
            tvName.setText(hashMap.get(MEMBER_NAME));
            tvID.setText(String.valueOf(hashMap.get(MEMBER_ID)));
            tvDisease.setText(hashMap.get(TASK_NAME));
            tvDate.setText(hashMap.get(FOLLOW_UPDATE)!=null?CommonUtils.getDateFromISO(hashMap.get(FOLLOW_UPDATE)):"--");
            rlTask.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    TaskListModel taskModel=new TaskListModel();
                    taskModel.setId(hashMap.get(TASK_ID)!=null?Integer.parseInt(hashMap.get(TASK_ID)):0);
                    taskModel.setMemberName(hashMap.get(MEMBER_NAME)!=null?hashMap.get(MEMBER_NAME):"");
                    taskModel.setMemberId(hashMap.get(MEMBER_ID)!=null?Integer.parseInt(hashMap.get(MEMBER_ID)):0);
                    taskModel.setStatusId(hashMap.get(TASK_STATUS_ID)!=null?Integer.parseInt(hashMap.get(TASK_STATUS_ID)):0);
                    taskModel.setTaskName(hashMap.get(TASK_NAME)!=null?hashMap.get(TASK_NAME):"");
                    if (hashMap.get(FOLLOW_UPDATE)!=null && !hashMap.get(FOLLOW_UPDATE).equalsIgnoreCase("")){
                        taskModel.setFollowUpdate(hashMap.get(FOLLOW_UPDATE));
                    }else {
                        taskModel.setFollowUpdate(null);
                    }

                    taskModel.setTodoType(hashMap.get(IS_TODO_TYPE) != null && Boolean.parseBoolean(hashMap.get(IS_TODO_TYPE)));
                    onSearchItemClickedListener.provideTaskListModel(taskModel);
                }
            });

            if (hashMap.get(IS_TODO_TYPE)!=null) {
                if (hashMap.get(IS_TODO_TYPE).equalsIgnoreCase("true")) {
                    imgTaskType.setImageResource(R.drawable.ic_one_to_one);
                } else {
                    imgTaskType.setImageResource(R.drawable.ic_workflow);
                }
            }
        }
    }
}
