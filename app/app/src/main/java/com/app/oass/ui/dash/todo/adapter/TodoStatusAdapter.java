package com.app.fsm.ui.dash.todo.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.app.fsm.R;
import com.app.fsm.local_network.network.model.todo.TodoStatusModel;

import java.util.List;

public class TodoStatusAdapter extends BaseAdapter {

    private LayoutInflater layoutInflater;
    private List<TodoStatusModel> todoStatusModelList;

    public TodoStatusAdapter(@NonNull Context context, @NonNull List<TodoStatusModel> todoStatusModelList) {
        this.todoStatusModelList=todoStatusModelList;
        this.layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        int count = todoStatusModelList.size();
        return count > 0 ? count - 1 : count;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        ItemVH itemVH;


        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.row_spinner_list, parent, false);
            itemVH = new ItemVH();
            itemVH.tvTitle = convertView.findViewById(R.id.txt_title);
            convertView.setTag(itemVH);
        } else
            itemVH = (ItemVH) convertView.getTag();
        itemVH.tvTitle.setText(todoStatusModelList.get(position).getName());
        return convertView;
    }

    @Nullable
    @Override
    public TodoStatusModel getItem(int position) {
        return todoStatusModelList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    static class ItemVH {
        TextView tvTitle;
    }
}