package com.app.fsm.ui.dash.todo.asynctask;

import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.app.fsm.local_network.network.model.todo.TodoModel;
import com.app.fsm.local_network.repository.OaasDatabase;
import com.app.fsm.local_network.repository.dao.TodoDao;
import com.app.fsm.ui.dash.todo.OnTodoFetchListener;

public final class GetTodoModelAsyncTask extends AsyncTask<Void, Void, TodoModel> {

    private OaasDatabase oaasDatabase;
    private OnTodoFetchListener onTodoFetchListener;
    private int todoId;

    public GetTodoModelAsyncTask(@NonNull OnTodoFetchListener onTodoFetchListener
            , @NonNull OaasDatabase oaasDatabase
            ,int todoId) {
        this.oaasDatabase = oaasDatabase;
        this.onTodoFetchListener = onTodoFetchListener;
        this.todoId=todoId;
    }

    @Override
    protected TodoModel doInBackground(Void... params) {
        TodoDao todoDao = oaasDatabase.provideTodoModelDao();
        return todoDao.provideTodoModel(todoId);
    }

    @Override
    protected void onPostExecute(TodoModel todoModel) {
        if (onTodoFetchListener != null) {
            onTodoFetchListener.onTodoFetched(todoModel);
        }
    }
}

