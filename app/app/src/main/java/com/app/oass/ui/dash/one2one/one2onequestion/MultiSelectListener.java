package com.app.fsm.ui.dash.one2one.one2onequestion;

import java.util.List;

public interface MultiSelectListener {
    void onItemSelectedListener(List<MultipleSelectionAnswerModel> multipleSelectionAnswerModelList);
}
