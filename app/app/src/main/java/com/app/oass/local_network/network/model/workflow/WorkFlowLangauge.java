package com.app.fsm.local_network.network.model.workflow;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@Entity
public class WorkFlowLangauge implements Serializable {

    @SerializedName("language")
    @Expose
    private Integer language;
    @SerializedName("workflowTitle")
    @Expose
    private String workflowTitle;
    @SerializedName("member")
    @Expose
    private String member;
    @SerializedName("workflowLabel")
    @Expose
    private String workflowLabel;
    @SerializedName("category")
    @Expose
    private String category;
    @SerializedName("followup")
    @Expose
    private String followup;
    @SerializedName("comments")
    @Expose
    private String comments;
    @SerializedName("enableWrite")
    @Expose
    private Boolean enableWrite;
    @SerializedName("enableforroles")
    @Expose
    private String enableforroles;
    @SerializedName("assignedTo")
    @Expose
    private String assignedTo;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("priority")
    @Expose
    private String priority;
    @SerializedName("saveLabel")
    @Expose
    private String saveLabel;
    @SerializedName("updateLabel")
    @Expose
    private String updateLabel;
    @SerializedName("cancelLabel")
    @Expose
    private String cancelLabel;
    @SerializedName("showLabel")
    @Expose
    private String showLabel;
    @SerializedName("hideLabel")
    @Expose
    private String hideLabel;
    @SerializedName("searchLabel")
    @Expose
    private String searchLabel;
    @SerializedName("addLabel")
    @Expose
    private String addLabel;
    @SerializedName("organizationId")
    @Expose
    private String organizationId;
    @SerializedName("lastmodifiedby")
    @Expose
    private Integer lastmodifiedby;
    @SerializedName("lastmodifiedtime")
    @Expose
    private String lastmodifiedtime;
    @SerializedName("lastmodifiedrole")
    @Expose
    private Integer lastmodifiedrole;
    @SerializedName("createdby")
    @Expose
    private Integer createdby;
    @SerializedName("createdtime")
    @Expose
    private String createdtime;
    @SerializedName("createdrole")
    @Expose
    private Integer createdrole;
    @SerializedName("deleteflag")
    @Expose
    private Boolean deleteflag;
    @SerializedName("parentLanguage")
    @Expose
    private Boolean parentLanguage;

    @PrimaryKey(autoGenerate = true)
    @NonNull
    @SerializedName("id")
    @Expose
    private Integer id;
    private final static long serialVersionUID = 5162821547837895677L;

    public Integer getLanguage() {
        return language;
    }

    public void setLanguage(Integer language) {
        this.language = language;
    }

    public String getWorkflowTitle() {
        return workflowTitle;
    }

    public void setWorkflowTitle(String workflowTitle) {
        this.workflowTitle = workflowTitle;
    }

    public String getMember() {
        return member;
    }

    public void setMember(String member) {
        this.member = member;
    }

    public String getWorkflowLabel() {
        return workflowLabel;
    }

    public void setWorkflowLabel(String workflowLabel) {
        this.workflowLabel = workflowLabel;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getFollowup() {
        return followup;
    }

    public void setFollowup(String followup) {
        this.followup = followup;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public Boolean getEnableWrite() {
        return enableWrite;
    }

    public void setEnableWrite(Boolean enableWrite) {
        this.enableWrite = enableWrite;
    }

    public String getEnableforroles() {
        return enableforroles;
    }

    public void setEnableforroles(String enableforroles) {
        this.enableforroles = enableforroles;
    }

    public String getAssignedTo() {
        return assignedTo;
    }

    public void setAssignedTo(String assignedTo) {
        this.assignedTo = assignedTo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getSaveLabel() {
        return saveLabel;
    }

    public void setSaveLabel(String saveLabel) {
        this.saveLabel = saveLabel;
    }

    public String getUpdateLabel() {
        return updateLabel;
    }

    public void setUpdateLabel(String updateLabel) {
        this.updateLabel = updateLabel;
    }

    public String getCancelLabel() {
        return cancelLabel;
    }

    public void setCancelLabel(String cancelLabel) {
        this.cancelLabel = cancelLabel;
    }

    public String getShowLabel() {
        return showLabel;
    }

    public void setShowLabel(String showLabel) {
        this.showLabel = showLabel;
    }

    public String getHideLabel() {
        return hideLabel;
    }

    public void setHideLabel(String hideLabel) {
        this.hideLabel = hideLabel;
    }

    public String getSearchLabel() {
        return searchLabel;
    }

    public void setSearchLabel(String searchLabel) {
        this.searchLabel = searchLabel;
    }

    public String getAddLabel() {
        return addLabel;
    }

    public void setAddLabel(String addLabel) {
        this.addLabel = addLabel;
    }

    public String getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(String organizationId) {
        this.organizationId = organizationId;
    }

    public Integer getLastmodifiedby() {
        return lastmodifiedby;
    }

    public void setLastmodifiedby(Integer lastmodifiedby) {
        this.lastmodifiedby = lastmodifiedby;
    }

    public String getLastmodifiedtime() {
        return lastmodifiedtime;
    }

    public void setLastmodifiedtime(String lastmodifiedtime) {
        this.lastmodifiedtime = lastmodifiedtime;
    }

    public Integer getLastmodifiedrole() {
        return lastmodifiedrole;
    }

    public void setLastmodifiedrole(Integer lastmodifiedrole) {
        this.lastmodifiedrole = lastmodifiedrole;
    }

    public Integer getCreatedby() {
        return createdby;
    }

    public void setCreatedby(Integer createdby) {
        this.createdby = createdby;
    }

    public String getCreatedtime() {
        return createdtime;
    }

    public void setCreatedtime(String createdtime) {
        this.createdtime = createdtime;
    }

    public Integer getCreatedrole() {
        return createdrole;
    }

    public void setCreatedrole(Integer createdrole) {
        this.createdrole = createdrole;
    }

    public Boolean getDeleteflag() {
        return deleteflag;
    }

    public void setDeleteflag(Boolean deleteflag) {
        this.deleteflag = deleteflag;
    }

    public Boolean getParentLanguage() {
        return parentLanguage;
    }

    public void setParentLanguage(Boolean parentLanguage) {
        this.parentLanguage = parentLanguage;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
