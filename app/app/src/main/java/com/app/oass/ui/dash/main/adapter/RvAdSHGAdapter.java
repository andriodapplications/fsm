/*
 * Developed by Avinash Kumar singh on 24/1/19 3:49 PM
 * Last Modified 16/1/19 5:57 PM.
 *
 * Copyright (c) 2019.  All rights reserved.
 */

package com.app.fsm.ui.dash.main.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.widget.CircularProgressDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.fsm.R;
import com.app.fsm.local_network.network.model.FeatureModel;
import com.app.fsm.ui.dash.main.OnMemberCallback;
import com.app.fsm.utils.GlideApp;
import com.bumptech.glide.RequestBuilder;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RvAdSHGAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private List<FeatureModel> featureModelList;
    private OnMemberCallback onMemberCallback;

    public RvAdSHGAdapter(List<FeatureModel> featureModelList,OnMemberCallback onMemberCallback) {
        this.onMemberCallback=onMemberCallback;
        this.featureModelList = featureModelList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view =LayoutInflater.from(parent.getContext()).inflate(R.layout.row_ad_shg_layout, parent, false);
        return new RvViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, final int position) {

        if (holder instanceof RvViewHolder) {
            ((RvViewHolder) holder).bind(featureModelList.get(position),onMemberCallback,position);
        }
    }

    @Override
    public int getItemCount() {
        return featureModelList.size();
    }



    class RvViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img)
        ImageView img;

        @BindView(R.id.txt_title)
        TextView txtTitle;

        @BindView(R.id.rl_member)
        RelativeLayout rlMember;

        public RvViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(final FeatureModel featureModel, final OnMemberCallback onMemberCallback, final int pos) {
            txtTitle.setText(featureModel.getStrOption());
            img.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                img.setImageDrawable(img.getContext().getDrawable(featureModel.getIc_option()));
            } else {
                img.setImageResource(featureModel.getIc_option());
            }

            rlMember.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onMemberCallback.onMemberCallback(pos);
                }
            });
        }
    }



    public RequestBuilder<Drawable> getThumnail(Context context, String thumbnailUrl) {
        RequestBuilder<Drawable> thumbnailRequest = GlideApp
                .with(context)
                .load(thumbnailUrl);
        return thumbnailRequest;
    }


    public CircularProgressDrawable getCircularProgressDrawable(Context context) {

        CircularProgressDrawable circularProgressDrawable = new CircularProgressDrawable(context);
        circularProgressDrawable.setStrokeWidth(5f);
        circularProgressDrawable.setCenterRadius(30f);
        circularProgressDrawable.start();
        return circularProgressDrawable;

    }
}
